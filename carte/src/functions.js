function make_list_from_range(from,to){
  let list = [];
  let interval = to - from;
  while (interval--) {
    list.push(from + interval);
  }
  list.reverse();
  list.push(to);
  return list;
}

function create_style(style_content){
  let style = document.createElement("style");
  style.classList.add("to_reset");
  style.innerHTML = style_content;
  document.head.appendChild(style);
}

function group_places_per_date(prepared_data,from,to) {
  let places = {};
  let date_list = make_list_from_range(from,to);
  for (var i = 0; i < date_list.length; i++) {
    if (prepared_data[date_list[i]]) {
      for (var place in prepared_data[date_list[i]]) {
        if (!places[place]) {
          places[place] = prepared_data[date_list[i]][place];  
        } else {
          merge_prepared_data(prepared_data[date_list[i]][place],places[place]);
        }
      }
    }
  }
  return places;
}

function merge_prepared_data(what,where) {
  for (var key in what) {
    if (!where[key]) {
      where[key] = what[key];
    } else {
      if (Object.keys(where[key]).length) {
        merge_prepared_data(what[key],where[key]);
      } else if (what[key] > 0) {
        where[key] += what[key];
      }
    }
  }
}

function get_total_from_data(data,total) {
  for (var key in data) {
    if (Object.keys(data[key]).length) {
      get_total_from_data(data[key],total);
    } else if (data[key] > 0) {
      total.result += data[key];
    }
  }
}

function get_all_child_total_from_data(data) {
  let results = {};
  for (var i in data) {
    let total = {"result":0};
    get_total_from_data(data[i],total);
    results[i] = total.result;
  }
  return results;
}

function sort_obj_by_value(obj) {
  let sortable = [];
  for (var key in obj) {
      sortable.push([key, obj[key]]);
  } 
  sortable.sort(function(a, b) {
    return a[1] - b[1];
  });
  return sortable;
}

let colors = {
  "ar" : "#005",
  "pl" : "#631",
  "eschl" : "#583",
  "sen" : "#C46",
  "ter" : "#C97",
  "soph" : "#BCE",
  "eur" : "#BEB",
  "undefined" : "#0000",
}
