// Titles files table
let title_files_table = {
  "Ioannes Auratus Candido Lectori Salutem": "Ae1548_Auratus_p1",
  "Franciscus Asulanus Lectori": "Ae1552_Asulanus_p1",
  "Illustrissimo nobilissimoque adolescenti Mariano Sauello Franciscus Robortellus. S. D.": "Ae1552_Robortello_p1",
  "Franciscus Robortellus Mariano Sauello. S. D.": "Ae1552_Robortello_p2",
  "Hae sunt coniecturae quibus antiquum librum manu scriptum emedare studuimus.": "Ae1552_Robortello_p3",
  "Aeschyli poetae uita ex Lilii Gregorii Gyraldi De poetarum Historia Dialogo VI": "Ae1552_Robortello_p4",
  "ΜΙΧΑΗΛῼ ΟΣΠΙΤΆΛΕΙ βουλεύτη Αδριανὸς Τούρνεϐος εὖ πράττειν.": "Ae1552_Turnebus_p1",
  "ΒΕΡΝΆΡΔΟΥ ΒΕΡΤΡΑΝΔΟΥ. Ῥηγίνου πρὸς τοὺς ἐντευξομένους ἀναγνωσὰς. Επίγραμμα. ": "Ae1555_Bertrandus_p1",
  "Eiusdem ad lectorem.": "Ae1555_Bertrandus_p2",
  "In translationem Aeschyli Paulus Ottenthalerus Oeniuallensise": "Ae1555_Ottenthalerus_p1",
  "ΛΑΜΠΡΟΤΆΤΩ ἈΝΔΡΊ ἸΩάννῃ τῷ κεζηλύῳ προέδρῳ, Ἰωάννης ὁ Σανραύιος εὖ πράττειν.": "Ae1555_Sanravius_p1",
  "Petrus Victorius Lectori S.": "Ae1557_Victorius_p1",
  "Ad Lectorem. M. Garbitius.": "Ae1559_Garbitius_p1",
  "Patricii et senatorii ordinis uiro pietate, uirtute et sapientia in primis eminenti, Hieronymo Bomgartnero, Domino suo obseruandissimo, salutem et felicitatem in Christo Domino nostro precatur Mathias Garbitius Graecae linguae et morum doctrinae professor ordinarius Tubingae in Academia inclyta.": "Ae1559_Garbitius_p2",
  "Mathiae Garbitii Illyrici in Aeschyli Prometheum Scholia Praefatio.": "Ae1559_Garbitius_p4",
  "Τὰ τοῦ δράματος πρόσωπα": "Ae1559_Garbitius_p5",
  "M. Toxites": "Ae1559_Toxites_p1",
  "Ἀνδρὶ λογιωτάτῳ ΠΕΤΡΩ ΒΙΚΤΩΡΙΩ, ΒΙΛΕΛΜΟΣ ΚΑΝΤΗΡΟΣ εὖ πράττειν": "Ae1580_Canterus_p1",
  "Gulielmi Canteri in Aeschylum Prolegomena": "Ae1580_Canterus_p2",
  "E Gul. Canteri Novarum Lectionum Libro V.": "Ae1580_Canterus_p3",
  "Ex eiusdem poematum libro IIII, Aeschyli tragoediarum septem argumenta.": "Ae1580_Canterus_p4",
  "Ad illustrissimum DN. Vlricum ducem Megap., principem Vandalorum, Comitem Suerinensem, prouinciarum Rostochianae et Stargardensis Dominum. Prooemium Ioannis Caselii.": "Ae1581_Caselius_p1",
  "Argumentum tragoediae Aeschyli, Septem ad Thebas ducum": "Ae1581_Caselius_p2",
  "Q. Sept. Florens Christianus Federico Morello suo S. D.": "Ae1585_Christianus_p1",
  "Hypothesis uel (ut uocant) argumentum Aeschyli Tragoediae, quae inscribitur Septem ad Thebas": "Ae1585_Christianus_p2",
  "Ad Federicum Morellum Typogr. Regium, De Florentis Christiani stylo Tragico": "Ae1585_Melissus_p1",
  "In Quint.Septimi Florentis Christiani poemata": "Ae1585_Morellus_p1",
  "Σκιπίωνου Καρτερομάχου τοῦ πιστωριέως": "Ar1498_Carteromacus_p1",
  "Aldus Manutius Romanus, Danieli Clario Parmensi, s.p.d": "Ar1498_Manutius_p1",
  "Μάρκος Μουσοῦρος ὁ Κρὴς τοῖς ἐντευξομένοις εὖ πράττειν": "Ar1498_Musurus_p1",
  "Illustrissimo ac eruditissimo Alberto Pio principi Carpensi, ingeniorum fautori, Franciscus": "Ar1501_Passius_p1",
  "Antonii Zandemariae equitis hyersolymitani Parmensis.ad lectorem ": "Ar1501_Passius_p2",
  "Tranquilli Molossi Cremonensis Apologeticon.": "Ar1501_Passius_p3",
  "Argumentum": "Eu1551_Gelous_p2",
  "Francisci Passii Carpensis Plutus Comoedia incipit Prologus ": "Ar1501_Passius_p5",
  "Bernardus Iunta, nobili patritio domino Francisco Accolto electo episcopo anconitano. S.P.D. ": "Ar1515_Iunta_p1",
  "Bernardus Iunta, nobili patritio domino Francisco Accolto electo episcopo anconitano. S.P.D. ": "Ar1516_Iunta_p1",
  "Bernardus Iunta Lectori. S. ": "Ar1516_Iunta_p2",
  "Petrus Mosellanus Protegensis Ioanni Caesario Iuliacensi medico utriusque literaturae doctissimo s.d.p.": "Ar1517_Mosellanus_p1",
  "Theodoricus Martinus Alostensis Studiosae Lovaniensis Academiae Juventuti S.D. ": "Ar1518_Martinus_p1",
  "Imaginibus ac eruditione claro, D. Nicolao Amsdorffo, Theologo, Phil. Mel. s.d.": "Ar1521_Melanchthon_p1",
  "Ioannes Frobenius τοῖς φιλέλλησιν εὖ πράττειν ": "Ar1524_Frobenius_p1",
  "Benedicto Accoltae Archiepiscopo Rauennati ac cle. VII pont. Maximi a secretis poenitiss. Antonius Fracinus Varchiensis. s.": "Ar1525_Francinus_p1",
  "Benedicto Accoltae Archiepiscopo Rauennati ac cle. VII pont. Maximi a secretis poenitiss. Antonius Fracinus Varchiensis. s. ": "Ar1525_Iunta_p1",
  "Σεβαστιάνου τοῦ δουκκίου ἐπίγραμμα ": "Ar1525_Sebastianos_p1",
  "Ιωάννης Χεράδαμος Τωάννῃ Κληρικῷ τῷ τῆς Ἀγγλείας ῥήτορι εὖ πράττειν.": "Ar1528_Cheradamus_p1",
  "Ιωάννης Χεράδαμος φιλανθρωποτάτῳ νέῳ Θωμᾷ Οὐιντήρῳ εὖ πράττειν.": "Ar1528_Cheradamus_p2",
  "Ιωάννης Χεράδαμος ἐλλογιμωτάτῳ Πέτρῳ Δανησίῳ εὖ πράττειν. ": "Ar1528_Cheradamus_p3",
  "Ιωάννης Χεράδαμος εὑβουλοτάτῳ Ἰωάννῃ βιολλείῳ εὖ πράττειν. ": "Ar1528_Cheradamus_p4",
  "Ιωάννης Χεράδαμος χρησιμωτάτῳ γυμναστῇ Τωάννῃ Ταρτάσσῳ εὖ πράττειν. ": "Ar1528_Cheradamus_p5",
  "Ιωάννης Χεράδαμος ἐλλογιμωτάτῳ' Ἀυθονίῳ Λαπιθέῳ εὖ πράττειν. ": "Ar1528_Cheradamus_p6",
  "Ιωάννης Χεράδαμος ἐλλογιμωτάτῳ Νικόλεῳ Βηράλδῳ εὖ πράττειν.": "Ar1528_Cheradamus_p7",
  "Ιωάννης Χεράδαμος σοφωτάτῳ τῶν ἱατρῶν Πωάύνῃ Ρουηλλίῳ εὖ πράττειν. ": "Ar1528_Cheradamus_p8",
  "Ιωάννης Χεράδαμος ὁμιλητικοτάτῳ Γουιλλέλμῳ Κουίνῳ εὖ πράττειν. ": "Ar1528_Cheradamus_p9",
  "Ad lectorem": "Sq1589_A_Raphelengius_p2",
  "Clarissimo ac ornatissimo uiro Seuerino Bonero, Burgrauio, Zuppario, magnoque apud Cracouiam regni Poloniae Procuratori, Byeczensi et Rapsteinensi Praesidi etc. Thomas Venatorius S. P. D.": "Ar1531_Venatorius_p2",
  "And. Cratander Lectori S.": "Ar1532_Cratander_p1",
  "Simon Grynaeus juveni studioso s.": "Ar1532_Grynaeus_p1",
  "Clarissimo eximioque utriusque iuris licentiato, Marco Laurino, collegii divi Donatiani apud Brugas decano, Adrianus Chilius salutem plurimam dicit. ": "Ar1533_Chilius_p1",
  "Petrus Curius": "Ar1533_Curius_p1",
  "Idem Curius": "Ar1533_Curius_p2",
  "Iohannes Theodori Neruii": "Ar1533_Nervius_p1",
  "Clarissimo eruditissimoque uiro D. Ioanni Tartesio, praesidi collegii Aquitanici apud Burdegalenses Petrus Nannius. S.P.D. ": "Ar1534_Nannius",
  "Ματθαίῳ Καδριγαρίῳ γνώμῃ τῆς βουλῆς τουτέστι σοφωτάτῳ τῶν νῦν καὶ τῶν παλαὶ συμβούλων Ι. Χεράδαμος εὖ πλουτεῖν": "Ar1535_Cheradamus_p1",
  "Epistula nuncupatoria": "Ar1539_Divus_p1",
  "Argumentum praesentis fabulae Pluti est hoc ": "Ar1539_Divus_p2",
  "Christianus Wechelus Candido Lectori S. ": "Ar1540_Wechelus_p1",
  "ΦΡΑΓΚΙΣΚΩ τῷ τῆϛ Φραγκίαϛ βασιλεῖ, τῶν χριστιανῶν, μονάρχων τῷ πάνυ σεβασμίῳ χʹἱεροπρεπεῖ, Αἰγίδιοϛ ὁ Βουρδῖνοϛ εὖ πράττειν καὶ τὰ δόξαντα ἐπιτελεῖν.": "Ar1545_Burdinus_p1",
  "D. GONCALLO PINARIO, Tyngitanorum Episcopo Ioannis tertii Lusitaniae Regis in Gallia Legato auunculo suo, M. Cabedius. S. ": "Ar1547_Cabedius_p1",
  "CLARISSIMO ET OMNIS BONAE LITERATURAE PERITISSIMO DOMINO PHILIPpo Melanchthoni, Sigis. Gelenius S.D. ": "Ar1547_Gelenius_p1",
  "ΑΓΓΕΛΟΣ ΚΑΝΙΝΙΟΣ ΝΕΟΙΣ τοῖς φιλέλλησιν, εὖ πράττειν": "Ar1548_Caninius_p1",
  "Illustrissimae Dominae Ianae Navarrorum Principi Carolus Girardus Bituricus Salute.": "Ar1549_Girardus_p1",
  "LECTORI": "Ar1549_Girardus_p2",
  "Reuendo in Christo Patri Gerardo Neoclesiano Abbati ad D. Paulum, Lambertus Hortensius Montfortius S. D.": "Ar1556_Hortensius_p1",
  "Vita Aristophanis": "Ar1556_Hortensius_p2",
  "Scopus praesentis dramatis": "Ar1556_Hortensius_p3",
  "Argumentum Pluti": "Ar1556_Hortensius_p4",
  "Illustrissimo ac Amplissimo S. R. E. Cardinali et Principi Tridentino, Christophoro Madrucio. Martius Martiranus.": "Soph1556_Martiranus",
  "Ornatissimo uiro domino Iacobo Vutenengo, decano ad diuum Petrum, et Vicario episcopi VltraiectiniLambertus Hortensius Montfortius Salutem dat": "Ar1557_Hortensius_p1",
  "Nebularum siue nubium Aristophanis, Comici clarissimi, argumentum.": "Ar1557_Hortensius_p2",
  "Ornatissimo viro d’Iacobo Vutenengo, decano ad diuum Petru et Vicario episcopi Vltraiectini. Lambertus Hortensius Salutem dat": "Ar1557_Hortensius_p3",
  "Lambertus Hortensius Montfortius nobili uiro D. Theodorico Zuleno, etc. Equiti Aurato, Salutem Dat": "Ar1561_A_Hortensius_p1",
  "Argumentum fabulae Equitum": "Ar1561_A_Hortensius_p2",
  "Eiusdem Argumenti Perioche carmine Iambico trimetro": "Ar1561_A_Hortensius_p3",
  "Scopus Poetae": "Ar1561_A_Hortensius_p4",
  "Ornatissimo, atque erudito uiro D. Henrico Ruzaeo, ordinis militiae Hierosolimitanae, et Mandatori in Ingen. Lambertus Hortensius Monfortius salutem dicit plurimam.": "Ar1561_B_Hortensius_p1",
  "Argumentum Ranarum": "Ar1561_B_Hortensius_p2",
  "Scopus fabulae Ranarum et poetae consilium": "Ar1561_B_Hortensius_p3",
  "Perioche argumenti, iambicum trimetrum.": "Ar1561_B_Hortensius_p4",
  "Diuo Rudolpho II Caesari, Romano Imperatori electo, Augustus Pius Felix Pater Patriae Regi Germaniae, Bohemiae, Hungariae etc., Archiduci Austriae, Duci Burgundiae, Comiti Tyrolano, etc., Principi Optimo Maximo, uita, salus et uictoria": "Ar1586_Frischlinus_p1",
  "In Aristophanem Latinum Nicodemi Frisclini, Epigramma Friderici SYLBURGII": "Ar1586_Frischlinus_p2",
  "Aliud eiusdem in eundem - Κῶμον Ἀριστοφάνους διὰ Φρισχλίου Αὐσονίοισι": "Ar1586_Frischlinus_p3",
  "In uersum et emendatum a clarissimo uiro Domino Nicodemo Frischlino Aristophanem, lepidissimum Comicum": "Ar1586_Frischlinus_p4",
  "In Aristophanem factum ex Graeco Latinum a Nicodemo Frischlino, Elegidion Lamberti Ludolphi Pithopoei Dauentriensis": "Ar1586_Frischlinus_p5",
  "In Aristophanem clarissimi uiri domini Nicodemi Frischlini, Poetae laureati et comitis Caesarei": "Ar1586_Frischlinus_p6",
  "Ad Clarissimum uirum Nicodemum Frischlinum Aristophanis interpretem": "Ar1586_Frischlinus_p7",
  "Aristophanes Frischlianus de se": "Ar1586_Frischlinus_p8",
  "Vita Aristophanis a Nicodemo Frischlino conscripta": "Ar1586_Frischlinus_p9",
  "ΕΙΣ ΤΗΝ ΑΡΙΣΤΟΦΑΝΙΚΗΝ ΕΙΡΗΝΗΝ ΜΕΤΑΦΡΑΣΤΕΙΣΑΝ ῥωμαϊστὶ ἀπὸ Φλ. Χριστιανοῦ.": "Ar1589_Auratus_p1",
  "Q. Septimus Florens Christianus V. C. Jacobo Augusto Thuano, Aemerio, Christophori Senatus quondam Principis filio, S.P.D.": "Ar1589_Christianus_p1",
  "ΦΛΩΡ. ΧΡΙΣΤΙΑΝΟΥ ΥΠΟΘΕΣΙΣ ΔΙ’ ΙΑΜΒΩΝ": "Ar1589_Christianus_p2",
  "PROLOGVS": "Ar1589_Christianus_p3",
  "ΦΕΔΕΡΙΚΟΥ ΜΟΡΕΛΛΟΥ ΕΙΣ ΕΙΡΗΝΗΝ δίγλωττον": "Ar1589_Morellus_p1",
  "Typographus lectori": "Ar1600_Raphelengius_p1",
  "QUINTI SEPTIMI FLORENTIS CHRISTIANI PROLOGUS IN VESPAS ARISTOPHANICAS Ad Senatum Populumque forensem": "Ar1607_Christianus_p1",
  "": "Pla1605_Rhodomanus_p1",
  "Insigni Pietate, nobilitate et doctrina ornatissima uiro, Domino Odoardo Biseto, Domino de Charlay, Domino suo plurimum obseruando Aemilius Portus Francisci Porti Cretensis Filius S. P.": "Ar1607_Portus_p2",
  "Aemilius Portus Fr. Porti Cretensis F. candido lectori S.": "Ar1607_Portus_p3",
  "Typographus Lectori Beniuolo S.": "Ar1625_Maire_p1",
  "Aristophanis uita et scripta.": "Ar1625_Maire_p2",
  " Aldus Romanus Demetrio Chalcondylae viro clarissimo salutem plurimam dat.": "Eu1503_Manutius_p1",
  " Georgus Anselmus nepos Tranquillo Molosso Cremonensis salutem dat. ": "Eu1506_Anselmus_p1",
  "Reuerendo in Christo patri Guilielmo Archiepiscopo Cantuariensi primati Angliae Erasmus Roterdamus.S.P.D.": "Eu1506_Erasmus_p1",
  "<l>AD R. P. GUILIELMUM ARCHIEPISCOPUM</l> <l>Cantuariensem, Erasmi carmen Iambicum trimetrum.</l>": "Eu1506_Erasmus_p2",
  "Argumentum Hecubae per eundem Erasmum": "Eu1506_Erasmus_p3",
  "Argumentum Iphigeniae in Aulide": "Eu1506_Erasmus_p4",
  "Erasmus Lectori S.P.D.": "Eu1506_Erasmus_p5",
  " Illustrissimo ac serenissimo principi Francisco Valesio Valesiorum duci et angolismorum comiti D. suo metuendissimo Franciscus Tissardus Ambaciaeus V. juris doctor S.P.D.": "Eu1506_Tissardus_p2",
  "Medea Euripidis Narratio": "Eu1506_Tissardus_p3",
  "Hippolytis Euripidis Narratio": "Eu1506_Tissardus_p4",
  "Alcestidis Euripidis Narratio": "Eu1506_Tissardus_p5",
  "Guilielmo Archiepiscopo Cantuariensi Erasmus S.P.D.": "Eu1507_Erasmus_p2",
  " <author ref=\"#apostolios_arsenius\">Arsenius Archiepiscopus Monembasiae</author> <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence en minuscules, précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->  <respStmt> <resp>Transcription</resp> <persName>Alexia DEDIEU</persName> </respStmt> <respStmt> <resp>Traduction</resp> <persName>Alexia DEDIEU</persName> </respStmt> <respStmt> <resp>Encodage</resp> <name>Sarah GAUCHER</name> </respStmt> <principal>Malika BASTIN-HAMMOU</principal> <respStmt> <resp>Modélisation et structuration</resp> <persName>Elisabeth GRESLOU</persName> <persName>Elysabeth HUE-GAY</persName> </respStmt> <respStmt> <resp>Première version du modèle d'encodage</resp> <persName>Elisabeth GRESLOU</persName> <persName>Anne Garcia-Fernandez</persName> </respStmt> </titleStmt>  <publicationStmt> <publisher/> <availability> <p>Le site est en accès restreint</p> </availability> <pubPlace>ithac.elan-numerique.fr</pubPlace> </publicationStmt> <sourceDesc> <bibl> <title>Scholia in septem Euripide tragoedias ex antiquis exemplaribus ab Arsenio archiepiscopo monembasiae collecta, et nunc primum in lucem edita": "Eu1534_Apostolis_p1",
  "Joannus Hervagius Lectori S. D.": "Eu1537_Hervagius_p1",
  "Clarissimo D Hectori Hoxuirio Frisio Praesidi Traiextensi uiro nobili iuxta atque erudite Petr. Tiara Salutem dicit.": "Eu1543_Tiara_p1",
  "Argumentum Fabulae": "Eu1543_Tiara_p2",
  "Ad Illustrissimum Principem Ioannem a Lucenburgo, Iueraci Abbatem, Georgii Buchanani Praefatio.": "Eu1544_Buchananus_p1",
  "Petrus Victorius Nicolao Ardinghello Cardinali sal": "Eu1545_Victorius_p1",
  "Argumentum Petri Victori": "Eu1545_Victorius_p2",
  "Sigismundus Gelous clarissimo et doctissimo uiro Martino Calmantzehi pastori ecclesiae Neapolitanae apud pannonios ad Carpathum S. D. ": "Eu1551_Gelous_p1",
  "Aliud argumentum": "Eu1551_Gelous_p3",
  "Expositio. Fabulae narratio apud nullum scriptorem legitur.": "Eu1551_Gelous_p4",
  "Generosissimis ac uirtutum omnium studiosissimis iuuenibus, Dominis Sebastiano et Amando Truchses a Rinfeld, amicis carissimis suis, Ioannes Oporinus S.": "Eu1551_Oporinus_p1",
  "ΙΩΑΝΝΟΥ ΑΥΡΑΤΟΥ ὑπόθεσις": "Eu1555_Auratus_p1",
  "Ioachim Camerarius Pabenpergensis, optimarum artium Magistro et Philosophiae professori Matthaeo Heuslero, amico suo S.D": "Eu1555_Camerarius_p1",
  "Vera nobilitate Praestanti Reuerendo Domino Iulio Commerstadio Praeposito Misnensi Domin. Sanct. Colendissimo M. Matthaeus Heuslerus Salutem dat": "Eu1555_Heuslerus_p1",
  "ILLVSTRISSIMO AC AMPLISSIMO S. R. E. CARDINALI ET PRINCIPI TRIDENTINO, CHRISTOPHORO MADRVCIO. MARTIVS MARTIRANVS.": "Eu1556_Martiranus_p1",
  "Ad Illustrissimam Principem Dominam Margaritam Henrici secundi Francorum regis sororem, in Alcestin Praefatio.": "Eu1557_Buchananus_p1",
  " <author ref=\"#xylandrus_guilelmus\">Guilelmus Xylandrus</author> <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence en minuscules, précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->  <respStmt> <resp>Transcription</resp> <name>Alexia DEDIEU</name> </respStmt> <respStmt> <resp>Traduction</resp> <name>Alexia DEDIEU</name> </respStmt> <respStmt> <resp>Encodage</resp> <name>Sarah GAUCHER</name> </respStmt> <principal>Malika BASTIN-HAMMOU</principal> <respStmt> <resp>Modélisation et structuration</resp> <persName>Elisabeth GRESLOU</persName> <persName>Elysabeth HUE-GAY</persName> </respStmt> <respStmt> <resp>Première version du modèle d'encodage</resp> <persName>Elisabeth GRESLOU</persName> <persName>Anne Garcia-Fernandez</persName> </respStmt> </titleStmt>  <publicationStmt> <publisher/> <availability> <p>Le site est en accès restreint</p> </availability> <pubPlace>ithac.elan-numerique.fr</pubPlace> </publicationStmt> <sourceDesc> <bibl> <title>Euripidis Tragoediae, quae hodie extant, omnes Latinè soluta oratione redditae, ita ut versus versui respondeat": "Eu1558_Xylandrus_p1",
  "Euripidis uita Guilielmo Xylandro Augustano autore.": "Eu1558_Xylandrus_p2",
  "Duo sunt praecipui…": "Eu1558_Xylandrus_p3",
  "Euripidis Iphigenia in Aulide. Argumentum, Xylandri.": "Eu1558_Xylandrus_p4",
  "Haec tragoedia est imago tyranni...": "Eu1558_Xylandrus_p5",
  "Euripidis Helena. Argumentum Xylandri.": "Eu1558_Xylandrus_p6",
  "Gaspari Stiblini Praefatio in Andromachen": "Eu1562_Stiblinus_p12",
  "Argumentum et Praefatio Gaspari Stiblini in Supplices Euripidis": "Eu1562_Stiblinus_p13",
  "Praefatio Gaspari Stiblini In Iphigeniam In Aulide": "Eu1562_Stiblinus_p14",
  "Argumentum Et Praefatio Gaspari Stiblini In Iphigeniam Tauricam": "Eu1562_Stiblinus_p15",
  "Argumentum Et Praefatiuncula Gaspari Stiblini In Euripidis Rhesum": "Eu1562_Stiblinus_p16",
  "Gaspari Stiblini Praefatio In Euripidis Troadas": "Eu1562_Stiblinus_p17",
  "Praefatio Gaspari Stiblini in Euripidis Bacchas": "Eu1562_Stiblinus_p18",
  "Praefatio Gaspari Stiblini in Cyclopem": "Eu1562_Stiblinus_p19",
  " <author ref=\"#stiblinus_gasparus\">Gasparus Stiblinus</author> <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence en minuscules, précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->  <respStmt> <resp>Transcription</resp> <persName>Alexia DEDIEU</persName> </respStmt> <respStmt> <resp>Traduction</resp> <persName>Alexia DEDIEU</persName> </respStmt> <respStmt> <resp>Encodage</resp> <name>Sarah GAUCHER</name> </respStmt> <principal>Malika BASTIN-HAMMOU</principal> <respStmt> <resp>Modélisation et structuration</resp> <persName>Elisabeth GRESLOU</persName> <persName>Elysabeth HUE-GAY</persName> </respStmt> <respStmt> <resp>Première version du modèle d'encodage</resp> <persName>Elisabeth GRESLOU</persName> <persName>Anne Garcia-Fernandez</persName> </respStmt> </titleStmt>  <publicationStmt> <publisher/> <availability> <p>Le site est en accès restreint</p> </availability> <pubPlace>ithac.elan-numerique.fr</pubPlace> </publicationStmt> <sourceDesc> <bibl> <title>Euripides Poeta Tragicorum princeps, in Latinum sermonem conuersus, adiecto eregione textu Graeco : CUM ANNOTATIONIBUS ET PRAEFATIONIBUS in omnes eius Tragoediae, autore GASPARO STIBLINO, Accesserunt JACOBI MICYLLI, de Euripidis uita, ex diuersis autoribus collecta : item, De tragoedia et eius partibus προλεγομένα quaedam. Item IOANNIS BRODAEI Turonensis Annotationes doctissimae numquam antea in lucem editae. Ad haec Rerum et uerborum toto Opere Praecipue memorabilium copiosus INDEX. Cum caesaris Maiestatis et Christianissimi Gallorum Regis gratia ac priuilegio, ad decennium. Basileae, per Ioannem Oporinum.": "Eu1562_Stiblinus_p26",
  "Praefatio Gaspari Stiblini in Heraclidas": "Eu1562_Stiblinus_p20",
  "Praefatio et Argumentum Gaspari Stiblini in Euripidis Helenam ": "Eu1562_Stiblinus_p21",
  "Praefatio Gaspari Stiblini in Ionem": "Eu1562_Stiblinus_p22",
  "Praefatio Gaspari Stiblini In Herculem Furentem": "Eu1562_Stiblinus_p23",
  "Gaspari Stiblini In Electram Euripidis Praefatio et Scholia": "Eu1562_Stiblinus_p24",
  "Gasparus Stiblinus Domino Iohanni Oporino Salutem Dat Plurimam": "Eu1562_Stiblinus_p25",
  "Gasparus Stiblinus aequo lectori salutem plurimam dicit": "Eu1562_Stiblinus_p2",
  "Ad clarissimos et ornatissimos uiros, Dominum Simonem A Pfirt, et Dominum Ioannem Fabri V.I. Doctorem, Caesareos Senatores in Ensisheim, Gaspari Stiblini carmen in omnes Euripidis Tragoedias.": "Eu1562_Stiblinus_p3",
  "ΧΑΣΠΑΡ Ο ΣΤΙΒΛΙΝΟΣ ΙΩΑΝΝΗΙ ΤΩΙ ΟΠΩΡΙΝΩΙ εὖ διάγειν.": "Eu1562_Stiblinus_p4",
  "ΝΙΚΟΛΑΟΣ Ο ΓΑΛΛΩΤΟΣ εἰς τὸν μεταφράστην.": "Eu1562_Stiblinus_p5",
  "Ad candidum lectorem Xylandri praefatio, editionis huius rationem ostendens.": "Eu1562_Xylandrus_p1",
  "Autores Sequentium Interpretationum.": "Eu1567_Stephanus_p0",
  "Ioannes Sturmius Tidemanno Gisio Dantiscano S.P.D.": "Eu1567_Sturmius_p1",
  " <!-- Indiquer la langue du paratexte dans l'attribut xml:lang : \"la\" pour le latin et \"grc\" pour le grec --> <!-- Attention ! ne pas faire de retour charriot dans le titre --> <author ref=\"#canterus_guilelmus\">Gulielmus Canterus</author> <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence en minuscules, précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->  <!-- Déclarations de responsabilité : indiquent qui a effectué quelle tâche --> <!-- Ajouter autant de tâches (<respStmt>) que nécessaire --> <respStmt> <resp>Transcription</resp> <name>Alexia DEDIEU</name> </respStmt> <respStmt> <resp>Traduction</resp> <name>Alexia DEDIEU</name> </respStmt> <respStmt> <resp>Encodage</resp> <name>Sarah GAUCHER</name> </respStmt>  <!-- Attention ! Ne pas modifier les tâches ci-dessous --> <respStmt> <resp>Modélisation et structuration</resp> <persName>Elisabeth GRESLOU</persName> <persName>Elysabeth HUE-GAY</persName> </respStmt> <respStmt> <resp>Première version du modèle d'encodage</resp> <persName>Elisabeth GRESLOU</persName> <persName>Anne GARCIA-FERNANDEZ</persName> </respStmt> <principal>Malika BASTIN-HAMMOU</principal> </titleStmt>  <!-- Informations de publication sur le laboratoire numérique du projet. Ne pas modifier--> <publicationStmt> <publisher/> <availability> <p>Le site est en accès restreint</p> </availability> <pubPlace>ithac.elan-numerique.fr</pubPlace> </publicationStmt>  <!-- Informations bibliographiques sur l'édition ancienne d'où le paratexte est tiré --> <sourceDesc> <bibl> <title>ΕΥΡΙΠΙΔΟΥ ΤΡΑΓΩΔΙΑΙ ΙΘ. EVRIPIDIS TRAGOEDIAE XIX, IN QVIBVS PRAETER INFINITA MENDA SUBLATA, carminum omnium ratio hactenus ignorata nunc primum proditur : opera GVILELMI CANTERI VLTRAIECTINI, ANTVERPIAE, Officina Christophori Plantini, Regij prototypographi m. d. lxxi. ": "Eu1571_Canterus_p1",
  "Gulielmi Canteri in Euripidem Prolegomena": "Eu1571_Canterus_p2",
  "Ex eiusdem poematum lib. quarto. Euripidis tragoediarum XIX. argumenta. ": "Eu1571_Canterus_p5",
  "Guilelmi Canteri in Euripidem notae. Ad Clarissimium Virum Iohannem Cratonem a Craftheim, Caesaris Maiestatis Archiatrum, Praefatio.": "Eu1571_Canterus_p6",
  "Ad Elisabetham serenissimam Angliae, Franciae, Hiberniae Reginam": "Eu1577_Calaminus_p1",
  "Prologus conscriptus a petro albino niuemontio Profes. Publ. ": "Eu1581_Albinus_p1",
  " <!-- Indiquer la langue du paratexte dans l'attribut xml:lang : \"la\" pour le latin et \"grc\" pour le grec --> <!-- Attention ! ne pas faire de retour charriot dans le titre --> <author ref=\"#albinus_petrus\">Petrus Albinus</author> <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence en minuscules, précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->  <!-- Déclarations de responsabilité : indiquent qui a effectué quelle tâche --> <!-- Ajouter autant de tâches (<respStmt>) que nécessaire --> <respStmt> <resp>Transcription</resp> <name>Alexia DEDIEU</name> </respStmt> <respStmt> <resp>Traduction</resp> <name>Alexia DEDIEU</name> </respStmt> <respStmt> <resp>Encodage</resp> <name>Sarah GAUCHER</name> </respStmt>  <!-- Attention ! Ne pas modifier les tâches ci-dessous --> <respStmt> <resp>Modélisation et structuration</resp> <persName>Elisabeth GRESLOU</persName> <persName>Elysabeth HUE-GAY</persName> </respStmt> <respStmt> <resp>Première version du modèle d'encodage</resp> <persName>Elisabeth GRESLOU</persName> <persName>Anne GARCIA-FERNANDEZ</persName> </respStmt> <principal>Malika BASTIN-HAMMOU</principal> </titleStmt>  <!-- Informations de publication sur le laboratoire numérique du projet. Ne pas modifier--> <publicationStmt> <publisher/> <availability> <p>Le site est en accès restreint</p> </availability> <pubPlace>ithac.elan-numerique.fr</pubPlace> </publicationStmt>  <!-- Informations bibliographiques sur l'édition ancienne d'où le paratexte est tiré --> <sourceDesc> <bibl> <title>Alcestis Euripidis conuersa a Georgio Buchanano V. CL. In Memoriam Lectissimae et omnium matronalium uirtutum laudibus ornatissimae feminae, Anna Stympfeliae Mariaebergensis, coniugis Magnifici et Clarissimi Viri Dn Viti Ortelii VVinshemii, V. I. D. Consiliarii Saxonici et Professoris in Academia Vitebergensi, exhibita a VVitebergae a filiis, Accesserunt noui Prologi et Epilogus una cum Epicediis et Apitaphiis quibusdam, in eiusdem honorem conscriptis. ": "Eu1581_Albinus_p5",
  " <!-- Indiquer la langue du paratexte dans l'attribut xml:lang : \"la\" pour le latin et \"grc\" pour le grec --> <!-- Attention ! ne pas faire de retour charriot dans le titre --> <author ref=\"#mencius_balthasar\">Balthasar Mencius Nimecensis</author> <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence en minuscules, précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->  <!-- Déclarations de responsabilité : indiquent qui a effectué quelle tâche --> <!-- Ajouter autant de tâches (<respStmt>) que nécessaire --> <respStmt> <resp>Transcription</resp> <name>Alexia DEDIEU</name> </respStmt> <respStmt> <resp>Traduction</resp> <name>Alexia DEDIEU</name> </respStmt> <respStmt> <resp>Encodage</resp> <name>Sarah GAUCHER</name> </respStmt>  <!-- Attention ! Ne pas modifier les tâches ci-dessous --> <respStmt> <resp>Modélisation et structuration</resp> <persName>Elisabeth GRESLOU</persName> <persName>Elysabeth HUE-GAY</persName> </respStmt> <respStmt> <resp>Première version du modèle d'encodage</resp> <persName>Elisabeth GRESLOU</persName> <persName>Anne GARCIA-FERNANDEZ</persName> </respStmt> <principal>Malika BASTIN-HAMMOU</principal> </titleStmt>  <!-- Informations de publication sur le laboratoire numérique du projet. Ne pas modifier--> <publicationStmt> <publisher/> <availability> <p>Le site est en accès restreint</p> </availability> <pubPlace>ithac.elan-numerique.fr</pubPlace> </publicationStmt>  <!-- Informations bibliographiques sur l'édition ancienne d'où le paratexte est tiré --> <sourceDesc> <bibl> <title>Alcestis Euripidis conuersa a Georgio Buchanano V. CL. In Memoriam Lectissimae et omnium matronalium uirtutum laudibus ornatissimae feminae, Anna Stympfeliae Mariaebergensis, coniugis Magnifici et Clarissimi Viri Dn Viti Ortelii VVinshemii, V. I. D. Consiliarii Saxonici et Professoris in Academia Vitebergensi, exhibita a VVitebergae a filiis, Accesserunt noui Prologi et Epilogus una cum Epicediis et Apitaphiis quibusdam, in eiusdem honorem conscriptis. ": "Eu1581_Mencius_p1",
  "Eximiae spei adolescentibus et pueris, Vito, Georgio, Iohanni, Casparo, Christiano, et Paulo, Magnifici atque Excellentissimi Viri, Dn. Viti Ortelii VVinshemii, V.I. D. et Professoris, in inclita Viteberg. Academia et c. Filiis Georgius Nucelius Annaebergensis. ": "Eu1581_Nucelius_p1",
  "Nicolao Micault Indeveldii Domino et priutai consilii Senatori, Georgius Ratallerus.": "Eu1581_Ratallerus_p1",
  "Argumentum Phoenissarum per Georgium Ratallerum.": "Eu1581_Ratallerus_p2",
  "Euripidis poetae tragici Hippolytus Coronatus, Georgio Ratallero interprete. Argumentum Hippolyti auctore Georgio Ratallero.": "Eu1581_Ratallerus_p3",
  "Egregia uirtute atque eruditione uiro D. Theodoro Cantero Georgius Ratallerus S.": "Eu1581_Ratallerus_p4",
  "Argumentum Andromachae per Georgium Ratallerum.": "Eu1581_Ratallerus_p5",
  " <!-- Indiquer la langue du paratexte dans l'attribut xml:lang : \"la\" pour le latin et \"grc\" pour le grec --> <!-- Attention ! ne pas faire de retour charriot dans le titre --> <author ref=\"#utenhouius_carolus\">Carolus Vtenhouius</author> <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence en minuscules, précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->  <!-- Déclarations de responsabilité : indiquent qui a effectué quelle tâche --> <!-- Ajouter autant de tâches (<respStmt>) que nécessaire --> <respStmt> <resp>Transcription</resp> <name>Alexia DEDIEU</name> </respStmt> <respStmt> <resp>Traduction</resp> <name>Alexia DEDIEU</name> </respStmt> <respStmt> <resp>Encodage</resp> <name>Sarah GAUCHER</name> </respStmt>  <!-- Attention ! Ne pas modifier les tâches ci-dessous --> <respStmt> <resp>Modélisation et structuration</resp> <persName>Elisabeth GRESLOU</persName> <persName>Elysabeth HUE-GAY</persName> </respStmt> <respStmt> <resp>Première version du modèle d'encodage</resp> <persName>Elisabeth GRESLOU</persName> <persName>Anne GARCIA-FERNANDEZ</persName> </respStmt> <principal>Malika BASTIN-HAMMOU</principal> </titleStmt>  <!-- Informations de publication sur le laboratoire numérique du projet. Ne pas modifier--> <publicationStmt> <publisher/> <availability> <p>Le site est en accès restreint</p> </availability> <pubPlace>ithac.elan-numerique.fr</pubPlace> </publicationStmt>  <!-- Informations bibliographiques sur l'édition ancienne d'où le paratexte est tiré --> <sourceDesc> <bibl> <title>Alcestis Euripidis conuersa a Georgio Buchanano V. CL. In Memoriam Lectissimae et omnium matronalium uirtutum laudibus ornatissimae feminae, Anna Stympfeliae Mariaebergensis, coniugis Magnifici et Clarissimi Viri Dn Viti Ortelii VVinshemii, V. I. D. Consiliarii Saxonici et Professoris in Academia Vitebergensi, exhibita a VVitebergae a filiis, Accesserunt noui Prologi et Epilogus una cum Epicediis et Apitaphiis quibusdam, in eiusdem honorem conscriptis. ": "Eu1581_Vtenhouius_p1",
  " <!-- Indiquer la langue du paratexte dans l'attribut xml:lang : \"la\" pour le latin et \"grc\" pour le grec --> <!-- Attention ! ne pas faire de retour charriot dans le titre --> <author ref=\"#wanckelius_ioannes\">Ioannes Wanckelius</author> <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence en minuscules, précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->  <!-- Déclarations de responsabilité : indiquent qui a effectué quelle tâche --> <!-- Ajouter autant de tâches (<respStmt>) que nécessaire --> <respStmt> <resp>Transcription</resp> <name>Alexia DEDIEU</name> </respStmt> <respStmt> <resp>Traduction</resp> <name>Alexia DEDIEU</name> </respStmt> <respStmt> <resp>Encodage</resp> <name>Sarah GAUCHER</name> </respStmt>  <!-- Attention ! Ne pas modifier les tâches ci-dessous --> <respStmt> <resp>Modélisation et structuration</resp> <persName>Elisabeth GRESLOU</persName> <persName>Elysabeth HUE-GAY</persName> </respStmt> <respStmt> <resp>Première version du modèle d'encodage</resp> <persName>Elisabeth GRESLOU</persName> <persName>Anne GARCIA-FERNANDEZ</persName> </respStmt> <principal>Malika BASTIN-HAMMOU</principal> </titleStmt>  <!-- Informations de publication sur le laboratoire numérique du projet. Ne pas modifier--> <publicationStmt> <publisher/> <availability> <p>Le site est en accès restreint</p> </availability> <pubPlace>ithac.elan-numerique.fr</pubPlace> </publicationStmt>  <!-- Informations bibliographiques sur l'édition ancienne d'où le paratexte est tiré --> <sourceDesc> <bibl> <title>Alcestis Euripidis conuersa a Georgio Buchanano V. CL. In Memoriam Lectissimae et omnium matronalium uirtutum laudibus ornatissimae feminae, Anna Stympfeliae Mariaebergensis, coniugis Magnifici et Clarissimi Viri Dn Viti Ortelii VVinshemii, V. I. D. Consiliarii Saxonici et Professoris in Academia Vitebergensi, exhibita a VVitebergae a filiis, Accesserunt noui Prologi et Epilogus una cum Epicediis et Apitaphiis quibusdam, in eiusdem honorem conscriptis. ": "Eu1581_Wanckelius_p1",
  "Illustrissimis styriae proceribus ordinis equestris aeternae felicitatis uoto Nicolaus Gabllman i<uris> c<onsultus> dico, consecro.": "Eu1592_Gabllmanus_p1",
  "Cur inscribatur Phoenissarum tragoedia": "Eu1592_Gabllmanus_p2",
  "Argumentum tragoediae": "Eu1592_Gabllmanus_p3",
  "Ego Petrus Aegidius": "Eu1593_Aegidius_p1",
  "Nos Ioannes Dimas Loris": "Eu1593_Dimas_p1",
  "Quintus Septimius Florens Christianus Nobili et eruditissimo uiro Iano Douzae Nordouici Toparchae salutem dat.": "Eu1594_Christianus_p1",
  "Quintus Septimius Florens Christianus Nobili et eruditissimo uiro iuueni Iano Douzae filio salutem dat.": "Eu1594_Christianus_p2",
  "Quintus Septimius Florens Christianus Optimo et erudito uiro Gerardo Tuningio salutem dat.": "Eu1594_Christianus_p3",
  "In V.C. Quintui Septimi Florentis Christiani Andromacham Latinam": "Eu1594_Christianus_p4",
  "ΦΛ. ΧΡΙΣΤΙΑΝΟΥ ὑπόθεσις τοῦ δράματος, δι’ἰάμβων, ἐν οἷς ᾿Ανδρομάχη ἐστιν ἀχροστιχὶς.": "Eu1594_Christianus_p5",
  "Idem Latine.": "Eu1594_Christianus_p6",
  "Vlrico Chrytraeo Domino Dauidis, uiri clarissimi Filio. Iohannes Posselius Salutem Plurimam Dat.": "Eu1595_Posselius_p1",
  "Nobilissimo et amplissimo uiro, D. Georgio Ludouico ab Hutten, serenissimi Principis, Electoris Palatini intimo Consiliario, D. ac Patrono suo plurimum obseruando Aemilius Portus FR. P. CR. F. S.P.D.P.Q.F.O.": "Eu1597_Portus_p1",
  "Reuerendissimo in Christo Patri Domino Francisco Maunio Burdegalensium archiepiscopo, Aquitaniae primate, Baptista Sapinus, consiliarius Regius, S.": "Eu1602_Sapinus_p1",
  "Viro clarissimo Antonio Fayo, Paulus Stephanus.": "Eu1602_Stephanus_p1",
  "Reuerendissimo in Christo patri et domino Iacobo Zeno pontifici patauino Georgius Alexandrinus Salutem plurimam dicit.": "Pla1472_Merula_p1",
  "De uita Comoediisque Plauti excerpta quaedam ex Auctoribus grauissimis. ": "Pla1472_Merula_p2",
  "Pauli Canalis patricii Veneti carmen": "Pla1499_Canalis_p1",
  "Andreae Maronis Brisiensis epigramma": "Pla1499_Maro_p1",
  "Dominicus Palladius Soranus ad Bernardum Saracenum Venetum Plauti interpretem peritissimum.": "Pla1499_Palladius_p1",
  "Idem": "Pla1499_Palladius_p2",
  "Reuerendissimo in Christo patri Francisco Marcello Pontifici Tragurino Bernardus Saracenus Venetus S. P. D.": "Pla1499_Saracenus_p1",
  "Ioannes Petrus Valla inclyto utriusque iuris doctori Scaramuzae Trivultio Mediolanensi patricio primario S<alutem> D<icit> eternam": "Pla1499_Valla_p1",
  "Iohannis Petri Vallae in plautinas comoedias commentationes.": "Pla1499_Valla_p2",
  "Philippus Beroaldus lectori salutem dicit.": "Pla1500_Beroaldus_p1",
  " <!-- Indiquer la langue du paratexte dans l'attribut xml:lang : \"la\" pour le latin et \"grc\" pour le grec --> <!-- Attention ! ne pas faire de retour charriot dans le titre --> <author ref=\"#calcaterra_antonius\">Antonius Maria Calcaterra</author> <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence en minuscules, précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->  <!-- Déclarations de responsabilité : indiquent qui a effectué quelle tâche --> <!-- Ajouter autant de tâches (<respStmt>) que nécessaire --> <respStmt> <resp>Transcription</resp> <name>Dalia DAHR</name> </respStmt> <respStmt> <resp>Traduction</resp> <name>Déborah BOIJOUX</name> </respStmt> <respStmt> <resp>Révision</resp> <name>Matthieu FERRAND</name> </respStmt> <respStmt> <resp>Encodage</resp> <name>Sarah GAUCHER</name> </respStmt> <!-- Attention ! Ne pas modifier les tâches ci-dessous --> <respStmt> <resp>Modélisation et structuration</resp> <persName>Elisabeth GRESLOU</persName> <persName>Elysabeth HUE-GAY</persName> </respStmt> <principal>Malika BASTIN-HAMMOU</principal> </titleStmt>  <!-- Informations de publication sur le laboratoire numérique du projet. Ne pas modifier--> <publicationStmt> <publisher/> <availability> <p>Le site est en accès restreint</p> </availability> <pubPlace>ithac.elan-numerique.fr</pubPlace> </publicationStmt>  <!-- Informations bibliographiques sur l'édition ancienne d'où le paratexte est tiré --> <sourceDesc> <bibl> <title>Plautus integer cum interpretatione Joannis Baptistae Pii.": "Pla1500_Calcaterra_p2",
  "Sebastiani Ducii": "Pla1500_Ducius_p1",
  "Ioannis Alberti Marliani mediolanensis patritii": "Pla1500_Marlianus_p1",
  " <!-- Indiquer la langue du paratexte dans l'attribut xml:lang : \"la\" pour le latin et \"grc\" pour le grec --> <!-- Attention ! ne pas faire de retour charriot dans le titre --> <author ref=\"#pius_ioannes\">Ioannes Baptista Pius</author> <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence en minuscules, précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->  <!-- Déclarations de responsabilité : indiquent qui a effectué quelle tâche --> <!-- Ajouter autant de tâches (<respStmt>) que nécessaire --> <respStmt> <resp>Transcription</resp> <name>Dalia DAHR</name> </respStmt> <respStmt> <resp>Traduction</resp> <name>Déborah BOIJOUX</name> </respStmt> <respStmt> <resp>Révision</resp> <name>Matthieu FERRAND</name> </respStmt> <respStmt> <resp>Encodage</resp> <name>Sarah GAUCHER</name> </respStmt> <!-- Attention ! Ne pas modifier les tâches ci-dessous --> <respStmt> <resp>Modélisation et structuration</resp> <persName>Elisabeth GRESLOU</persName> <persName>Elysabeth HUE-GAY</persName> </respStmt> <principal>Malika BASTIN-HAMMOU</principal> </titleStmt>  <!-- Informations de publication sur le laboratoire numérique du projet. Ne pas modifier--> <publicationStmt> <publisher/> <availability> <p>Le site est en accès restreint</p> </availability> <pubPlace>ithac.elan-numerique.fr</pubPlace> </publicationStmt>  <!-- Informations bibliographiques sur l'édition ancienne d'où le paratexte est tiré --> <sourceDesc> <bibl> <title>Plautus integer cum interpretatione Joannis Baptistae Pii.": "Pla1500_Pius_p7",
  "Plauti uita": "Pla1500_Pius_p3",
  " <!-- Indiquer la langue du paratexte dans l'attribut xml:lang : \"la\" pour le latin et \"grc\" pour le grec --> <!-- Attention ! ne pas faire de retour charriot dans le titre --> <author ref=\"#salandus_ioannes\">Ioannes Salandus</author> <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence en minuscules, précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->  <!-- Déclarations de responsabilité : indiquent qui a effectué quelle tâche --> <!-- Ajouter autant de tâches (<respStmt>) que nécessaire --> <respStmt> <resp>Transcription</resp> <name>Dalia DAHR</name> </respStmt> <respStmt> <resp>Traduction</resp> <name>Déborah BOIJOUX</name> </respStmt> <respStmt> <resp>Révision</resp> <name>Matthieu FERRAND</name> </respStmt> <respStmt> <resp>Encodage</resp> <name>Sarah GAUCHER</name> </respStmt> <!-- Attention ! Ne pas modifier les tâches ci-dessous --> <respStmt> <resp>Modélisation et structuration</resp> <persName>Elisabeth GRESLOU</persName> <persName>Elysabeth HUE-GAY</persName> </respStmt> <principal>Malika BASTIN-HAMMOU</principal> </titleStmt>  <!-- Informations de publication sur le laboratoire numérique du projet. Ne pas modifier--> <publicationStmt> <publisher/> <availability> <p>Le site est en accès restreint</p> </availability> <pubPlace>ithac.elan-numerique.fr</pubPlace> </publicationStmt>  <!-- Informations bibliographiques sur l'édition ancienne d'où le paratexte est tiré --> <sourceDesc> <bibl> <title>Plautus integer cum interpretatione Joannis Baptistae Pii.": "Pla1500_Salandus_p1",
  "Ioannes Britannicus Salutem Plurimam Alouisio Dardano Veneto.": "Pla1506_Britannicus_p1",
  "Pylades Buccardus Salutem Plurimam Alouisio Dardano Veneto.": "Pla1506_Buccardus_p1",
  "Clarissimo ac ornatissimo uiro domino Ioanni Dynchin, iurium interpreti dignissimo Reuerendissimique in Christo patris ac illustrissimi principis et domini domini Iacobi Episcopi Treuirensis Cancellario bene merentissimo Ioannes Adelphus Mulingus Argentinensis sese commendat.": "Pla1508_Adelphus_p1",
  "Obvio cuique bonae frugi Salutem": "Pla1511_Soardus_p1",
  "Symon Charpentarius Parrhisiensis P. Fausto Andrelino foroliuiensi, poeta laureato, Regio necnon Regineo clarissimoque oratori, foelicitatem": "Pla1512_Charpentarius_p1",
  "Symonis Charpentarii Parrhisii in plautinas viginti Comoedias praelibatio seu praefatio cum poetae vita necnon carminis iambici et Comoediarum ratione, aliis quibusdam annexis quae hic adnodatu dignissima visa sunt et Comoediarum lectoribus atque auditoribus profutura.": "Pla1512_Charpentarius_p2a",
  "De comoedia et eius partibus": "Pla1512_Charpentarius_p2b",
  "Laurentio Medici Florentinae Reipublicae Optimatum clarissimo, Nicolaus Angelius Bucinensis. S.": "Pla1514_Angelius_p1",
  "Francisci Asulani in recognitionem M. Accii Plauti Epistola ad Nicolaum Sconbergum Archiepiscopum Campanum": "Pla1522_Asulanus_p1",
  "Plauti uita ex Petro Crinito de Poetis Latinis.": "Pla1522_Crinitus_p1",
  "Epistola. Ioachimus Camerarius Pabepergensis Francisco Crammio Sagano S. D.": "Pla1558_Camerarius_p6",
  "Epistula nuncupatoria. Prooemium Ioachimi Camerarii, de fabulis Plautinis, ad Illustrissimos pueros Franciscum Othonem et Fridericum fratres.": "Pla1558_Camerarius_p2",
  "De Carminibus Comicis": "Pla1558_Camerarius_p3",
  "Ioachimus, epistola": "Pla1558_Camerarius_p4",
  "De Plauto σκάζων": "Pla1558_Camerarius_p5",
  "Ornatissimo maximis uirtutibus et summa doctrina uiro Ioaschimo Camerario, amico suo semper obseruando, Georgius Fabricius S. D.": "Pla1558_Fabricius_p1",
  "Εἰς τὸν Πλαῦτον Διονυσίου Λαμβίνου Ιω.Αὐρᾶτος ποιητὴς βασιλικὸς.": "Pla1576_Auratus_p1",
  "Iacobus Helias graecarum litterarum doctor regius lectoribus S. D.": "Pla1576_Helias_p1",
  "Clarissimo et eruditissimo viro Germano Valenti Guellio PP. Senatori amplissimo, Germanus Lambinus Salutem dat.": "Pla1576_Lambinus_p1",
  "Ad lectorem In Plautum Nobilissimi et doctissimi uiri Iani Dousae opera restitutum.": "Pla1587_Benedicti_p1",
  "In Iani Dousae Plautinam restitutionem Epigramma": "Pla1587_Bredius_p1",
  "In Iani Dousae centurionatum Plautinum Adrianus Burchius": "Pla1587_Burchius_p1",
  "Viro amplissimo Danieli Rogersio Albimontio Anglo, Augustissimae Heroinae, Dominae Elisabethae Angliae, Franciae, Hiberniaeque Reginae, Oratori Ianus Dousa filius S.D.": "Pla1587_Dousa_p1",
  "Eiusdem ad eundem elegia": "Pla1587_Dousa_p2",
  "In Plautum Viri Nobilissimi Iani Dousae opera emendatum, Iani Gruteri Carmen Elicium.": "Pla1587_Gruterus_p1",
  "Ad Frid. Taubmannum": "Pla1605_Blansdorff_p1",
  "Ede hilares mecum plausus Musea iuuentus": "Pla1605_Cremcovius_p1",
  "Risus magister, et leporis conditor": "Pla1605_Gentilis_p1",
  "Tot nuper scatuit Plauti Comoedia mendis": "Pla1605_Peiferus_p1",
  "In editionem Plautinam F.T.": "Pla1605_Rittershusius_p1",
  "Aliud": "Pla1605_Sebaldi_p1",
  "Postquam est mortem aptus Plautus, Comoedia luget": "Pla1605_Siberus_p1",
  "Optimis maximisque Reipublicae Litterariae Triumviris, Josepho Scaligero, Justo Lipsio, Isaaco Casaubono, Fridericus Taubmanus S. D.": "Pla1605_Taubmanus_p1",
  "Ad lectorem candidum et eruditum": "Pla1605_Taubmanus_p2",
  "Diua parens Latii, late dominata per Orbem": "Pla1605_Treutlerus_p1",
  "Aldus Romanus Ioanni Lascari viro praeclaro ac doctissimo s.d.": "Soph1502_Manutius",
  "Antonius Francinus Varchiensis, Ioanni Baptistae Egnatio Veneto Salutem.": "Soph1522_Francinus",
  "Epistola Dedicatoria": "Soph1533_Lonicerus",
  "Commentarii Interpretationum Argumenti Thebaidos Fabularum Sophoclis, Authore Ioachimo Camerario Quaestore iam recens natis atque editis. ": "Soph1534_Camerarius_p1",
  "De Tragico Carmine et illius praecipuis authoribus apud Graecos. ": "Soph1534_Camerarius_p2",
  "Claudius Theraeus Ioannis Sturmio S. P. D.": "Soph1540_Theraeus",
  "Nobilissimo atque ornatissimo uiro Mario Sauorgnano, Ioannis Baptista a Burgofrancho, Papiensis, S. D.": "Soph1543_Gabia",
  "Reuerendissimo et illustrissimo Georgio Armaniaco Rhutenen. Episcopo, ac Sanctae Romanae Ecclesiae Cardinale dignissimo. Bartholomeus Marlianus S. D.": "Soph1545_Marlianus_p1",
  "Ad Amplissimum Virum Cardinalem Armeniacum, Horatii Nuclei carmen.": "Soph1545_Nucleus",
  "De autoribus Tragoediae Joachimus Camerarius.": "Soph1546_Camerarius_p1",
  "In Œdipum Tyrannum Sophoclis Argumentum Ioachimi Camerarii. ": "Soph1546_Camerarius_p2",
  "In Antigonen Sophoclis Argumentum Ioachimi Camerarii Pabenbergensis": "Soph1546_Camerarius_p3",
  "In Œdipum Coloneum Argumentum Iachimi Camerarii.": "Soph1546_Camerarius_p4",
  "Inclyto Regi Angliae et Franciae Eduardo Principi Clementiis. Vitus Winshemius S.D.": "Soph1546_Winshemius_p1",
  "In Aiacem Sophoclis.": "Soph1546_Winshemius_p2",
  "In Electram Sophoclis.": "Soph1546_Winshemius_p3",
  "Antigone Sophoclis.": "Soph1546_Winshemius_p4",
  "In Oedipum Coloneum Sophoclis.": "Soph1546_Winshemius_p5",
  " Trachiniae Sophoclis. Argumentum. ": "Soph1546_Winshemius_p6",
  "Sophoclis Philoctetes. Argumentum Fabulae. ": "Soph1546_Winshemius_p7",
  "Bernardus Iuncta Lectori Sal": "Soph1547_Iunta",
  "Generosissimo Heroi, Ludovico a Flandria, Domino de Praet, Equiti Summi Ordinis Velleris Aurei, etc. Georgius Rotaller": "Soph1550_Ratallerus_p1",
  "Ad candidum lectorem Georgius Rotaller": "Soph1550_Ratallerus_p2",
  "In duas Sophoclis Tragoedias Aiacem Flagelliferum, et Antigonem, a Georgio Rotallero Phrysio uersas, Cornelius Scribonius. ": "Soph1550_Scribonius",
  "Stephani Strati Iureconsul. Antuerpiani Epistola, qua Georgium Rotallerum ad Sophoclis sui editionem adhortatur, Calend. August. M. D. L. ": "Soph1550_Stratus",
  " <author ref=\"#turnebus_adrianus\">Adrianus Turnebus</author> <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence en minuscules, précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->  <respStmt> <resp>Transcription</resp> <persName/> </respStmt> <respStmt> <resp>Traduction</resp> <persName/> </respStmt> <respStmt> <resp>Révision</resp> <name/> </respStmt> <respStmt> <resp>Encodage</resp> <name/> </respStmt> <principal>Malika BASTIN-HAMMOU</principal> <respStmt> <resp>Modélisation et structuration</resp> <persName>Elisabeth GRESLOU</persName> <persName>Elysabeth HUE-GAY</persName> </respStmt> <respStmt> <resp>Première version du modèle d'encodage</resp> <persName>Elisabeth GRESLOU</persName> <persName>Anne Garcia-Fernandez</persName> </respStmt> </titleStmt>  <publicationStmt> <publisher/> <availability> <p>Le site est en accès restreint</p> </availability> <pubPlace>ithac.elan-numerique.fr</pubPlace> </publicationStmt> <sourceDesc> <bibl> <title>ΣΟΦΟΚΛΕΟΥΣ ΤΡΑΓΩΔΙΑΙ": "Soph1553_Turnebus_p1",
  "Ioachimus Camerarius Ioanni Oporino S. D.": "Soph1556_Camerarius_p1",
  "In Sophoclem de Consilio Auctoris. ": "Soph1556_Camerarius_p2",
  "De Genere Scripti. ": "Soph1556_Camerarius_p3",
  "De Authore Harum Tragoediarum.": "Soph1556_Camerarius_p4",
  "Iani Guillini Parisini in Authoris Commendationem. ": "Soph1557a_Guillinus",
  "Ad Momum. Petri Boni. ": "Soph1557a_Lalamantius_p1",
  "Clarissimo et Imprimis Pio, Petro Marsilio Cipierro, Abbati de Mortemario, Priori Samphoriano, et Canonico Heduensi, obseruandis suo, Ioannes Lalamantius, S. P. ": "Soph1557a_Lalamantius_p2",
  "In Sophoclis Tragoedias Praefatiuncula Per Ioannem Lalamantium. ": "Soph1557a_Lalamantius_p4",
  " <author ref=\"#lalamantius_ioannes\">Johannes Lalamantius</author>  <respStmt> <resp>Transcription</resp> <persName>Diandra CRISTACHE</persName> <name>Sarah GAUCHER</name> </respStmt> <respStmt> <resp>Traduction</resp> <persName>Sarah GAUCHER</persName> </respStmt> <respStmt> <resp>Encodage</resp> <name>Sarah GAUCHER</name> </respStmt> <respStmt> <resp>Modélisation et structuration</resp> <persName>Elisabeth GRESLOU</persName> <persName>Elysabeth HUE-GAY</persName> </respStmt>  <principal>Malika BASTIN-HAMMOU</principal> </titleStmt>  <publicationStmt> <publisher/> <availability> <p>Le site est en accès restreint</p> </availability> <pubPlace>ithac.elan-numerique.fr</pubPlace> </publicationStmt>  <sourceDesc> <bibl> <title>Sophoclis Tragicorum ueterum facile principis Tragoediae, quotquot extant, septem. Aiax, Electra, Oedipus Tyrannus, Antigone, Oedipus in Colono, Trachiniae, Philoctetes. Nunc primum Latinae factae, et in lucem emissae per Ioannem Lalamantium apud Augustudunum Heduorum Medicum. ": "Soph1557a_Lalamantius_p5",
  "Carminis Ratio. ": "Soph1557a_Lalamantius_p6",
  " <author ref=\"#meggisserus_hieronymus\">Hieronymus Meggisserus</author>  <respStmt> <resp>Transcription</resp> <persName>Diandra CRISTACHE</persName> </respStmt> <respStmt> <resp>Traduction</resp> <persName>Sarah GAUCHER</persName> </respStmt> <respStmt> <resp>Encodage</resp> <name>Diandra CRISTACHE</name> <name>Sarah GAUCHER</name> </respStmt> <respStmt> <resp>Modélisation et structuration</resp> <persName>Elisabeth GRESLOU</persName> <persName>Elysabeth HUE-GAY</persName> </respStmt>  <principal>Malika BASTIN-HAMMOU</principal> </titleStmt>  <publicationStmt> <publisher/> <availability> <p>Le site est en accès restreint</p> </availability> <pubPlace>ithac.elan-numerique.fr</pubPlace> </publicationStmt>  <sourceDesc> <bibl> <title>Sophoclis Tragoediae Septem, Latino carmine redditae, et Annotationibus illustratae, per Thomam Naogeorgum Straubigensem. ": "Soph1558_Meggisserus_p1",
  "Generoso ac amplissimo uiro D. Ioanni Iacobo Fuggero, Domino Vissenhorni ac Kirchpergae, consiliario Caesareo ac Regio, etc. patrono suo colendissimo, Tho. Naogeorgus S. D. P.": "Soph1558_Naogeorgus",
  "Christianus Pierius, ad lectorem et Thomam Naogeorgum.": "Soph1558_Pierius",
  "Sebastianus Pontanus Vlmensis": "Soph1558_Pontanus_p1",
  "Jacobus Scheggius Medicinae ac Philosophiae, ad Thomam Naogeorgum. ": "Soph1558_Scheggius",
  "Ad Thomam Naogeorgum, Ioannes Secceruitius Vuratislauiensis. ": "Soph1558_Secceruitius",
  "Ioannes Sturmius.": "Soph1558_Sturmius",
  "Micaelus Toxites Thomae Naogeorgo suo.": "Soph1558_Toxites",
  "Clarissimo Viro Stephano Stratio Biturigibus legum antecessori, Georgius Rotallerus.": "Soph1567_Ratallerus_p3",
  "Henricus Stephanus Lectori <foreign xml:lang=\"grc\" >φιλοσοφοκλεῖ</foreign> S. P. D. ": "Soph1568_Stephanus",
  " <author ref=\"#falkenburgius_gerartus\">Gerartus Falkenburgius</author>  <respStmt> <resp>Transcription</resp> <persName>NR</persName> </respStmt> <respStmt> <resp>Traduction</resp> <persName>NR</persName> </respStmt> <respStmt> <resp>Encodage</resp> <name>Sarah GAUCHER</name> </respStmt> <respStmt> <resp>Modélisation et structuration</resp> <persName>Elisabeth GRESLOU</persName> <persName>Elysabeth HUE-GAY</persName> </respStmt>  <principal>Malika BASTIN-HAMMOU</principal> </titleStmt>  <publicationStmt> <publisher/> <availability> <p>Le site est en accès restreint</p> </availability> <pubPlace>ithac.elan-numerique.fr</pubPlace> </publicationStmt>  <sourceDesc> <bibl> <title>Tragoediae Sophoclis quotquot extant carmine latino redditae. Georgio Ratallero in supremo apud belgas regio senatu Mechliniae Consiliario, et libellorum supplicum Magistro, interprete.": "Soph1570_Falkenburgius_p1",
  "Nicolaus Nicolai Grudius Georgio Ratallero S. P. Dolam.": "Soph1570_Grudius",
  "Adriani Mylii Senatoris Regii apud Hollandos ad Lectorem Epistola.": "Soph1570_Mylius",
  "Ioachimus Polites Georgio Ratallero S. P. Atrebatum.": "Soph1570_Polites",
  "Illustri uiro D. Frederico Perenotto Domino a Champagney Sancto Lupo etc. Georgius Ratallerus S. P. D.": "Soph1570_Ratallerus_p1",
  " De Ratione Versuum ad Lectorem.": "Soph1570_Ratallerus_p2",
  " <author ref=\"#gardesius_ioannes\">Ioannes Gardesius</author>  <respStmt> <resp>Transcription</resp> <persName>Daindra CRISTACHE</persName> </respStmt> <respStmt> <resp>Traduction</resp> <persName>NR</persName> </respStmt> <respStmt> <resp>Encodage</resp> <name>Sarah GAUCHER</name> </respStmt> <respStmt> <resp>Modélisation et structuration</resp> <persName>Elisabeth GRESLOU</persName> <persName>Elysabeth HUE-GAY</persName> </respStmt>  <principal>Malika BASTIN-HAMMOU</principal> </titleStmt>  <publicationStmt> <publisher/> <availability> <p>Le site est en accès restreint</p> </availability> <pubPlace>ithac.elan-numerique.fr</pubPlace> </publicationStmt>  <sourceDesc> <bibl> <title>Sophoclis Aiax Lorarius, stylo tragico a Iosepho Scaligero Iulii F. translato. ": "Soph1574_Gardesius_p1",
  "ΑΝΔΡΙ ΛΑΜΠΡΟΤΑΤΩ ΓΕΩΡΓΙΩ ΡΑΤΑΛΛΕΡΩ ΒΙΛΕΔΜΟΣ ΚΑΝΤΗΡΟΣ ΕΥ ΠΡΑΤΤΕΙΝ ": "Soph1579_Canterus_p1",
  "Gulielmi Canteri in Sophoclem Prolegomena. ": "Soph1579_Canterus_p2",
  "Studiosis Graecae Linguae S. P. D.": "Soph1583_Codicillus_p1",
  "Generoso ac Magnifico Heroi et Domino: Domino Ladislao Seniori a Lobkouitz, Baroni in Chlumecio et Gistebnicia S. C. M. Consiliario atque pro Regi, et in inclyto Boemiae Regno supremo Curiae Magistro, etc. Domino atque Mecaenati colendo. S. P. D. ": "Soph1583_Codicillus_p2",
  "De Autoribus Tragoediae. Joachimus Camerarius. ": "Soph1584_Camerarius",
  "In Fl. Christiani Philoctetem": "Soph1586_Auratus_p1",
  "Q. Sept. Florens Christianus Doctissimo uiro Nicolao Gulonio Graecarum literarum Regio doctori, S. dico.": "Soph1586_Christianus_p1",
  "Ad Sophoclis Philoctetam in Lemno Glossemata.": "Soph1586_Christianus_p2",
  "Hypothesis uel argumentum (ut uocant) Philocteta in Lemnoa ": "Soph1586_Christianus_p3",
  "Q. Sept. Florenti Christiano uiro ornatissimo et doctissimo, N. Gulonius S.P.D.": "Soph1586_Gulonius_p1",
  "Argumentum in Trachinias, Thoma Naogeorgo interprete.": "Soph1588_Naogeorgus",
  "Periocha in tragoediam Aiacis": "Soph1609_Scaliger",
  "Carolus Fernandus salutem plurimam dicit Petro Cohardo aduocato regio ": "Sq1488_Fernandus_p1",
  "Carolus Fernandus lectorem alloquitur ": "Sq1488_Fernandus_p2",
  "Gellii Bernardini Marmitae ad illustrem D. Guielmum de Rupeforti magnum Cancellarium Franciae in tragoedias Senecae interpretatio ": "Sq1491_Marmitta_p1",
  "Benedicti Philologi Florentini praefatio, super L. Anneae Senecae Tragoediis, ad Dominicum Beneuenium Diui Laurentii Canonicum": "Sq1506_Riccardinus_p1",
  "De tragoedia": "Sq1506_Riccardinus_p2",
  "De partibus tragoediae": "Sq1506_Riccardinus_p3",
  "Senecarum uita ex Crinito": "Sq1511_Crinitus_p1",
  "Aegidius Maseriensis Iacobo de Molendino perspicacissimo sacrae theologiae professori uigilantissimoque Gymnasii Bonorum Puerorum pastori, salutem": "Sq1511_Maserius_p1",
  "Bonorum Puerorum Chorus": "Sq1511_Maserius_p2",
  "Tragoediae diffinitio": "Sq1511_Maserius_p3",
  "De Pomponio autem Secundo et Seneca Tragico iudicium Terentianus faciens sic scriptum reliquit": "Sq1511_Maserius_p4",
  "Si quid lectores optimi seminotatum aut inuersum percipiatis aequo ferte animo ; pauca enim sunt, ut reor, quae legentem si mediocriter sapiat remorentur.": "Sq1511_Maserius_p5",
  "Aegidius Maseriensis ad dominum Nicolaum de Wattenwill Bernensem iambicum senarium carmen": "Sq1511_Maserius_p6",
  "Petrus de Stainuilla in libri commendationem": "Sq1511_Stainvilla_p1",
  "Generoso admodum uiro raroque sapientis eloquentiae lepore decorato D. Ioanni Landano ; Landae Bougoniique in dioecesi Nannetensi domino humanissimo ; et Guilielmi Landani uulgo de la Lande procuratoris olim Britanniae generalis primogenito heredique dignissimo, Iodocus Badius Ascensius salutem dicit": "Sq1514_Ascensius_p1",
  "Polydori Comitis Cabaliati in defensionem Caietani Cremonensis, praeceptoris sui ": "Sq1514_Cabaliatus_p1",
  "Danielis Apologia": "Sq1514_Gaietanus_p1",
  "[Tragica farrago]": "Sq1514_Gaietanus_p2",
  "Aegidii Maserii ad Stephanum Antoniumque, lapitheos fratres iambicum carmen ": "Sq1514_Maserius_p1",
  "Aegidii Maserii de uariis tragici praesertim carminis metris : primum De iambico Regula": "Sq1514_Maserius_p2",
  "Hieronymus Avantius Veronensis Latino iuuenali protonotario apostolico romano patricio ornatissimo salutem plurimam dicit": "Sq1517_Avantius_p1",
  "Dimensiones tragoediarum Senecae per eundem Hieronymum Avantium.": "Sq1517_Avantius_p2",
  "De carmine Iambico trimetro": "Sq1566_Avantius_p1",
  "Illustris ducis ac domini D. Wolfgangi Palatini Rheni, Ducis Bauariae, Comitis Veldentii, filiis, D. D. Othoni Henrico, et Friderico fratribus salutem dat Georgius Fabricius Chemnicensis ": "Sq1566_Fabricius_p1",
  "De reliquis carminum generibus": "Sq1566_Fabricius_p2",
  "Lilius Gregorius Gyraldus. Dialogo VIII": "Sq1566_Gyraldus_p1",
  "Ad amplissimum virum, clarissimumque I. C. Ludovicum Delrio, regium consiliarium, libellorumque supplicum magistrum etc. Hieronymi Delrio praefatio": "Sq1576_Delrio_p1",
  "Martinus Antonius Delrio lectori salutem dat": "Sq1576_Delrio_p2",
  "Praeludia quaedam de tragoedia, tragicis et Seneca tragoediographo ": "Sq1576_Delrio_p3",
  "Ad Martinum Antonium Delrio Augustinus Descobar Benventanus": "Sq1576_Descobar_p1",
  "Ioannis Dominici Florentii Romani Carmen": "Sq1576_Florentius_p1",
  "Adolphi Mekerchi Brugensis": "Sq1576_Mekerchius_p1",
  "De Senecae tragoediis Martino Antonio Delrio Restitutis et Commentario Illustratis, Francisci Nansii Isembergensis societatis Jesu.Christi.": "Sq1576_Nansius_p1",
  "Quae tibi rependent, docte LIPSI, praemia…": "Sq1589_A_Dousa_p1",
  "Iustus Lipsius Francisco Raphelengio, Francisci Filio, Plantiniano salutem dat.": "Sq1589_A_Lipsius_p1",
  "Iusto Lipsio, uiro clarissimo, Franciscus Raphelengius, Francisci Filius Plantinianus, Dicat Consecratque": "Sq1589_A_Raphelengius_p1",
  "Franciscus F. F. Raphelengius Lectori S.": "Sq1589_A_Raphelengius_p3",
  "Othoni Grynradio Viro Nobili Salutem": "Sq1589_B_Commelinus_p1",
  "Quam grauis immineat saeuis fortuna tyrannis…": "Sq1589_B_Posthius_p1",
  "Janus Dousa Nordouix etc. ": "Sq1604_Dousa_p1",
  "Magnifico amplissimoque Ludivico Culmano, serenissimi electoris Palatini Friderici IV, in sanctiore consilio pro praesidi": "Sq1604_Gruterus_p1",
  "Amplissimo nobilissimoque uiro Iohanni Milandro Domino de Poederoeyen illustrissimi principis Mauritii a Nassou a consiliis et secretis Daniel Heinsius salutem dicit": "Sq1611_Heinsius_p1",
  "Amico Lectori.": "Sq1611_Heinsius_p2",
  "Danielis Heinsii in Lucii et Marci Annaei Senecae ac reliquorum quae extant tragoedias animaduersiones et notae.": "Sq1611_Heinsius_p3",
  "De Farnabio ad lectorem ": "Sq1613_Audoenus_p1",
  "Viro litterarum humaniorum studiosissimo Thomae Rolando": "Sq1613_Farnabius_p1",
  "Ad aequanimos lectores": "Sq1613_Farnabius_p2",
  "Comedias trusatilis Plauti mola ": "Sq1613_Jonson_p1",
  "Viro de re literaria oprime merito ; Thomae Farnabio": "Sq1613_Tomkins_p1",
  "Eidem.": "Sq1613_Tomkins_p2",
  "Ad eundem": "Sq1613_Wheare_p1",
  " De opere, operis autore, autoris interprete, interpretis amicus": "Sq1613_Whitakerus_p1",
  "Io<docus> Ba<dius> Ascensius lectoribus salutem dicit": "Te1493_Ascensius_p1",
  "Ioannis Egidii Nuceriensis epigramma ad iuuenes": "Te1493_Egidius_p1",
  "Guido Iuuenalis Germano de Ganeio uiro senatorio bonarumque litterarum amantissimo Salutem Plurimam Dat": "Te1493_Juvenalis_p1",
  "Guido Iuuenalis Nicholao de Capella uiro multis laudibus efferendo S.P.D.": "Te1493_Juvenalis_p2",
  "Guido Iuuenalis Martino Guerrando iuris utriusque et pontificii et caesarii peritissimo atque uenerandi in Christo patris praesulis Cenomanensis secretario dignissimo S.P.D.": "Te1493_Juvenalis_p3",
  "Guido Iuuenalis Nicholaum Peletarium iuris legum consultissimum atque in omni doctrina conspicuum multa salute iubet impartiri": "Te1493_Juvenalis_p4",
  "Guido Iuuenalis Michaeli Burello sacrosanctae theologiae professori perquam erudito S.P.D.": "Te1493_Juvenalis_p5",
  "Guidonis Iuuenalis natione Cenomani epigramma super causa operis suscepti.": "Te1493_Juvenalis_p6",
  "Figure declaratio. Argumenti lucidior secundum hanc figuram declaratio": "Te1496_Gruninger_p1",
  "Figurae huius quae secundae comediae Eunuchi principalis est et argumenti declaratio clarior": "Te1496_Gruninger_p2",
  "Declaratio figurae": "Te1496_Gruninger_p3",
  "Declaratio huius figurae in Comediam Terentii quae in ordine quarta est atque Adelphos vocitata": "Te1496_Gruninger_p4",
  "Declaratio super quintam Therentii comoediam in figuram hanc argumentum declarans lucidius": "Te1496_Gruninger_p5",
  "Figurae sequentis declaratio in sextam et ultimam Threntii Comediam Ecyram dictam introductoria": "Te1496_Gruninger_p6",
  "In laudem Terentianae lectionis, Epigramma Heinrici Bebelii Iustingensis": "Te1499_Bebelius_p1",
  "Iacobus Locher Philomusus poeta et orator laureatus Iohanni Grüninger librorum impressori ac ciui Argen. Prudenti et honesto. In laudem Therentii S.P.D.": "Te1499_Locher_p1",
  "Ad lectorem Epigramma eiusdem Philomusi": "Te1499_Locher_p2",
  "Vita Terentii, excerpta de dictis D. F. Petrarca": "Te1499_Petrarca_p1",
  "Jodocus Badius Ascensius Magistro Herveo Besino, iureconsulto, litteris eruditissimo amicorumque primario, salutem plurimam dicit.": "Te1504_Ascensius_p1",
  "Ad studiosam Britanniae maioris quae nunc Anglia dicitur iuuentutem hexastichon Ascensianum": "Te1504_Ascensius_p2",
  " Quid sit poeta et quanta eius dignitas. Caput I.": "Te1504_Ascensius_p3a",
  "Quotuplicia sint poetarum scripta": "Te1504_Ascensius_p3b",
  "De triplici carminum stilo et pedum ludentium ornamentis. Capitulum III.": "Te1504_Ascensius_p3c",
  "Descriptiones et differentiae tragoedia et comoediae. Capitulum IV.": "Te1504_Ascensius_p3d",
  "De origine et inuentione satyrarum tragoediarum et comediarum. Capitulum V.": "Te1504_Ascensius_p3e",
  "De Comoedia Antiqua. Capitulum VI.": "Te1504_Ascensius_p3f",
  "De Instrumentis et prosceniis dramatum precipue comediarum. Capitulum VII.": "Te1504_Ascensius_p3g",
  "De theatro et eius constructoribus. Capitulum VIII.": "Te1504_Ascensius_p3h",
  "De scenis et prosceniis. Capitulum IX.": "Te1504_Ascensius_p3i",
  "De personis et earum indumentis et coloribus. Capitulum X.": "Te1504_Ascensius_p3j",
  "De prosceniarum ornatu et instructione. Capitulum XI.": "Te1504_Ascensius_p3k",
  "De ludis romanis et festivitatibus in quibus agi fueuerunt comoediae. Capitulum XVII.": "Te1504_Ascensius_p3l",
  "De speciebus Comediarum. Capitulum XIII.": "Te1504_Ascensius_p3m",
  "De qualitatibus comediarum. Capitulum XIIII.": "Te1504_Ascensius_p3n",
  "De membris comoediarum. Capitulum XV.": "Te1504_Ascensius_p3o",
  "De partibus comoediarum et primum de tribus non princibalibus. Capitulum XVI. ": "Te1504_Ascensius_p3p",
  "De prologis et eorum speciebus. Capitulum XVII. ": "Te1504_Ascensius_p3q",
  "De tribus partibus principalibus in comoedia. Capitulum XVIII. ": "Te1504_Ascensius_p3r",
  "De actibus et eorum distinctione in comoediis. Capitulum XIX. ": "Te1504_Ascensius_p3s",
  "De decoro et primo personarum. Capitulum XX.": "Te1504_Ascensius_p3t",
  "De rerum decoro. Capitulum XXI.": "Te1504_Ascensius_p3u",
  "De verborum decoro. Capitulum XXII.": "Te1504_Ascensius_p3v",
  "De decoro totius operis. Capitulum XXIII. ": "Te1504_Ascensius_p3w",
  "Jodoci Badii Ascensii familiaria in Terentium prenotamenta.": "Te1504_Ascensius_p3",
  "De quattuor causis huius operis. Capitulum XXIIII. ": "Te1504_Ascensius_p3x",
  "De Terentii vita. Capitulum XXV. ": "Te1504_Ascensius_p3y",
  "Jodoci Badii Ascensii familiaria in Terentium prenotamenta. De forma operibus et laude Terentii. Capi(tulum) XXVI. ": "Te1504_Ascensius_p3z",
  "Studioso lectori": "Te1530_Barlandus_p1",
  "Ad Louanienses Tyrunculos": "Te1530_Barlandus_p2",
  "Erudito uiro Ioanni Feuyno Furnio, Iuris utriusque Doctori, Scholasteri et Canonico S. Donatiani apud Brugenses Adrianus Barlandus S. P. D.": "Te1530_Barlandus_p3",
  "Haec habui, Feuyne ornatissime…": "Te1530_Barlandus_p4",
  "Des. Erasmus Roterodamus Ioanni et Stanislao Boneris fratribus Polonis s.d.": "Te1532_Erasmus_p1",
  "Des. Erasmus Roterodamus de metris": "Te1532_Erasmus_p2",
  "Optimae spei adolescentulo Iodoco Vuillichio iuniori sui fratris filio, Gregorius Vuagnerus Resellianus, S. P. D. ": "Te1550_Wagnerus_p1",
  "P. Antesignanus Rapistagnensis Antonio, Theophilo, et Ioanni saraeis fratribus salutem dicit ": "Te1560_Antesignarus_p1",
  "Hac autem editione...": "Te1560_Antesignarus_p2"
}

// PLACES

let places = {};

Place = function(svg_el){
  this.name = svg_el.getAttribute("pubplace");
  this.ancients = {
    "ar": 0,
    "pl" : 0,
    "eschl" : 0,
    "sen" : 0,
    "ter" : 0,
    "soph" : 0,
    "eur" : 0,
  }
  this.svg_el = svg_el;
  this.ancients_elements = {};
  this.setup_ancients_circles();
  places[this.name] = this;
}

let svgns = "http://www.w3.org/2000/svg";
let svg_container = document.getElementById("svg2");

function generate_dots(n,glyph){
  let str = '';
  for (var i = 0; i < n; i++) {
    str+= glyph;
  }
  return str;
}

let clicked = false;

Place.prototype.setup_ancients_circles = function() {
  this.pos_x = this.svg_el.getAttribute("cx");
  this.pos_y = this.svg_el.getAttribute("cy");
  
  this.text = document.createElementNS('http://www.w3.org/2000/svg', 'text');
  
  let order = [];
  for (var ancient in this.ancients) {
    this.ancients_elements[ancient] = document.createElementNS(svgns,"circle");
    this.ancients_elements[ancient].place = this;
    this.ancients_elements[ancient].setAttributeNS(null, "cx", this.pos_x);
    this.ancients_elements[ancient].setAttributeNS(null, "cy", this.pos_y);
    this.ancients_elements[ancient].setAttributeNS(null, "r", '0px');
    this.ancients_elements[ancient].setAttributeNS(null, "ancient",ancient);
    this.ancients_elements[ancient].setAttributeNS(null, "ancient_circle",this.name+'_'+ancient);
    this.ancients_elements[ancient].linked_text = this.text;
    this.ancients_elements[ancient].addEventListener('mouseenter',function(){
      if (clicked != this.place.name) {
        clicked = false;
        let circle_count = document.getElementsByClassName("text_count_circle");
        for (var i in places) {
          for (var ancient in places[i].ancients) {
            event = document.createEvent("HTMLEvents");
            event.initEvent("mouseout", true, true);
            event.e;ventName = "mouseout";
            if (!places[i].ancients_elements[ancient]) break;
            places[i].ancients_elements[ancient].dispatchEvent(event);
          }
          for (var j = 0; j < circle_count.length; j++) {
            if (circle_count[j].getAttribute("place") == this.place.name) continue;
            circle_count[j].style.display = "none";
          }
        }
      }
      let cards = document.getElementsByClassName("paratxt_card");
      let circle_count = document.getElementsByClassName("text_count_circle");
      this.linked_text.style.opacity = '1';
      for (var i in places) {
        for (var j = 0; j < cards.length; j++) {
          if (cards[j].getAttribute("place") == this.place.name) continue;
          cards[j].style.display = "none";
        }
        for (var j = 0; j < cards.length; j++) {
          if (cards[j].getAttribute("place") == this.place.name) continue;
          cards[j].style.display = "none";
        }
        for (var j in places[i].ancients_elements) {
          if (places[i] == this.place) continue;
          places[i].ancients_elements[j].style.opacity = 0.1;
        }
      }
      select_place(this.place);
    });
    this.ancients_elements[ancient].addEventListener('mousedown',function(){
      for (var i in places) {
        for (var ancient in places[i].ancients) {
          event = document.createEvent("HTMLEvents");
          event.initEvent("mouseout", true, true);
          event.e;ventName = "mouseout";
          if (!places[i].ancients_elements[ancient]) break;
          places[i].ancients_elements[ancient].dispatchEvent(event);
        }
      }
      let circle_count = document.getElementsByClassName("text_count_circle");
      let cards = document.getElementsByClassName("paratxt_card");
      this.linked_text.style.opacity = '1';
      for (var i in places) {
        for (var j = 0; j < cards.length; j++) {
          if (cards[j].getAttribute("place") == this.place.name) continue;
          cards[j].style.display = "none";
        }
        for (var j = 0; j < circle_count.length; j++) {
          if (circle_count[j].getAttribute("place") == this.place.name) continue;
          circle_count[j].style.display = "none";
        }
        for (var j in places[i].ancients_elements) {
          if (places[i] == this.place) continue;
          places[i].ancients_elements[j].style.opacity = 0.1;
        }
      }
      select_place(this.place);
      if (clicked == this.place.name) {
        clicked = false;
      } else {
        clicked = this.place.name;
      }
    });
    this.ancients_elements[ancient].addEventListener('mouseout',function(){
      if (clicked) return;
      let cards = document.getElementsByClassName("paratxt_card");
      let circle_count = document.getElementsByClassName("text_count_circle");
      this.linked_text.style.opacity = '0';
      for (var j = 0; j < cards.length; j++) {
        cards[j].style.display = "block";
      }
      for (var j = 0; j < circle_count.length; j++) {
        circle_count[j].style.display = "block";
      }
      for (var i in places) {
        for (var j in places[i].ancients_elements) {
          places[i].ancients_elements[j].style.opacity = 1;
        }
      }
      reset_ancients_global();
      update_ancients_global();
      update_paratxt();
      update_total_ancients();
      document.getElementById("pubplace_focus_info").textContent = "Lieu de publication selectionné ·········································· aucun";
    });
    order.push(this.ancients_elements[ancient]);
  }
  order.reverse();
  for (var i = 0; i < order.length; i ++) {
    svg_container.appendChild(order[i]);
  }
 
  this.text.setAttribute('x',this.pos_x);
  this.text.setAttribute('y',this.pos_y);
  this.text.textContent = alignments[this.name]; // alignments est déclaré dans common/alignments.js
  this.text.style.opacity = '0';

  svg_container.appendChild(this.text);
}

function select_place(place) {
  reset_ancients_global();
  update_ancients_local(place);
  update_paratxt();
  update_total_ancients();
  let text_length = ("Lieu de publication selectionné" + alignments[place.name]).length+2;
  document.getElementById("pubplace_focus_info").textContent = "Lieu de publication selectionné " + generate_dots(80-text_length,'·')+ ' ' + alignments[place.name];
}

Place.prototype.updateSize = function(ratio){
  let previous_size = 0;
  let base = 6;
  for (var ancient in this.ancients) {
    if (this.ancients_elements[ancient] == undefined) continue;
    let size = !this.ancients[ancient] ? 0 : this.ancients[ancient] < base ? base : this.ancients[ancient];
    this.ancients_elements[ancient].setAttributeNS(null, "r", (size+previous_size)*ratio);
    previous_size += size;
  }
  this.text.setAttribute('x',parseInt(this.pos_x)+previous_size*ratio+2);
}

// MAP

let viewBox = {
  x : 80,
  y : 80,
  width : 200,
  height : 200
}

let zoom_level = 1;

function zoomIn(){
    if (zoom_level > 20) return;
    viewBox.x = viewBox.x + viewBox.width / 32;
    viewBox.y = viewBox.y + viewBox.height / 32;
    viewBox.width = viewBox.width / 1.0625;
    viewBox.height = viewBox.height / 1.0625;
    zoom_level++;
    svg_container.setAttributeNS(null,"viewBox",viewBox.x + ' ' + viewBox.y + ' ' + viewBox.width + ' '+ viewBox.height);
}

function zoomOut(){
    if (zoom_level < -15) return;
    viewBox.x = viewBox.x - viewBox.width / 16;
    viewBox.y = viewBox.y - viewBox.height / 16;
    viewBox.width = viewBox.width * 1.0625;
    viewBox.height = viewBox.height * 1.0625;
    zoom_level--;
    svg_container.setAttributeNS(null,"viewBox",viewBox.x + ' ' + viewBox.y + ' ' + viewBox.width + ' '+ viewBox.height);
}

function panning() {
}

let cursor_x = 0;
let cursor_y = 0;

document.addEventListener("mousemove",function(e){
  cursor_x = e.clientX;
  cursor_y = e.clientY;
  //console.log(cursor_x,cursor_y);
});

document.addEventListener("keydown",function(e){
  //zoomIn();
});

// print years

// RESET CARTO

let ancients_global = {
  "ar": 0,
  "pl" : 0,
  "eschl" : 0,
  "sen" : 0,
  "ter" : 0,
  "soph" : 0,
  "eur" : 0,
}

let ancients_names = {
  "ar": "Aristophanes",
  "pl" : "Plautus",
  "eschl" : "Aeschylus",
  "sen" : "Seneca",
  "ter" : "Terentius",
  "soph" : "Sophocles",
  "eur" : "Euripides"
}

function update_ancients_local(place) {
  for (var ancient in place.ancients) {
    if (ancients_global[ancient] == undefined) continue;
    ancients_global[ancient] += place.ancients[ancient];
  }
  for (var ancient in ancients_global) {
    let quantity = generate_dots(Math.floor(ancients_global[ancient]/2+1),'#');
    let text_length = (ancients_names[ancient]+'  '+ancients_global[ancient]).length+quantity.length+1;
    document.getElementById(ancient+"_info").innerHTML = "<span>"+quantity + '</span> ' + ancients_names[ancient] + ' ' + generate_dots(80-text_length,'·') + ' ' +ancients_global[ancient];
  }
}

function update_ancients_global() {
  for (var place in places) {
    for (var ancient in places[place].ancients) {
      if (ancients_global[ancient] == undefined) continue;
      ancients_global[ancient] += places[place].ancients[ancient];
    }
  }
  for (var ancient in ancients_global) {
    let quantity = generate_dots(Math.floor(ancients_global[ancient]/2+1),'#');
    let text_length = (ancients_names[ancient]+'  '+ancients_global[ancient]).length+quantity.length+1;
    document.getElementById(ancient+"_info").innerHTML = "<span>"+quantity + '</span> ' + ancients_names[ancient] + ' ' + generate_dots(80-text_length,'·') + ' ' +ancients_global[ancient];
  }
}

function update_paratxt(){
  let n_prtxt = 0;
    for (var ancient in ancients_global) {
      if (ancients_global[ancient] == undefined) continue;
      n_prtxt += ancients_global[ancient];
    }
  let text_length = ('Paratextes  '+n_prtxt).length;
  document.getElementById("paratxt_info").textContent = 'Paratextes '+generate_dots(80-text_length,'·')+' ' +n_prtxt; 
}

function update_total_ancients(){
  let n_ancients = 0;
    for (var ancient in ancients_global) {
      if (ancients_global[ancient] == undefined || !ancients_global[ancient]) continue;
      n_ancients += 1;
    }
  let text_length = ('Auteurs anciens cités '+n_ancients).length+1;
  document.getElementById("total_ancients_info").textContent = 'Auteurs anciens cités '+generate_dots(80-text_length,'·')+' ' +n_ancients; 
}

function reset_ancients_global() {
  for (var ancient in ancients_global) {
    ancients_global[ancient] = 0;
  }
}

function reset_carto() {
  reset_ancients_global();
  for (var place in places) {
    for (var ancient in places[place].ancients) {
      places[place].ancients[ancient] = 0;
      places[place].updateSize(1);
    }
  }
}

let paratext_section = document.getElementById("paratxt_list");

function create_paratextes_cards(place,year,place_name) {
  for (var ancient in place) {
    for (var title in place[ancient]) {
      let card = document.createElement("div");
      card.setAttribute("file",title_files_table[title]);
      card.addEventListener("mousedown",function(){
        window.open("../detail/?file="+this.getAttribute("file"));
      });
      card.classList.add("paratxt_card");
      card.setAttribute("place",place_name);
      let date_card = document.createElement("p");
      var text_length = ("Date"+year).length+2;
      date_card.textContent = "Date "+ generate_dots(80-text_length,'·') +' '+year;
      card.appendChild(date_card);
      //var br = document.createElement("br");
      //card.appendChild(br);
      let title_card = document.createElement("p");
      text_length = ("Titre "+title).length+2;
      title_card.textContent = "Titre ·"+generate_dots(80-text_length,'·')+' '+title;
      card.appendChild(title_card);
      let author_card = document.createElement("p");
      text_length = ("Author "+Object.keys(place[ancient][title])[0]).length+2;
      author_card.textContent = "Author ·"+generate_dots(80-text_length,'·')+' '+Object.keys(place[ancient][title])[0];
      card.appendChild(author_card);
      let place_card = document.createElement("p");
      text_length = ("Place "+place_name).length+2;
      place_card.textContent = "Place ·"+generate_dots(80-text_length,'·')+' '+place_name;
      card.appendChild(place_card);
      let ancient_card = document.createElement("p");
      text_length = ("Auteur antique "+ancient).length+2;
      ancient_card.textContent = "Auteur antique ·"+generate_dots(80-text_length,'·')+' '+ancient;
      card.appendChild(ancient_card);
      paratext_section.appendChild(card); 
    }
  }
}

function print_years_on_map(years) {
  reset_carto();
  let text_length = ("Période selectionnée" + (years.length > 1 ? ( years[0] + ' - ' + years[years.length-1] ) : years[0])).length+2;
  document.getElementById("period_info").textContent = "Période selectionnée " + generate_dots(80-text_length,'·') +' '+ (years.length > 1 ? ( years[0] + ' - ' + years[years.length-1] ) : years[0]); 
  text_length = ("Années" +years.length).length + 2;
  document.getElementById("years_info").textContent = "Années " + generate_dots(80-text_length,'·') + ' ' +  years.length;
  let pubplaces = {};
  let pubplaces_number = 0;
  while (paratext_section.children.length) paratext_section.removeChild(paratext_section.children[0]);
  while (years.length) {
    let year = years.pop();
    let places_data = date_place_ancient_author[year];
    if (places_data == undefined) continue;
    for (var place in places_data) {
      if (!pubplaces[place]) {
        pubplaces[place] = true;
        pubplaces_number++;
      }
      create_paratextes_cards(places_data[place],year,place);
      let total_ancients = get_all_child_total_from_data(places_data[place]);
      for (var ancient in places_data[place]) {
        if (places[place] == undefined) continue;
        places[place].ancients[ancient] += total_ancients[ancient];
        places[place].updateSize(0.15);
      }
    }
  }
  text_length = ("Lieux de publication" + pubplaces_number).length+2;
  document.getElementById("pubplace_info").textContent = "Lieux de publication " + generate_dots(80 - text_length,'·') + ' ' + pubplaces_number;
  if (clicked) {
    update_ancients_local(places[clicked]);
      let cards = document.getElementsByClassName("paratxt_card");

        for (var j = 0; j < cards.length; j++) {
          if (cards[j].getAttribute("place") == clicked) continue;
          cards[j].style.display = "none";
        }
  } else {
    update_ancients_global();
  }
  update_total_ancients();
  update_paratxt();
}

// Pubplaces SVG

let pubplaces_svg_elements = document.querySelectorAll("[pubplace]");
console.log(pubplaces_svg_elements);
for (var i = 0; i < pubplaces_svg_elements.length; i++) {
  new Place(pubplaces_svg_elements[i]);
}

// PARATEXT DETAIL

// INIT

window.onload = function () {
build_chrono_ui(1460,1640,date_place_ancient_author);
chrono_thumb_B_x = chrono_thumb_B_max_x - 8;
}
zoomOut();

document.getElementById("pubplace_focus_info").textContent = "Lieu de publication selectionné ·········································· aucun";
