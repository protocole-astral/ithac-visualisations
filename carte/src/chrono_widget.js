// RANGE VISU FUNCTIONS

// mouse
let mouse_x = 0;
let mouse_y = 0;

// CHRONO UI
let chrono_thumb_A_clicked = false;
let chrono_thumb_A_x = 0;
let chrono_thumb_A_min_x = 0;
let chrono_thumb_A_max_x = 0;
let chrono_thumb_B_clicked = false;
let chrono_thumb_B_x = 0;
let chrono_thumb_B_min_x = 0;
let chrono_thumb_B_max_x = 0;

const chrono_thumb_A = document.createElement("div");
chrono_thumb_A.classList.add("chrono_thumb");
chrono_thumb_A.classList.add("chrono_thumb_A");
const chrono_thumb_B = document.createElement("div");
chrono_thumb_B.classList.add("chrono_thumb");
chrono_thumb_B.classList.add("chrono_thumb_B");

chrono_thumb_A.addEventListener("mousedown",function(){
  event.preventDefault();
  chrono_thumb_A_clicked = true;
});

chrono_thumb_B.addEventListener("mousedown",function(){
  event.preventDefault();
  chrono_thumb_B_clicked = true;
});

let frame = 0;

function chrono_thumbs_handling(chrono_min_date,chrono_max_date){
  // UI
  chrono_thumb_A_min_x = chrono_thumb_A.parentNode.getBoundingClientRect().x;
  chrono_thumb_A_max_x = chrono_thumb_A.parentNode.getBoundingClientRect().width;
  chrono_thumb_B_min_x = chrono_thumb_A.parentNode.getBoundingClientRect().x;
  chrono_thumb_B_max_x = chrono_thumb_A.parentNode.getBoundingClientRect().width;
  if (chrono_thumb_A_clicked) {
    pos_x = mouse_x - chrono_thumb_A_min_x;
    chrono_thumb_A_x = pos_x > 0 && pos_x < chrono_thumb_A_max_x ? pos_x : chrono_thumb_A_x;
    chrono_thumb_B_x = chrono_thumb_A_x > chrono_thumb_B_x ? chrono_thumb_A_x : chrono_thumb_B_x;
  }
  if (chrono_thumb_B_clicked) {
    pos_x = mouse_x - chrono_thumb_B_min_x;
    chrono_thumb_B_x = pos_x > 0 && pos_x < chrono_thumb_B_max_x-8 ? pos_x : chrono_thumb_B_x;
    chrono_thumb_A_x = chrono_thumb_B_x < chrono_thumb_A_x ? chrono_thumb_B_x : chrono_thumb_A_x;
  }
  chrono_thumb_A.style.marginLeft = "calc("+chrono_thumb_A_x+"px - 0.5rem)";
  chrono_thumb_B.style.marginLeft = "calc("+(chrono_thumb_B_x - chrono_thumb_A_x)+"px - 1rem)";
  // DATES
  if (chrono_thumb_A_clicked || chrono_thumb_B_clicked) {
    let chrono_min_range = Math.floor(chrono_min_date + (chrono_thumb_A_x / (chrono_thumb_B_max_x / (chrono_max_date - chrono_min_date))));
    let chrono_max_range = Math.floor(chrono_min_date + (chrono_thumb_B_x / (chrono_thumb_B_max_x / (chrono_max_date - chrono_min_date))));
    let textcount_circles = document.getElementsByClassName("text_count_circle");
    for (var i = 0; i < textcount_circles.length; i++) {
      textcount_circles[i].style.borderColor = "var(--color-c)";
    }
    for (var i = chrono_min_date; i < chrono_min_range; i++) {
      let textcount = document.getElementsByClassName("text_count_"+i);
      if (textcount) {
        for (var j = 0; j < textcount.length; j++) {
          textcount[j].style.borderColor = "var(--color-d)";
        }
      }
    }
    for (var i = chrono_max_range+1; i < chrono_max_date; i++) {
      let textcount = document.getElementsByClassName("text_count_"+i);
      if (textcount) {
        for (var j = 0; j < textcount.length; j++) {
          textcount[j].style.borderColor = "var(--color-d)";
        }
      }
    }
    print_years_on_map(make_list_from_range(chrono_min_range,chrono_max_range));
  }
  frame++;
  setTimeout(function(){chrono_thumbs_handling(chrono_min_date,chrono_max_date)},17*2);
};
/*
function build_chrono_ui_section(){
  let section = document.getElementById("chrono_ui");
  section.id = "chrono_ui";
  document.body.appendChild(section);
}*/
//  <section id="chrono_ui">
//    <h1 id="title">Chronologie des paratextes</h1><br><br><br><br>
//    <p id="years">Années :</p>
//    <p id="n_pubplaces">Lieux de publication :</p>
//    <p id="n_paratexts">Nombre de paratextes :</p><br>
//  </section>

function build_chrono_ui(chrono_min_date,chrono_max_date,data_per_date){
  let chrono_bar = document.createElement("div");
  chrono_bar.classList.add("chrono_bar");
  chrono_bar.appendChild(chrono_thumb_A);
  chrono_bar.appendChild(chrono_thumb_B);
  let chrono_graduation = document.createElement("div");
  chrono_graduation.classList.add('chrono_graduation');
  let chrono_texts = document.createElement("div");
  chrono_texts.classList.add('chrono_texts');
  chrono_graduation.classList.add('chrono_graduation');
  chrono_ui_section = document.getElementById("chrono_ui");
  map_side_section = document.getElementById("map_side");
  histogram_section = document.getElementById("histogram_section");

  // Infos
  let info_ancients = document.createElement("p");
  info_ancients.id = "info_ancients";
  info_ancients.style.marginBottom = "1rem";
  info_ancients.textContent = "------------------------------------ INFOS -------------------------------------";
  chrono_ui_section.appendChild(info_ancients);

  let period_info = document.createElement("p");
  period_info.id = "period_info";
  period_info.textContent = "Période selectionnée :";
  chrono_ui_section.appendChild(period_info);
  
  let years_info = document.createElement("p");
  years_info.id = "years_info";
  years_info.textContent = "Années :";
  chrono_ui_section.appendChild(years_info);
  
  let pubplace_info = document.createElement("p");
  pubplace_info.id = "pubplace_info";
  pubplace_info.textContent = "Lieux de publication :";
  chrono_ui_section.appendChild(pubplace_info);
  
  let pubplace_focus_info = document.createElement("p");
  pubplace_focus_info.id = "pubplace_focus_info";
  pubplace_focus_info.textContent = "Lieu de publication selectionné :";
  chrono_ui_section.appendChild(pubplace_focus_info);
  
  let total_ancients_info = document.createElement("p");
  total_ancients_info.id = "total_ancients_info";
  total_ancients_info.textContent = "Auteurs anciens cités :";
  chrono_ui_section.appendChild(total_ancients_info);
  
  let detail_ancients = document.createElement("p");
  detail_ancients.style.marginTop = "1rem";
  detail_ancients.style.marginBottom = "1rem";
  detail_ancients.id = "detail_ancients";
  detail_ancients.textContent = "------------------------------------ Détail ------------------------------------";
  chrono_ui_section.appendChild(detail_ancients);
 
  let ar_info = document.createElement("p");
  ar_info.id = "ar_info";
  ar_info.textContent = "Aristophanes :";
  chrono_ui_section.appendChild(ar_info);
 
  let pl_info = document.createElement("p");
  pl_info.id = "pl_info";
  pl_info.textContent = "Plautus :";
  chrono_ui_section.appendChild(pl_info);
 
  let eschl_info = document.createElement("p");
  eschl_info.id = "eschl_info";
  eschl_info.textContent = "Aeschylus :";
  chrono_ui_section.appendChild(eschl_info);
 
  let sen_info = document.createElement("p");
  sen_info.id = "sen_info";
  sen_info.textContent = "Seneca :";
  chrono_ui_section.appendChild(sen_info);
 
  let ter_info = document.createElement("p");
  ter_info.id = "ter_info";
  ter_info.textContent = "Terentius :";
  chrono_ui_section.appendChild(ter_info);
 
  let soph_info = document.createElement("p");
  soph_info.id = "soph_info";
  soph_info.textContent = "Sophocles :";
  chrono_ui_section.appendChild(soph_info);
 
  let eur_info = document.createElement("p");
  eur_info.id = "eur_info";
  eur_info.textContent = "Euripides :";
  chrono_ui_section.appendChild(eur_info);
  
  let total_ancients = document.createElement("p");
  total_ancients.style.marginTop = "1rem";
  total_ancients.style.marginBottom = "1rem";
  total_ancients.id = "total_ancients";
  total_ancients.textContent = "------------------------------------ Total -------------------------------------";
  chrono_ui_section.appendChild(total_ancients);
  
  let paratxt_info = document.createElement("p");
  paratxt_info.id = "paratxt_info";
  paratxt_info.textContent = "Paratextes :";
  chrono_ui_section.appendChild(paratxt_info);
  
 
  // HISTOGRAM

  histogram_section.appendChild(chrono_texts);
  histogram_section.appendChild(chrono_graduation);
  histogram_section.appendChild(chrono_bar);
  chrono_thumbs_handling(chrono_min_date,chrono_max_date);
  for (var i = 0 ; i < chrono_max_date - chrono_min_date-1; i++) {
    let text_count = document.createElement('div');
    text_count.classList.add("text_count");
    for (var j in data_per_date[chrono_min_date+i]) {
      let text_count_circle = document.createElement('div');
      text_count_circle.classList.add("text_count_circle");
      text_count_circle.classList.add("text_count_"+(chrono_min_date+i));
      let circle_size = 0;
      circle_size = data_per_date[chrono_min_date+i][j];
      let size = get_all_child_total_from_data({circle_size}).circle_size;
      text_count_circle.setAttribute("quantity",size);
      text_count_circle.setAttribute("place",j);
      text_count_circle.addEventListener("mouseenter",function(){
        text_count_circle.style.overflow = "visible";
         // alignments est déclaré dans common/alignments.js
        text_count_circle.textContent = '_'+alignments[this.getAttribute("place")]+':'+this.getAttribute("quantity");
      });
      text_count_circle.addEventListener("mouseout",function(){
        text_count_circle.style.overflow = "hidden";
        text_count_circle.textContent = "";
      });
      text_count_circle.style.width = '1vw';
      text_count_circle.style.height = (size*5+8) + 'px';
      if (circle_size) text_count.appendChild(text_count_circle);
    }
    text_count.style.width = (chrono_thumb_A_max_x / (chrono_max_date - chrono_min_date)) + 'px';
    chrono_texts.appendChild(text_count);
    if (Number.isInteger(i / 5)) {
      let mark = document.createElement('div');
      mark.classList.add('chrono_mark');
      if (Number.isInteger(i / 20)) {
        let content = document.createElement('p');
        content.textContent = ''+(chrono_min_date + i);
        mark.appendChild(content);
        mark.style.height = '2.5rem';
      } else {
        mark.style.marginTop = '1.5rem';
        mark.style.height = '1rem';
      }
      mark.style.width = (chrono_thumb_A_max_x / (chrono_max_date - chrono_min_date))*5 + 'px';
      chrono_graduation.appendChild(mark);
    }
  }
/*
Aristote : 86

Platon : 59

Eschl : 26

Sénèque : 53

Ter : 56

Sophocle : 63

Euripide : 85
*/

  print_years_on_map(make_list_from_range(chrono_min_date,chrono_max_date));
}

document.addEventListener("mousemove",function(e){
  mouse_x = e.clientX;
  mouse_y = e.clientY;
});
document.addEventListener("mouseup",function(){
  chrono_thumb_A_clicked = false;
  chrono_thumb_B_clicked = false;
});
