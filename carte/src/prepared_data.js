let date_place_ancient_author = {
  "1472": {
    "uenetia": {
      "pl": {
        "De uita Comoediisque Plauti excerpta quaedam ex Auctoribus grauissimis. ": {
          "merula_georgius": 1
        },
        "Reuerendissimo in Christo patri et domino Iacobo Zeno pontifici patauino Georgius Alexandrinus Salutem plurimam dicit.": {
          "merula_georgius": 1
        }
      }
    }
  },
  "1488": {
    "lutetia": {
      "sen": {
        "Carolus Fernandus lectorem alloquitur ": {
          "fernandus_carolus": 1
        },
        "Carolus Fernandus salutem plurimam dicit Petro Cohardo aduocato regio ": {
          "fernandus_carolus": 1
        }
      }
    }
  },
  "1491": {
    "lugdunum": {
      "sen": {
        "Gellii Bernardini Marmitae ad illustrem D. Guielmum de Rupeforti magnum Cancellarium Franciae in tragoedias Senecae interpretatio ": {
          "marmita_gellius": 1
        }
      }
    }
  },
  "1493": {
    "lugdunum": {
      "ter": {
        "Guido Iuuenalis Germano de Ganeio uiro senatorio bonarumque litterarum amantissimo Salutem Plurimam Dat": {
          "iuuenalis_guido": 1
        },
        "Guido Iuuenalis Martino Guerrando iuris utriusque et pontificii et caesarii peritissimo atque uenerandi in Christo patris praesulis Cenomanensis secretario dignissimo S.P.D.": {
          "iuuenalis_guido": 1
        },
        "Guido Iuuenalis Michaeli Burello sacrosanctae theologiae professori perquam erudito S.P.D.": {
          "iuuenalis_guido": 1
        },
        "Guido Iuuenalis Nicholao de Capella uiro multis laudibus efferendo S.P.D.": {
          "iuuenalis_guido": 1
        },
        "Guido Iuuenalis Nicholaum Peletarium iuris legum consultissimum atque in omni doctrina conspicuum multa salute iubet impartiri": {
          "iuuenalis_guido": 1
        },
        "Guidonis Iuuenalis natione Cenomani epigramma super causa operis suscepti.": {
          "iuuenalis_guido": 1
        },
        "Io&lt;docus&gt; Ba&lt;dius&gt; Ascensius lectoribus salutem dicit": {
          "ascensius_iodocus": 1
        },
        "Ioannis Egidii Nuceriensis epigramma ad iuuenes": {
          "egidius_ioannes": 1
        }
      }
    }
  },
  "1496": {
    "argentorum": {
      "ter": {
        "Declaratio figurae": {
          "gruninger_ioannes": 1
        },
        "Declaratio huius figurae in Comediam Terentii quae in ordine quarta est atque Adelphos vocitata": {
          "gruninger_ioannes": 1
        },
        "Declaratio super quintam Therentii comoediam in figuram hanc argumentum declarans lucidius": {
          "gruninger_ioannes": 1
        },
        "Figurae huius quae secundae comediae Eunuchi principalis est et argumenti declaratio clarior": {
          "gruninger_ioannes": 1
        },
        "Figurae sequentis declaratio in sextam et ultimam Threntii Comediam Ecyram dictam introductoria": {
          "gruninger_ioannes": 1
        },
        "Figure declaratio. Argumenti lucidior secundum hanc figuram declaratio": {
          "gruninger_ioannes": 1
        }
      }
    }
  },
  "1498": {
    "uenetia": {
      "ar": {
        "Aldus Manutius Romanus, Danieli Clario Parmensi, s.p.d": {
          "manutius_aldus": 1
        },
        "Μάρκος Μουσοῦρος ὁ Κρὴς τοῖς ἐντευξομένοις εὖ πράττειν": {
          "musurus_marcus": 1
        },
        "Σκιπίωνου Καρτερομάχου τοῦ πιστωριέως": {
          "carteromacus_scipio": 1
        }
      }
    }
  },
  "1499": {
    "argentorum": {
      "ter": {
        "Ad lectorem Epigramma eiusdem Philomusi": {
          "locher_iacobus": 1
        },
        "Iacobus Locher Philomusus poeta et orator laureatus Iohanni Grüninger librorum impressori ac ciui Argen. Prudenti et honesto. In laudem Therentii S.P.D.": {
          "locher_iacobus": 1
        },
        "In laudem Terentianae lectionis, Epigramma Heinrici Bebelii Iustingensis": {
          "bebelius_heinricus": 1
        },
        "Vita Terentii, excerpta de dictis D. F. Petrarca": {
          "petrarca_franciscus": 1
        }
      }
    },
    "mediolanum": {
      "pl": {
        "Andreae Maronis Brisiensis epigramma": {
          "maro_andrea": 1
        },
        "Dominicus Palladius Soranus ad Bernardum Saracenum Venetum Plauti interpretem peritissimum.": {
          "palladius_dominicus": 1
        },
        "Idem": {
          "palladius_dominicus": 1
        },
        "Ioannes Petrus Valla inclyto utriusque iuris doctori Scaramuzae Trivultio Mediolanensi patricio primario S&lt;alutem&gt; D&lt;icit&gt; eternam": {
          "ualla_ioannes": 1
        },
        "Iohannis Petri Vallae in plautinas comoedias commentationes.": {
          "undefined": 1
        },
        "Pauli Canalis patricii Veneti carmen": {
          "canalis_paulus": 1
        },
        "Reuerendissimo in Christo patri Francisco Marcello Pontifici Tragurino Bernardus Saracenus Venetus S. P. D.": {
          "saracenus_bernardinus": 1
        }
      }
    }
  },
  "1500": {
    "mediolanum": {
      "pl": {
        "": {
          "calcaterra_antonius": 2,
          "pius_ioannes": 6,
          "salandus_ioannes": 1
        },
        "Ioannis Alberti Marliani mediolanensis patritii": {
          "marlianus_ioannes": 1
        },
        "Philippus Beroaldus lectori salutem dicit.": {
          "beroaldus_philippus": 1
        },
        "Plauti uita": {
          "pius_ioannes": 1
        },
        "Sebastiani Ducii": {
          "ducius_sebastianus": 1
        }
      }
    }
  },
  "1501": {
    "parma": {
      "ar": {
        "Antonii Zandemariae equitis hyersolymitani Parmensis.ad lectorem ": {
          "passius_franciscus": 1
        },
        "Argumentum": {
          "passius_franciscus": 1
        },
        "Francisci Passii Carpensis Plutus Comoedia incipit Prologus ": {
          "passius_franciscus": 1
        },
        "Illustrissimo ac eruditissimo Alberto Pio principi Carpensi, ingeniorum fautori, Franciscus": {
          "passius_franciscus": 1
        },
        "Tranquilli Molossi Cremonensis Apologeticon.": {
          "passius_franciscus": 1
        }
      }
    }
  },
  "1502": {
    "uenetia": {
      "soph": {
        "Aldus Romanus Ioanni Lascari viro praeclaro ac doctissimo s.d.": {
          "manutius_aldus": 1
        }
      }
    }
  },
  "1503": {
    "uenetia": {
      "eur": {
        " Aldus Romanus Demetrio Chalcondylae viro clarissimo salutem plurimam dat.": {
          "manutius_aldus": 1
        }
      }
    }
  },
  "1504": {/*
    "identifiant_place": {
      "identifiant_person": {
        "titre du paratexte": {
          "identifiant_auteur": 1
        }
      }
    },*/
    "londinium": {
      "ter": {
        " Quid sit poeta et quanta eius dignitas. Caput I.": {
          "ascensius_iodocus": 1
        },
        "Ad studiosam Britanniae maioris quae nunc Anglia dicitur iuuentutem hexastichon Ascensianum": {
          "ascensius_iodocus": 1
        },
        "De decoro et primo personarum. Capitulum XX.": {
          "ascensius_iodocus": 1
        },
        "De ludis romanis et festivitatibus in quibus agi fueuerunt comoediae. Capitulum XVII.": {
          "ascensius_iodocus": 1
        },
        "De quattuor causis huius operis. Capitulum XXIIII. ": {
          "ascensius_iodocus": 1
        },
        "De actibus et eorum distinctione in comoediis. Capitulum XIX. ": {
          "ascensius_iodocus": 1
        },
        "De Comoedia Antiqua. Capitulum VI.": {
          "ascensius_iodocus": 1
        },
        "De decoro totius operis. Capitulum XXIII. ": {
          "ascensius_iodocus": 1
        },
        "De Instrumentis et prosceniis dramatum precipue comediarum. Capitulum VII.": {
          "ascensius_iodocus": 1
        },
        "De membris comoediarum. Capitulum XV.": {
          "ascensius_iodocus": 1
        },
        "De origine et inuentione satyrarum tragoediarum et comediarum. Capitulum V.": {
          "ascensius_iodocus": 1
        },
        "De partibus comoediarum et primum de tribus non princibalibus. Capitulum XVI. ": {
          "ascensius_iodocus": 1
        },
        "De personis et earum indumentis et coloribus. Capitulum X.": {
          "ascensius_iodocus": 1
        },
        "De prologis et eorum speciebus. Capitulum XVII. ": {
          "ascensius_iodocus": 1
        },
        "De prosceniarum ornatu et instructione. Capitulum XI.": {
          "ascensius_iodocus": 1
        },
        "De qualitatibus comediarum. Capitulum XIIII.": {
          "ascensius_iodocus": 1
        },
        "De rerum decoro. Capitulum XXI.": {
          "ascensius_iodocus": 1
        },
        "De scenis et prosceniis. Capitulum IX.": {
          "ascensius_iodocus": 1
        },
        "De speciebus Comediarum. Capitulum XIII.": {
          "ascensius_iodocus": 1
        },
        "De Terentii vita. Capitulum XXV. ": {
          "ascensius_iodocus": 1
        },
        "De theatro et eius constructoribus. Capitulum VIII.": {
          "ascensius_iodocus": 1
        },
        "De tribus partibus principalibus in comoedia. Capitulum XVIII. ": {
          "ascensius_iodocus": 1
        },
        "De triplici carminum stilo et pedum ludentium ornamentis. Capitulum III.": {
          "ascensius_iodocus": 1
        },
        "De verborum decoro. Capitulum XXII.": {
          "ascensius_iodocus": 1
        },
        "Descriptiones et differentiae tragoedia et comoediae. Capitulum IV.": {
          "ascensius_iodocus": 1
        },
        "Jodoci Badii Ascensii familiaria in Terentium prenotamenta.": {
          "ascensius_iodocus": 1
        },
        "Jodoci Badii Ascensii familiaria in Terentium prenotamenta. De forma operibus et laude Terentii. Capi(tulum) XXVI. ": {
          "ascensius_iodocus": 1
        },
        "Jodocus Badius Ascensius Magistro Herveo Besino, iureconsulto, litteris eruditissimo amicorumque primario, salutem plurimam dicit.": {
          "ascensius_iodocus": 1
        },
        "Quotuplicia sint poetarum scripta": {
          "ascensius_iodocus": 1
        }
      }
    }
  },
  "1506": {
    "brixia": {
      "pl": {
        "Ioannes Britannicus Salutem Plurimam Alouisio Dardano Veneto.": {
          "britannicus_ioannes": 1
        },
        "Pylades Buccardus Salutem Plurimam Alouisio Dardano Veneto.": {
          "pylades_buccardus": 1
        }
      }
    },
    "florentia": {
      "sen": {
        "Benedicti Philologi Florentini praefatio, super L. Anneae Senecae Tragoediis, ad Dominicum Beneuenium Diui Laurentii Canonicum": {
          "riccardinus_benedictus": 1
        },
        "De partibus tragoediae": {
          "riccardinus_benedictus": 1
        },
        "De tragoedia": {
          "riccardinus_benedictus": 1
        }
      }
    },/*
    "identifiant_place": {
      "identifiant_person": {
        " Illustrissimo ac serenissimo principi Francisco Valesio Valesiorum duci et angolismorum comiti D. suo metuendissimo Franciscus Tissardus Ambaciaeus V. juris doctor S.P.D.": {
          "identifiant_auteur": 1
        },
        "Alcestidis Euripidis Narratio": {
          "identifiant_auteur": 1
        },
        "Hippolytis Euripidis Narratio": {
          "identifiant_auteur": 1
        },
        "Medea Euripidis Narratio": {
          "identifiant_auteur": 1
        }
      }
    },*/
    "lutetia": {
      "eur": {
        "&lt;l&gt;AD R. P. GUILIELMUM ARCHIEPISCOPUM&lt;/l&gt; &lt;l&gt;Cantuariensem, Erasmi carmen Iambicum trimetrum.&lt;/l&gt;": {
          "erasmus_desiderius": 1
        },
        "Argumentum Hecubae per eundem Erasmum": {
          "erasmus_desiderius": 1
        },
        "Argumentum Iphigeniae in Aulide": {
          "erasmus_desiderius": 1
        },
        "Erasmus Lectori S.P.D.": {
          "erasmus_desiderius": 1
        },
        "Reuerendo in Christo patri Guilielmo Archiepiscopo Cantuariensi primati Angliae Erasmus Roterdamus.S.P.D.": {
          "erasmus_desiderius": 1
        }
      }
    },
    "parma": {
      "eur": {
        " Georgus Anselmus nepos Tranquillo Molosso Cremonensis salutem dat. ": {
          "anselmus_georgius": 1
        }
      }
    }
  },
  "1507": {
    "uenetia": {
      "eur": {
        "Ad lectorem": {
          "erasmus_desiderius": 1
        },
        "Guilielmo Archiepiscopo Cantuariensi Erasmus S.P.D.": {
          "erasmus_desiderius": 1
        }
      }
    }
  },
  "1508": {
    "argentorum": {
      "pl": {
        "Clarissimo ac ornatissimo uiro domino Ioanni Dynchin, iurium interpreti dignissimo Reuerendissimique in Christo patris ac illustrissimi principis et domini domini Iacobi Episcopi Treuirensis Cancellario bene merentissimo Ioannes Adelphus Mulingus Argentinensis sese commendat.": {
          "mulingus_ioannes": 1
        }
      }
    }
  },
  "1511": {
    "lutetia": {
      "sen": {
        "Aegidius Maseriensis ad dominum Nicolaum de Wattenwill Bernensem iambicum senarium carmen": {
          "maserius_aegidius": 1
        },
        "Aegidius Maseriensis Iacobo de Molendino perspicacissimo sacrae theologiae professori uigilantissimoque Gymnasii Bonorum Puerorum pastori, salutem": {
          "maserius_aegidius": 1
        },
        "Bonorum Puerorum Chorus": {
          "maserius_aegidius": 1
        },
        "De Pomponio autem Secundo et Seneca Tragico iudicium Terentianus faciens sic scriptum reliquit": {
          "maserius_aegidius": 1
        },
        "Petrus de Stainuilla in libri commendationem": {
          "stainuilla_petrus": 1
        },
        "Senecarum uita ex Crinito": {
          "crinitus_petrus": 1
        },
        "Si quid lectores optimi seminotatum aut inuersum percipiatis aequo ferte animo ; pauca enim sunt, ut reor, quae legentem si mediocriter sapiat remorentur.": {
          "maserius_aegidius": 1
        },
        "Tragoediae diffinitio": {
          "maserius_aegidius": 1
        }
      }
    },
    "uenetia": {
      "pl": {
        "Obvio cuique bonae frugi Salutem": {
          "soardus_lazarus": 1
        }
      }
    }
  },
  "1512": {
    "uenetia": {
      "pl": {
        "De comoedia et eius partibus": {
          "charpentarius_symon": 1
        },
        "Symon Charpentarius Parrhisiensis P. Fausto Andrelino foroliuiensi, poeta laureato, Regio necnon Regineo clarissimoque oratori, foelicitatem": {
          "charpentarius_symon": 1
        },
        "Symonis Charpentarii Parrhisii in plautinas viginti Comoedias praelibatio seu praefatio cum poetae vita necnon carminis iambici et Comoediarum ratione, aliis quibusdam annexis quae hic adnodatu dignissima visa sunt et Comoediarum lectoribus atque auditoribus profutura.": {
          "charpentarius_symon": 1
        }
      }
    }
  },
  "1514": {
    "florentia": {
      "pl": {
        "Laurentio Medici Florentinae Reipublicae Optimatum clarissimo, Nicolaus Angelius Bucinensis. S.": {
          "angelius_nicolaus": 1
        }
      }
    },
    "lutetia": {
      "sen": {
        "[Tragica farrago]": {
          "gaietanus_daniel": 1
        },
        "Aegidii Maserii ad Stephanum Antoniumque, lapitheos fratres iambicum carmen ": {
          "maserius_aegidius": 1
        },
        "Aegidii Maserii de uariis tragici praesertim carminis metris : primum De iambico Regula": {
          "maserius_aegidius": 1
        },
        "Danielis Apologia": {
          "gaietanus_daniel": 1
        },
        "Generoso admodum uiro raroque sapientis eloquentiae lepore decorato D. Ioanni Landano&nbsp;; Landae Bougoniique in dioecesi Nannetensi domino humanissimo&nbsp;; et Guilielmi Landani uulgo de la Lande procuratoris olim Britanniae generalis primogenito heredique dignissimo, Iodocus Badius Ascensius salutem dicit": {
          "ascensius_iodocus": 1
        },
        "Polydori Comitis Cabaliati in defensionem Caietani Cremonensis, praeceptoris sui ": {
          "cabaliatus_polydorus": 1
        }
      }
    }
  },
  "1515": {
    "florentia": {
      "ar": {
        "Bernardus Iunta, nobili patritio domino Francisco Accolto electo episcopo anconitano. S.P.D. ": {
          "iunta_bernardus": 1
        }
      }
    }
  },
  "1516": {
    "florentia": {
      "ar": {
        "Bernardus Iunta Lectori. S. ": {
          "iunta_bernardus": 1
        },
        "Bernardus Iunta, nobili patritio domino Francisco Accolto electo episcopo anconitano. S.P.D. ": {
          "iunta_bernardus": 1
        }
      }
    }
  },
  "1517": {
    "haganoa": {
      "ar": {
        "Petrus Mosellanus Protegensis Ioanni Caesario Iuliacensi medico utriusque literaturae doctissimo s.d.p.": {
          "mosellanus_petrus": 1
        }
      }
    },
    "uenetia": {
      "sen": {
        "Dimensiones tragoediarum Senecae per eundem Hieronymum Avantium.": {
          "auantius_hieronymus": 1
        },
        "Hieronymus Avantius Veronensis Latino iuuenali protonotario apostolico romano patricio ornatissimo salutem plurimam dicit": {
          "auantius_hieronymus": 1
        }
      }
    }
  },
  "1518": {
    "alost": {
      "ar": {
        "Theodoricus Martinus Alostensis Studiosae Lovaniensis Academiae Juventuti S.D. ": {
          "martinus_theodoricus": 1
        }
      }
    },
    "uenetia": {
      "eschl": {
        "Franciscus Asulanus Lectori": {
          "asulanus_franciscus": 1
        }
      }
    }
  },
  "1521": {
    "uitteberga": {
      "ar": {
        "Imaginibus ac eruditione claro, D. Nicolao Amsdorffo, Theologo, Phil. Mel. s.d.": {
          "melanchtonus_philippus": 1
        }
      }
    }
  },
  "1522": {
    "florentia": {
      "soph": {
        "Antonius Francinus Varchiensis, Ioanni Baptistae Egnatio Veneto Salutem.": {
          "francinus_antonius": 1
        }
      }
    },
    "uenetia": {
      "pl": {
        "Francisci Asulani in recognitionem M. Accii Plauti Epistola ad Nicolaum Sconbergum Archiepiscopum Campanum": {
          "asulanus_franciscus": 1
        },
        "Plauti uita ex Petro Crinito de Poetis Latinis.": {
          "crinints_petrus": 1
        }
      }
    }
  },
  "1524": {
    "basilea": {
      "ar": {
        "Ioannes Frobenius τοῖς φιλέλλησιν εὖ πράττειν ": {
          "frobenius_ioannes": 1
        }
      }
    }
  },
  "1525": {
    "florentia": {
      "ar": {
        "Benedicto Accoltae Archiepiscopo Rauennati ac cle. VII pont. Maximi a secretis poenitiss. Antonius Fracinus Varchiensis. s.": {
          "francinus_antonius": 1
        },
        "Benedicto Accoltae Archiepiscopo Rauennati ac cle. VII pont. Maximi a secretis poenitiss. Antonius Fracinus Varchiensis. s. ": {
          "iunta_bernardus": 1
        },
        "Σεβαστιάνου τοῦ δουκκίου ἐπίγραμμα ": {
          "iunta_bernardus": 1,
          "undefined": 1
        }
      }
    }
  },
  "1528": {
    "lutetia": {
      "ar": {
        "Ιωάννης Χεράδαμος ἐλλογιμωτάτῳ Νικόλεῳ Βηράλδῳ εὖ πράττειν.": {
          "cheradamus_ioannes": 1
        },
        "Ιωάννης Χεράδαμος ἐλλογιμωτάτῳ Πέτρῳ Δανησίῳ εὖ πράττειν. ": {
          "cheradamus_ioannes": 1
        },
        "Ιωάννης Χεράδαμος ἐλλογιμωτάτῳ' Ἀυθονίῳ Λαπιθέῳ εὖ πράττειν. ": {
          "cheradamus_ioannes": 1
        },
        "Ιωάννης Χεράδαμος εὑβουλοτάτῳ Ἰωάννῃ βιολλείῳ εὖ πράττειν. ": {
          "cheradamus_ioannes": 1
        },
        "Ιωάννης Χεράδαμος ὁμιλητικοτάτῳ Γουιλλέλμῳ Κουίνῳ εὖ πράττειν. ": {
          "cheradamus_ioannes": 1
        },
        "Ιωάννης Χεράδαμος σοφωτάτῳ τῶν ἱατρῶν Πωάύνῃ Ρουηλλίῳ εὖ πράττειν. ": {
          "cheradamus_ioannes": 1
        },
        "Ιωάννης Χεράδαμος Τωάννῃ Κληρικῷ τῷ τῆς Ἀγγλείας ῥήτορι εὖ πράττειν.": {
          "cheradamus_ioannes": 1
        },
        "Ιωάννης Χεράδαμος φιλανθρωποτάτῳ νέῳ Θωμᾷ Οὐιντήρῳ εὖ πράττειν.": {
          "cheradamus_ioannes": 1
        },
        "Ιωάννης Χεράδαμος χρησιμωτάτῳ γυμναστῇ Τωάννῃ Ταρτάσσῳ εὖ πράττειν. ": {
          "cheradamus_ioannes": 1
        }
      }
    }
  },
  "1530": {
    "louanium": {
      "ter": {
        "Ad Louanienses Tyrunculos": {
          "barlandus_adrianus": 1
        },
        "Erudito uiro Ioanni Feuyno Furnio, Iuris utriusque Doctori, Scholasteri et Canonico S. Donatiani apud Brugenses Adrianus Barlandus S. P. D.": {
          "barlandus_adrianus": 1
        },
        "Haec habui, Feuyne ornatissime…": {
          "barlandus_adrianus": 1
        },
        "Studioso lectori": {
          "barlandus_adrianus": 1
        }
      }
    }
  },
  "1531": {
    "nuremberga": {
      "ar": {
        "Ad lectorem": {
          "uenatorius_thomas": 1
        },
        "Clarissimo ac ornatissimo uiro Seuerino Bonero, Burgrauio, Zuppario, magnoque apud Cracouiam regni Poloniae Procuratori, Byeczensi et Rapsteinensi Praesidi etc. Thomas Venatorius S. P. D.": {
          "uenatorius_thomas": 1
        }
      }
    }
  },
  "1532": {
    "basilea": {
      "ar": {
        "And. Cratander Lectori S.": {
          "cratander_andreas": 1
        },
        "Simon Grynaeus juveni studioso s.": {
          "grynaeus_simon": 1
        }
      },
      "ter": {
        "Des. Erasmus Roterodamus de metris": {
          "desiderius_erasmus": 1
        },
        "Des. Erasmus Roterodamus Ioanni et Stanislao Boneris fratribus Polonis s.d.": {
          "desiderius_erasmus": 1
        }
      }
    }
  },
  "1533": {
    "antuerpia": {
      "ar": {
        "Argumentum": {
          "chilius_adrianus": 1
        },
        "Clarissimo eximioque utriusque iuris licentiato, Marco Laurino, collegii divi Donatiani apud Brugas decano, Adrianus Chilius salutem plurimam dicit. ": {
          "chilius_adrianus": 1
        },
        "Idem Curius": {
          "curius_petrus": 1
        },
        "Iohannes Theodori Neruii": {
          "neruius_ioannes": 1
        },
        "Petrus Curius": {
          "curius_petrus": 1
        }
      }
    },
    "basilea": {
      "soph": {
        "Epistola Dedicatoria": {
          "lonicerus_ioannes": 1
        }
      }
    }
  },
  "1534": {
    "haganoa": {
      "soph": {
        "Commentarii Interpretationum Argumenti Thebaidos Fabularum Sophoclis, Authore Ioachimo Camerario Quaestore iam recens natis atque editis. ": {
          "camerarius_ioachimus": 1
        },
        "De Tragico Carmine et illius praecipuis authoribus apud Graecos. ": {
          "camerarius_ioachimus": 1
        }
      }
    },
    "louanium": {
      "ar": {
        "Clarissimo eruditissimoque uiro D. Ioanni Tartesio, praesidi collegii Aquitanici apud Burdegalenses Petrus Nannius. S.P.D. ": {
          "nannius_petrus": 1
        }
      }
    },
    "uenetia": {
      "eur": {
        "": {
          "apostolios_arsenius": 1
        }
      }
    }
  },
  "1535": {
    "lutetia": {
      "ar": {
        "Ματθαίῳ Καδριγαρίῳ γνώμῃ τῆς βουλῆς τουτέστι σοφωτάτῳ τῶν νῦν καὶ τῶν παλαὶ συμβούλων Ι. Χεράδαμος εὖ πλουτεῖν": {
          "cheradamus_ioannes": 1
        }
      }
    }
  },
  "1537": {
    "basilea": {
      "eur": {
        "Joannus Hervagius Lectori S. D.": {
          "heruagius_ioannes": 1
        }
      }
    }
  },
  "1539": {
    "basilea": {
      "ar": {
        "Argumentum praesentis fabulae Pluti est hoc ": {
          "diuus_andreas": 1
        },
        "Epistula nuncupatoria": {
          "diuus_andreas": 1
        }
      }
    }
  },
  "1540": {
    "argentorum": {
      "soph": {
        "Claudius Theraeus Ioannis Sturmio S. P. D.": {
          "theraeus_claudius": 1
        }
      }
    },
    "lutetia": {
      "ar": {
        "Christianus Wechelus Candido Lectori S. ": {
          "wechelus_christianus": 1
        }
      }
    }
  },
  "1543": {
    "uenetia": {
      "soph": {
        "Nobilissimo atque ornatissimo uiro Mario Sauorgnano, Ioannis Baptista a Burgofrancho, Papiensis, S. D.": {
          "gabia_ioannes": 1
        }
      }
    },
    "ultraiectum": {
      "eur": {
        "Argumentum Fabulae": {
          "tiara_petreius": 1
        },
        "Clarissimo D Hectori Hoxuirio Frisio Praesidi Traiextensi uiro nobili iuxta atque erudite Petr. Tiara Salutem dicit.": {
          "tiara_petreius": 1
        }
      }
    }
  },
  "1544": {
    "lutetia": {
      "eur": {
        "Ad Illustrissimum Principem Ioannem a Lucenburgo, Iueraci Abbatem, Georgii Buchanani Praefatio.": {
          "buchananus_georgius": 1
        }
      }
    }
  },
  "1545": {
    "florentia": {
      "eur": {
        "Argumentum Petri Victori": {
          "uictorius_petrus": 1
        },
        "Petrus Victorius Nicolao Ardinghello Cardinali sal": {
          "uictorius_petrus": 1
        }
      }
    },
    "lutetia": {
      "ar": {
        "ΦΡΑΓΚΙΣΚΩ τῷ τῆϛ Φραγκίαϛ βασιλεῖ, τῶν χριστιανῶν, μονάρχων τῷ πάνυ σεβασμίῳ χʹἱεροπρεπεῖ, Αἰγίδιοϛ ὁ Βουρδῖνοϛ εὖ πράττειν καὶ τὰ δόξαντα ἐπιτελεῖν.": {
          "burdinus_aegidius": 1
        }
      }
    },
    "roma": {
      "soph": {
        "Ad Amplissimum Virum Cardinalem Armeniacum, Horatii Nuclei carmen.": {
          "nucleus_horatius": 1
        },
        "Reuerendissimo et illustrissimo Georgio Armaniaco Rhutenen. Episcopo, ac Sanctae Romanae Ecclesiae Cardinale dignissimo. Bartholomeus Marlianus S. D.": {
          "marlianus_bartholomeus": 1
        }
      }
    }
  },
  "1546": {
    "francofortum": {
      "soph": {
        " Trachiniae Sophoclis. Argumentum. ": {
          "winshemius_uitus": 1
        },
        "Antigone Sophoclis.": {
          "winshemius_uitus": 1
        },
        "In Aiacem Sophoclis.": {
          "winshemius_uitus": 1
        },
        "In Electram Sophoclis.": {
          "winshemius_uitus": 1
        },
        "In Oedipum Coloneum Sophoclis.": {
          "winshemius_uitus": 1
        },
        "Inclyto Regi Angliae et Franciae Eduardo Principi Clementiis. Vitus Winshemius S.D.": {
          "winshemius_uitus": 1
        },
        "Sophoclis Philoctetes. Argumentum Fabulae. ": {
          "winshemius_uitus": 1
        }
      }
    },
    "francofortum ": {
      "soph": {
        "De autoribus Tragoediae Joachimus Camerarius.": {
          "camerarius_ioachimus": 1
        },
        "In Antigonen Sophoclis Argumentum Ioachimi Camerarii Pabenbergensis": {
          "camerarius_ioachimus": 1
        },
        "In Œdipum Coloneum Argumentum Iachimi Camerarii.": {
          "camerarius_ioachimus": 1
        },
        "In Œdipum Tyrannum Sophoclis Argumentum Ioachimi Camerarii. ": {
          "camerarius_ioachimus": 1
        }
      }
    }
  },
  "1547": {
    "basilea": {
      "ar": {
        "CLARISSIMO ET OMNIS BONAE LITERATURAE PERITISSIMO DOMINO PHILIPpo Melanchthoni, Sigis. Gelenius S.D. ": {
          "gelenius_sigismundus": 1
        }
      }
    },
    "florentia": {
      "soph": {
        "Bernardus Iuncta Lectori Sal": {
          "iunta_bernardus": 1
        }
      }
    },
    "lutetia": {
      "ar": {
        "D. GONCALLO PINARIO, Tyngitanorum Episcopo Ioannis tertii Lusitaniae Regis in Gallia Legato auunculo suo, M. Cabedius. S. ": {
          "cabedius_michael": 1
        }
      }
    }
  },
  "1548": {
    "lugdunum": {
      "ar": {
        "ΑΓΓΕΛΟΣ ΚΑΝΙΝΙΟΣ ΝΕΟΙΣ τοῖς φιλέλλησιν, εὖ πράττειν": {
          "caninius_angelus": 1
        }
      }
    },
    "lutetia": {
      "eschl": {
        "Ioannes Auratus Candido Lectori Salutem": {
          "auratus_ioannes": 1
        }
      }
    }
  },
  "1549": {
    "lutetia": {
      "ar": {
        "Argumentum": {
          "girardus_carolus": 1
        },
        "Illustrissimae Dominae Ianae Navarrorum Principi Carolus Girardus Bituricus Salute.": {
          "girardus_carolus": 1
        },
        "LECTORI": {
          "girardus_carolus": 1
        }
      }
    }
  },
  "1550": {
    "colonia": {
      "ter": {
        "Optimae spei adolescentulo Iodoco Vuillichio iuniori sui fratris filio, Gregorius Vuagnerus Resellianus, S. P. D. ": {
          "wagnerus_gregorius": 1
        }
      }
    },
    "lugdunum": {
      "soph": {
        "Ad candidum lectorem Georgius Rotaller": {
          "ratallerus_georgius": 1
        },
        "Generosissimo Heroi, Ludovico a Flandria, Domino de Praet, Equiti Summi Ordinis Velleris Aurei, etc. Georgius Rotaller": {
          "ratallerus_georgius": 1
        },
        "In duas Sophoclis Tragoedias Aiacem Flagelliferum, et Antigonem, a Georgio Rotallero Phrysio uersas, Cornelius Scribonius. ": {
          "grapheus_cornelius ": 1
        },
        "Stephani Strati Iureconsul. Antuerpiani Epistola, qua Georgium Rotallerum ad Sophoclis sui editionem adhortatur, Calend. August. M. D. L. ": {
          "stratius_stephanus": 1
        }
      }
    }
  },
  "1551": {
    "basilea": {
      "eur": {
        "Aliud argumentum": {
          "gelous_sigismundus": 1
        },
        "Argumentum": {
          "gelous_sigismundus": 1
        },
        "Expositio. Fabulae narratio apud nullum scriptorem legitur.": {
          "gelous_sigismundus": 1
        },
        "Generosissimis ac uirtutum omnium studiosissimis iuuenibus, Dominis Sebastiano et Amando Truchses a Rinfeld, amicis carissimis suis, Ioannes Oporinus S.": {
          "oporinus_ioannes": 1
        },
        "Sigismundus Gelous clarissimo et doctissimo uiro Martino Calmantzehi pastori ecclesiae Neapolitanae apud pannonios ad Carpathum S. D. ": {
          "gelous_sigismundus": 1
        }
      }
    }
  },
  "1552": {
    "lutetia": {
      "eschl": {
        "ΜΙΧΑΗΛῼ ΟΣΠΙΤΆΛΕΙ βουλεύτη Αδριανὸς Τούρνεϐος εὖ πράττειν.": {
          "turnebus_adrianus": 1
        }
      }
    },
    "uenetia": {
      "eschl": {
        "Aeschyli poetae uita ex Lilii Gregorii Gyraldi De poetarum Historia Dialogo VI": {
          "robortellus_franciscus": 1
        },
        "Franciscus Robortellus Mariano Sauello. S. D.": {
          "robortellus_franciscus": 1
        },
        "Hae sunt coniecturae quibus antiquum librum manu scriptum emedare studuimus.": {
          "robortellus_franciscus": 1
        },
        "Illustrissimo nobilissimoque adolescenti Mariano Sauello Franciscus Robortellus. S. D.": {
          "robortellus_franciscus": 1
        }
      }
    }
  },
  "1553": {
    "turnebus_adrianus": {
      "soph": {
        "": {
          "turnebus_adrianus": 1
        }
      }
    }
  },
  "1555": {
    "basilea": {
      "eschl": {
        "Eiusdem ad lectorem.": {
          "bertrandus_bernardus": 1
        },
        "In translationem Aeschyli Paulus Ottenthalerus Oeniuallensise": {
          "otthentalerus_paulus": 1
        },
        "ΒΕΡΝΆΡΔΟΥ ΒΕΡΤΡΑΝΔΟΥ. Ῥηγίνου πρὸς τοὺς ἐντευξομένους ἀναγνωσὰς. Επίγραμμα. ": {
          "bertrandus_bernardus": 1
        },
        "ΛΑΜΠΡΟΤΆΤΩ ἈΝΔΡΊ ἸΩάννῃ τῷ κεζηλύῳ προέδρῳ, Ἰωάννης ὁ Σανραύιος εὖ πράττειν.": {
          "sanrauius_ioannes": 1
        }
      }
    },
    "lipsia": {
      "eur": {
        "Vera nobilitate Praestanti Reuerendo Domino Iulio Commerstadio Praeposito Misnensi Domin. Sanct. Colendissimo M. Matthaeus Heuslerus Salutem dat": {
          "heuslerus_matthaeus": 1
        }
      }
    },
    "lutetia": {
      "eur": {
        "ΙΩΑΝΝΟΥ ΑΥΡΑΤΟΥ ὑπόθεσις": {
          "auratus_ioannes": 1
        }
      }
    },
    "undefined": {}
  },
  "1556": {
    "basilea": {
      "soph": {
        "De Authore Harum Tragoediarum.": {
          "camerarius_ioachimus": 1
        },
        "De Genere Scripti. ": {
          "camerarius_ioachimus": 1
        },
        "In Sophoclem de Consilio Auctoris. ": {
          "camerarius_ioachimus": 1
        },
        "Ioachimus Camerarius Ioanni Oporino S. D.": {
          "camerarius_ioachimus": 1
        }
      }
    },
    "ultraiectum": {
      "ar": {
        "Argumentum": {
          "hortensius_lambertus": 1
        },
        "Argumentum Pluti": {
          "hortensius_lambertus": 1
        },
        "Reuendo in Christo Patri Gerardo Neoclesiano Abbati ad D. Paulum, Lambertus Hortensius Montfortius S. D.": {
          "hortensius_lambertus": 1
        },
        "Scopus praesentis dramatis": {
          "hortensius_lambertus": 1
        },
        "Vita Aristophanis": {
          "hortensius_lambertus": 1
        }
      }
    },
    "undefined": {}
  },
  "1557": {
    "identifiant_place": {
      "eschl": {
        "Petrus Victorius Lectori S.": {
          "uictorius_petrus": 1
        }
      }
    },
    "lutetia": {
      "eur": {
        "Ad Illustrissimam Principem Dominam Margaritam Henrici secundi Francorum regis sororem, in Alcestin Praefatio.": {
          "buchananus_georgius": 1
        }
      },
      "soph": {
        "": {
          "lalamantius_ioannes": 1
        },
        "Ad Momum. Petri Boni. ": {
          "bonus_petrus": 1
        },
        "Carminis Ratio. ": {
          "lalamantius_ioannes": 1
        },
        "Clarissimo et Imprimis Pio, Petro Marsilio Cipierro, Abbati de Mortemario, Priori Samphoriano, et Canonico Heduensi, obseruandis suo, Ioannes Lalamantius, S. P. ": {
          "lalamantius_ioannes": 1
        },
        "Iani Guillini Parisini in Authoris Commendationem. ": {
          "guillinus_ianus": 1
        },
        "In Sophoclis Tragoedias Praefatiuncula Per Ioannem Lalamantium. ": {
          "lalamantius_ioannes": 1
        }
      }
    },
    "ultraiectum": {
      "ar": {
        "Nebularum siue nubium Aristophanis, Comici clarissimi, argumentum.": {
          "hortensius_lambertus": 1
        },
        "Ornatissimo uiro domino Iacobo Vutenengo, decano ad diuum Petrum, et Vicario episcopi VltraiectiniLambertus Hortensius Montfortius Salutem dat": {
          "hortensius_lambertus": 1
        },
        "Ornatissimo viro d’Iacobo Vutenengo, decano ad diuum Petru et Vicario episcopi Vltraiectini. Lambertus Hortensius Salutem dat": {
          "hortensius_lambertus": 1
        }
      }
    }
  },
  "1558": {
    "augusta_uindelicorum": {
      "eur": {
        "Ad lectorem": {
          "balticus_martinus": 1
        }
      }
    },
    "basilea": {
      "eur": {
        "": {
          "xylandrus_guilelmus": 1
        },
        "Duo sunt praecipui…": {
          "melanchtonus_philippus": 1
        },
        "Euripidis Helena. Argumentum Xylandri.": {
          "xylandrus_guilelmus": 1
        },
        "Euripidis Iphigenia in Aulide. Argumentum, Xylandri.": {
          "xylandrus_guilelmus": 1
        },
        "Euripidis uita Guilielmo Xylandro Augustano autore.": {
          "xylandrus_guilelmus": 1
        },
        "Haec tragoedia est imago tyranni...": {
          "melanchtonus_philippus": 1
        }
      },
      "pl": {
        "De Carminibus Comicis": {
          "camerarius_ioachimus": 1
        },
        "De Plauto σκάζων": {
          "camerarius_ioachimus": 1
        },
        "Epistola. Ioachimus Camerarius Pabepergensis Francisco Crammio Sagano S. D.": {
          "camerarius_ioachimus": 2
        },
        "Epistula nuncupatoria. Prooemium Ioachimi Camerarii, de fabulis Plautinis, ad Illustrissimos pueros Franciscum Othonem et Fridericum fratres.": {
          "camerarius_ioachimus": 1
        },
        "Ioachimus, epistola": {
          "camerarius_ioachimus": 1
        },
        "Ornatissimo maximis uirtutibus et summa doctrina uiro Ioaschimo Camerario, amico suo semper obseruando, Georgius Fabricius S. D.": {
          "fabricius_georgius": 1
        }
      },
      "soph": {
        "": {
          "meggisserus_hieronymus": 1
        },
        "Ad Thomam Naogeorgum, Ioannes Secceruitius Vuratislauiensis. ": {
          "secceruitius_ioannes": 1
        },
        "Christianus Pierius, ad lectorem et Thomam Naogeorgum.": {
          "pierius_christianus": 1
        },
        "Generoso ac amplissimo uiro D. Ioanni Iacobo Fuggero, Domino Vissenhorni ac Kirchpergae, consiliario Caesareo ac Regio, etc. patrono suo colendissimo, Tho. Naogeorgus S. D. P.": {
          "naogeorgus_thomas": 1
        },
        "Ioannes Sturmius.": {
          "sturmius_ioannes": 1
        },
        "Jacobus Scheggius Medicinae ac Philosophiae, ad Thomam Naogeorgum. ": {
          "scheggius_iacobus": 1
        },
        "Micaelus Toxites Thomae Naogeorgo suo.": {
          "toxites_michael": 1
        },
        "Sebastianus Pontanus Vlmensis": {
          "pontanus_sebastianus": 1
        }
      }
    }
  },
  "1559": {
    "basilea": {
      "eschl": {
        "Ad Lectorem. M. Garbitius.": {
          "garbitius_mathias": 1
        },
        "M. Toxites": {
          "toxites_michael": 1
        },
        "Mathiae Garbitii Illyrici in Aeschyli Prometheum Scholia Praefatio.": {
          "garbitius_mathias": 1
        },
        "Patricii et senatorii ordinis uiro pietate, uirtute et sapientia in primis eminenti, Hieronymo Bomgartnero, Domino suo obseruandissimo, salutem et felicitatem in Christo Domino nostro precatur Mathias Garbitius Graecae linguae et morum doctrinae professor ordinarius Tubingae in Academia inclyta.": {
          "garbitius_mathias": 1
        },
        "Τὰ τοῦ δράματος πρόσωπα": {
          "garbitius_mathias": 1
        }
      }
    }
  },
  "1560": {
    "lugdunum": {
      "ter": {
        "Hac autem editione...": {
          "antesignanus_petrus": 1
        },
        "P. Antesignanus Rapistagnensis Antonio, Theophilo, et Ioanni saraeis fratribus salutem dicit ": {
          "antesignanus_petrus": 1
        }
      }
    }
  },
  "1561": {
    "ultraiectum": {
      "ar": {
        "Argumentum fabulae Equitum": {
          "hortensius_lambertus": 1
        },
        "Argumentum Ranarum": {
          "hortensius_lambertus": 1
        },
        "Eiusdem Argumenti Perioche carmine Iambico trimetro": {
          "hortensius_lambertus": 1
        },
        "Lambertus Hortensius Montfortius nobili uiro D. Theodorico Zuleno, etc. Equiti Aurato, Salutem Dat": {
          "hortensius_lambertus": 1
        },
        "Ornatissimo, atque erudito uiro D.&nbsp;Henrico Ruzaeo, ordinis militiae Hierosolimitanae, et Mandatori in Ingen. Lambertus Hortensius Monfortius salutem dicit plurimam.": {
          "hortensius_lambertus": 1
        },
        "Perioche argumenti, iambicum trimetrum.": {
          "hortensius_lambertus": 1
        },
        "Scopus fabulae Ranarum et poetae consilium": {
          "hortensius_lambertus": 1
        },
        "Scopus Poetae": {
          "hortensius_lambertus": 1
        }
      }
    }
  },
  "1562": {
    "basilea": {
      "eur": {
        "": {
          "stiblinus_gasparus": 2
        },
        "Ad clarissimos et ornatissimos uiros, Dominum Simonem A Pfirt, et Dominum Ioannem Fabri V.I. Doctorem, Caesareos Senatores in Ensisheim, Gaspari Stiblini carmen in omnes Euripidis Tragoedias.": {
          "stiblinus_gasparus": 1
        },
        "Argumentum Et Praefatio Gaspari Stiblini In Iphigeniam Tauricam": {
          "stiblinus_gasparus": 1
        },
        "Argumentum et Praefatio Gaspari Stiblini in Supplices Euripidis": {
          "stiblinus_gasparus": 1
        },
        "Argumentum Et Praefatiuncula Gaspari Stiblini In Euripidis Rhesum": {
          "stiblinus_gasparus": 1
        },
        "Gaspari Stiblini In Electram Euripidis Praefatio et Scholia": {
          "stiblinus_gasparus": 1
        },
        "Gaspari Stiblini Praefatio in Andromachen": {
          "stiblinus_gasparus": 1
        },
        "Gaspari Stiblini Praefatio In Euripidis Troadas": {
          "stiblinus_gasparus": 1
        },
        "Gasparus Stiblinus aequo lectori salutem plurimam dicit": {
          "gasparus_stiblinus": 1
        },
        "Gasparus Stiblinus Domino Iohanni Oporino Salutem Dat Plurimam": {
          "stiblinus_gasparus": 1
        },
        "Praefatio et Argumentum Gaspari Stiblini in Euripidis Helenam ": {
          "stiblinus_gasparus": 1
        },
        "Praefatio Gaspari Stiblini in Cyclopem": {
          "stiblinus_gasparus": 1
        },
        "Praefatio Gaspari Stiblini in Euripidis Bacchas": {
          "stiblinus_gasparus": 1
        },
        "Praefatio Gaspari Stiblini in Heraclidas": {
          "stiblinus_gasparus": 1
        },
        "Praefatio Gaspari Stiblini In Herculem Furentem": {
          "stiblinus_gasparus": 1
        },
        "Praefatio Gaspari Stiblini in Ionem": {
          "stiblinus_stiblinus": 1
        },
        "Praefatio Gaspari Stiblini In Iphigeniam In Aulide": {
          "stiblinus_gasparus": 1
        },
        "ΝΙΚΟΛΑΟΣ Ο ΓΑΛΛΩΤΟΣ εἰς τὸν μεταφράστην.": {
          "gallotus_nicolaus": 1
        },
        "ΧΑΣΠΑΡ Ο ΣΤΙΒΛΙΝΟΣ ΙΩΑΝΝΗΙ ΤΩΙ ΟΠΩΡΙΝΩΙ εὖ διάγειν.": {
          "stiblinus_gasparus": 1
        }
      }
    },
    "francofortum": {
      "eur": {
        "Ad candidum lectorem Xylandri praefatio, editionis huius rationem ostendens.": {
          "xylandrus_guilelmus": 1
        }
      }
    }
  },
  "1566": {
    "lipsia": {
      "sen": {
        "De carmine Iambico trimetro": {
          "auantius_hieronymus": 1
        },
        "De reliquis carminum generibus": {
          "fabricius_georgius": 1
        },
        "Illustris ducis ac domini D. Wolfgangi Palatini Rheni, Ducis Bauariae, Comitis Veldentii, filiis, D. D. Othoni Henrico, et Friderico fratribus salutem dat Georgius Fabricius Chemnicensis ": {
          "fabricius_georgius": 1
        },
        "Lilius Gregorius Gyraldus. Dialogo VIII": {
          "gregorius_lilius": 1
        }
      }
    }
  },
  "1567": {
    "argentorum": {
      "eur": {
        "Ioannes Sturmius Tidemanno Gisio Dantiscano S.P.D.": {
          "sturmius_ioannes": 1
        }
      }
    },
    "geneua": {
      "eur": {
        "Autores Sequentium Interpretationum.": {
          "stephanus_henricus": 1
        }
      },
      "soph": {
        "Clarissimo Viro Stephano Stratio Biturigibus legum antecessori, Georgius Rotallerus.": {
          "ratallerus_georgius": 1
        }
      }
    }
  },
  "1568": {
    "lutetia": {
      "soph": {
        "Henricus Stephanus Lectori &lt;foreign xml:lang=\"grc\" &gt;φιλοσοφοκλεῖ&lt;/foreign&gt; S. P. D. ": {
          "stephanus henricus": 1
        }
      }
    }
  },
  "1570": {
    "antuerpia": {
      "soph": {
        "": {
          "falkenburgius_gerartus": 1
        },
        " De Ratione Versuum ad Lectorem.": {
          "ratallerus_georgius": 1
        },
        "Adriani Mylii Senatoris Regii apud Hollandos ad Lectorem Epistola.": {
          "mylius_adrianus": 1
        },
        "Illustri uiro D. Frederico Perenotto Domino a Champagney Sancto Lupo etc. Georgius Ratallerus S. P. D.": {
          "ratallerus_georgius": 1
        },
        "Ioachimus Polites Georgio Ratallero S. P. Atrebatum.": {
          "polites_ioachimus": 1
        },
        "Nicolaus Nicolai Grudius Georgio Ratallero S. P. Dolam.": {
          "grudius_nicolaus": 1
        }
      }
    }
  },
  "1571": {
    "antuerpia": {
      "eur": {
        "": {
          "canterus_guilelmus": 1
        },
        "Ex eiusdem poematum lib. quarto. Euripidis tragoediarum XIX. argumenta. ": {
          "canterus_guilelmus": 1
        },
        "Guilelmi Canteri in Euripidem notae. Ad Clarissimium Virum Iohannem Cratonem a Craftheim, Caesaris Maiestatis Archiatrum, Praefatio.": {
          "canterus_guilelmus": 1
        },
        "Gulielmi Canteri in Euripidem Prolegomena": {
          "canterus_guilelmus": 1
        }
      }
    }
  },
  "1574": {
    "augusta_uindelicorum": {
      "soph": {
        "": {
          "gardesius_ioannes": 1
        }
      }
    }
  },
  "1576": {
    "antuerpia": {
      "sen": {
        "Ad amplissimum virum, clarissimumque I. C. Ludovicum Delrio, regium consiliarium, libellorumque supplicum magistrum etc. Hieronymi Delrio praefatio": {
          "delrio_hieronymus": 1
        },
        "Ad Martinum Antonium Delrio Augustinus Descobar Benventanus": {
          "descobar_augustinus": 1
        },
        "Adolphi Mekerchi Brugensis": {
          "mekerchius_adolphus": 1
        },
        "De Senecae tragoediis Martino Antonio Delrio Restitutis et Commentario Illustratis, Francisci Nansii Isembergensis societatis Jesu.Christi.": {
          "nansius_franciscus": 1
        },
        "Ioannis Dominici Florentii Romani Carmen": {
          "florentius_ioannes": 1
        },
        "Martinus Antonius Delrio lectori salutem dat": {
          "delrio_martinus": 1
        },
        "Praeludia quaedam de tragoedia, tragicis et Seneca tragoediographo ": {
          "delrio_martinus": 1
        }
      }
    },
    "lutetia": {
      "pl": {
        "Clarissimo et eruditissimo viro Germano Valenti Guellio PP. Senatori amplissimo, Germanus Lambinus Salutem dat.": {
          "lambinus_germanus": 1
        },
        "Iacobus Helias graecarum litterarum doctor regius lectoribus&nbsp;S.&nbsp;D.": {
          "helias_iacobus": 1
        },
        "Εἰς τὸν Πλαῦτον Διονυσίου Λαμβίνου Ιω.Αὐρᾶτος ποιητὴς βασιλικὸς.": {
          "auratus_ioannes": 1
        }
      }
    }
  },
  "1577": {
    "argentorum": {
      "eur": {
        "Ad Elisabetham serenissimam Angliae, Franciae, Hiberniae Reginam": {
          "calaminus_georgius": 1
        }
      }
    }
  },
  "1579": {
    "antuerpia": {
      "soph": {
        "Gulielmi Canteri in Sophoclem Prolegomena. ": {
          "canterus_guilelmus": 1
        },
        "ΑΝΔΡΙ ΛΑΜΠΡΟΤΑΤΩ ΓΕΩΡΓΙΩ ΡΑΤΑΛΛΕΡΩ ΒΙΛΕΔΜΟΣ ΚΑΝΤΗΡΟΣ ΕΥ ΠΡΑΤΤΕΙΝ ": {
          "canterus_guilelmus": 1
        }
      }
    }
  },
  "1580": {
    "antuerpia": {
      "eschl": {
        "Ex eiusdem poematum libro IIII, Aeschyli tragoediarum septem argumenta.": {
          "canterus_guilelmus": 1
        },
        "Gulielmi Canteri in Aeschylum Prolegomena": {
          "canterus_guilelmus": 1
        },
        "Ἀνδρὶ λογιωτάτῳ ΠΕΤΡΩ ΒΙΚΤΩΡΙΩ, ΒΙΛΕΛΜΟΣ ΚΑΝΤΗΡΟΣ εὖ πράττειν": {
          "canterus_guilelmus": 1
        }
      },
      "undefined": {}
    }
  },
  "1581": {
    "antuerpia": {
      "eur": {
        "Argumentum Andromachae per Georgium Ratallerum.": {
          "ratallerus_georgius": 1
        },
        "Argumentum Phoenissarum per Georgium Ratallerum.": {
          "ratallerus_georgius": 1
        },
        "Egregia uirtute atque eruditione uiro D. Theodoro Cantero Georgius Ratallerus S.": {
          "ratallerus_georgius": 1
        },
        "Euripidis poetae tragici Hippolytus Coronatus, Georgio Ratallero interprete. Argumentum Hippolyti auctore Georgio Ratallero.": {
          "ratallerus_georgius": 1
        },
        "Nicolao Micault Indeveldii Domino et priutai consilii Senatori, Georgius Ratallerus.": {
          "ratallerus_georgius": 1
        }
      }
    },
    "rostochium": {
      "eschl": {
        "Ad illustrissimum DN. Vlricum ducem Megap., principem Vandalorum, Comitem Suerinensem, prouinciarum Rostochianae et Stargardensis Dominum. Prooemium Ioannis Caselii.": {
          "caselius_ioannes": 1
        },
        "Argumentum tragoediae Aeschyli, Septem ad Thebas ducum": {
          "caselius_ioannes": 1
        }
      }
    },
    "uitteberga": {
      "eur": {
        "": {
          "albinus_petrus": 4,
          "mencius_balthasar": 1,
          "utenhouius_carolus": 1,
          "wanckelius_ioannes": 1
        },
        "Eximiae spei adolescentibus et pueris, Vito, Georgio, Iohanni, Casparo, Christiano, et Paulo, Magnifici atque Excellentissimi Viri, Dn. Viti Ortelii VVinshemii, V.I. D. et Professoris, in inclita Viteberg. Academia et c. Filiis Georgius Nucelius Annaebergensis. ": {
          "nucelius_georgius": 1
        },
        "Prologus conscriptus a petro albino niuemontio Profes. Publ. ": {
          "albinus_petrus": 1
        }
      }
    }
  },
  "1583": {
    "praha": {
      "soph": {
        "Generoso ac Magnifico Heroi et Domino: Domino Ladislao Seniori a Lobkouitz, Baroni in Chlumecio et Gistebnicia S. C. M. Consiliario atque pro Regi, et in inclyto Boemiae Regno supremo Curiae Magistro, etc. Domino atque Mecaenati colendo. S. P. D. ": {
          "codicillus_petrus": 1
        },
        "Studiosis Graecae Linguae S. P. D.": {
          "codicillus_petrus": 1
        }
      }
    }
  },
  "1584": {
    "argentorum": {
      "soph": {
        "De Autoribus Tragoediae. Joachimus Camerarius. ": {
          "camerarius_ioachimus": 1
        }
      }
    }
  },
  "1585": {
    "lutetia": {
      "eschl": {
        "Ad Federicum Morellum Typogr. Regium, De Florentis Christiani stylo Tragico": {
          "melissus_paulus": 1
        },
        "Hypothesis uel (ut uocant) argumentum Aeschyli Tragoediae, quae inscribitur Septem ad Thebas": {
          "christianus_florens": 1
        },
        "In Quint.Septimi Florentis Christiani poemata": {
          "morellus_federicus": 1
        },
        "Q. Sept. Florens Christianus Federico Morello suo S. D.": {
          "christianus_florens": 1
        }
      }
    }
  },
  "1586": {
    "francofortum": {
      "ar": {
        "Ad Clarissimum uirum Nicodemum Frischlinum Aristophanis interpretem": {
          "frenzelius_salomon": 1
        },
        "Aliud eiusdem in eundem - Κῶμον Ἀριστοφάνους διὰ Φρισχλίου Αὐσονίοισι": {
          "sylburgius_fridericus": 1
        },
        "Aristophanes Frischlianus de se": {
          "rosbachius_ioannes": 1
        },
        "Diuo Rudolpho II Caesari, Romano Imperatori electo, Augustus Pius Felix Pater Patriae Regi Germaniae, Bohemiae, Hungariae etc., Archiduci Austriae, Duci Burgundiae, Comiti Tyrolano, etc., Principi Optimo Maximo, uita, salus et uictoria": {
          "frischlinus_nicodemus": 1
        },
        "In Aristophanem clarissimi uiri domini Nicodemi Frischlini, Poetae laureati et comitis Caesarei": {
          "baderus_matthaeus": 1
        },
        "In Aristophanem factum ex Graeco Latinum a Nicodemo Frischlino, Elegidion Lamberti Ludolphi Pithopoei Dauentriensis": {
          "pithopoeius_lambertus": 1
        },
        "In Aristophanem Latinum Nicodemi Frisclini, Epigramma Friderici SYLBURGII": {
          "sylburgius_fridericus": 1
        },
        "In uersum et emendatum a clarissimo uiro Domino Nicodemo Frischlino Aristophanem, lepidissimum Comicum": {
          "modius_franciscus": 1
        },
        "Vita Aristophanis a Nicodemo Frischlino conscripta": {
          "frischlinus_nicodemus": 1
        }
      }
    },
    "lutetia": {
      "soph": {
        "Ad Sophoclis Philoctetam in Lemno Glossemata.": {
          "christianus_florens": 1
        },
        "Hypothesis uel argumentum (ut uocant) Philocteta in Lemnoa ": {
          "christianus_florens": 1
        },
        "In Fl. Christiani Philoctetem": {
          "auratus_ioannes": 1
        },
        "Q. Sept. Florens Christianus Doctissimo uiro Nicolao Gulonio Graecarum literarum Regio doctori, S. dico.": {
          "christianus_florens": 1
        },
        "Q. Sept. Florenti Christiano uiro ornatissimo et doctissimo, N. Gulonius S.P.D.": {
          "gulonius_nicolaus": 1
        }
      }
    }
  },
  "1587": {
    "lugdunum": {
      "pl": {
        "Ad lectorem In Plautum Nobilissimi et doctissimi uiri Iani Dousae opera restitutum.": {
          "benedicti_georgius": 1
        },
        "Eiusdem ad eundem elegia": {
          "dousa_ianus_f": 1
        },
        "In Iani Dousae centurionatum Plautinum Adrianus Burchius": {
          "burchius_adrianus": 1
        },
        "In Iani Dousae Plautinam restitutionem Epigramma": {
          "bredius_franciscus": 1
        },
        "In Plautum Viri Nobilissimi Iani Dousae opera emendatum, Iani Gruteri Carmen Elicium.": {
          "gruterus_ianus": 1
        },
        "Viro amplissimo Danieli Rogersio Albimontio Anglo, Augustissimae Heroinae, Dominae Elisabethae Angliae, Franciae, Hiberniaeque Reginae, Oratori Ianus Dousa filius S.D.": {
          "dousa_ianus_f": 1
        }
      }
    }
  },
  "1588": {},
  "1589": {
    "antuerpia": {
      "sen": {
        "Ad lectorem": {
          "raphelengius_franciscus_f": 1
        },
        "Franciscus F. F. Raphelengius Lectori S.": {
          "raphelengius_franciscus_f": 1
        },
        "Iusto Lipsio, uiro clarissimo, Franciscus Raphelengius, Francisci Filius Plantinianus, Dicat Consecratque": {
          "raphelengius_franciscus_f": 1
        },
        "Iustus Lipsius Francisco Raphelengio, Francisci Filio, Plantiniano salutem dat.": {
          "lipsius_iustus": 1
        },
        "Quae tibi rependent, docte LIPSI, praemia…": {
          "dousa_ianus_f": 1
        }
      }
    },
    "heidelberga": {
      "sen": {
        "Othoni Grynradio Viro Nobili Salutem": {
          "commelinus_hieronymus": 1
        },
        "Quam grauis immineat saeuis fortuna tyrannis…": {
          "posthius_ioannes": 1
        }
      }
    },
    "lutetia": {
      "ar": {
        "PROLOGVS": {
          "christianus_florens": 1
        },
        "Q. Septimus Florens Christianus V. C. Jacobo Augusto Thuano, Aemerio, Christophori Senatus quondam Principis filio, S.P.D.": {
          "christianus_florens": 1
        },
        "ΕΙΣ ΤΗΝ ΑΡΙΣΤΟΦΑΝΙΚΗΝ ΕΙΡΗΝΗΝ ΜΕΤΑΦΡΑΣΤΕΙΣΑΝ ῥωμαϊστὶ ἀπὸ Φλ. Χριστιανοῦ.": {
          "auratus_ioannes": 1
        },
        "ΦΕΔΕΡΙΚΟΥ ΜΟΡΕΛΛΟΥ ΕΙΣ ΕΙΡΗΝΗΝ δίγλωττον": {
          "morellus_federicus": 1
        },
        "ΦΛΩΡ. ΧΡΙΣΤΙΑΝΟΥ ΥΠΟΘΕΣΙΣ ΔΙ’ ΙΑΜΒΩΝ": {
          "christianus_florens": 1
        }
      }
    }
  },
  "1592": {},
  "1593": {
    "barcinona": {
      "eur": {
        "Ego Petrus Aegidius": {
          "aegidius_petrus": 1
        },
        "Nos Ioannes Dimas Loris": {
          "dimas_ioannes": 1
        }
      }
    }
  },
  "1594": {
    "lugdunum_batauorum": {
      "eur": {
        "Idem Latine.": {
          "christianus_florens": 1
        },
        "In V.C. Quintui Septimi Florentis Christiani Andromacham Latinam": {
          "christianus_florens": 1
        },
        "Quintus Septimius Florens Christianus Nobili et eruditissimo uiro Iano Douzae Nordouici Toparchae salutem dat.": {
          "christianus_florens": 1
        },
        "Quintus Septimius Florens Christianus Nobili et eruditissimo uiro iuueni Iano Douzae filio salutem dat.": {
          "christianus_florens": 1
        },
        "Quintus Septimius Florens Christianus Optimo et erudito uiro Gerardo Tuningio salutem dat.": {
          "christianus_florens": 1
        },
        "ΦΛ. ΧΡΙΣΤΙΑΝΟΥ ὑπόθεσις τοῦ δράματος, δι’ἰάμβων, ἐν οἷς ᾿Ανδρομάχη ἐστιν ἀχροστιχὶς.": {
          "christianus_florens": 1
        }
      }
    }
  },
  "1595": {
    "rostochium": {
      "eur": {
        "Vlrico Chrytraeo Domino Dauidis, uiri clarissimi Filio. Iohannes Posselius Salutem Plurimam Dat.": {
          "posselius_ioannes": 1
        }
      }
    }
  },
  "1597": {
    "heidelberga": {
      "eur": {
        "Nobilissimo et amplissimo uiro, D. Georgio Ludouico ab Hutten, serenissimi Principis, Electoris Palatini intimo Consiliario, D. ac Patrono suo plurimum obseruando Aemilius Portus FR. P. CR. F. S.P.D.P.Q.F.O.": {
          "identifiant_auteur": 1
        }
      }
    }
  },
  "1600": {
    "lugdunum": {
      "ar": {
        "Typographus lectori": {
          "raphelengius_christophorus": 1
        }
      }
    }
  },
  "1602": {
    "geneua": {
      "eur": {
        "Reuerendissimo in Christo Patri Domino Francisco Maunio Burdegalensium archiepiscopo, Aquitaniae primate, Baptista Sapinus, consiliarius Regius, S.": {
          "sapinus_baptista": 1
        },
        "Viro clarissimo Antonio Fayo, Paulus Stephanus.": {
          "stephanus_paulus": 1
        }
      }
    }
  },
  "1604": {
    "heidelberga": {
      "sen": {
        "Janus Dousa Nordouix etc. ": {
          "dousa_ianus": 1
        },
        "Magnifico amplissimoque Ludivico Culmano, serenissimi electoris Palatini Friderici IV, in sanctiore consilio pro praesidi": {
          "gruterus_ianus": 1
        }
      }
    }
  },
  "1605": {
    "uitteberga": {
      "pl": {
        "": {
          "rhodomanus_laurentius": 1
        },
        "Ad Frid. Taubmannum": {
          "blansdorff_ioannes": 1
        },
        "Ad lectorem candidum et eruditum": {
          "taubmanus_fridericus": 1
        },
        "Aliud": {
          "sebaldi_uitus": 1
        },
        "Diua parens Latii, late dominata per Orbem": {
          "treutlerus_hieronymus": 1
        },
        "Ede hilares mecum plausus Musea iuuentus": {
          "cremcouius_ualentinus": 1
        },
        "In editionem Plautinam F.T.": {
          "rittershusius_cunradus": 1
        },
        "Optimis maximisque Reipublicae Litterariae Triumviris, Josepho Scaligero, Justo Lipsio, Isaaco Casaubono, Fridericus Taubmanus S. D.": {
          "taubmanus_fridericus": 1
        },
        "Postquam est mortem aptus Plautus, Comoedia luget": {
          "siberus_adamus": 1
        },
        "Risus magister, et leporis conditor": {
          "gentilis_scipio": 1
        },
        "Tot nuper scatuit Plauti Comoedia mendis": {
          "peiferus_dauidus": 1
        }
      }
    }
  },
  "1607": {
    "geneua": {
      "ar": {
        "": {
          "portus_aemilius": 1
        },
        "Aemilius Portus Fr. Porti Cretensis F. candido lectori S.": {
          "portus_aemilius": 1
        },
        "Insigni Pietate, nobilitate et doctrina ornatissima uiro, Domino Odoardo Biseto, Domino de Charlay, Domino suo plurimum obseruando Aemilius Portus Francisci Porti Cretensis Filius S. P.": {
          "portus_aemilius": 1
        },
        "QUINTI SEPTIMI FLORENTIS CHRISTIANI PROLOGUS IN VESPAS ARISTOPHANICAS Ad Senatum Populumque forensem": {
          "christianus_florens": 1
        }
      }
    }
  },
  "1609": {},
  "1611": {
    "lugdunum_batauorum": {
      "sen": {
        "Amico Lectori.": {
          "heinsius_danielis": 1
        },
        "Amplissimo nobilissimoque uiro Iohanni Milandro Domino de Poederoeyen illustrissimi principis Mauritii a Nassou a consiliis et secretis Daniel Heinsius salutem dicit": {
          "heinsius_danielis": 1
        },
        "Danielis Heinsii in Lucii et Marci Annaei Senecae ac reliquorum quae extant tragoedias animaduersiones et notae.": {
          "heinsius_danielis": 1
        }
      }
    }
  },
  "1613": {
    "londinium": {
      "sen": {
        " De opere, operis autore, autoris interprete, interpretis amicus": {
          "whitakerus_laurentius": 1
        },
        "Ad aequanimos lectores": {
          "farnabius_thomas": 1
        },
        "Ad eundem": {
          "wheare_degory": 1
        },
        "Comedias trusatilis Plauti mola ": {
          "jonson_ben": 1
        },
        "De Farnabio ad lectorem ": {
          "audoenus_ioannes": 1
        },
        "Eidem.": {
          "tomkins_nathaniel": 1
        },
        "Viro de re literaria oprime merito ; Thomae Farnabio": {
          "tomkins_nathaniel": 1
        },
        "Viro litterarum humaniorum studiosissimo Thomae Rolando": {
          "farnabius_thomas": 1
        }
      }
    }
  },
  "1625": {
    "lugdunum_batauorum": {
      "ar": {
        "Aristophanis uita et scripta.": {
          "maire_ioannes": 1
        },
        "Typographus Lectori Beniuolo S.": {
          "maire_ioannes": 1
        }
      }
    }
  }
}

// // OLD
// 
// let date_place_ancient_author = {
//   "1472": {
//     "uenetia": {
//       "pl": {
//         "merula_georgius": 2
//       }
//     }
//   },
//   "1488": {
//     "lutetia": {
//       "sen": {
//         "fernandus_carolus": 2
//       }
//     }
//   },
//   "1491": {
//     "lugdunum": {
//       "sen": {
//         "marmita_gellius": 1
//       }
//     }
//   },
//   "1493": {
//     "lugdunum": {
//       "ter": {
//         "ascensius_iodocus": 1,
//         "egidius_ioannes": 1,
//         "iuuenalis_guido": 6
//       }
//     }
//   },
//   "1496": {
//     "argentorum": {
//       "ter": {
//         "gruninger_ioannes": 6
//       }
//     }
//   },
//   "1498": {
//     "uenetia": {
//       "ar": {
//         "carteromacus_scipio": 1,
//         "manutius_aldus": 1,
//         "musurus_marcus": 1
//       }
//     }
//   },
//   "1499": {
//     "argentorum": {
//       "ter": {
//         "bebelius_heinricus": 1,
//         "locher_iacobus": 2,
//         "petrarca_franciscus": 1
//       }
//     },
//     "mediolanum": {
//       "pl": {
//         "canalis_paulus": 1,
//         "maro_andrea": 1,
//         "palladius_dominicus": 2,
//         "saracenus_bernardinus": 1,
//         "ualla_ioannes": 1,
//         "undefined": 1
//       }
//     }
//   },
//   "1500": {
//     "mediolanum": {
//       "pl": {
//         "beroaldus_philippus": 1,
//         "calcaterra_antonius": 2,
//         "ducius_sebastianus": 1,
//         "marlianus_ioannes": 1,
//         "pius_ioannes": 7,
//         "salandus_ioannes": 1
//       }
//     }
//   },
//   "1501": {
//     "parma": {
//       "ar": {
//         "passius_franciscus": 5
//       }
//     }
//   },
//   "1502": {
//     "uenetia": {
//       "soph": {
//         "manutius_aldus": 1
//       }
//     }
//   },
//   "1503": {
//     "uenetia": {
//       "eur": {
//         "manutius_aldus": 1
//       }
//     }
//   },
//   "1504": {
//     "identifiant_place": {
//       "identifiant_person": {
//         "identifiant_auteur": 1
//       }
//     },
//     "londinium": {
//       "ter": {
//         "ascensius_iodocus": 29
//       }
//     }
//   },
//   "1506": {
//     "brixia": {
//       "pl": {
//         "britannicus_ioannes": 1,
//         "pylades_buccardus": 1
//       }
//     },
//     "florentia": {
//       "sen": {
//         "riccardinus_benedictus": 3
//       }
//     },
//     "identifiant_place": {
//       "identifiant_person": {
//         "identifiant_auteur": 4
//       }
//     },
//     "lutetia": {
//       "eur": {
//         "erasmus_desiderius": 5
//       }
//     },
//     "parma": {
//       "eur": {
//         "anselmus_georgius": 1
//       }
//     }
//   },
//   "1507": {
//     "uenetia": {
//       "eur": {
//         "erasmus_desiderius": 2
//       }
//     }
//   },
//   "1508": {
//     "argentorum": {
//       "pl": {
//         "mulingus_ioannes": 1
//       }
//     }
//   },
//   "1511": {
//     "lutetia": {
//       "sen": {
//         "crinitus_petrus": 1,
//         "maserius_aegidius": 6,
//         "stainuilla_petrus": 1
//       }
//     },
//     "uenetia": {
//       "pl": {
//         "soardus_lazarus": 1
//       }
//     }
//   },
//   "1512": {
//     "uenetia": {
//       "pl": {
//         "charpentarius_symon": 3
//       }
//     }
//   },
//   "1514": {
//     "florentia": {
//       "pl": {
//         "angelius_nicolaus": 1
//       }
//     },
//     "lutetia": {
//       "sen": {
//         "ascensius_iodocus": 1,
//         "cabaliatus_polydorus": 1,
//         "gaietanus_daniel": 2,
//         "maserius_aegidius": 2
//       }
//     }
//   },
//   "1515": {
//     "florentia": {
//       "ar": {
//         "iunta_bernardus": 1
//       }
//     }
//   },
//   "1516": {
//     "florentia": {
//       "ar": {
//         "iunta_bernardus": 2
//       }
//     }
//   },
//   "1517": {
//     "haganoa": {
//       "ar": {
//         "mosellanus_petrus": 1
//       }
//     },
//     "uenetia": {
//       "sen": {
//         "auantius_hieronymus": 2
//       }
//     }
//   },
//   "1518": {
//     "alost": {
//       "ar": {
//         "martinus_theodoricus": 1
//       }
//     },
//     "uenetia": {
//       "eschl": {
//         "asulanus_franciscus": 1
//       }
//     }
//   },
//   "1521": {
//     "uitteberga": {
//       "ar": {
//         "melanchtonus_philippus": 1
//       }
//     }
//   },
//   "1522": {
//     "florentia": {
//       "soph": {
//         "francinus_antonius": 1
//       }
//     },
//     "uenetia": {
//       "pl": {
//         "asulanus_franciscus": 1,
//         "crinints_petrus": 1
//       }
//     }
//   },
//   "1524": {
//     "basilea": {
//       "ar": {
//         "frobenius_ioannes": 1
//       }
//     }
//   },
//   "1525": {
//     "florentia": {
//       "ar": {
//         "francinus_antonius": 1,
//         "iunta_bernardus": 2,
//         "undefined": 1
//       }
//     }
//   },
//   "1528": {
//     "lutetia": {
//       "ar": {
//         "cheradamus_ioannes": 9
//       }
//     }
//   },
//   "1530": {
//     "louanium": {
//       "ter": {
//         "barlandus_adrianus": 4
//       }
//     }
//   },
//   "1531": {
//     "nuremberga": {
//       "ar": {
//         "uenatorius_thomas": 2
//       }
//     }
//   },
//   "1532": {
//     "basilea": {
//       "ar": {
//         "cratander_andreas": 1,
//         "grynaeus_simon": 1
//       },
//       "ter": {
//         "desiderius_erasmus": 2
//       }
//     }
//   },
//   "1533": {
//     "antuerpia": {
//       "ar": {
//         "chilius_adrianus": 2,
//         "curius_petrus": 2,
//         "neruius_ioannes": 1
//       }
//     },
//     "basilea": {
//       "soph": {
//         "lonicerus_ioannes": 1
//       }
//     }
//   },
//   "1534": {
//     "haganoa": {
//       "soph": {
//         "camerarius_ioachimus": 2
//       }
//     },
//     "louanium": {
//       "ar": {
//         "nannius_petrus": 1
//       }
//     },
//     "uenetia": {
//       "eur": {
//         "apostolios_arsenius": 1
//       }
//     }
//   },
//   "1535": {
//     "lutetia": {
//       "ar": {
//         "cheradamus_ioannes": 1
//       }
//     }
//   },
//   "1537": {
//     "basilea": {
//       "eur": {
//         "heruagius_ioannes": 1
//       }
//     }
//   },
//   "1539": {
//     "basilea": {
//       "ar": {
//         "diuus_andreas": 2
//       }
//     }
//   },
//   "1540": {
//     "argentorum": {
//       "soph": {
//         "theraeus_claudius": 1
//       }
//     },
//     "lutetia": {
//       "ar": {
//         "wechelus_christianus": 1
//       }
//     }
//   },
//   "1543": {
//     "uenetia": {
//       "soph": {
//         "gabia_ioannes": 1
//       }
//     },
//     "ultraiectum": {
//       "eur": {
//         "tiara_petreius": 2
//       }
//     }
//   },
//   "1544": {
//     "lutetia": {
//       "eur": {
//         "buchananus_georgius": 1
//       }
//     }
//   },
//   "1545": {
//     "florentia": {
//       "eur": {
//         "uictorius_petrus": 2
//       }
//     },
//     "lutetia": {
//       "ar": {
//         "burdinus_aegidius": 1
//       }
//     },
//     "roma": {
//       "soph": {
//         "marlianus_bartholomeus": 1,
//         "nucleus_horatius": 1
//       }
//     }
//   },
//   "1546": {
//     "francofortum": {
//       "soph": {
//         "winshemius_uitus": 7
//       }
//     },
//     "francofortum": {
//       "soph": {
//         "camerarius_ioachimus": 4
//       }
//     }
//   },
//   "1547": {
//     "basilea": {
//       "ar": {
//         "gelenius_sigismundus": 1
//       }
//     },
//     "florentia": {
//       "soph": {
//         "iunta_bernardus": 1
//       }
//     },
//     "lutetia": {
//       "ar": {
//         "cabedius_michael": 1
//       }
//     }
//   },
//   "1548": {
//     "lugdunum": {
//       "ar": {
//         "caninius_angelus": 1
//       }
//     },
//     "lutetia": {
//       "eschl": {
//         "auratus_ioannes": 1
//       }
//     }
//   },
//   "1549": {
//     "lutetia": {
//       "ar": {
//         "girardus_carolus": 3
//       }
//     }
//   },
//   "1550": {
//     "colonia": {
//       "ter": {
//         "wagnerus_gregorius": 1
//       }
//     },
//     "lugdunum": {
//       "soph": {
//         "grapheus_cornelius": 1,
//         "ratallerus_georgius": 2,
//         "stratius_stephanus": 1
//       }
//     }
//   },
//   "1551": {
//     "basilea": {
//       "eur": {
//         "gelous_sigismundus": 4,
//         "oporinus_ioannes": 1
//       }
//     }
//   },
//   "1552": {
//     "lutetia": {
//       "eschl": {
//         "turnebus_adrianus": 1
//       }
//     },
//     "uenetia": {
//       "eschl": {
//         "robortellus_franciscus": 4
//       }
//     }
//   },
//   "1553": {
//     "turnebus_adrianus": {
//       "soph": {
//         "turnebus_adrianus": 1
//       }
//     }
//   },
//   "1555": {
//     "basilea": {
//       "eschl": {
//         "bertrandus_bernardus": 2,
//         "otthentalerus_paulus": 1,
//         "sanrauius_ioannes": 1
//       }
//     },
//     "lipsia": {
//       "eur": {
//         "heuslerus_matthaeus": 1
//       }
//     },
//     "lutetia": {
//       "eur": {
//         "auratus_ioannes": 1
//       }
//     },
//     /*"undefined": {}*/
//   },
//   "1556": {
//     "basilea": {
//       "soph": {
//         "camerarius_ioachimus": 4
//       }
//     },
//     "ultraiectum": {
//       "ar": {
//         "hortensius_lambertus": 5
//       }
//     },
//     /*"undefined": {}*/
//   },
//   "1557": {/*
//     "identifiant_place": {
//       "eschl": {
//         "uictorius_petrus": 1
//       }
//     },*/
//     "lutetia": {
//       "eur": {
//         "buchananus_georgius": 1
//       },
//       "soph": {
//         "bonus_petrus": 1,
//         "guillinus_ianus": 1,
//         "lalamantius_ioannes": 4
//       }
//     },
//     "ultraiectum": {
//       "ar": {
//         "hortensius_lambertus": 3
//       }
//     }
//   },
//   "1558": {
//     "augusta_uindelicorum": {
//       "eur": {
//         "balticus_martinus": 1
//       }
//     },
//     "basilea": {
//       "eur": {
//         "melanchtonus_philippus": 2,
//         "xylandrus_guilelmus": 4
//       },
//       "pl": {
//         "camerarius_ioachimus": 6,
//         "fabricius_georgius": 1
//       },
//       "soph": {
//         "meggisserus_hieronymus": 1,
//         "naogeorgus_thomas": 1,
//         "pierius_christianus": 1,
//         "pontanus_sebastianus": 1,
//         "scheggius_iacobus": 1,
//         "secceruitius_ioannes": 1,
//         "sturmius_ioannes": 1,
//         "toxites_michael": 1
//       }
//     }
//   },
//   "1559": {
//     "basilea": {
//       "eschl": {
//         "garbitius_mathias": 4,
//         "toxites_michael": 1
//       }
//     }
//   },
//   "1560": {
//     "lugdunum": {
//       "ter": {
//         "antesignanus_petrus": 2
//       }
//     }
//   },
//   "1561": {
//     "ultraiectum": {
//       "ar": {
//         "hortensius_lambertus": 8
//       }
//     }
//   },
//   "1562": {
//     "basilea": {
//       "eur": {
//         "gallotus_nicolaus": 1,
//         "gasparus_stiblinus": 1,
//         "stiblinus_gasparus": 17,
//         "stiblinus_stiblinus": 1
//       }
//     },
//     "francofortum": {
//       "eur": {
//         "xylandrus_guilelmus": 1
//       }
//     }
//   },
//   "1566": {
//     "lipsia": {
//       "sen": {
//         "auantius_hieronymus": 1,
//         "fabricius_georgius": 2,
//         "gregorius_lilius": 1
//       }
//     }
//   },
//   "1567": {
//     "argentorum": {
//       "eur": {
//         "sturmius_ioannes": 1
//       }
//     },
//     "geneua": {
//       "eur": {
//         "stephanus_henricus": 1
//       },
//       "soph": {
//         "ratallerus_georgius": 1
//       }
//     }
//   },
//   "1568": {
//     "lutetia": {
//       "soph": {
//         "stephanus henricus": 1
//       }
//     }
//   },
//   "1570": {
//     "antuerpia": {
//       "soph": {
//         "falkenburgius_gerartus": 1,
//         "grudius_nicolaus": 1,
//         "mylius_adrianus": 1,
//         "polites_ioachimus": 1,
//         "ratallerus_georgius": 2
//       }
//     }
//   },
//   "1571": {
//     "antuerpia": {
//       "eur": {
//         "canterus_guilelmus": 4
//       }
//     }
//   },
//   "1574": {
//     "augusta_uindelicorum": {
//       "soph": {
//         "gardesius_ioannes": 1
//       }
//     }
//   },
//   "1576": {
//     "antuerpia": {
//       "sen": {
//         "delrio_hieronymus": 1,
//         "delrio_martinus": 2,
//         "descobar_augustinus": 1,
//         "florentius_ioannes": 1,
//         "mekerchius_adolphus": 1,
//         "nansius_franciscus": 1
//       }
//     },
//     "lutetia": {
//       "pl": {
//         "auratus_ioannes": 1,
//         "helias_iacobus": 1,
//         "lambinus_germanus": 1
//       }
//     }
//   },
//   "1577": {
//     "argentorum": {
//       "eur": {
//         "calaminus_georgius": 1
//       }
//     }
//   },
//   "1579": {
//     "antuerpia": {
//       "soph": {
//         "canterus_guilelmus": 2
//       }
//     }
//   },
//   "1580": {
//     "antuerpia": {
//       "eschl": {
//         "canterus_guilelmus": 3
//       },
//       "undefined": {}
//     }
//   },
//   "1581": {
//     "antuerpia": {
//       "eur": {
//         "ratallerus_georgius": 5
//       }
//     },
//     "rostochium": {
//       "eschl": {
//         "caselius_ioannes": 2
//       }
//     },
//     "uitteberga": {
//       "eur": {
//         "albinus_petrus": 5,
//         "mencius_balthasar": 1,
//         "nucelius_georgius": 1,
//         "utenhouius_carolus": 1,
//         "wanckelius_ioannes": 1
//       }
//     }
//   },
//   "1583": {
//     "praha": {
//       "soph": {
//         "codicillus_petrus": 2
//       }
//     }
//   },
//   "1584": {
//     "argentorum": {
//       "soph": {
//         "camerarius_ioachimus": 1
//       }
//     }
//   },
//   "1585": {
//     "lutetia": {
//       "eschl": {
//         "christianus_florens": 2,
//         "melissus_paulus": 1,
//         "morellus_federicus": 1
//       }
//     }
//   },
//   "1586": {
//     "francofortum": {
//       "ar": {
//         "baderus_matthaeus": 1,
//         "frenzelius_salomon": 1,
//         "frischlinus_nicodemus": 2,
//         "modius_franciscus": 1,
//         "pithopoeius_lambertus": 1,
//         "rosbachius_ioannes": 1,
//         "sylburgius_fridericus": 2
//       }
//     },
//     "lutetia": {
//       "soph": {
//         "auratus_ioannes": 1,
//         "christianus_florens": 3,
//         "gulonius_nicolaus": 1
//       }
//     }
//   },
//   "1587": {
//     "lugdunum": {
//       "pl": {
//         "benedicti_georgius": 1,
//         "bredius_franciscus": 1,
//         "burchius_adrianus": 1,
//         "dousa_ianus_f": 2,
//         "gruterus_ianus": 1
//       }
//     }
//   },
//   "1588": {},
//   "1589": {
//     "antuerpia": {
//       "sen": {
//         "dousa_ianus_f": 1,
//         "lipsius_iustus": 1,
//         "raphelengius_franciscus_f": 3
//       }
//     },
//     "heidelberga": {
//       "sen": {
//         "commelinus_hieronymus": 1,
//         "posthius_ioannes": 1
//       }
//     },
//     "lutetia": {
//       "ar": {
//         "auratus_ioannes": 1,
//         "christianus_florens": 3,
//         "morellus_federicus": 1
//       }
//     }
//   },
//   "1592": {},
//   "1593": {
//     "barcinona": {
//       "eur": {
//         "aegidius_petrus": 1,
//         "dimas_ioannes": 1
//       }
//     }
//   },
//   "1594": {
//     "lugdunum_batauorum": {
//       "eur": {
//         "christianus_florens": 6
//       }
//     }
//   },
//   "1595": {
//     "rostochium": {
//       "eur": {
//         "posselius_ioannes": 1
//       }
//     }
//   },
//   "1597": {
//     "heidelberga": {
//       "eur": {
//         "identifiant_auteur": 1
//       }
//     }
//   },
//   "1600": {
//     "lugdunum": {
//       "ar": {
//         "raphelengius_christophorus": 1
//       }
//     }
//   },
//   "1602": {
//     "geneua": {
//       "eur": {
//         "sapinus_baptista": 1,
//         "stephanus_paulus": 1
//       }
//     }
//   },
//   "1604": {
//     "heidelberga": {
//       "sen": {
//         "dousa_ianus": 1,
//         "gruterus_ianus": 1
//       }
//     }
//   },
//   "1605": {
//     "uitteberga": {
//       "pl": {
//         "blansdorff_ioannes": 1,
//         "cremcouius_ualentinus": 1,
//         "gentilis_scipio": 1,
//         "peiferus_dauidus": 1,
//         "rhodomanus_laurentius": 1,
//         "rittershusius_cunradus": 1,
//         "sebaldi_uitus": 1,
//         "siberus_adamus": 1,
//         "taubmanus_fridericus": 2,
//         "treutlerus_hieronymus": 1
//       }
//     }
//   },
//   "1607": {
//     "geneua": {
//       "ar": {
//         "christianus_florens": 1,
//         "portus_aemilius": 3
//       }
//     }
//   },
//   "1609": {},
//   "1611": {
//     "lugdunum_batauorum": {
//       "sen": {
//         "heinsius_danielis": 3
//       }
//     }
//   },
//   "1613": {
//     "londinium": {
//       "sen": {
//         "audoenus_ioannes": 1,
//         "farnabius_thomas": 2,
//         "jonson_ben": 1,
//         "tomkins_nathaniel": 2,
//         "wheare_degory": 1,
//         "whitakerus_laurentius": 1
//       }
//     }
//   },
//   "1625": {
//     "lugdunum_batauorum": {
//       "ar": {
//         "maire_ioannes": 2
//       }
//     }
//   }
// }
