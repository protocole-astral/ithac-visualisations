var sanitizer = {
    "body": "xmlbody",
    "head": "xmlhead",

    '<title xml:lang="la"/>': '<title xml:lang="la"/></title>',
    '<title xml:lang="grc"/>': '<title xml:lang="grc"/></title>',

    "<ab": "<p",
    "</ab>": "</p>"
}
var sanitizer_for_html = {
    "<head": "<xmlhead",
    "</head>": "</xmlhead>",

    "<ab": "<p",
    "</ab>": "</p>",

    "<persName": "<span class='persName'",
    "</persName>": "</span>",

    "<seg": "<span class='seg'",
    "</seg>": "</span>",
    "<note": "<span class='note'",
    "</note>": "</span>",

    "<foreign": "<span class='foreign'",
    "</foreign>": "</span>",

    "<title": "<span class='title'",
    "</title>": "</span>",

    "<p/>": "<p></p>"
}
// ajout des regexp
// sanitizer[/<title.*?xml:lang="(.*?)"[^\>]+>/g] = '<title xml:lang="$1"></title>';

var data_model = {
    "paratxt_title": {
        "definition": "Titre du paratexte",
        "path": "tei teiHeader fileDesc titleStmt title",
        "readable_value": "innerHTML",
        "attribute_value": undefined,
        "is_translatable": false,
        "is_mutiple": false
    },
    "paratxt_lang": {
        "definition": "Langue du paratexte",
        "path": "tei teiHeader fileDesc titleStmt title",
        "alternative_path": "tei teiHeader fileDesc titleStmt title[xml\\:lang]",
        "readable_value": undefined,
        "attribute_value": "xml:lang",
        "is_translatable": false,
        "is_mutiple": false,
        "alignment": {
            "la": "Latin",
            "grc": "Grec"
        }
    },
    "paratxt_author": {
        "definition": "Auteur du paratexte",
        "path": "tei teiHeader fileDesc titleStmt author",
        "readable_value": "innerHTML",
        "attribute_value": "ref",
        "is_translatable": false,
        "is_mutiple": false
    },
    "paratxt_pubplace": {
        "definition": "Lieu de publication",
        "path": "tei teiHeader fileDesc sourceDesc bibl pubPlace",
        "readable_value": "innerHTML",
        "attribute_value": "ref",
        "is_translatable": false,
        "is_mutiple": false
    },
    "paratxt_date": {
        "definition": "Date",
        "path": "tei teiHeader fileDesc sourceDesc bibl date",
        "readable_value": "innerHTML",
        "attribute_value": "when",
        "is_translatable": false,
        "is_mutiple": false
    },
    "paratxt_publisher": {
        "definition": "Imprimeur et/ou libraire",
        "path": "tei teiHeader fileDesc sourceDesc bibl publisher",
        "readable_value": "innerHTML",
        "attribute_value": "ref",
        "is_translatable": false,
        "is_mutiple": false
    },
    "paratxt_editor": {
        "definition": "Éditeur scientifique/Traducteur",
        "path": "tei teiHeader fileDesc sourceDesc bibl editor",
        "readable_value": "innerHTML",
        "attribute_value": "ref",
        "is_translatable": false,
        "is_mutiple": true
    },
    "paratxt_head": {
        "definition": "",
        "path": "tei text xmlbody xmlhead",
        "readable_value": "innerHTML",
        "attribute_value": undefined,
        "is_translatable": true,
        "is_mutiple": false
    },
    "paratxt_ab": {
        "definition": "",
        "path": "tei text xmlbody p",
        "readable_value": "innerHTML",
        "attribute_value": undefined,
        "is_translatable": true,
        "is_mutiple": false
    },
    "paratxt_ana": {
        "definition": "Encodage sémantique",
        "path": "tei text *",
        "readable_value": undefined,
        "attribute_value": "ana",
        "is_list_of_values": true,
        "is_translatable": false,
        "is_mutiple": false,
        "alignment": {
            "TH": "Théâtre",
            "TH_dram": "Théâtre - Genre",
            "TH_repr": "Théâtre - Représentation",
            "TH_hist": "Théâtre - Histoire",
            "TH_fin": "Théâtre - Finalité",
            "RH": "Rhétorique",
            "PH": "Philologie",
            "PE": "Pédagogie",
            "M": "Métrique",
            "T": "Traduction",
            "PA": "Paratexte",
            "PHILO": "Philosophie"
        }
    },
    "ancient_title": {
        "definition": "Titre de l’œuvre",
        "path": "tei teiHeader fileDesc sourceDesc bibl title",
        "readable_value": "innerHTML",
        "attribute_value": undefined,
        "is_translatable": false,
        "is_mutiple": false
    },
    "ancient_author": {
        "definition": "Auteur",
        "path": "tei teiHeader fileDesc sourceDesc bibl author",
        "readable_value": "innerHTML",
        "attribute_value": "ref",
        "is_translatable": false,
        "is_mutiple": false
    }
}
var hierarchical_choices = [
    "paratxt_title",
    "paratxt_lang",
    "paratxt_author",
    "paratxt_pubplace",
    "paratxt_date",
    "paratxt_publisher",
    "paratxt_editor",
    "paratxt_ana",
    "ancient_title",
    "ancient_author"
];