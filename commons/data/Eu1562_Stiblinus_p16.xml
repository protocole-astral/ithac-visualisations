<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="../encodageTEI/ITHAC_odd.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="../encodageTEI/ITHAC_odd.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">

    <teiHeader>

        <fileDesc>

            <titleStmt>

                <title xml:lang="la">Argumentum Et Praefatiuncula Gaspari Stiblini In Euripidis
                    Rhesum</title>
                <author ref="#stiblinus_gasparus">Gasparus Stiblinus</author>
                <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence en minuscules, précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->

                <respStmt>
                    <resp>Transcription</resp>
                    <persName>Sarah GAUCHER</persName>
                </respStmt>
                <respStmt>
                    <resp>Traduction</resp>
                    <persName>Christian NICOLAS</persName>
                </respStmt>
                <respStmt>
                    <resp>Encodage</resp>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <principal>Malika BASTIN-HAMMOU</principal>
                <respStmt>
                    <resp>Modélisation et structuration</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Elysabeth HUE-GAY</persName>
                </respStmt>
                <respStmt>
                    <resp>Première version du modèle d'encodage</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Anne Garcia-Fernandez</persName>
                </respStmt>
            </titleStmt>

            <publicationStmt>
                <publisher/>
                <availability>
                    <p>Le site est en accès restreint</p>
                </availability>
                <pubPlace>ithac.elan-numerique.fr</pubPlace>
            </publicationStmt>
            <sourceDesc>
                <bibl>
                    <title>Euripides Poeta Tragicorum princeps, in Latinum sermonem conuersus,
                        adiecto eregione textu Graeco : CUM ANNOTATIONIBUS ET PRAEFATIONIBUS in
                        omnes eius Tragoediae, autore GASPARO STIBLINO, Accesserunt JACOBI MICYLLI,
                        de Euripidis uita, ex diuersis autoribus collecta : item, De tragoedia et
                        eius partibus προλεγομένα quaedam. Item IOANNIS BRODAEI Turonensis
                        Annotationes doctissimae numquam antea in lucem editae. Ad haec Rerum et
                        uerborum toto Opere Praecipue memorabilium copiosus INDEX. Cum caesaris
                        Maiestatis et Christianissimi Gallorum Regis gratia ac priuilegio, ad
                        decennium. Basileae, per Ioannem Oporinum.</title>
                    <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->
                    <author ref="#eur">Euripides</author>
                    <pubPlace ref="#basilea">Basilea</pubPlace>
                    <publisher ref="#oporinus_ioannes">Iohannes Oporinus</publisher>
                    <date when="1562">1562</date>
                    <!-- dans l'attribut @when, l'année en chiffre arabes -->
                    <editor ref="#stiblinus_gasparus">Gasparus Stiblinus</editor>
                    <editor role="traducteur" ref="#stiblinus_gasparus">Gasparus Stiblinus</editor>
                    <!-- pour les liens google book, remplacer les & par &amp; pour que ça fonctionne -->
                </bibl>

                <listBibl>
                    <bibl/>
                    <!-- créer un <bibl> par référence -->
                </listBibl>

            </sourceDesc>
        </fileDesc>

        <profileDesc>

            <abstract>
                <p/>
            </abstract>

            <langUsage>
                <!-- ne pas modifier -->
                <language ident="fr">Français</language>
                <language ident="la">Latin</language>
                <language ident="grc">Grec</language>
            </langUsage>

        </profileDesc>
        <revisionDesc>
            <change when="2022-02-19">Sarah GAUCHER : identifiants Header, mise en conformité
                Elan</change>
            <change when="2021-12-15">Sarah GAUCHER : Encodage de la transcription et de la
                traduction</change>
            <change when="2021-11-19">Sarah GAUCHER : création de la fiche TEI et remplissage du
                Header</change>
        </revisionDesc>
    </teiHeader>

    <text>
        <body>

            <head type="orig" xml:id="Eu1562_Stiblinus_p16_0">Argumentum et Praefatiuncula <persName
                    ref="#stiblinus_gasparus">Gaspari Stiblini</persName> In <persName ref="#eur"
                    >Euripidis</persName>
                <title ref="#eur_rhes">Rhesum</title></head>
            <head type="trad" corresp="#Eu1562_Stiblinus_p16_0">Argument et petite préface de Gaspar
                Stiblinus sur le <title>Rhésus</title> d’Euripide.</head>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_1"><seg type="paraphrase"> Rhesus Thrax dux
                    Byzantiorum <seg type="incise">(ut Suidas tradit)</seg> bellicis artibus
                    rebusque fortiter gestis inter suos clarissimus extitit. <bibl resp="#SG"
                            ><author ref="#suid">Suid.</author>, <title ref="#suid_l"
                            >Lexicon</title>, <biblScope>ρ 146</biblScope>.</bibl>
                </seg></ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_1">Rhésus le Thrace, chef des Byzantins
                (selon <seg>Suidas<note>Suidas ou Souda, est le nom d’un texte byzantin (plutôt que
                        d’un auteur) encyclopédique rangé alphabétiquement.</note></seg>), par sa
                technique à la guerre et des exploits héroïques, était le plus célèbre des
                siens.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_2">Is, cum bellum Troianum iam non Asiam
                solum sed totum paene orbem terrarum concuteret, ut cui admiscerentur omnium gentium
                et nationum reges ac duces, multis legationibus ab Hectore sollicitatus ad Ilium
                ualido cum exercitu uenit: serius tamen Hectoris expectatione quod prius regni sui
                fines aduersus Scytharum incursiones tueri necesse habuerit.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_2">Alors que la guerre de Troie frappait
                non seulement l’Asie mais presque la terre entière, au point qu’on voyait s’y
                joindre les rois et chefs de tous pays et nations, sollicité par plusieurs
                ambassades envoyées par Hector, il se rendit à Ilion avec sa vaillante armée, mais
                trop tard au gré d’Hector, parce qu’il avait dû d’abord protéger les frontières de
                son royaume contre les incursions des Scythes.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_3">Prima autem nocte in Troianorum castris,
                cum iam certam spem uictoriae concepisset seque cum suis quieti dedisset, nihil
                periculi metuens omniumque rerum securus (quippe iam fractas Graecorum res
                dictitabant), a Diomede et Vlysse ductu Mineruae obtruncatur.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_3">Or lors de sa première nuit au camp
                troyen, alors qu’il était presque sûr de la victoire et qu’il s’était abandonné,
                avec les siens, au sommeil, sans craindre aucun danger et entièrement en sécurité
                (car la puissance grecque, disait-on, était brisée), Diomède et Ulysse, sous la
                conduite de Minerve, le décapitent.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_4">Iuno enim Graecorum rebus timens propter
                aduentum Thracum misit Palladem, quae in exitium Rhesi istos iam dictos heroas
                excitet: a quibus interficitur et Dolon exploratum a Troianis missus hortatu Aeneae,
                qui Hectoris arbitrantis fugam moliri Graecos ardorem moderatur reuocatque eum a
                nimis callido consilio, quo uniuersis copiis oppugnare noctu munitiones Graecorum
                omnesque hostes in fuga opprimere constituebat.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_4">Car Junon, craignant pour les affaires
                des Grecs à cause de l’arrivée des Thraces, envoya Pallas pour inciter les héros
                cités à tuer Rhésus ; il est donc tué par eux et Dolon fut envoyé en éclaireur par
                les Troyens, à l’instigation d’Énée, qui tempère l’ardeur d’Hector persuadé que les
                Grecs préparent leur fuite et le dissuade de mettre en œuvre un plan trop rusé
                consistant à attaquer avec toutes ses forces, de nuit, les retranchements des Grecs,
                de mettre en fuite et d’écraser tous les ennemis.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_5">Terpsichore, uel (ut alii) Euterpe
                mater, funus filii curat.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_5"><seg>Terpsichore ou (selon d’autres)
                    Euterpe, sa mère<note>La scholie à Eur. <title>Rhes.</title> 346 nomme comme
                        mère de Rhésus Clio ou Euterpe, mais Aristophane de Byzance cite le nom de
                        Terpsichore. Dans les trois cas, Rhésus est le fils d’une
                Muse.</note></seg>, s’occupe des funérailles de son fils.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_6">Acerbum autem dolorem tam Troianis quam
                Thracibus fortissimi principis inopinatus ac miserabilis casus attulit.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_6">Or le sort inopiné et pitoyable de ce
                prince très vaillant suscita chez les Troyens et chez les Thraces une douleur
                amère.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_7">Sunt qui scribant Rhesum primo die
                secundo marte pugnasse multosque Graecos fudisse, ac secunda demum nocte insidiis
                exploratorum iugulatum periisse.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_7">Certains écrivent que Rhésus combattit
                le premier jour avec grand succès, qu’il défit de nombreux Grecs et que, la seconde
                nuit précisément, il mourut égorgé, piégé par des éclaireurs.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_8">Ceterum hoc totum argumentum sumptum est
                ex <title ref="#hom_il">Iliad.</title>
                <foreign xml:lang="grc">κ</foreign>. </ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_8">D’ailleurs tout cet argument est tiré
                de <title>L’Iliade</title> chant 10.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_9">Haec de Argumento, nunc de scopo huius
                fabulae.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_9">Voici pour l’argument, voyons
                maintenant l’objectif de cette pièce.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_10">In hoc dramate proponitur pulcherimma
                quaedam imago militaris oeconomiae, siue <foreign xml:lang="grc"
                    >διοικήσεως</foreign>: in qua non tam ualent uis et prompta audacia
                praesentesque in periculis animi quam tuta consilia ac mentes cautae futurorumque
                praesagae.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_10">Dans ce drame est proposée une très
                belle image d’organisation militaire, ou dioikêsis, dans laquelle ce qui a de la
                valeur n’est pas tant l’audace téméraire ou le courage constant dans le danger que
                les plans sûrs et les intelligences prudentes qui anticipent l’avenir.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_11">Hector totus calet amore pugnandi, nec
                animus nec uires desunt: at Aeneae prudentia sedata quomodo res et recte et tuto
                geri possit perspicit.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_11">Hector est tout ardent du désir de
                combattre et il ne manque ni de courage ni de forces, mais la prudence apaisée
                d’Énée examine comment l’affaire pourrait être menée bien et sûrement.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_12">Hectores isti feruidi saepe maximos
                exercitus in discrimen adducunt hostibusque sua temeritate uictoriam ueluti
                propinant.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_12">Ce sont ces Hector bouillants qui
                souvent conduisent leurs armées au point critique et par leur aveuglement, offrent,
                pour ainsi dire, la victoire aux ennemis.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_13">Sed Aeneae ingenii solertia prudentique
                consilio nonnumquam imbecilles robustissimas copias fuderunt.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_13">Mais des Énée à l’intelligence habile
                et aux conseils avisés, ont parfois, malgré leur faiblesse, défait des forces très
                puissantes.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_14">Vnde <persName ref="#hom"
                    >Homerus</persName> prudentiam coniunctam cum uiribus et uirtute militari esse
                debere hoc uersu docet :</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_14">C’est pourquoi Homère montre dans ce
                vers qu’il faut que la prudence se combine avec la force et le courage militaires
                :</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_15"><cit type="refauteur">
                    <quote xml:lang="grc">ἀμφότερον, βασιλεύς τ’ ἀγαθὸς κρατερός τ’
                        αἰχμητής.</quote>
                    <bibl resp="#SG"><author ref="#hom">Hom.</author>
                        <title ref="#hom_il">Il.</title>
                        <biblScope>3.179</biblScope>.</bibl>
                </cit></ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_15">« les deux en même temps, bon roi,
                vaillant soldat ».</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_16">Lege huc illam apud <persName ref="#ou"
                    >Ouidium</persName> in 12. <title ref="#ou_m">Transform.</title>
                <seg type="paraphrase"> Vlyssis et Aiacis super armis Achillis contentionem <bibl
                        resp="#SG"><author ref="#ou">Ov.</author>, <title ref="#ou_m">Met.</title>
                        <biblScope>12.620-13.383</biblScope></bibl>
                </seg> : in qua egregia comparatio bonorum animi (quae sunt sapientia, eloquentia,
                solertia, et quaedam omnium affectuum moderatio) et corporis, quae sunt robor,
                duritia, et laborum patientia, sanitas, unde immodica propriarum uirium uenit
                fiducia.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_16">Va lire chez Ovide,
                    <title>Met.</title> 12 le débat entre Ulysse et Ajax sur les armes d’Achille ;
                on y trouve une excellente comparaison entre les qualités d’esprit (que sont la
                sagesse, l’éloquence, l’habileté et cette modération de toutes les passions) et
                celles du corps (que sont la robustesse, la dureté, l’endurance, la santé), d’où
                vient une excessive confiance en ses propres forces.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_17">Formam luculentam utriusque uirtutis
                tam bellicae quam politicae <title ref="#xen_cyr">Cyri paedia</title> apud <persName
                    ref="#xen">Xenophontem</persName> adumbrat.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_17">La brillante beauté des deux
                qualités, celle de chef de guerre et celle d’homme d’état, est esquissée dans la
                    <title>Cyropédie</title> de Xénophon.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_18">Meminerit etiam adolescens hic exhiberi
                imaginem quandam uitae militaris, in qua omnia incerta, periculosa, turbida, plena
                insidiarum: item euentus rerum insperati et subiti, ac plurima mortis imago ob
                oculos uersatur.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_18">Le jeune homme se souviendra qu’on y
                montre une certaine image de la vie de soldat, où tout est incertain, dangereux,
                troublé, plein de pièges ; en outre, les événements prennent un tour inespéré et
                rapide, et on a sous les yeux une fréquente image de la mort.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_19">Quorum tamen incommodorum maior pars
                ducum industria decidi potest.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_19">Et pourtant une bonne part de ces
                inconvénients peut être supprimée par le zèle des chefs.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_20">Nimia autem securitas et fiducia Rheso
                inopinatae necis causa extitit: quae duae res saepe faciunt ut maximas calamitates
                ualidissimi exercitus accipiant.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_20">Une tranquillité et une confiance
                excessives ont causé la mort inopinée de Rhésus, deux états qui font souvent advenir
                d’immenses catastrophes aux armées les plus fortes.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_21"><persName ref="#cic">Cicero</persName>
                in <title ref="#cic_pomp">oratione pro lege Manilia</title><cit type="hyperref">
                    <quote><said direct="false">has fere dotes in summo imperatore requirit:
                            felicitatem, scientiam rei militaris, uirtutem, auctoritatem: item
                            laborem in negotiis, fortitudinem in periculis, industriam in agendo,
                            celeritatem in conficiendo, consilium in prouidendo.</said></quote>
                    <bibl resp="#SG"><author ref="#cic">Cic.</author>, <title ref="#cic_pomp"
                            >Pomp.</title>
                        <biblScope>28-29</biblScope>.</bibl>
                </cit></ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_21">Cicéron, dans son <title>Disours pour
                    la loi Manilia</title>, demande chez un chef suprême toutes ces qualités :
                chance, stratégie, vertu, autorité, et aussi endurance dans les épreuves, courage
                dans les dangers, zèle dans l’action, rapidité dans l’exécution, intelligence dans
                la prévoyance.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_22">Sed haec hactenus.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_22">Mais en voilà assez.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_23">Argumentum actus primi.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_23">Argument de l’acte 1.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_24">Vigiles excitant Hectorem imperatorem
                nocte intempesta nuntiantque magnos ignes passim per Graecorum castra lucere:
                incertum esse quidnam nouarum rerum moliantur hostes.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_24">Les sentinelles réveillent Hector en
                pleine nuit et lui annoncent que de grands feux brillent partout dans le camp grec :
                on ne sait pas quelle nouvelle ruse trament les ennemis.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_25">Vnde uiros in arma rapiendos suadent ne
                subito improuisi aliquo periculo implicentur.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_25">Ils persuadent donc les hommes de
                prendre les armes pour ne pas être surpris par un danger soudain.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_26">Hector ex his coniecturam facit Graecos
                fugam adornare: iamque ipsos in fuga persequi ac concidere gestit.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_26">Hector, de là, conclut que les Grecs
                préparent leur fuite et il brûle aussitôt les poursuivre dans leur fuite et de les
                massacrer.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_27">Quod dum agitaret cum choro, Aeneas
                interuenit et quid sibi uelint motus isti nocturni in exercitu percontatur:
                cognitaque re prudentissime consulit ne quid praecipitanter ageret neue noctu
                exercitum in discrimen adduceret, praesertim cum nondum satis constet quid hostes
                machinentur.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_27">Pendant qu’il en délibérait avec le
                chœur, Énée intervient et se demande la raison de ces manœuvres nocturnes pour
                l’armée, en apprend le fin mot et conseille fort intelligemment de ne rien
                précipiter ni de mener de nuit l’armée au désastre, surtout sans savoir précisément
                ce que les ennemis trament.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_28">Quin potius exploratorem mittendum ut
                per hunc cognitis hostium consiliis postea quid agendum tutius deliberari
                possit.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_28">Mieux : il faut qu’il fasse envoyer
                un éclaireur pour connaître les intentions des ennemis et ensuite décider ce qu’il
                faut faire.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_29">2. Dolon ultro suam operam offert hac
                in re pacisciturque de praemio cum Hectore seque iam ad ipsum factum accingit.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_29">2. Dolon offre spontanément son aide
                et s’entend avec Hector sur la récompense puis se charge de la mission.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_30">3. Chorus fausta precatione euntem
                Dolonem in manifestum periculum prosequitur, Apollinemque orat ut comes et socius
                esse uelit uiro tam praeclarum facinus obeunti pro patria utque eum uictorem et
                multa Graecorum clade ouantem Troianis reddat.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_30">3. Le chœur, dans une prière
                propitiatoire, accompagne le départ de Dolon vers un danger évident et supplie
                Apollon de bien vouloir suivre et aider un homme qui va au-devant d’un acte si
                glorieux pour sa patrie et de le faire revenir victorieux et sous les hourras de la
                grande défaite des Grecs.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_31">Argumentum actus secundi.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_31">Argument de l’acte II</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_32">Pastor quidam Rhesum Thracum regem
                aduentare per Idae nemora cum ualido ac firmo exercitu nuntiat: quem pastorem Hector
                uix admittit ad colloquium, ut erat uir uehementis animi et qui paucos tolerare
                posset.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_32">Un berger annonce que le roi thrace
                Rhésus s’approche par les bois de l’Ida avec son armée vaillante et forte ; Hector
                peine à accepter ce berger dans la conversation, tant il était homme à l’esprit
                ardent et enclin à supporter peu de gens.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_33">Deinde spe uictoriae ferociens, quam
                iam in manu habere uidebatur, aspernatur auxilia Rhesi, ut cuius opera iam magna ex
                parte confecto bello opus non esset.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_33">Puis rendu impatient par l’espoir de
                la victoire, qu’il croyait tenir en main, il ne tient pas compte de l’apport de
                Rhésus, dans l’idée que son aide, puisque la guerre est quasi-achevée, n’est pas
                nécessaire.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_34">Persuadet tamen Chorus et nuntius ne
                fastidiret regem sed reciperet.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_34">Cependant le chœur et un messager
                persuadent le roi de ne pas se montrer méprisant et de le recevoir.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_35">2. Chorus aduenientem iam Rhesum cum
                pulchre instructis copiis laeto omine faustisque precationibus excipit.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_35">2. Le chœur accueille l’arrivée de
                Rhésus et de ses troupes bien rangées avec des paroles de bon augure et des prières
                propitiatoires.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_36">Argumentum actus tertii.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_36">Argument de l’acte 3.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_37">Actus tertius continet congressum Rhesi
                et Hectoris, in quo Hector primum cum Rheso expostulat eique ingratitudinem obiicit,
                ut qui multis antea prouocatus beneficiis bello difficillimo rebus ita accisis sero
                ueniat auxilio.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_37">L’acte 3 contient la rencontre de
                Rhésus et d’Hector, lors de laquelle Hector commence par se plaindre de Rhésus et
                lui reproche son ingratitude, lui qui, souvent sollicité, et avec la promesse de
                nombreux bénéfices, à participer à une guerre très dure, vient porter un secours
                tardif quand la situation est tranchée.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_38">Contra Rhesus culpam morae longioris in
                Scythicum bellum transfert, quod se ad Troiam properantem retardarit.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_38">Mais Rhésus repousse la faute sur la
                longue période qu’il a dû consacrer à une guerre contre les Scythes, qui a retardé
                son départ imminent pour Troie.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_39">Deinde operam suam expromptam offert
                uictoriamque Hectori suorum nauitate se pariturum pollicetur, applaudente interim
                Choro alacritati et potentiae Thracis.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_39">Puis il offre son aide immédiate et
                promet à Hector qu’avec l’activité de ses troupes il lui apportera la victoire,
                pendant que le chœur applaudit l’ardeur et la puissance du Thrace.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_40">2. Idem Rhesus multis uerbis praedicat
                quae in Graecos designare uelit facinora atrocemque minatur uindictam: idque
                stolide, nimirum more humano, quo futurorum casuum ignari et incauti, dum saepe
                nimium nostris rebus fidimus, in ipsum ruimus exitium.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_40">2. Le même Rhésus vante longuement
                les actions qu’il réserve aux Grecs et les menace d’une vengeance atroce, le tout
                bêtement et suivant un usage tout humain qui nous fait, dans notre ignorance du
                futur et notre imprévoyance, dans ce fréquent excès de confiance en nos chances,
                courir à notre propre ruine.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_41">Cuius tamen fiduciam Hector moderari
                cupit, ut cui notior esset Graecorum uis ac fortitudo, quam toties periclitatus erat
                eique tandem post longum colloquium stationis locum assignat.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_41">Pourtant Hector cherche à modérer sa
                confiance, en homme qui connait la valeur et la force des Grecs, dont il a si
                souvent fait l’essai, et finit, après un long dialogue, par lui assigner un endroit
                où camper.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_42">3. Chorus uigilum dispensat custodias,
                quarum uices more militari uariant.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_42">3. Le chœur répartit les tours de
                garde, dont il fixe l’alternance selon l’usage militaire.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_43">Argumentum actus quarti</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_43">Argument de l’acte 4.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_44">Disceptantibus inter se exploratoribus
                Graecis, Vlysse uidelicet et Diomede (quorum hic ulterius progrediendum et ad
                Hectoris aut Paridis tentorium contendendum suadet: ille contentus Dolonis caede
                cautus ac sibi metuens ad naues redire cupit), Pallas interuenit et eos ad
                occidendum Rhesum per insidias, quibus facile noctis beneficio ad hanc rem
                designandam uti possint, hortatur, ac quomodo id fieri debeat ostendit.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_44">Il y a débat entre les éclaireurs des
                Grecs, à savoir Ulysse et Diomède : ce dernier prône qu’on aille plus loin et qu’on
                vise la tente d’Hector et celle de Pâris, l’autre, satisfait du meurtre de Dolon,
                prudent et craintif pour lui-même, souhaite rentrer aux navires ; Pallas intervient
                et les pousse à tuer Rhésus par ruse, en profitant des avantages de la nuit pour y
                parvenir et leur montre comment la chose doit se faire.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_45">Suam praeterea iungit operam dum Paridi
                Veneris induta uultus persuadet nihil periculi a Graecis in exercitu esse, iubetque
                eum bono securoque animo quiescere.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_45">Elle leur propose en outre son aide
                en persuadant Pâris, sous l’apparence de Vénus, qu’il n’y a aucun danger à aller
                affronter les Grecs et l’enjoint à se reposer l’esprit tranquille.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_46">2. Dum redeunt Graeci, Vlysses et
                Diomedes, interfecto iam Rheso, incidunt in uigilias, ac plane tum actum fuisset de
                ipsis nisi Vlysses edita tessera militari illos auertisset iam tela uibrantes.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_46">2. À leur retour, les Grecs Ulysse et
                Diomède, après avoir tué Rhésus, tombent sur des sentinelles et c’en était fait
                d’eux si Ulysse, montrant un mot d’ordre militaire, n’avait détourné leurs traits
                déjà vibrants.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_47">Et dum Chorus coniecturis quibusdam
                diuinat Vlyssem fuisse qui tantas turbas in exercitu excitarit, aduenit auriga Rhesi
                nuntiaturus domini sui caedem.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_47">Et le temps que le chœur devine par
                quelques déductions que c’était Ulysse qui provoquait tout ce trouble dans l’armée,
                arrive un aurige pour annoncer le meurtre de Rhésus son maître.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_48">Argumentum actus quinti.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_48">Argument de l’acte 5.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_49">Auriga Rhesi deplorat et suam et domini
                occisi uicem exponitque quomodo necopinato ex insidiis sint oppressi ac huius facti
                ipsos Troianos insimulat.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_49">L’aurige de Rhésus déplore son sort
                et celui de son maître assassiné et raconte comment ils se sont fait prendre à
                l’improviste dans une embûche et il accuse les Troyens eux-mêmes d’en être
                responsables.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_50">2. Hector accensus ira in uigiles
                animaduersurum se minatur, quorum socordiae caedem Rhesi imputabat.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_50">2. Hector furieux contre les
                sentinelles menace de blâme celles dont la lâcheté a causé la mort de Rhésus.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_51">Quam culpam sedulo Chorus
                deprecatur.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_51">Le chœur se défend vivement de la
                faute.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_52">Auriga uero Hectoris ad aedes ducitur
                male saucius ac curae committitur medicorum.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_52">L’aurige est conduit au palais
                d’Hector gravement blessé et il est confié aux soins des médecins.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_53">3. Musa complexa filium caesum deplorat
                miserabile funus ac rem tristem luget execraturque auctores caedis filii.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_53">3. La Muse embrasse son fils
                massacré, déplore son deuil lamentable, pleure un événement si funeste et maudit les
                auteurs du meurtre de son fils.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_54">Deinde <foreign xml:lang="grc"
                    >παρέργως</foreign> externa quaedam, non tamen aliena a praesenti argumento, de
                educatione, genere et fortuna Rhesi exponit immortalitatemque filio impetraturam se
                dicit ac eam caedem altera cuiusdam Graeci herois (Achillis nimirum) caede
                compensandam.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_54">Puis d’une façon accessoire, elle
                expose des faits externes, qui ne sont pourtant pas sans rapport au présent
                argument, sur l’éducation, la race et la fortune de Rhésus et elle dit qu’elle
                obtiendra l’immortalité pour son fils et que cette mort sera vengée par celle d’un
                héros grec (Achille évidemment).</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p16_55">Hector interea exercitum in aciem et
                pugnas aduersus Graecos educere parat.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p16_55">Entre temps Hector fait sortir
                l’armée en ordre de bataille et part au combat contre les Grecs.</ab>


        </body>
    </text>
</TEI>
