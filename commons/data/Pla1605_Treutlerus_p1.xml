<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">

    <teiHeader>

        <fileDesc>

            <titleStmt>

                <title xml:lang="la">Diua parens Latii, late dominata per Orbem</title>
                <!-- Indiquer la langue du paratexte dans l'attribut xml:lang : "la" pour le latin et "grc" pour le grec -->
                <!-- Attention ! ne pas faire de retour charriot dans le titre -->
                <author ref="#treutlerus_hieronymus">Hieronymus Treutlerus</author>
                <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence en minuscules, précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->

                <!-- Déclarations de responsabilité : indiquent qui a effectué quelle tâche -->
                <!-- Ajouter autant de tâches (<respStmt>) que nécessaire -->
                <respStmt>
                    <resp>Transcription</resp>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <respStmt>
                    <resp>Traduction</resp>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <respStmt>
                    <resp>Révision</resp>
                    <name>Christian NICOLAS</name>
                    <name>Mathieu FERRAND</name>
                </respStmt>
                <respStmt>
                    <resp>Encodage</resp>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <!-- Attention ! Ne pas modifier les tâches ci-dessous -->
                <respStmt>
                    <resp>Modélisation et structuration</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Elysabeth HUE-GAY</persName>
                </respStmt>
                <principal>Malika BASTIN-HAMMOU</principal>
            </titleStmt>

            <!-- Informations de publication sur le laboratoire numérique du projet. Ne pas modifier-->
            <publicationStmt>
                <publisher/>
                <availability>
                    <p>Le site est en accès restreint</p>
                </availability>
                <pubPlace>ithac.elan-numerique.fr</pubPlace>
            </publicationStmt>

            <!-- Informations bibliographiques sur l'édition ancienne d'où le paratexte est tiré -->
            <sourceDesc>
                <bibl>
                    <title>M. Acci Plauti Lat. comoediae facile principis fabulae XX superstites,
                        cum novo et luculento commentario doctorum virorum opera Friderici Taubmani,
                        Professoris Acad.</title>
                    <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->
                    <author ref="#pl">Plautus</author>
                    <pubPlace ref="#uitteberga">Vittebergae</pubPlace>
                    <publisher ref="#meissnerus_wolffgangus">Wolffgangus Meissnerus</publisher>
                    <publisher ref="#schurerus_zacharia">Zacharia Schurerus</publisher>
                    <date when="1605">1605</date>
                    <!-- dans l'attribut @when, l'année en chiffre arabes -->
                    <editor ref="#taubmanus_fridericus">Fridericus Taubmanus</editor>
                    <ref
                        target="https://www.google.fr/books/edition/M_AcciI_Plauti_Lat_Comoediae_facile_prin/XjtpAAAAcAAJ?hl=fr&amp;gbpv=1&amp;dq=Risus+magister,+et+leporis+conditor&amp;pg=PP12&amp;printsec=frontcover"
                        >https://www.google.fr/books/edition/M_AcciI_Plauti_Lat_Comoediae_facile_prin/XjtpAAAAcAAJ?hl=fr&amp;gbpv=1&amp;dq=Risus+magister,+et+leporis+conditor&amp;pg=PP12&amp;printsec=frontcover</ref>
                    <!-- pour les liens google book, remplacer les & par &amp; pour que ça fonctionne -->
                </bibl>
                <listBibl>
                    <bibl/>
                </listBibl>
            </sourceDesc>
        </fileDesc>

        <profileDesc>

            <abstract>
                <p>Poème composite, fait de couples d’hexamètres suivis de sénaires iambiques. On
                    retrouve cette curieuse disposition dans un poème de Sylburg dans
                    Ar_Frischlin_1586 et dans cette même édition, dans le poème élogieux de
                    Ruttershius.</p>
            </abstract>

            <langUsage>
                <!-- ne pas modifier -->
                <language ident="fr">Français</language>
                <language ident="la">Latin</language>
                <language ident="grc">Grec</language>
            </langUsage>

        </profileDesc>
        <revisionDesc>
            <change when="2022-03-25">Sarah GAUCHER : crétion de la TEI, encodage de la traduction
                et de la transcription</change>
        </revisionDesc>
    </teiHeader>

    <text>
        <body ana="#PA #PH">
            <ab type="orig" xml:id="Pla1605_Treutlerus_p1_1"><l>Diua parens Latii, late dominata per
                    Orbem,</l><l> dum praeuideret, quod pararent barbari,</l><l> in <persName
                        ref="#pl">Plauti</persName> pluteos et scrinia lecta reponit</l><l>
                    Latinitatis quod fuit purissimum. </l></ab>
            <ab type="trad" corresp="#Pla1605_Treutlerus_p1_1">La déesse mère du Latium, maîtresse
                de la terre tout entière, comme elle prévoyait ce que préparaient les barbares, met
                à l'abri sur les étagères de Plaute et dans ses coffres précieux la fine fleur de la
                langue latine.</ab>
            <ab type="orig" xml:id="Pla1605_Treutlerus_p1_2"><l>Nunc postquam Latiae affulsit lux
                    reddita linguae,</l><l> haec ipsa proma conda rursus eruens</l><l> thesauros
                    ueteres non amplius inuidet Orbi,</l><l> solerter olim quod sagax seruauerat
                    :</l><l> uerum ope <persName ref="#taubmanus_fridericus">Taubmani</persName>
                    cuius bona dextera <persName ref="#pl">Plauto</persName></l><l> paedore
                    obinquinata uincla liberat. </l></ab>
            <ab type="trad" corresp="#Pla1605_Treutlerus_p1_2">À présent que la lumière de la langue
                latine est revenue et brille, cette bibliothècaire prévoyante, déterrant à nouveau
                ses vieux trésors, ne jalouse plus à la Terre ce qu’elle avait habilement mis en
                réserve ; mais avec l’aide de Taubman et grâce à son habileté, elle libère Plaute de
                ses chaînes salies par la crasse.</ab>
            <ab type="orig" xml:id="Pla1605_Treutlerus_p1_3"><l>Ergo dum <persName ref="#pl"
                        >Plautus</persName>, dum lingua Latina uigebit, </l><l><persName
                        ref="#taubmanus_fridericus">Taubmane</persName>, <persName ref="#pl"
                        >Plauti</persName> liberator audies. </l></ab>
            <ab type="trad" corresp="#Pla1605_Treutlerus_p1_3">Donc, tant que Plaute, tant que la
                langue latine, fleuriront, Taubman, tu seras nommé libérateur de Plaute.</ab>
            <ab type="orig" xml:id="Pla1605_Treutlerus_p1_4"><persName ref="#treutlerus_hieronymus"
                    >Hieron. Treulterus</persName> IC. Caesareus Regiusque Consul.</ab>
            <ab type="trad" corresp="#Pla1605_Treutlerus_p1_4">Hieronymus Treulterus, jurisconsult,
                conseiller de l’Empereur et du Roi.</ab>


        </body>
    </text>
</TEI>
