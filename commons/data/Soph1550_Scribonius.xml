<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
    

    <teiHeader>

        <fileDesc>

            <titleStmt>

                <title xml:lang="la">In duas Sophoclis Tragoedias Aiacem Flagelliferum, et
                    Antigonem, a Georgio Rotallero Phrysio uersas, Cornelius Scribonius. </title>
                <author ref="#grapheus_cornelius
                    ">Cornelius Scribonius</author>

                <respStmt>
                    <resp>Transcription</resp>
                    <persName>Diandra CRISTACHE</persName>
                </respStmt>
                <respStmt>
                    <resp>Traduction</resp>
                    <persName>Christian NICOLAS</persName>
                </respStmt>
                <respStmt>
                    <resp>Encodage</resp>
                    <name>Diandra CRISTACHE</name>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <respStmt>
                    <resp>Modélisation et structuration</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Elysabeth HUE-GAY</persName>
                </respStmt>

                <principal>Malika BASTIN-HAMMOU</principal>
            </titleStmt>

            <publicationStmt>
                <publisher/>
                <availability>
                    <p>Le site est en accès restreint</p>
                </availability>
                <pubPlace>ithac.elan-numerique.fr</pubPlace>
            </publicationStmt>

            <sourceDesc>

                <bibl>
                    <title>Sophoclis Aiax Flagellifer, et Antigone. Eiusdem Electra. Georgio
                        Rotallero Interprete.</title>
                    <author ref="#soph">Sophocles</author>
                    <pubPlace ref="#lugdunum">Lugduni</pubPlace>
                    <publisher ref="#gryphius_sebastianus">Sebastianus Gryphus</publisher>
                    <date when="1550">1550</date>
                    <editor ref="#ratallerus_georgius">Georgius Ratallerus</editor>
                </bibl>
            </sourceDesc>
        </fileDesc>

        <profileDesc>

            <abstract>
                <p>Type d'édition : Traduction latine. </p>
                <p>Paratexte : 21 pages ; dédicace (8 pages en latin) ; epistola (9 pages en latin)
                    ; argumentum fabulae (2 pages en latin)</p>
            </abstract>

            <langUsage>
                <language ident="la">Latin</language>
                <language ident="grc">Grec</language>
            </langUsage>

        </profileDesc>

        <revisionDesc>
            <change when="2022-02-17">Sarah GAUCHER : Vérification et ajouts PersName, PlaceName,
                hi, title, quote, seg, foreign. Vérification de la mise en conformité du
                schéma</change>
            <change when="2021-03-29">Diandra CRISTACHE : Mise à jour et chargement sur le
                Pensoir</change>
            <change when="2020-10-20">Diandra CRISTACHE : Encodage de la transcription et de la
                traduction</change>
        </revisionDesc>

    </teiHeader>

    <text>
        <body>

            <head type="orig" xml:id="Soph1550_Scribonius_0">In duas <persName ref="#soph"
                    >Sophoclis</persName> Tragoedias <title ref="#soph_aj">Aiacem
                    Flagelliferum</title>, et <title ref="#soph_aj">Antigonem</title>, a <persName
                    ref="#ratallerus_geogius">Georgio Rotallero Phrysio</persName> uersas, <persName
                    ref="#grapheus_cornelius">Cornelius Scribonius</persName>.</head>
            <head type="trad" corresp="#Soph1550_Scribonius_0">Sur les deux tragédies de Sophocle,
                    <title>Ajax porte-fouet</title> et <title>Antigone</title> traduites par Georg
                Rotaller de Frise, <seg>Cornelius Scribonius<note>Cornelius Scribonius (Grapheus),
                        alias Cornelius Schijver ou Corneille de Schryver (1482-1558), né à Alost,
                        mort à Anvers, est professeur de latin à Anvers, poète et écrivain néolatin.
                    </note></seg>. </head>

            <ab type="orig" xml:id="Soph1550_Scribonius_1">
                <l>O Teucer, ô Tegmessa, tristeis ponite</l>
                <l>Luctus, querelas, lacrimas.</l>
            </ab>
            <ab type="trad" corresp="#Soph1550_Scribonius_1" ana="#PA">Ô Teucer, ô Tecmesse, déposez
                vos sinistres deuils, plaintes, larmes. </ab>

            <ab type="orig" xml:id="Soph1550_Scribonius_2">
                <l>Qui nuper occidit sua ipsius manu</l>
                <l>Iam uester ille Aiax (nece</l>
                <l>Procul) reuixit, mentis ex insania</l>
                <l>Rursum receptis sensibus.</l>
            </ab>
            <ab type="trad" corresp="#Soph1550_Scribonius_2" ana="#TH_dram">Celui qui naguère s’est
                tué de sa main, votre Ajax, s’éloignant de la mort, revit, revenu de sa folie et
                après avoir recouvré ses sens.</ab>

            <ab type="orig" xml:id="Soph1550_Scribonius_3">
                <l>Quin tu quoque heus Creon miserrime omnium</l>
                <l>Regum, dolere desine.</l>
            </ab>
            <ab type="trad" corresp="#Soph1550_Scribonius_3" ana="#TH_dram">Et toi aussi, Créon,
                hélas le plus malheureux de tous les rois, cesse d’avoir mal. </ab>

            <ab type="orig" xml:id="Soph1550_Scribonius_4">
                <l>Tua illa coniux, cum tuo illo filio</l>
                <l>Eiusque sponsa nobili</l>
                <l>Stulta tua tyrannide extincti, nouae</l>
                <l>Vitae fruuntur munere.</l>
            </ab>
            <ab type="trad" corresp="#Soph1550_Scribonius_4" ana="#TH_dram">Car ta femme, avec ton
                fils et sa noble promise, tous tués par ta stupide autorité de tyran, jouissent du
                don d’une seconde vie. </ab>

            <ab type="orig" xml:id="Soph1550_Scribonius_5">
                <l>Hos eruditus ille praedulcissima</l>
                <l>Potens <persName ref="#ratallerus_georgius">Rotallerus</persName> lyra</l>
                <l>More Orpheos reduxit e nigrantibus</l>
                <l>Horrentis Orci faucibus.</l>
            </ab>
            <ab type="trad" corresp="#Soph1550_Scribonius_5" ana="#TH_dram">C’est le savant
                Rotaller, fort de sa lyre si suave, qui, tel Orphée, les a fait revenir du gouffre
                sombre de l’horrible Orcus. </ab>

            <ab type="orig" xml:id="Soph1550_Scribonius_6">
                <l>Quique antea Sermone sunt usi Attico</l>
                <l>Graiis nitenteis palliis:</l>
                <l>Nunc illius beneficio urbis Romuli</l>
                <l>Ciueis, eunt clari in togis,</l>
                <l>Perinde Romanis loquenteis uocibus,</l>
                <l>Ac si illa in urbe a fasciis</l>
                <l>Siue educati, siue prognati sient</l>
                <l>Latiis utrinque parentibus.</l>
            </ab>
            <ab type="trad" corresp="#Soph1550_Scribonius_6" ana="#TH_dram">Eux qui naguère
                parlaient attique et brillaient dans leur pallium grec, aujourd’hui citoyens romains
                grâce aux bienfaits de Rotaller, vont tout étincelants dans leur toge, parlant avec
                des voix romaines comme si, dès le berceau, ils avaient reçu une éducation ou même
                une naissance de deux parents latins ! </ab>
        </body>
    </text>
</TEI>
