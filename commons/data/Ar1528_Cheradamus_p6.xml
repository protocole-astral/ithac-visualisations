<?xml version="1.0" encoding="UTF-8"?>


<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">

    <teiHeader>

        <fileDesc>

            <titleStmt>

                <title xml:lang="grc">Ιωάννης Χεράδαμος ἐλλογιμωτάτῳ' Ἀυθονίῳ Λαπιθέῳ εὖ
                    πράττειν. </title>
                <!-- Indiquer la langue du paratexte dans l'attribut xml:lang : "la" pour le latin et "grc" pour le grec -->
                <!-- Attention ! ne pas faire de retour charriot dans le titre -->
                <author ref="#cheradamus_ioannes">Johannes Cheradamus</author>
                <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence en minuscules, précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->

                <!-- Déclarations de responsabilité : indiquent qui a effectué quelle tâche -->
                <!-- Ajouter autant de tâches (<respStmt>) que nécessaire -->

                <!-- Tâche n°1 -->
                <respStmt>
                    <resp>Transcription</resp>
                    <name/>
                </respStmt>
                <!-- Tâche n°2 -->
                <respStmt>
                    <resp>Traduction</resp>
                    <name/>
                </respStmt>
                <!-- Tâche n°3 -->
                <respStmt>
                    <resp>Encodage</resp>
                    <name>Sarah GAUCHER</name>
                </respStmt>


                <!-- Attention ! Ne pas modifier les tâches ci-dessous -->
                <principal>Malika BASTIN-HAMMOU</principal>
                <respStmt>
                    <resp>Modélisation et structuration</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Elysabeth HUE-GAY</persName>
                </respStmt>
                <respStmt>
                    <resp>Première version du modèle d'encodage</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Anne Garcia-Fernandez</persName>
                </respStmt>
            </titleStmt>

            <!-- Informations de publication sur le laboratoire numérique du projet. Ne pas modifier-->
            <publicationStmt>
                <publisher/>
                <availability>
                    <p>Le site est en accès restreint</p>
                </availability>
                <pubPlace>ithac.elan-numerique.fr</pubPlace>
            </publicationStmt>

            <!-- Informations bibliographiques sur l'édition ancienne d'où le paratexte est tiré -->
            <sourceDesc>
                <bibl>
                    <title>Aristophanus Eutrapelōtatu Kōmōdiai ennea : Plutos, Nephelai,
                        Batrachoi, Hippeis, Acharnes, Sphēkes, Ornithes, Eirēnē, Ekklēsiazusai.
                        = Aristophanis facetissimi comoedi[a]e nouem : Plutus, Nebul[a]e, Ran[a]e,
                        Equites, Acharnes, Vesp[a]e, Aues, Pax, Concionantes.</title>
                    <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->
                    <author ref="#ar">Aristophanes</author>
                    <pubPlace ref="#lutetia">Lutetiae</pubPlace>
                    <publisher ref="#gormontius_egidius">Egidius Gormontius</publisher>
                    <date when="1528">1528</date>
                    <!-- dans l'attribut @when, l'année en chiffre arabes -->
                    <editor ref="#cheradamus_ioannes">Johannes Cheradamus</editor>
                    <ref
                        target="http://digital.bib-bvb.de/view/bvbmets/viewer.0.6.4.jsp?folder_id=0&amp;dvs=1626286478015~556&amp;pid=13380143&amp;locale=en&amp;usePid1=true&amp;usePid2=true"/>
                    <!-- pour les liens google book, remplacer les & par &amp; pour que ça fonctionne -->
                </bibl>
                <listBibl>
                    <bibl/>
                    <!-- créer un <bibl> par référence -->
                </listBibl>

            </sourceDesc>
        </fileDesc>

        <profileDesc>

            <abstract>
                <p/>
                <!-- s'il s'agit d'une liste, utiliser les éléments ci-dessous -->
                <list>
                    <item/>
                    <item/>
                    <item/>
                </list>
            </abstract>

            <langUsage>
                <!-- ne pas modifier -->
                <language ident="fr">Français</language>
                <language ident="la">Latin</language>
                <language ident="grc">Grec</language>
            </langUsage>

        </profileDesc>
        <revisionDesc>
            <change when="2022-03-03">Sarah GAUCHER : identifiants Header, mise en conformité
                Elan</change>
        </revisionDesc>
    </teiHeader>

    <text>
        <body>

            <!-- RAPPEL : on encode le texte latin ou grec; on n'encode dans le texte de la traduction que les notes d'éditeur (=membres du projet) -->

            <!-- Le couple d'éléments <head> contient le titre ou l'incipit du paratexte -->
            <!-- L'identifiant du TITRE est composé du titre de la fiche TT suivi d'un underscore (_) puis du chiffre 0 -->
            <head type="orig" xml:id="Ar1528_Cheradamus_p6">Ιωάννης Χεράδαμος ἐλλογιμωτάτῳ' Ἀυθονίῳ
                Λαπιθέῳ εὖ πράττειν. </head>
            <!-- Le deuxième élément du couple contient la traduction française du titre ou incipit -->
            <!-- L'identifiant de la traduction est le même que celui du latin, précédé d'un hashtag (#) -->
            <head type="trad" corresp="#Ar1528_Cheradamus_p6"/>

            <!-- Chaque couple d'éléments <ab> contient le texte du paratexte (cf. tableau dans la fiche TT) -->
            <!-- Attention ! Ne pas oublier de mettre à jour les identifiants en rouge ou utiliser le fichier xml qui permet de générer automatiquement les <ab> et leurs identifiants -->
            <!-- L'identifiant de l'UNITE TEXTUELLE est composé du titre de la fiche TT suivi d'un underscore (_) puis du chiffre 1, 2, 3... -->

            <ab type="orig" xml:id="Ar1528_Cheradamus_p6_1"/>
            <ab type="trad" corresp="#Ar1528_Cheradamus_p6_1"/>
            <ab type="orig" xml:id="Ar1528_Cheradamus_p6_2"/>
            <ab type="trad" corresp="#Ar1528_Cheradamus_p6_2"/>
            <ab type="orig" xml:id="Ar1528_Cheradamus_p6_3"/>
            <ab type="trad" corresp="#Ar1528_Cheradamus_p6_3"/>
            <ab type="orig" xml:id="Ar1528_Cheradamus_p6_4"/>
            <ab type="trad" corresp="#Ar1528_Cheradamus_p6_4"/>
            <ab type="orig" xml:id="Ar1528_Cheradamus_p6_5"/>
            <ab type="trad" corresp="#Ar1528_Cheradamus_p6_5"/>
            <ab type="orig" xml:id="Ar1528_Cheradamus_p6_6"/>
            <ab type="trad" corresp="#Ar1528_Cheradamus_p6_6"/>
            <ab type="orig" xml:id="Ar1528_Cheradamus_p6_7"/>
            <ab type="trad" corresp="#Ar1528_Cheradamus_p6_7"/>
            <ab type="orig" xml:id="Ar1528_Cheradamus_p6_8"/>
            <ab type="trad" corresp="#Ar1528_Cheradamus_p6_8"/>
            <ab type="orig" xml:id="Ar1528_Cheradamus_p6_9"/>
            <ab type="trad" corresp="#Ar1528_Cheradamus_p6_9"/>
            <ab type="orig" xml:id="Ar1528_Cheradamus_p6_10"/>
            <ab type="trad" corresp="#Ar1528_Cheradamus_p6_10"/>
            <ab type="orig" xml:id="Ar1528_Cheradamus_p6_11"/>
            <ab type="trad" corresp="#Ar1528_Cheradamus_p6_11"/>
            <ab type="orig" xml:id="Ar1528_Cheradamus_p6_12"/>
            <ab type="trad" corresp="#Ar1528_Cheradamus_p6_12"/>
            <ab type="orig" xml:id="Ar1528_Cheradamus_p6_13"/>
            <ab type="trad" corresp="#Ar1528_Cheradamus_p6_13"/>
            <ab type="orig" xml:id="Ar1528_Cheradamus_p6_14"/>
            <ab type="trad" corresp="#Ar1528_Cheradamus_p6_14"/>
            <ab type="orig" xml:id="Ar1528_Cheradamus_p6_15"/>
            <ab type="trad" corresp="#Ar1528_Cheradamus_p6_15"/>
            <ab type="orig" xml:id="Ar1528_Cheradamus_p6_16"/>
            <ab type="trad" corresp="#Ar1528_Cheradamus_p6_16"/>
            <ab type="orig" xml:id="Ar1528_Cheradamus_p6_17"/>
            <ab type="trad" corresp="#Ar1528_Cheradamus_p6_17"/>
            <ab type="orig" xml:id="Ar1528_Cheradamus_p6_18"/>
            <ab type="trad" corresp="#Ar1528_Cheradamus_p6_18"/>
            <ab type="orig" xml:id="Ar1528_Cheradamus_p6_19"/>
            <ab type="trad" corresp="#Ar1528_Cheradamus_p6_19"/>
            <ab type="orig" xml:id="Ar1528_Cheradamus_p6_20"/>
            <ab type="trad" corresp="#Ar1528_Cheradamus_p6_20"/>
            <ab type="orig" xml:id="Ar1528_Cheradamus_p6_21"/>
            <ab type="trad" corresp="#Ar1528_Cheradamus_p6_21"/>
            <ab type="orig" xml:id="Ar1528_Cheradamus_p6_22"/>
            <ab type="trad" corresp="#Ar1528_Cheradamus_p6_22"/>
            <ab type="orig" xml:id="Ar1528_Cheradamus_p6_23"/>
            <ab type="trad" corresp="#Ar1528_Cheradamus_p6_23"/>
            <ab type="orig" xml:id="Ar1528_Cheradamus_p6_24"/>
            <ab type="trad" corresp="#Ar1528_Cheradamus_p6_24"/>
            <ab type="orig" xml:id="Ar1528_Cheradamus_p6_25"/>
            <ab type="trad" corresp="#Ar1528_Cheradamus_p6_25"/>
            <ab type="orig" xml:id="Ar1528_Cheradamus_p6_26"/>
            <ab type="trad" corresp="#Ar1528_Cheradamus_p6_26"/>
            <ab type="orig" xml:id="Ar1528_Cheradamus_p6_27"/>
            <ab type="trad" corresp="#Ar1528_Cheradamus_p6_27"/>
            <ab type="orig" xml:id="Ar1528_Cheradamus_p6_28"/>
            <ab type="trad" corresp="#Ar1528_Cheradamus_p6_28"/>
            <ab type="orig" xml:id="Ar1528_Cheradamus_p6_29"/>
            <ab type="trad" corresp="#Ar1528_Cheradamus_p6_29"/>
            <ab type="orig" xml:id="Ar1528_Cheradamus_p6_30"/>
            <ab type="trad" corresp="#Ar1528_Cheradamus_p6_30"/>
            <ab type="orig" xml:id="Ar1528_Cheradamus_p6_31"/>
            <ab type="trad" corresp="#Ar1528_Cheradamus_p6_31"/>
            <ab type="orig" xml:id="Ar1528_Cheradamus_p6_32"/>
            <ab type="trad" corresp="#Ar1528_Cheradamus_p6_32"/>
            <ab type="orig" xml:id="Ar1528_Cheradamus_p6_33"/>
            <ab type="trad" corresp="#Ar1528_Cheradamus_p6_33"/>
            <ab type="orig" xml:id="Ar1528_Cheradamus_p6_34"/>
            <ab type="trad" corresp="#Ar1528_Cheradamus_p6_34"/>
            <ab type="orig" xml:id="Ar1528_Cheradamus_p6_35"/>
            <ab type="trad" corresp="#Ar1528_Cheradamus_p6_35"/>
            <ab type="orig" xml:id="Ar1528_Cheradamus_p6_36"/>
            <ab type="trad" corresp="#Ar1528_Cheradamus_p6_36"/>
            <ab type="orig" xml:id="Ar1528_Cheradamus_p6_37"/>
            <ab type="trad" corresp="#Ar1528_Cheradamus_p6_37"/>
            <ab type="orig" xml:id="Ar1528_Cheradamus_p6_38"/>
            <ab type="trad" corresp="#Ar1528_Cheradamus_p6_38"/>
            <ab type="orig" xml:id="Ar1528_Cheradamus_p6_39"/>
            <ab type="trad" corresp="#Ar1528_Cheradamus_p6_39"/>
            <ab type="orig" xml:id="Ar1528_Cheradamus_p6_40"/>
            <ab type="trad" corresp="#Ar1528_Cheradamus_p6_40"/>
            <ab type="orig" xml:id="Ar1528_Cheradamus_p6_41"/>
            <ab type="trad" corresp="#Ar1528_Cheradamus_p6_41"/>
            <ab type="orig" xml:id="Ar1528_Cheradamus_p6_42"/>
            <ab type="trad" corresp="#Ar1528_Cheradamus_p6_42"/>
            <ab type="orig" xml:id="Ar1528_Cheradamus_p6_43"/>
            <ab type="trad" corresp="#Ar1528_Cheradamus_p6_43"/>
            <ab type="orig" xml:id="Ar1528_Cheradamus_p6_44"/>
            <ab type="trad" corresp="#Ar1528_Cheradamus_p6_44"/>
            <ab type="orig" xml:id="Ar1528_Cheradamus_p6_45"/>
            <ab type="trad" corresp="#Ar1528_Cheradamus_p6_45"/>
            <ab type="orig" xml:id="Ar1528_Cheradamus_p6_46"/>
            <ab type="trad" corresp="#Ar1528_Cheradamus_p6_46"/>
            <ab type="orig" xml:id="Ar1528_Cheradamus_p6_47"/>
            <ab type="trad" corresp="#Ar1528_Cheradamus_p6_47"/>
            <ab type="orig" xml:id="Ar1528_Cheradamus_p6_48"/>
            <ab type="trad" corresp="#Ar1528_Cheradamus_p6_48"/>
            <ab type="orig" xml:id="Ar1528_Cheradamus_p6_49"/>
            <ab type="trad" corresp="#Ar1528_Cheradamus_p6_49"/>
            <ab type="orig" xml:id="Ar1528_Cheradamus_p6_50"/>
            <ab type="trad" corresp="#Ar1528_Cheradamus_p6_50"/>

        </body>
    </text>
</TEI>
