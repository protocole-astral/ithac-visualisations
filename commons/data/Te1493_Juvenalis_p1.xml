<?xml version="1.0" encoding="UTF-8"?>


<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">

    <teiHeader>

        <fileDesc>

            <titleStmt>

                <title xml:lang="la">Guido Iuuenalis Germano de Ganeio uiro senatorio bonarumque
                    litterarum amantissimo Salutem Plurimam Dat</title>
                <author ref="#iuuenalis_guido">Guido Juvenalis</author>

                <respStmt>
                    <resp>Transcription</resp>
                    <name>Christian NICOLAS</name>
                </respStmt>
                <respStmt>
                    <resp>Traduction</resp>
                    <name>Christian NICOLAS</name>
                </respStmt>
                <respStmt>
                    <resp>Révision</resp>
                    <name>Laure HERMAND</name>
                </respStmt>
                <respStmt>
                    <resp>Encodage</resp>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <respStmt>
                    <resp>Commentaire</resp>
                    <name>Laure Hermand</name>
                </respStmt>
                <!-- Attention ! Ne pas modifier les tâches ci-dessous -->
                <respStmt>
                    <resp>Modélisation et structuration</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Elysabeth HUE-GAY</persName>
                </respStmt>
                <respStmt>
                    <resp>Première version du modèle d'encodage</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Anne Garcia-Fernandez</persName>
                </respStmt>
                <principal>Malika BASTIN-HAMMOU</principal>
            </titleStmt>

            <!-- Informations de publication sur le laboratoire numérique du projet. Ne pas modifier-->
            <publicationStmt>
                <publisher/>
                <availability>
                    <p>Le site est en accès restreint</p>
                </availability>
                <pubPlace>ithac.elan-numerique.fr</pubPlace>
            </publicationStmt>

            <sourceDesc>

                <bibl>
                    <title>Guidonis Iuuenalis natione Cenomani in Terentium familiarissima
                        interpretatio cum figuris unicuique scaenae praepositis.</title>
                    <author ref="#ter">Terentius</author>
                    <pubPlace ref="#lugdunum">Lugduni</pubPlace>
                    <publisher ref="#trechsel_ioannes">Iohannes Trechsel</publisher>
                    <date when="1493">1493</date>
                    <editor ref="#ascensius_iodocus">Iodocus Badius</editor>
                    <editor ref="#iuuenalis_guido">Guido Juvenalis</editor>
                    <ref
                        target="https://gallica.bnf.fr/ark:/12148/bpt6k8710568w.r=trechsel%20t%C3%A9rence?rk=42918;4"
                        >https://gallica.bnf.fr/ark:/12148/bpt6k8710568w.r=trechsel%20t%C3%A9rence?rk=42918;4</ref>
                </bibl>

                <listBibl>
                    <bibl/>
                </listBibl>
            </sourceDesc>
        </fileDesc>

        <profileDesc>

            <abstract>
                <p/>
            </abstract>

            <langUsage>
                <language ident="fr">Français</language>
                <language ident="la">Latin</language>

            </langUsage>

        </profileDesc>

        <revisionDesc>
            <change when="2022-04-08">Sarah GAUCHER : création de la fiche TEI, encodage de la
                transcription</change>
        </revisionDesc>

    </teiHeader>

    <text>
        <body>

            <head type="orig" xml:id="Te1493_Juvenalis_p1_0">Guido Iuuenalis Germano de Ganeio uiro
                senatorio bonarumque litterarum amantissimo S&lt;alutem&gt; P&lt;lurimam&gt;
                D&lt;at&gt;</head>

            <head corresp="#Te1493_Juvenalis_p1_0" type="trad">Guy Jouenneaux donne un salut appuyé
                à Germanus de Ganeio, homme de rang sénatorial et amoureux des belles
                lettres.</head>
            <ab type="orig" xml:id="Te1493_Juvenalis_p1_1">Cum inter uarias multiplicesque
                interpretationes nostras, uir litterarum bonarum gloria conspicue, succissiuo
                tumultuarioque studio in festiua dicta Terentii interpretatiunculae nonnihil
                confecissemus, quae multis foret aliquantum adiumento, quibus interpretum facilis
                copia non est aut quia pauperie premuntur usque eo ut gymnasia emporiaque bonarum
                litterarum adire non possint, aut quia non satis stabilia iecerunt fundamenta, ut
                facile uiros graues et litteratos capere ualeant, qui tantam indignitatem subire non
                sustinent, ut ad structuram ita familiarem descendant, ut etiam minutissima quaeque
                ante iuuenum oculos ponant, quorum discendi cupiditatem miseratus (quantum mihi pro
                facultate licuit) minima quaeque in lucem prodidi, haud indignum ratus si nec ipsas
                quidem particulas (quae plurimam plerumque ignoratae afferunt structurae et
                intellectui difficultatem) intactas praetermitterem, quem quidem laborem fateor et
                quidem egregie iuuene aliquanto in doctrinis prouectiore indignum esse sed tamen non
                adeo longe progressis minime contemnendum, cum, inquam, eidem lucubrationi numerosae
                ueluti fastigium extremamque manum admouissem, statui (ut fieri solet) aliquem mihi
                necessitudine iunctum in ipso statim (ut ita dicam) uestibulo conuenire, uehementer
                quidem motus Caii iureconsulti uerbis titulo de iuris origine dicentis cuiusque rei
                potentissimam partem principium esse et cum in foro causas dicentibus nefas (ut sic
                dicam) esse uideatur nulla praefatione facta iudici rem exponere, quanto magis
                interpretationem promittentibus inconueniens sit futurum omissis initiis atque
                illotis (ut sic dicatur) manibus protinus materiam interpretationis tractare !</ab>
            <ab type="trad" corresp="#Te1493_Juvenalis_p1_1">Alors que, parmi les commentaires
                variés et multiformes que j’ai donnés, ô homme remarqué par la gloire des belles
                lettres, lors d’un travail secondaire et supplémentaire, j’avais achevé une partie
                de mon petit commentaire sur les bons mots térentiens, susceptible d’aider quelque
                peu ceux qui n’ont pas facilement accès aux commentateurs, ou qui sont trop pauvres
                pour fréquenter les écoles et le commerce des belles lettres, ou qui n’ont pas jeté
                des bases assez solides pour pouvoir facilement comprendre des hommes sérieux et
                cultivés qui ne consentent pas à assumer la honte suprême de s’abaisser à des
                fondements si vulgaires et d’aller jusqu’à mettre de menus détails devant les yeux
                des jeunes gens, j’ai, moi, eu pitié de leur soif d’apprendre et (autant que j’en ai
                eu la possibilité), j’en ai illustré le minimum, dans l’idée qu’il valait la peine
                que je ne laisse pas tomber sans y toucher ces petites parties mêmes qui, quand on
                les a ignorées, compliquent l’apprentissage des bases et la compréhension, travail
                qui, je l’avoue, et même fortement, est indigne d’un jeune homme un peu aguerri dans
                le cursus, mais qu’il ne faut néanmoins pas du tout mépriser pour les moins avancés
                ; alors, dis-je, que j’avais mis le faîte et la dernière main à ce labeur multiple,
                je décidai (comme c’est habituel) de rencontrer à ce moment quelque parent lié à moi
                dans ce vestibule, pourrait-on dire, impressionné que j’étais par les termes du
                jurisconsulte Caius, au titre de l’origine du droit, où il dit que dans chaque
                chose, la partie la plus importante est le début, et, puisque ceux qui plaident au
                tribunal trouveraient néfaste (si je puis le dire ainsi) d’exposer l’affaire au juge
                sans avoir au préalable fait un préambule, combien plus inconvenant il serait que
                ceux qui promettent un commentaire traitent de but en blanc la matière du
                commentaire sans faire de préface et sans s’être lavé les mains (pour le dire ainsi)
                !</ab>
            <ab type="orig" xml:id="Te1493_Juvenalis_p1_2">Nam, ut ipse inquit, praefationes ipsae
                et libentius nos ad lectionem propositae materiae producunt, et, cum eo uenerimus,
                eius euidentiorem praestant intellectum.<note>Cette phrase et la fin de la
                    précédente sont une quasi-citation du Digeste, 1.2.1 : <quote xml:lang="la">et
                        certe cuiusque rei potissima pars principium est. Deinde si in foro causas
                        dicentibus nefas ut ita dixerim uidetur esse nulla praefatione facta iudici
                        rem exponere, quanto magis interpretationem promittentibus inconueniens erit
                        omissis initiis atque origine non repetita atque illotis ut ita dixerim
                        manibus protinus materiam interpretationis tractare ! Namque nisi fallor
                        istae praefationes et libentius nos ad lectionem propositae materiae
                        producunt et cum ibi uenerimus, euidentiorem praestant
                    intellectum.</quote></note></ab>
            <ab type="trad" corresp="#Te1493_Juvenalis_p1_2">Car, comme le dit Gaius lui-même, les
                préfaces en soi d’une part nous incitent à lire la matière proposée, d’autre part,
                quand nous y sommes passés, elles clarifient le sens.</ab>
            <ab type="orig" xml:id="Te1493_Juvenalis_p1_3">Et cum in magno meorum amicorum numero
                quaeritassem quisnam potissimum hoc tempore compellandus esset, tandem tui ipsius,
                uir celeberrime, mihi incessit recordatio quam huiuscemodi mente coepi uolutare
                :</ab>
            <ab type="trad" corresp="#Te1493_Juvenalis_p1_3">Et comme, dans la liste nombreuse de
                mes amis, j’avais cherché qui principalement interpeller en cette occasion, c’est de
                toi finalement, homme très illustre, que le souvenir s’est imposé à moi et c’est
                dans cet état d’esprit que j’ai commencé à l’évoquer :</ab>
            <ab type="orig" xml:id="Te1493_Juvenalis_p1_4">« Quid tamdiu fluitans in uarias
                distraheris sententias ut eum tua epistola alloquaris quem conueniendum quaeris
                ?</ab>
            <ab type="trad" corresp="#Te1493_Juvenalis_p1_4">« Pourquoi, si longtemps indécis, te
                laisser tirailler entre plusieurs possibilités pour apostropher dans cette épître
                celui à qui tu fixes ce rendez-vous ? </ab>
            <ab type="orig" xml:id="Te1493_Juvenalis_p1_5">Nescis uirum illum senatorium, litterarum
                bonarum amatorem praecipuum, te sua sponte in suam beniuolentiam recepisse, tibique
                honestissimum in sua gratia praebuisse locum, idque usque adeo ut se tibi suaque
                familiariter utenda praestiterit, te ipsum saepenumero ad epulas uocans, non
                commessationis compotationisue gratia, ut Graeci loquuntur, sed melius ut nostri
                Latini causa conuictus, seu mauis conuiuendi ?</ab>
            <ab type="trad" corresp="#Te1493_Juvenalis_p1_5">As-tu oublié que c’est cet homme de
                rang sénatorial et principal ami des lettres qui t’a de son propre chef donné une
                bienveillante protection, qui t’a offert dans sa faveur une place très honorable,
                allant même jusqu’à se mettre, lui et ses biens, à ta disposition, t’invitant
                souvent à sa table, non pas pour un banquet ou un symposium, comme disent les Grecs,
                mais, mieux, pour que nous soyons convives de notre latin, ou, si tu préfères, pour
                cette convivialité ?</ab>
            <ab type="orig" xml:id="Te1493_Juvenalis_p1_6">Quantum autem suauitatis utilitatisque
                afferant eiusmodi litterata conuiuia non facile dici potest.</ab>
            <ab type="trad" corresp="#Te1493_Juvenalis_p1_6">Or tout ce qu’apporte d’agréable et
                d’utile ce type de banquets lettrés ne peut pas se décrire facilement.</ab>
            <ab type="orig" xml:id="Te1493_Juvenalis_p1_7">Nam Ciceronis sententia, id a beate
                uiuendum pertinere putandum est.</ab>
            <ab type="trad" corresp="#Te1493_Juvenalis_p1_7">Car d’après Cicéron, il faut considérer
                que cela participe du bonheur.</ab>
            <ab type="orig" xml:id="Te1493_Juvenalis_p1_8">Cuius ad Paetum uerba sunt.<note>Suit une
                    citation presque exacte d’un passage de la lettre <title>Fam.</title> 9.24.3 à
                    Papirius Paetus.</note></ab>
            <ab type="trad" corresp="#Te1493_Juvenalis_p1_8">Voici ce qu’il écrit à Paetus.</ab>
            <ab type="orig" xml:id="Te1493_Juvenalis_p1_9"><seg>Sed<note>Variante usuelle : <foreign
                            xml:lang="la">Et</foreign>.</note></seg> mehercule, mi Paete, extra
                iocum moneo te, quod pertinere ad beate uiuendum arbitror, ut cum uiris bonis,
                iucundis, amantibus tui uiuas. </ab>
            <ab type="trad" corresp="#Te1493_Juvenalis_p1_9">‘Mais ma foi, mon cher Paetus, toute
                plaisanterie mise à part, je t’engage (chose qui participe du bonheur, selon moi) à
                vivre avec des hommes de bien, agréables et qui t’aiment.</ab>
            <ab type="orig" xml:id="Te1493_Juvenalis_p1_10"><seg>Nihil enim est aptius<note>Variante
                        usuelle : <foreign xml:lang="la">Nihil est aptius</foreign>.</note></seg>
                uitae, nihil ad beate uiuendum accommodatius, nec id ad uoluptatem refero sed ad
                        <seg>comitatem<note>Variante usuelle : <foreign xml:lang="la"
                            >communitatem</foreign>.</note></seg> uitae atque uictus remissionemque
                animorum, quae maxime sermone efficitur familiari, qui est in conuiuiis dulcissimus,
                ut sapientius nostri quam Graeci : illi <foreign xml:lang="grc">συμπόσια</foreign>
                aut <foreign xml:lang="grc">σύνδειπνα</foreign>, id est compotationes aut
                concenationes, nos conuiuia, quod tum maxime simul uiuitur.</ab>
            <ab type="trad" corresp="#Te1493_Juvenalis_p1_10">Car rien n’est plus compatible avec la
                vie, rien n’est plus adapté au bonheur, et je ne rapporte pas cela au plaisir mais à
                la douceur de la vie et du vivre ensemble, au relâchement des âmes qui se réalise
                surtout dans la conversation familière, si agréable dans les banquets, en sorte que
                nous avons plus de sagesse que les Grecs : eux appellent cela <foreign
                    xml:lang="grc">συμπόσιον</foreign> ou <foreign xml:lang="grc"
                    >σύνδειπνον</foreign>, c’est-à-dire ‘action de boire ou de dîner ensemble’, nous
                nous disons <foreign xml:lang="la">conuiuium</foreign>, parce que c’est à ce
                moment-là surtout que l’on vit ensemble’.</ab>
            <ab type="orig" xml:id="Te1493_Juvenalis_p1_11">Haec ille.</ab>
            <ab type="trad" corresp="#Te1493_Juvenalis_p1_11">Ce sont ses mots.</ab>
            <ab type="orig" xml:id="Te1493_Juvenalis_p1_12">Et profecto, ut interim a cogitatione
                mea ad te ipsum sermonem uertam, uir percelebris, tua conuiuia mihi eorum simillima
                uidentur quae Athenis Taurus philosophus sibi iunctioribus sectatoribus celebrare
                solebat.</ab>
            <ab type="trad" corresp="#Te1493_Juvenalis_p1_12">Et assurément, pour dériver maintenant
                mon propos de mon dialogue intérieur vers toi, homme très illustre, tes banquets me
                font l’effet de ceux qu’à Athènes <seg>le philosophe Taurus avait coutume
                    d’organiser avec ses élèves les plus intimes<note>Lucius Calvisius Taurus de
                        Béryte est un philosophe platonicien de la fin du deuxième siècle de notre
                        ère dont Aulu-Gelle a suivi les cours pendant son séjour athénien. Il
                        organisait des repas et des sorties culturelles avec ses élèves, où
                        s’échangeaient des propos de table comparables à ceux de Plutarque. Voir
                        Gell. <title>Noct.</title> 713, 17.8, 18.10, 12.5.</note></seg>.</ab>
            <ab type="orig" xml:id="Te1493_Juvenalis_p1_13">Nam cum domum suam eos uocaret, ne
                omnino (ut dicitur) immunes et <foreign xml:lang="grc">ἀσύμβολοι</foreign> uenirent,
                hoc est quod uulgo dici solet ne omnino uenirent manibus uacuis ac sine protione,
                coniectabant singuli ad cenulam non cupedias ciborum uel cibos lautiores, sed
                argutias quaestionum.</ab>
            <ab type="trad" corresp="#Te1493_Juvenalis_p1_13">Car quand il les invitait chez eux,
                pour éviter (comme on dit) d’arriver exempts et sans écot, c’est-à-dire pour le dire
                plus simplement pour éviter d’arriver les mains vides et sans leur participation aux
                frais, ils imaginaient chacun de son côté pour le petit repas non pas des
                gourmandises à manger ou des mets succulents mais des questions à débattre.</ab>
            <ab type="orig" xml:id="Te1493_Juvenalis_p1_14">Vnusquisque enim eorum commentus
                paratusque ibat quod quaereret eratque initium loquendi edundi finis.<note>Récriture
                    très fidèle (avec glose) de Gell. 7.13.1 : <quote xml:lang="la">cum domum suam
                        nos uocaret, ne omnino, ut dicitur, immunes et asymboli ueniremus,
                        coniectabamus ad cenulam non cuppedias ciborum, sed argutias quaestionum.
                        Vnusquisque igitur nostrum commentus paratusque ibat, quod quaereret,
                        eratque initium loquendi edundi finis.</quote></note></ab>
            <ab type="trad" corresp="#Te1493_Juvenalis_p1_14">Car chacun d’eux avait réfléchi et
                arrivait paré de sa question et on commençait à parler quand on avait fini de
                manger.</ab>
            <ab type="orig" xml:id="Te1493_Juvenalis_p1_15">Quaerebant autem non grauia neque
                reuerenda sed qualia M. Varro ex <title>satiris Menippaeis</title> in conuiuiis
                quaerenda putat.</ab>
            <ab type="trad" corresp="#Te1493_Juvenalis_p1_15">Mais les questions qu’ils cherchaient
                n’étaient pas sérieuses ni importantes mais de celles que Varron, dans les
                    <title>Satires Ménippées</title>, conseille de poser dans les banquets.</ab>
            <ab type="orig" xml:id="Te1493_Juvenalis_p1_16">Censet enim sermones ad tempus esse
                habendos non super rebus anxiis aut tortuosis sed iucundos atque inuitabiles et cum
                quadam illecebra et uoluptate utiles, ex quibus ingenium nostrum uenustius fiat et
                    amoenius.<note>Quasi-citation d’Aulu-Gelle 13.11.4, évoquant la satire ménippée
                    nescis quid uesper fiat de Varron : <quote xml:lang="la">Sermones igitur id
                        temporis habendos censet non super rebus anxiis aut tortuosis, sed iucundos
                        atque inuitabiles et cum quadam inlecebra et uoluptate utiles, ex quibus
                        ingenium nostrum uenustius fiat et amoenius.</quote></note></ab>
            <ab type="trad" corresp="#Te1493_Juvenalis_p1_16">Car il estime qu’il faut avoir en
                cette occasion des conversations sur des sujets non inquiétants ni embrouillés mais
                agréables, intéressantes et, tout en étant charmantes et plaisantes, utiles,
                capables de rendre notre âme plus belle et plus délicate.</ab>
            <ab type="orig" xml:id="Te1493_Juvenalis_p1_17">Eius generis tua, uir doctissime, sensi
                esse conuiuia, in quibus nihil a nobis ultro citroque dici solet nisi quod reddatur
                ingenium uenustius atque ornatius.</ab>
            <ab type="trad" corresp="#Te1493_Juvenalis_p1_17">C’est de ce genre, homme très docte,
                que j’entendais dire qu’étaient tes banquets, des endroits où tout ce qui se dit
                mutuellement rend l’âme plus belle et plus élégante.</ab>
            <ab type="orig" xml:id="Te1493_Juvenalis_p1_18">Vbi enim edundi finis factus est, statim
                librum aliquem arripis reconditissimum et inuentu rarisimum.</ab>
            <ab type="trad" corresp="#Te1493_Juvenalis_p1_18">Car quand le repas prend fin, aussitôt
                tu prends un livre très rare et difficile à trouver.</ab>
            <ab type="orig" xml:id="Te1493_Juvenalis_p1_19">Si quid enim alicunde noui aduectum est,
                accurate percunctaris ubinam quane in officina uenditetur.</ab>
            <ab type="trad" corresp="#Te1493_Juvenalis_p1_19">Car si du nouveau est livré quelque
                part, tu t’enquiers précisément où et dans quelle boutique on le vend.</ab>
            <ab type="orig" xml:id="Te1493_Juvenalis_p1_20">Tunc enim raptim, nulla pecuniae habita
                ratione, quanticumque constare debeat, afferri tibi iubes et eius copiam ferri.</ab>
            <ab type="trad" corresp="#Te1493_Juvenalis_p1_20">Car alors aussitôt, sans tenir compte
                de l’argent, quelque somme que cela doive coûter, tu la fais apporter et en fais
                l’acquisition.</ab>
            <ab type="orig" xml:id="Te1493_Juvenalis_p1_21">Id re ipsa edoctus loquor, qui
                        <seg>tua<note>Nous corrigeons la coquille <foreign xml:lang="la"
                            >tuo</foreign>.</note></seg> in me beneuolentia tuoque beneficio libros
                aliquos abstrusissimos succissiua tumultuariaque lectione percurri.</ab>
            <ab type="trad" corresp="#Te1493_Juvenalis_p1_21">C’est par expérience que j’en parle,
                puisque grâce à ta bienveillance à mon endroit et à tes bienfaits, j’ai pu parcourir
                certains livres très compliqués dans une lecture secondaire et supplémentaire.</ab>
            <ab type="orig" xml:id="Te1493_Juvenalis_p1_22">Homericam enim <title>Iliadem</title>
                tuo munere lectitandam sum consecutus, ut cetera omittam uolumina minus inuentu
                rara, quae tua prolixa <seg>natura beneficiaque<note>Nous rétablissons l’ordre
                        nécessaire à la place de la coquille <foreign xml:lang="la">beneficiaque
                            natura</foreign>.</note></seg> mihi familiaria extiterunt.</ab>
            <ab type="trad" corresp="#Te1493_Juvenalis_p1_22">Car <title>L’Iliade</title> d’Homère,
                c’est grâce à ton cadeau que je l’ai obtenue à la lecture, pour ne rien dire de tous
                les autres volumes plus difficiles à trouver, que ta nature généreuse et tes
                bienfaits m’ont rendus familiers.</ab>
            <ab type="orig" xml:id="Te1493_Juvenalis_p1_23">Neque hac sola in me es liberalitate
                usus (quae non parui est facienda) sed etiam auaritiae tenacitatisue uinculis minime
                irretitus (quae uiriles hominum animos effeminat quaeque latentium indagatrix
                lucrorum est manifestaeque praedae auidissima uorago nec habendi fructu felix, sed
                quaerendi cupiditate miserrima qua constrictus Crassus aurisitibundus aurum bibit in
                suam perniciem), ipse, inquam, nequaquam illa districtus pecunias mihi benigne
                obtulisti, admonens ueri amici more, ne te abundante ullo modo me egere sustinerem
                quod quam mihi gratum acciderit, ipse sum mihi testis locupletissimus.</ab>
            <ab type="trad" corresp="#Te1493_Juvenalis_p1_23">Et tu ne m’as pas seulement traité
                avec cette générosité (laquelle n’est pas négligeable) mais, loin d’être retenu par
                les liens de l’avarice et de la pingrerie (qui amollissent les tempéraments virils
                des hommes, se mettent en chasse de gains cachés, sont le puits sans fond pour le
                butin dont on s’est emparé sans jamais se satisfaire de sa possession, mais
                seulement de la quête, dans cette avidité misérable sous les chaînes de laquelle
                Crassus, dans sa soif de l’or, <seg>a bu l’or pour sa perte<note>Crassus, torturé à
                        mort par les Parthes après la bataille de Carrhes, dut boire de l’or
                        fondu.</note></seg>), toi donc, dis-je, entièrement détaché de toute
                avarice, m’as largement offert tes richesses en me conseillant à la manière d’un
                véritable ami de ne pas supporter de manquer quand toi tu étais dans l’abondance :
                quel agrément j’en ai tiré, j’en suis mon meilleur témoin.</ab>
            <ab type="orig" xml:id="Te1493_Juvenalis_p1_24">Magno etenim opere gratulari mihi soleo
                cum ipse tantillus uiro tanto sum iucundus atque ex animi sententia, qui tamen etsi
                grauitate dignitateque pollet, tamen laudum mearum (si modo ullae meae sint) suopte
                ingenio motus nitidissimus praeco esse non dedignetur.</ab>
            <ab type="trad" corresp="#Te1493_Juvenalis_p1_24">Et je me félicite souvent beaucoup de
                ce que, petit comme je suis, j’arrive à être agréable à un si grand homme et que, de
                sa propre opinion, malgré le sérieux et l’importance de son pouvoir, il ne dédaigne
                toutefois pas d’être de mes mérites (pour peu que j’en aie), lesquels l’intéressent
                spontanément, le promoteur le plus brillant. </ab>
            <ab type="orig" xml:id="Te1493_Juvenalis_p1_25">Gloriosum enim est (ut Hector ille
                Naeuianus dicebat) laudari et a laudato uiro.<note>Allusion au fragment 17 des
                    tragédies de Naevius, extrait de l’Hector proficiscens, cité quatre fois par
                    Cicéron, notamment sous sa forme la plus complète en <title>Tusc.</title> 4.67 :
                        <quote xml:lang="la">Laetus sum laudari me abs te, pater, a laudato
                        uiro.</quote></note></ab>
            <ab type="trad" corresp="#Te1493_Juvenalis_p1_25">Car il y a de la gloire (comme le
                disait le fameux Hector de Naevius) à être loué et qui plus est par un homme
                lui-même loué.</ab>
            <ab type="orig" xml:id="Te1493_Juvenalis_p1_26">Huc accedit te non modo iuris utriusque
                et pontificii et caesarei studiis teneri, sed etiam philosophiae, <seg>quae, ut
                    inquit Cicero, tota frugifera et fructuosa nec ulla pars eius inculta ac deserta
                        est.<note>Voir Cic. <title>Off.</title> 3.5 : <quote xml:lang="la">Sed cum
                            tota philosophia mi Cicero frugifera et fructuosa nec ulla pars eius
                            inculta ac deserta sit…</quote></note></seg></ab>
            <ab type="trad" corresp="#Te1493_Juvenalis_p1_26">À cela s’ajoute que tu es pris par des
                études non seulement dans les deux droits, le pontifical et le césarien, mais aussi
                de philosophie, laquelle, comme le dit Cicéron, est tout entière fertile et
                fructueuse et nulle par inculte et désertique.</ab>
            <ab type="orig" xml:id="Te1493_Juvenalis_p1_27">Ipse enim mathematicis primis
                disciplinis, ut Graeci dicunt, animi tui neruos intendis, gnomonicam, geometriam,
                arithmeticam, astronomiam ceteraque id genus disciplinas relegens a quibus qui sunt
                penitus alieni, ut Taurus super Pythagora dicere solebat, repente nimis ac pedibus
                illotis ad alias artes pernoscendas nequicquam diuertunt, quod procul dubio tibi
                improperari non poterit, qui adeo cum dicendi genere forensi quietum et aequabile
                dicendi genus coniungis, ut mihi alter Demetrius Theophrasti discipulus exprimi
                uidearis : ut enim philosophiae fuit studiosus pariter et dicendi, sic tu quoque
                inter priuatos parietes facundiam acri studio alis, qua sis usurus in omnni causarum
                genere, modo forensi, modo deliberatiuo, et, si quando opus est, demonstratiuo.</ab>
            <ab type="trad" corresp="#Te1493_Juvenalis_p1_27">Toi-même en effet, tu entraînes la
                tension de ton esprit dans les sciences mathématiques (au sens des Grecs), relisant
                la gnomonique, la géométrie, l’arithmétique, l’astronomie et toutes les sciences de
                ce type, dont ceux qui restent trop éloignés, <seg>comme Taurus le disait souvent de
                        Pythagore<note>Cf. Gell. 1.9.8 : <quote xml:lang="la">Haec eadem super
                            Pythagora noster Taurus cum dixisset: 'nunc autem' inquit 'isti, qui
                            repente pedibus inlotis ad philosophos deuertunt, non est hoc satis,
                            quod sunt omnino ἀθεώρητοι, ἄμουσοι, ἀγεωμέτρητοι, sed legem etiam dant,
                            qua philosophari discant</quote>, « après nous avoir donné ces
                        indications sur Pythagore, notre cher Taurus disait : ‘maintenant les gens
                        s’établissent tout de suite chez le philosophe, les pieds mal lavés, et ce
                        n’est pas assez qu’ils soient totalement ignorants, réfractaires aux arts et
                        à la géométrie, ils édictent eux-mêmes quel sera l’ordre dans lequel ils
                        apprendront la philosophie’ » (trad. R. Marache, C.U.F.).</note></seg>,
                s’écartent sans raison, intempestivement et sans s’être lavé les pieds, de la
                connaissance complète de tous les autres arts, ce qu’on ne pourra à l’évidence pas
                te reprocher, toi qui mêles à l’éloquence du tribunal une éloquence tranquille et
                égale, au point qu’il me semble qu’on pourrait te surnommer le second
                    <seg>Démétrius, disciple de Théophraste<note>Cf. Cic. <title>Off.</title> 1.3 :
                            <quote xml:lang="la">et id quidem nemini uideo Graecorum adhuc
                            contigisse ut idem utroque in genere laboraret sequereturque et illud
                            forense dicendi et hoc quietum disputandi genus nisi forte Demetrius
                            Phalereus in hoc numero haberi potest disputator subtilis orator parum
                            uehemens dulcis tamen ut Theophrasti discipulum possis
                        agnoscere</quote>, « et, à vrai dire, je ne vois pas qu’il soit arrivé
                        jusqu’ici à aucun des Grecs, que le même auteur s’appliquât à l’un et
                        l’autre genre et poursuivît à la fois le premier, le genre du forum, et
                        celui-là, paisible, de la discussion philosophique ; à moins toutefois que
                        l’on puisse mettre du nombre Démétrius de Phalère, dialecticien subtil,
                        orateur sans grande puissance, mais agréable cependant, à quoi l’on peut
                        reconnaître le disciple de Théophraste » (trad. M. Testard,
                    C.U.F.).</note></seg> : car il était également féru de philosophie et
                d’éloquence, et toi aussi, de même, tu entretiens avec opiniâtreté entre tes murs
                une faconde prête à l’emploi dans tout genre de causes, tantôt judiciaire, tantôt
                délibératif, et même, si besoin, démonstratif.</ab>
            <ab type="orig" xml:id="Te1493_Juvenalis_p1_28">Quae cum omnia animaduertissem, adhuc
                apud me certaminis aliquid reliquum erat, foretne operae pretium ut tanti uiri
                auribus ineptiolis meis obstreperem.</ab>
            <ab type="trad" corresp="#Te1493_Juvenalis_p1_28">Après m’être fait toutes ces
                remarques, il me restait encore une hésitation : valait-il la peine que je casse les
                oreilles d’un si grand homme avec mes balivernes ?</ab>
            <ab type="orig" xml:id="Te1493_Juvenalis_p1_29">Quod dubium cum mecum reuolouerem, uenit
                in mentem tua ipsius innata mansuetudo, qua maximopere fretus animum ad scribendum
                appuli, futurum ratus ut scripta nostra nihil molestiae essent tuae dignitati
                allatura.</ab>
            <ab type="trad" corresp="#Te1493_Juvenalis_p1_29">Comme je retournais dans ma tête cette
                question, je me souvins de ta mansuétude innée, qui m’a pleinement rassuré ; aussi
                ai-je commencé à écrire, avec l’intime conviction que nos écrits n’apporteraient
                aucun désagrément à ta dignité.</ab>
            <ab type="orig" xml:id="Te1493_Juvenalis_p1_30">Quae spes ne me fallat, uehementer rogo
                ut hac ipsa in re eum te mihi praebeas qui mihi semper (ex quo me in tuam gratiam
                insinuaui) cognitus es, tantum in postremis a tua humanitate etiam atque etiam
                flagitans ut de instituta in me beniuolentia nihil remittas, sed potius (quoad eius
                fieri potest) conserues, foueas, alas, hoc existimans quantum ad me attinebit nullo
                me loco tuae honestati defuturum.</ab>
            <ab type="trad" corresp="#Te1493_Juvenalis_p1_30">Pour être exaucé dans mes espérances,
                je te demande instamment de te montrer à mon égard (ce qui m’a fait m’insinuer dans
                tes bonnes grâces) comme je t’ai toujours connu, réclamant seulement encore et
                encore, pour finir, de ton humanité, que tu ne retires rien de la bienveillance que
                tu as instaurée pour moi mais qu’au contraire, dans la mesure du possible, tu me la
                conserves, l’entretiennes, la nourrisses, avec la certitude que, pour ce qui me
                concerne, je ne manquerai jamais à ton honorabilité.</ab>
            <ab type="orig" xml:id="Te1493_Juvenalis_p1_31">Vale et me ama ut coepisti.</ab>
            <ab type="trad" corresp="#Te1493_Juvenalis_p1_31">Adieu et aime-moi comme depuis le
                début.</ab>

        </body>
    </text>
</TEI>
