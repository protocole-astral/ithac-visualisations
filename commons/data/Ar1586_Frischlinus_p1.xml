<?xml version="1.0" encoding="UTF-8"?>


<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">

   <teiHeader>

      <fileDesc>

         <titleStmt>
            <title xml:lang="la">Diuo Rudolpho II Caesari, Romano Imperatori electo, Augustus Pius
               Felix Pater Patriae Regi Germaniae, Bohemiae, Hungariae etc., Archiduci Austriae,
               Duci Burgundiae, Comiti Tyrolano, etc., Principi Optimo Maximo, uita, salus et
               uictoria</title>
            <author ref="#frischlinus_nicodemus">Nicodemus Frischlinus</author>

            <!-- Déclarations de responsabilité : indiquent qui a effectué quelle tâche -->
            <!-- Tâche n°1 -->
            <respStmt>
               <resp>Transcription</resp>
               <name>Christian NICOLAS</name>
            </respStmt>
            <!-- Tâche n°2 -->
            <respStmt>
               <resp>Traduction</resp>
               <name>Christian NICOLAS</name>
            </respStmt>
            <!-- Tâche n°3 -->
            <respStmt>
               <resp>Encodage</resp>
               <name>Malika BASTIN-HAMMOU</name>
               <name>Sarah GAUCHER</name>
            </respStmt>
            <!-- Ajouter autant de tâches que nécessaire -->

            <!-- Attention ! Ne pas modifier les tâches ci-dessous -->
            <principal>Malika BASTIN-HAMMOU</principal>
            <respStmt>
               <resp>Modélisation et structuration</resp>
               <persName>Elisabeth GRESLOU</persName>
               <persName>Elysabeth HUE-GAY</persName>
            </respStmt>
            <respStmt>
               <resp>Première version du modèle d'encodage</resp>
               <persName>Elisabeth GRESLOU</persName>
               <persName>Anne Garcia-Fernandez</persName>
            </respStmt>
         </titleStmt>

         <!-- Informations de publication sur le laboratoire numérique du projet. Ne pas modifier-->
         <publicationStmt>
            <publisher/>
            <availability>
               <p>Le site est en accès restreint</p>
            </availability>
            <pubPlace>ithac.elan-numerique.fr</pubPlace>
         </publicationStmt>

         <sourceDesc>
            <bibl>
               <title>Nicodemi Frischlini Aristophanes, veteris comoediae princeps: poeta longe
                  facetissimus et eloquentissimus, repurgatus a mendis, et imitatione Plauti atque
                  Terentii interpretatus, ita ut fere Carmen Carmini, numerus numero, pes pedi,
                  modus modo, Latinismus Graecismo respondeat. Opus Divo Rudolpho Caesari
                  Sacrum</title>
               <author ref="#ar">Aristophanes</author>
               <editor ref="#frischlinus_nicodemus">Nicodemus Frischlinus</editor>
               <publisher ref="#spies_ioannes">Ioannes Spies</publisher>
               <pubPlace ref="#francofurtum">Francoforti ad Moenum</pubPlace>
               <date when="1586">1586</date>
               <ref target="url_edition_numérique"/>
               <!-- pour les liens google book, remplacer les & par &amp; pour que ça fonctionne -->
            </bibl>

            <listBibl>
               <head>Bibliographie</head>
               <bibl><author>Thomas Baier</author><title>« Nicodemus Frischlin als
                     Aristophanes-Übersetzer »</title><editor>Ekkehard Stärk et Gregor
                     Vogt-Spira</editor><title>Dramatische Wäldchen. Festschrift für Eckard Lefèvre
                     zum 65.
                     Geburtstag</title><pubPlace>Hildesheim</pubPlace><publisher>Olms</publisher><date>2000</date></bibl>
               <bibl>
                  <author>Patrick Lucky Hadley</author>
                  <title>Athens in Rome, Rome in Germany. Nicodemus Frischlin and the Rehabilitation
                     of Aristophanes in the 16th Century</title>
                  <pubPlace>Tübingen</pubPlace>
                  <publisher>Narr Francke Attempto Verlag</publisher>
                  <date>2015</date>
               </bibl>
               <bibl><author>David Price</author><title>The Political Dramaturgy of Nicodemus
                     Frischlin : Essays on Humanist Drama in Germany</title><pubPlace>Chapel
                     Hill</pubPlace>
                  <publisher>University of North Carolina Press</publisher>,
                  <date>1990</date></bibl>
            </listBibl>
         </sourceDesc>

      </fileDesc>

      <profileDesc>

         <abstract>
            <p>Traduction latine de cinq comédies d’Aristophane.</p>
         </abstract>

         <langUsage>
            <!-- ne pas modifier -->
            <language ident="fr">Français</language>
            <language ident="la">Latin</language>
            <language ident="grc">Grec</language>
         </langUsage>

      </profileDesc>

      <revisionDesc>
         <change when="2022-03-03">Sarah GAUCHER : identifiants Header, mise en conformité
            Elan</change>
      </revisionDesc>
   </teiHeader>

   <text>

      <body>

         <head type="orig" ana="adress" xml:id="Ar1586_Frischlinus_p1_0">Diuo <seg><persName>Rudolpho II
               Caesari</persName><note>Rodolphe II, Empereur du Saint-Empire romain (1552-1612), de
               la famille des Habsbourg,</note></seg>, Romano Imperatori electo, Augustus Pius Felix
            Pater Patriae Regi Germaniae, Bohemiae, Hungariae etc., Archiduci Austriae, Duci
            Burgundiae, Comiti Tyrolano, etc., Principi Optimo Maximo, uita, salus et
            uictoria</head>
         <head type="trad" corresp="#Ar1586_Frischlinus_p1_0">Au Divin Rodolphe II, Empereur élu du
            Saint-Empire Romain, Auguste, Pieux, Heureux, Père de la Patrie, Roi de Germanie, de
            Bohême, de Hongrie, etc., Archiduc d’Autriche, Duc de Bourgogne, Compte du Tyrol, etc.,
            Très bon très grand Prince, vie, santé et victoire ! </head>

         <ab type="orig" xml:id="Ar1586_Frischlinus_p1_1">Etsi uereor, Diue Caesar, ne munus afferam
            Maiestate tua Imperatoria indignum, qui hasce <persName>Aristophanis</persName>
            <seg type="th">Comoedias</seg> ex illa ueteri Graecia in Romanum <seg type="th"
               >theatrum</seg> a me productas ante serenissimos oculos tuos tam audacter statuo,
            propterea quod sacratissimo tuo nomine nihil dignum esse censeo, quod non sit summo
            perfectum studio, summaque elaboratum industria, tamen non parum me recreat et reficit,
            in hoc timore ac metu, singularis illa animi tui Caesaris aequitas ac summa bonitas, qui
            profecto nec iustitiae tuae putabis esse, quem auctorem potentissimi Persarum ac
            Siculorum reges olim fuerunt admirati, eundem non lectum atque inauditum dimittere, nec
            sapientiae tuae, temeritatem ingratissimi huius saeculi, qua nunc uti solent nonnulli,
            in aliorum laboribus atque impensis remunerandis, tua auctoritate confirmare. </ab>
         <ab type="trad" corresp="#Ar1586_Frischlinus_p1_1">Même si je crains, divin César, de vous
            faire un cadeau indigne de Votre Majesté Impériale, moi qui ose placer devant vos yeux
            sérénissimes ces comédies d’Aristophane que j’ai transposées de la Grèce ancienne jusque
            sur le théâtre romain, d’autant plus que je pense que la seule chose digne de votre nom
            c’est celle qui a été parachevée avec le plus grand soin et élaborée avec le plus grand
            travail, pourtant ce m’est une douce récréation et réparation, dans cette crainte et
            cette peur, que cette bienveillance et cette suprême bonté de Votre esprit impérial,
            vous qui estimerez que votre justice, à l’égard d’un auteur que le très puissant roi de
               <seg type="note" corresp="#Ar1586_Frischlinus_p1_1_n1">Perse</seg><note
               xml:id="Ar1586_Frischlinus_p1_1_n1" type="editor">Il ne s’agit pas d’un fait
               historique mais d'une extrapolation à partir de ce qu’Aristophane lui-même rapporte
               dans la parabase des <title>Acharniens</title>, vv.646-651. </note>, et celui de
               <seg>Sicile</seg><note>Il s'agit sans doute d'une référence à Denys de Syracuse
               (399-344 av. J.-C), tyran de Syracuse, qui appela en vain Platon auprès de lui pour
               l’aider à organiser la République idéale. Denys aurait demandé à Platon quel auteur
               il devait lire pour comprendre Athènes, et Platon lui aurait répondu « prenez et
               lisez Aristophane ». L’anecdote est rapportée par Olympiodore dans sa Vie de
               Platon</note> ont autrefois admiré ne peut le laisser sans lecteurs ni auditeurs et
            que votre sagesse ne peut pas approuver de son autorité la témérité de notre siècle si
            ingrat, qui a cours fréquemment chez quelques-uns de nos contemporains, dans le fait de
            rémunérer/la rémunération du/ le travail et les efforts d’autrui. </ab>

         <ab type="orig" xml:id="Ar1586_Frischlinus_p1_2">Est enim uulgo hodie comparatum inter
            homines ceteros ut quae imperiti et iniqui rerum arbitri non intelligunt ea negligant,
            et quae negligunt, eadem uituperent, et quae uituperant, eadem ab aliis omnibus neglegi
            et uituperari expetant. </ab>
         <ab type="trad" corresp="#Ar1586_Frischlinus_p1_2">Car il est aujourd’hui communément
            acquis entre tous que ce que les ignares et injustes arbitres des choses ne comprennent
            pas, ils le rejettent, que ce qu’ils rejettent, ils le blâment également, et que ce
            qu’ils blâment, ils souhaitent que tous le rejettent et le blâment en même temps. </ab>

         <ab type="orig" xml:id="Ar1586_Frischlinus_p1_3">A qua praepostera sententia, quantum
            animus tuus, Diue Caesar, excultus optimarum artium et linguarum literarumque studiis ac
            uere animus heroicus abhorreat, nihil attinet hoc loco commemorare. De eo enim iampridem
            omnes mortales omnium generum, aetatum, ordinum ita iudicarunt, ut propter summam tuam
            sapientiam, iustitiam et aequitatem, unanimi consensu affirment omnes nulli potuisse
            clauum Imperii Romani, in his procellis ac tempestatibus negotiorum melius quam
            sacratissimo tuae Maiestati a Septemuiris commendari. </ab>
         <ab type="trad" corresp="#Ar1586_Frischlinus_p1_3">De cette pensée néfaste, combien, divin
            César, est éloigné votre esprit enrichi de l’étude de toutes les meilleures disciplines
            et des langues et littératures, il ne sert à rien de le rappeler ici. Car à ce sujet
            depuis longtemps déjà, tous les mortels de tout genre, de tout âge, de tout rang ont pu
            en juger, au point d’affirmer tous ensemble d’une même voix que, en raison de votre
            extrême sagesse, justice et équité, personne n’aurait pu recevoir de la part des
               <seg>Septemvirs</seg><note>Il s'agit des sept électeurs</note> le gouvernail de
            l’Empire Romain, dans cette période mouvementée de tempêtes dans les affaires, mieux que
            Votre très sainte Majesté. </ab>

         <ab type="orig" xml:id="Ar1586_Frischlinus_p1_4">Quamobrem non dubito quin meus
               <persName>Aristophanes</persName>, in hoc Romani Imperii amplissimo <seg type="th"
               >theatro</seg>, aequissimum sit habiturus si alium neminem uel te solum atque unum
               <seg type="th">spectatorem ac iudicem</seg>. </ab>
         <ab type="trad" corresp="#Ar1586_Frischlinus_p1_4">Aussi suis-je bien certain que mon
            Aristophane, dans ce si vaste théâtre de l’Empire Romain, saura trouver au moins en
            vous, à défaut de quelque autre, un seul et unique spectateur et critique. </ab>

         <ab type="orig" xml:id="Ar1586_Frischlinus_p1_5">Nam is auctor est
               <persName>Aristophanes</persName>, qui magna cum libertate homines seditiosos ac
            turbulentos <seg type="th">in scaenam producit</seg> eosque nominatim perstringit, qui
            principum in Republica uirorum dissensiones acerbe insectatur, qui temeritatem imperitae
            multitudinis et licentiam plebis seueriter castigat, qui denique nulli ordini, nulli
            aetati, nulli generi, nisi solis innocentibus atque immeritis sua libertate parcit. </ab>
         <ab type="trad" corresp="#Ar1586_Frischlinus_p1_5">Car c’est cet auteur, Aristophane, qui
            avec une grande liberté montre sur la scène les séditieux et les fauteurs de trouble et
            les pourfend nommément, c’est lui qui poursuit de son acrimonie les dissensions entre
            les grands de l’état, lui qui châtie sévèrement l’aveuglement de la foule inculte et la
            licence de la populace, lui enfin qui n’épargne de sa libre verve aucun rang, aucune
            génération, aucun genre, à l’exception des innocents et de ceux qui n’ont rien à se
            reprocher. </ab>

         <ab type="orig" xml:id="Ar1586_Frischlinus_p1_6">Itaque temporibus illis, quibus Graecia
            flagrauit eo intestini belli incendio, quod a <persName>Thucydide</persName>
            describitur, totos annos uiginti septem, ipse Cleonem, Brasidam, Lamachum et similes
            reipublicae pestes, nulla spe commodi, solo libertatis amore ac pacis studio in scenam
            productos, coram populo Atheniensi dirissimis et quibus digni erant modis, acerrime
            exagitauit. </ab>
         <ab type="trad" corresp="#Ar1586_Frischlinus_p1_6">Aussi, à l’époque où la Grèce s’enflamma
            dans l’incendie de la guerre intestine, qui est décrite par Thucydide, pendant
            vingt-sept années pleines, lui, mettant en scène Cléon, Brasidas, Lamachos et d’autres
            fléaux de la république, sans espérer quelque avantage mais seulement pour l’amour de la
            liberté et de la paix, en présence du peuple d’Athènes, il les déchira bien fort par les
            moyens les plus cruels et qu’ils méritaient. </ab>

         <ab type="orig" xml:id="Ar1586_Frischlinus_p1_7">Neque hoc sine magna fecit spectantium
            admiratione. Cum enim ceteri poetae suis scriptionibus nihil fere carperent, sed pessimo
            cuique assentarentur, unus prodiit <persName>Aristophanes</persName> qui contra
            principes Graeciae, a quibus ea discordia excitari uidebatur, conscripsit suas <seg
               type="th">Comoedias</seg> , easque populo exhibuit. Itaque huic uni in <seg type="th"
               >theatro</seg> applaudebatur, huius unius nomen, oratio, uultus, incessus ab
            Atheniensibus amabatur. Darius quoque Persarum rex, cognita e suis legatis poetae huius
            libertate, qua in deprauatis hominum moribus reprehendendis utebatur, et laudasse uiri
            studium dicitur et magno ipsum auri pondere donasse. </ab>
         <ab type="trad" corresp="#Ar1586_Frischlinus_p1_7">Et il fit tout cela dans la grande
            ferveur du public. Car alors que les autres poètes, loin de se montrer sarcastiques dans
            leurs écrits, acquiesçaient au contraire à la politique du pire, seul se leva
            Aristophane, qui écrivit ses comédies contre les chefs grecs qui semblaient avoir fait
            naître cette guerre civile et il les montra en public. C’est pourquoi lui seul recevait
            au théâtre des applaudissements, lui seul avait un nom, un discours, un visage, une
            démarche populaire auprès des Athéniens. <seg type="note"
               corresp="#Ar1586_Frischlinus_p1_7_n1"> Même Darius, roi des Perses, quand il eut vent
               par ses légats de la liberté dont ce poète faisait preuve pour reprendre les mœurs
               dépravées des hommes, loua, dit-on, le goût de notre homme et le gratifia d’un bon
               poids d’or</seg><note>Frischlin extrapole à partir de ce que prétend Aristophane dans
                  <title>Les Acharniens</title>.</note>. </ab>

         <ab type="orig" xml:id="Ar1586_Frischlinus_p1_8">Qui autem aduersabantur ei ualebant ii
            quidem in senatu multum, sed ciuibus iudundi non erant, idque propter studium
            contentionis et propter dissidia, quae ipsi mouebant in republica. Plebs enim perfuncta
            grauissimis seditionibus atque discordiis, otium malebat, et ordo equester nouarum rerum
            non erat cupidus sed sua tranquillitate et dignitate optimi cuiusque et uniuersae
            reipublicae gloria delectabatur, sicut hoc uidere est in <title>Equitibus</title>
            <persName>Aristophanicis</persName>. </ab>
         <ab type="trad" corresp="#Ar1586_Frischlinus_p1_8">Ceux qui s’opposaient à lui étaient
            certes puissants au sénat mais ne plaisaient pas aux citoyens, et cela en raison de leur
            goût pour la lutte et des dissensions qu’ils provoquaient dans le peuple. Car le peuple,
            après avoir vécu les plus graves séditions et discordes, préférait l’oisiveté ; et
            l’ordre équestre n’était pas favorable à une révolution mais se délectait de sa propre
            tranquillité, des honneurs de l’élite et de la gloire globale de l’état, comme on peut
            le voir dans <title>Les Cavaliers</title> d’Aristophane. </ab>

         <ab type="orig" xml:id="Ar1586_Frischlinus_p1_9">Omitto in hoc loco dicere quam seuerus
            iste fuerit censor, in castigandis forensibus rabulis et nugis ac nebulis sophisticis,
            quibus non modo ciuitas peruertebatur sed etiam pueritia, in ludis litterariis, tum
            grauabatur, tum etiam corrumpebatur. Qua in re etsi <persName>Socratem</persName> atque
               <persName>Euripidem</persName> inimicos suos et superciliosos contemptores, aliqua
            affecit iniuria, tamen ceteros sycophantas et sophistas pro illorum in rempublicam
            meritis non inique tractauit. </ab>
         <ab type="trad" corresp="#Ar1586_Frischlinus_p1_9">J’omets de dire ici le degré de sévérité
            de sa critique dès qu’il s’agissait de fustiger les brailleurs de l’agora, les
            balivernes et les nuées sophistiques, dont non seulement la cité était troublée mais
            dont aussi la jeunesse, dans les écoles littéraires, était parfois alourdie, parfois
            corrompue. A cet égard, même si Socrate et Euripide, ses ennemis et sourcilleux
            contempteurs, ont subi de sa part quelque injustice, il n’en reste pas moins vrai que
            les autres charlatans et sophistes, pour mauvais services rendus à l’État, il les a
            justement traités. </ab>

         <ab type="orig" xml:id="Ar1586_Frischlinus_p1_10">Eaque de causa tam grata fuit lectio
               <persName>Aristophanis</persName>
            <persName>Diuo Chrysostomo</persName> ut noctu eum puluino submoueret et e somno
            expergefactus, eum tam nocturna quam diurna manu uersaret. Quid dicam de
               <persName>Hierone</persName>, Syracusano rege, cui ab Atheniensibus petenti
            eloquentissimum auctorem, unde ipse linguae Atticae leporem posset eddiscere, misere
            illi non <persName>Socratem</persName>, non <persName>Demosthenem</persName>, non
               <persName>Herodotum</persName>, non <persName>Platonem</persName>, non
               <persName>Aristotelem</persName>, sed <persName>Aristophanem</persName> ? Neque mihi
            aliunde consecutus suam eloquentiam uidetur Diuus, quem dixi,
               <persName>Chrysostomus</persName>, praeterquam e sola lectione
               <persName>Aristophanis</persName>. </ab>
         <ab type="trad" corresp="#Ar1586_Frischlinus_p1_10"><seg type="note"
               corresp="#Ar1586_Frischlinus_p1_10_n1">Voilà pourquoi la lecture d’Aristophane
               plaisait tant à Saint Chrysostome que, la nuit, il le mettait sous son oreiller et,
               sitôt sorti du sommeil, de nuit comme de jour, il pouvait l’avoir en main</seg>
            <note xml:id="Ar1586_Frischlinus_p1_10_n1" type="editor">L'anecdote se trouve pour la
               première fois sous la plume d'Alde dans la princeps d'Aristophane (1498)</note>. <seg
               type="note" corresp="#Ar1586_Frischlinus_p1_10_n2"> Et que dire de Hiéron, roi de
               Syracuse, qui, demandant aux Athéniens l’auteur le plus éloquent pour pouvoir y
               apprendre le charme de la langue attique, reçut d’eux non pas Socrate ni Démosthène
               ni Hérodote ni Platon ni Aristote, mais Aristophane ?</seg>
            <note xml:id="Ar1586_Frischlinus_p1_10_n2" type="editor"> ???</note>Et pour moi, j’ai
            l’impression qu’il ne faut pas chercher ailleurs la source de l’éloquence de Saint
            Chrysostome, dont j’ai parlé, que dans la seule lecture d’Aristophane. </ab>

         <ab type="orig" xml:id="Ar1586_Frischlinus_p1_11">Quantum uero ad meam attinet
            interpretationem, dedi hanc ego operam ut e Plauto et <persName>Terentio</persName>,
            omnem fere afferrem Latinitatem, quacum permutarem Graecum poetae huius semonem, idque
            exemplo ipsiusmet <persName>Terentii</persName>, qui integras Comoedias Graecas fecit
            Latinas et fecit suas, mutuatus eas e Graecis, ut <title>Heautontimorumenon</title> a
               <persName>Diphilo</persName>, <title>Eunuchum</title> a
            <persName>Menandro</persName>, <title>Phormionem</title> ab
               <persName>Apollodoro</persName>. Etsi autem in Romanis hisce scaenis ad quas
            accommodaui ego Graeca colloquia, distribuendo singula in suos quinque actus, more <seg
               type="th">Latinae Comoediae</seg>, non retinui ego purum Iambum (nam hoc ne
               <persName>Plautus</persName> quidem et <persName>Terentius</persName> praestitere) ne
            uidelicet gratiam sermonis et leporem orationis amitterem, tamen in choris, ubi pleraque
            canuntur, obseruaui ut non modo sensum sensu et quidem perspicuo orationis genere sed
            etiam numerum numero, pedem pede, et modum modo commutarem </ab>
         <ab type="trad" corresp="#Ar1586_Frischlinus_p1_11"><seg type="traduction">Pour ce qui
               touche à ma traduction, j’ai tâché d’y apporter presque toute la latinité de Plaute
               et Térence à mettre en échange du grec de notre poète, et ce sur l’exemple de Térence
               lui-même, qui a transposé des comédies grecques intégrales pour en faire des latines
               et les faire siennes, en les empruntant aux grecs, comme
                  <title>L’Heautontimoroumenos</title> à Diphile, <title>L’Eunuque</title> à
               Ménandre, et <title>Phormion</title> à Apollodore.</seg>
            <seg type="métrique">Et même si, dans ces scènes romaines à la sauce desquelles j’ai
               accommodé ces dialogues grecs, en distribuant leur matière en cinq
                  actes<note>Frischlin est le premier traducteur de la comédie grecque à la diviser
                  en actes et scènes.</note>, à la mode de la comédie latine, je n’ai pas adopté la
               prosodie iambique pure (car après tout même Plaute et Térence ne l’ont pas fait),
               afin de ne pas perdre la grâce de la langue et le charme du discours, <seg>pourtant
                  dans les chœurs, où presque tout est chanté<note>Les parties chorales sont
                     chantées, sauf quand le coryphée dialogue avec un personnage.</note></seg> , je
               me suis fixé comme règle de faire coïncider dans la transposition non seulement le
               sens au sens (et en laissant transparaître le genre de discours), mais aussi le
               rythme au rythme, le pied au pied et la mesure à la mesure</seg>. </ab>

         <ab type="orig" xml:id="Ar1586_Frischlinus_p1_12">Ex quo intelligit Caesaria tua Maiestas
            quanto labore constiterit istaec mihi interpretatio. Nam qui huiusmodi quiddam ante me
            praestiterit in Graeco auctore et quem ego possim imitari, inueni inter Latinos neminem.
            Vellem autem fuisse mihi exemplar correctius unde meam instituere potuissem
            conuersionem. Nam existunt hodie recentiores quidam, uiri cetera satis docti, addo etiam
            satis arguti, qui laboribus aliorum tum libenter insidiantur, tum frequenter eorundem
            laudibus obtrectant. Si enim diuersam alicubi inueniant lectionem in manuscriptis
            exemplaribus, Deus bone, quantum ipsi exultant et quanto cum supercilio bonas aliorum
            interpretum operas despiciunt ! Quasi uero uel alii, uel nos hoc ipsum praestare non
            potuissemus, quod illi, si nobis libri idonei non defuissent. </ab>
         <ab type="trad" corresp="#Ar1586_Frischlinus_p1_12"><seg type="traduction">Votre Majesté
               Impériale peut en inférer ce que m’a coûté en travail cette traduction. Car quelqu’un
               qui ait fait avant moi quelque chose de comparable à l’égard d’un auteur grec et que
               je puisse imiter, je n’en ai rencontré aucun parmi les auteurs latins. Et j’aurais
               voulu avoir eu un exemplaire plus correct sur lequel pouvoir appuyer ma
               transposition. Car il y a aujourd’hui certains contemporains, hommes par ailleurs
               plutôt savants, j’ajouterais même plutôt subtils, qui passent leur temps à guetter
               les travaux des concurrents et, souvent, à dénigrer leurs mérites. Car si jamais ils
               trouvent ailleurs une leçon différente dans des manuscrits, mon Dieu, comme ils
               sautent de joie ! avec quel froncement de sourcil ils dénigrent le bon travail des
               autres traducteurs ! Comme s’il était vrai que les autres, ou nous, n’aurions pas
               obtenu le même résultat qu’eux si nous avions disposé des bons exemplaires !</seg>
         </ab>

         <ab type="orig" xml:id="Ar1586_Frischlinus_p1_13">Tuam igitur Caesariam Maiestatem supplex
            imploro, oro et obsecro ut ea primam hanc <persName>Aristophanicarum</persName>
            <seg type="th">Comoediarum</seg> partem, suo clementissimo patrocinio defendendam sibi
            suscipiat mihique tantam pro meis laboribus atque impensis mercedem rependat, ut ego
            Caesariae tuae Maiestatis ope subleuatus, etiam reliquos [sic pour reliquas] huius
            poetae fabulas, consimili stylo possim absoluere. </ab>
         <ab type="trad" corresp="#Ar1586_Frischlinus_p1_13"><seg type="note">C’est donc à genoux
               que j’implore, prie et supplie Votre Majesté Impériale de prendre sous la protection
               de son patronage très indulgent cette première partie des comédies d’Aristophane et
               de me verser le salaire que méritent mon travail et mes efforts, afin que, entretenu
               par la cassette de Votre Majesté impériale, je puisse m’acquitter aussi du reste des
               comédies de notre poète, dans un style similaire.<note>Frischlin n'a jamais traduit
                  les autres comédies d'Aristophane</note></seg></ab>

         <ab type="orig" xml:id="Ar1586_Frischlinus_p1_14">Si enim <persName>Scipio</persName> et
               <persName>Laelius</persName>, fortissimi imperatores, <persName>Terentium</persName>
            adiutarunt, siue in conscribendis, siue e Graecia in Latium transferendis <seg type="th"
               >Comoediis</seg>, si ueterum regum et imperatorum haec fuit prope unica et summa
            oblectatio ut, extructis amplissimis et magnificentissimis amphitheatris, possent
            spectare <seg type="th">Comoedias</seg> et <seg type="th">Tragoedias</seg>, si
               <persName>Darius</persName> et <persName>Hiero</persName>, potentissimi reges, tanti
            fecerunt <persName>Aristophanem</persName> quanti ab illis factum eum fuisse diximus, si
               <persName>Aristophanes</persName> ipse, ex quadringentis <seg type="th">ueteribus
               Comicis</seg>, qui omnes nunc olim perierunt, solus ad hanc aetatem superstes, et
            diuinitus nobis conseruatus peruenit, non dubitabo ego quin Caesaria tua Maiestas meum
            istum conatum clementissime sit promotura. </ab>
         <ab type="trad" corresp="#Ar1586_Frischlinus_p1_14">Car s’il est vrai que Scipion et
            Laelius, valeureux capitaines, ont aidé Térence, soit à écrire, soit à faire venir de
            Grèce à Rome des comédies grecques, si d’anciens rois et empereurs n’ont eu comme unique
            délassement suprême, après avoir fait sortir de terre de gigantesques et somptueux
            amphithéâtres, que de pouvoir regarder des comédies et des tragédies, si Darius et
            Hiéron, rois très puissants, ont autant prisé Aristophane que nous disons qu’ils l’ont
            prisé, si Aristophane lui-même, sur les quatre cents auteurs comiques antiques, qui ont
            tous aujourd’hui disparu depuis longtemps, a seul survécu jusqu’à notre époque et nous
            est parvenu providentiellement intact, je serai absolument certain que Votre Majesté
            Impériale mettra sa clémence infinie à promouvoir ce travail que j’ai fait. </ab>

         <ab type="orig" xml:id="Ar1586_Frischlinus_p1_15">Quod ut facere possit, Deum Optimum
            Maximum tota animi contentione precor, obtestorque, orando et rogitando, ut is Caesariae
            tuae Maiestati largiatur uitam, salutem et uictoriam. Francofordiae ad Moenum, Calendis
            Aprilis anni etc.86. Salutem Caesariae tuae Maiestati subiectissimus Nicodemus
            Frischlinus</ab>
         <ab type="trad" corresp="#Ar1586_Frischlinus_p1_15">Et pour qu’elle puisse le faire, de
            toute la force de mon âme, je prie et supplie en demandes et en sollicitations Dieu Très
            Bon Très Grand de répandre sur Votre Majesté Impériale ses largesses de vie, de santé et
            de victoire. A Francfort sur le Main, 1er avril de l’an 86. A Sa Majesté Impériale,
            bonjour de la part de son excellent sujet <persName>Nicodemus Frischlin</persName></ab>

      </body>

   </text>

</TEI>
