<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">

    <teiHeader>

        <fileDesc>

            <titleStmt>

                <title xml:lang="la">Francisci Asulani in recognitionem M. Accii Plauti Epistola ad
                    Nicolaum Sconbergum Archiepiscopum Campanum</title>
                <!-- Indiquer la langue du paratexte dans l'attribut xml:lang : "la" pour le latin et "grc" pour le grec -->
                <!-- Attention ! ne pas faire de retour charriot dans le titre -->
                <author ref="#asulanus_franciscus">Franciscus Asulanus</author>
                <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence en minuscules, précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->

                <!-- Déclarations de responsabilité : indiquent qui a effectué quelle tâche -->
                <!-- Ajouter autant de tâches (<respStmt>) que nécessaire -->
                <respStmt>
                    <resp>Transcription</resp>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <respStmt>
                    <resp>Traduction</resp>
                    <name>Mathieu FERRAND</name>
                </respStmt>

                <respStmt>
                    <resp>Encodage</resp>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <!-- Attention ! Ne pas modifier les tâches ci-dessous -->
                <respStmt>
                    <resp>Modélisation et structuration</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Elysabeth HUE-GAY</persName>
                </respStmt>
                <principal>Malika BASTIN-HAMMOU</principal>
            </titleStmt>

            <!-- Informations de publication sur le laboratoire numérique du projet. Ne pas modifier-->
            <publicationStmt>
                <publisher/>
                <availability>
                    <p>Le site est en accès restreint</p>
                </availability>
                <pubPlace>ithac.elan-numerique.fr</pubPlace>
            </publicationStmt>

            <!-- Informations bibliographiques sur l'édition ancienne d'où le paratexte est tiré -->
            <sourceDesc>
                <bibl>
                    <title>Ex Plauti comoediis XX quarum carmina magna ex parte in mensum suum
                        restituta sunt M.D.XXII. Index uerborum, quibus paulo abstrusioribus Plautus
                        utitur. Argumenta singularum Comoediarum. Authoris uita. Tralatio dictionum
                        graecarum.</title>
                    <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->
                    <author ref="#pl">Plautus</author>
                    <pubPlace ref="#uenetia">Venetiis</pubPlace>
                    <publisher ref="#asulanus_franciscus">Franciscus Asulanus</publisher>
                    <date when="1522">1522</date>
                    <!-- dans l'attribut @when, l'année en chiffre arabes -->
                    <editor ref="#manutius_aldus">Aldus Manutius</editor>
                    <editor ref="#erasmus_desiderius">Erasmus</editor>
                    <ref
                        target="https://books.google.fr/books?id=PYAKKbWf2F4C&amp;pg=PA226&amp;lpg=PA226&amp;dq=x+Plauti+comoediis+XX+quarum+carmina+magna+ex+parte&amp;source=bl&amp;ots=BSuU0dfsJE&amp;sig=ACfU3U1VEy9SMP1vVO4HTXCHRpUuXiAfrw&amp;hl=fr&amp;sa=X&amp;ved=2ahUKEwiVlp-9pfP1AhXIOcAKHTHbAMwQ6AF6BAgQEAM#v=onepage&amp;q=x%20Plauti%20comoediis%20XX%20quarum%20carmina%20magna%20ex%20parte&amp;f=false"
                        >https://books.google.fr/books?id=PYAKKbWf2F4C&amp;pg=PA226&amp;lpg=PA226&amp;dq=x+Plauti+comoediis+XX+quarum+carmina+magna+ex+parte&amp;source=bl&amp;ots=BSuU0dfsJE&amp;sig=ACfU3U1VEy9SMP1vVO4HTXCHRpUuXiAfrw&amp;hl=fr&amp;sa=X&amp;ved=2ahUKEwiVlp-9pfP1AhXIOcAKHTHbAMwQ6AF6BAgQEAM#v=onepage&amp;q=x%20Plauti%20comoediis%20XX%20quarum%20carmina%20magna%20ex%20parte&amp;f=false</ref>
                    <!-- pour les liens google book, remplacer les & par &amp; pour que ça fonctionne -->
                </bibl>
                <listBibl>
                    <bibl>Alexandre Vanautgaerden, « Erasme à Venise », dans Auteur, traducteur,
                        collaborateur, imprimeur... qui écrit ?, éd. M. Furno et R. Mouren, Paris,
                        Classiques Garnier, 2013, p. 69-116.</bibl>
                    <bibl>Alexandre Vanautgaerden, Érasme typographe. Humanisme et imprimerie au
                        XVIe siècle, Bruxelles-Genève, Académie royale-Droz, 2012.</bibl>
                    <bibl>Annaclara Cataldi Palau, Gian Francesco d’Asola e la tipografia aldina. La
                        vita, le edizioni, la biblioteca dell’Asolano, Gênes, Sagep Libri e
                        Communicazione, 1998.</bibl>
                </listBibl>
            </sourceDesc>
        </fileDesc>

        <profileDesc>

            <abstract>
                <p>L’héritier d’Alde Manuce, Francesco Asolano, publie, dans cette édition de 1522,
                    le texte de Plaute tel qu’il fut établi par Alde Manuce et Érasme, quatorze
                    années plus tôt. Érasme, en effet, avait été l’hôte de Manuce en 1508 ; pendant
                    ces dix à onze mois passés à Venise, le Rotterdamois travailla sur l’édition de
                    ses Adages ; il relut et corrigea aussi les manuscrits de Térence et Plaute que
                    préparait Manuce.</p>
                <p>Cette courte lettre évoque successivement la place qu’occupait le théâtre à
                    Sparte, à Athènes et à Rome, puis les qualités de Plaute, dont le texte est
                    parvenu mutilé. Fort heureusement, Alde Manuce et Erasme l’ont amendé, et
                    l’auteur de la liminaire se charge à présent de publier leur travail. La lettre
                    s’achève sur l’éloge du dédicataire.</p>
                <p>Alexandre Vanautgaerden, le premier, a publié et traduit cette lettre dans son
                    Érasme typographe (Droz, 2012, p. 167-168).</p>
                <p>Cette liminaire est suivie dans l’édition de 1522, d’un index verborum, des
                    argumenta de chaque pièce puis de la Plauti vita de Crinito.</p>
            </abstract>

            <langUsage>
                <!-- ne pas modifier -->
                <language ident="fr">Français</language>
                <language ident="la">Latin</language>
                <language ident="grc">Grec</language>
            </langUsage>

        </profileDesc>
        <revisionDesc>
            <change when="2022-02-28">Sarah GAUCHER : identifiants Header, mise en conformité
                Elan</change>
            <change when="2022-02-10">Sarah GAUCHER : création de la fiche TEI, encodage de la
                transcription et de la traduction</change>
        </revisionDesc>
    </teiHeader>

    <text>
        <body>
            <head type="orig" xml:id="Pla1522_Asulanus_p1_0"><persName ref="#asulanus_franciscus"
                    >Francisci Asulani</persName> in recognitionem <persName ref="#pl">M. Accii
                    Plauti</persName> Epistola ad <persName ref="#sconbergus_nicolaus"
                    role="destinataire">Nicolaum Sconbergum</persName> Archiepiscopum
                Campanum.</head>
            <head type="trad" corresp="#Pla1522_Asulanus_p1_0">Lettre de <seg>Franciscus Asulanus
                        <note>Gian Francesco Torresani d’Asola, fils de l’imprimeur vénitien Andrea
                        Torresani d’Asola et beau-frère d’Alde Manuce, dirigea l’atelier après la
                        mort de ce dernier en 1515 ; c’est à ce titre qu’il publie, en 1522,
                        l’édition des comédies de Plaute établie par Alde et Erasme, lors du séjour
                        de ce dernier à Venise en 1508</note></seg> sur l’édition corrigée de
                Plaute, à <seg>Nicolaus Sconbergus, Archevêque de Capoue<note>Nicolaus von Schönberg
                        (1472-1537), prélat allemand, fut évêque de Capoue de 1520 à 1536. En 1535,
                        Paul III le crée cardinal</note></seg>.</head>
            <ab type="orig" xml:id="Pla1522_Asulanus_p1_1">Monimentis literarum proditum est
                    <persName>Lycurgum</persName>, <seg type="allusion">qui Lacedaemoniis leges
                    admodum salutares sanxit, cauisse ne ciues sui in Theatris spectarent histriones
                    qui Comoedias aut Tragoedias agerent atque sui facti rationem hanc
                    sapientissimus uir afferebat, ne decere populum, quem prae aliis excellere
                    uolebat laude seruandarum legum, assuescere id audire, quod, uel ioco, uel
                    serio, esset contra leges factum.<bibl resp="#SG"><author ref="#plut"
                            >Plut.</author>, <title ref="#plut_apophthlac">Apophth. lac.</title>
                        <biblScope>239b</biblScope>. <note>Si Lycurgue n'est pas explicitement donné
                            comme instigateur de cette loi, il est question de lui avant et après ce
                            passage.</note></bibl></seg></ab>
            <ab type="trad" corresp="#Pla1522_Asulanus_p1_1" ana="#TH_hist #TH_repr">La tradition
                savante rapporte que Lycurgue, qui fit adopter des lois tout à fait salutaires pour
                les Lacédémoniens, a veillé à ce que ses concitoyens n’allassent pas aux théâtres
                voir les histrions qui jouaient comédies ou tragédies ; cet homme très sage s’en
                expliquait ainsi : il ne convenait pas que son peuple, dont il voulait qu’il
                excellât et s’illustrât, plus que tous les autres, par son respect pour les lois,
                s’habitue à entendre ce qui, par jeu ou pas, pouvait enfreindre les lois.</ab>
            <ab type="orig" xml:id="Pla1522_Asulanus_p1_2">Contra Athenienses ex hoc maximam gloriam
                se adipisci putabant, si utriusque rei et inuentores et amplificatores
                haberentur.</ab>
            <ab type="trad" corresp="#Pla1522_Asulanus_p1_2" ana="#TH_hist">Les Athéniens, au
                contraire, pensaient pouvoir tirer du théâtre la plus grande gloire, s’ils passaient
                pour avoir inventé et développé l’un et l’autre genre.</ab>
            <ab type="orig" xml:id="Pla1522_Asulanus_p1_3">Itaque Theatra, Orchestram, Scenam
                magnificentissime extruebant, et magnam insuper pecuniae uim in eiusmodi ludorum
                apparatum elargiebantur.</ab>
            <ab type="trad" corresp="#Pla1522_Asulanus_p1_3" ana="#TH_hist #TH_repr">C’est pourquoi
                ils construisaient des théâtres, avec orchestre et scène, tout à fait somptueux, et
                consacraient des fortunes à l’organisation de ce genre de spectacles.</ab>
            <ab type="orig" xml:id="Pla1522_Asulanus_p1_4">Atque hos nostri Romani, cum aliis multis
                in rebus, tum in hac potissimum imitati sunt.</ab>
            <ab type="trad" corresp="#Pla1522_Asulanus_p1_4" ana="#TH_hist #TH_repr">Nos ancêtres
                les Romains, comme en bien d’autres domaines, les imitèrent tout particulièrement en
                cela.</ab>
            <ab type="orig" xml:id="Pla1522_Asulanus_p1_5">Exstimarunt enim hilaritatem animorum
                maxime quaerendam esse Populo omnium gentium uictori, idque post bella multa
                confecta et imperium omnium prope gentium in Vrbe constitutum ; morem autem illum
                Lacedaemoniorum, ut nimis durus, nimisque inhumanum, repudiarunt, quandoquidem
                suapte natura se ad leges seruandas propensos esse intellegebant.</ab>
            <ab type="trad" corresp="#Pla1522_Asulanus_p1_5" ana="#TH_hist #TH_repr">Ils estimèrent
                en effet qu’il était de la plus grande importance de donner l’occasion de s’amuser à
                un peuple qui avait vaincu l’univers entier, et cela après avoir mené tant de
                guerres et avoir établi à Rome leur empire sur la totalité, ou presque, de l’univers
                ; ils rejetèrent l’attitude des Spartiates qui leur paraissait trop dure et trop
                inhumaine ; ils savaient du reste qu’ils étaient portés par nature à respecter les
                lois.</ab>
            <ab type="orig" xml:id="Pla1522_Asulanus_p1_6">Summam igitur auctoritatem semper
                tribuerunt Sarsinati <persName ref="#pl">Plauto</persName>, cum propter incredibilem
                elegantiam, et dicendi puritatem, tum propter festiuitatem, et singularem in iocis
                leporem.</ab>
            <ab type="trad" corresp="#Pla1522_Asulanus_p1_6" ana="#TH_hist #TH_dram">C’est ainsi
                qu’ils donnèrent à Plaute, de Sarsine, la préséance, en raison non seulement de son
                exceptionnelle élégance et de la pureté de son expression, mais aussi de son
                enjouement et de la grâce singulière de son humour.</ab>
            <ab type="orig" xml:id="Pla1522_Asulanus_p1_7">Et certe illius fabulae non minus cum
                agerentur delectarunt ciues romanos, quam, cum legerentur, tenuerunt doctissimum
                quemque.</ab>
            <ab type="trad" corresp="#Pla1522_Asulanus_p1_7" ana="#TH_hist #TH_dram #TH_repr">Et
                assurément les pièces de Plaute plurent au peuple de Rome, lorsqu’on les jouait,
                tout autant qu’elles passionnèrent les plus savants, lorsqu’on les lisait. </ab>
            <ab type="orig" xml:id="Pla1522_Asulanus_p1_8">Vtinam omnes illas haberemus, aut certe
                quae nunc habentur integrae et suo carminum sensu constantes extarent, recentiores
                certe illarum enarratores non tam anxie laborarent in quaerenda significatione
                multorum uerborum et sententia auctoris, cum emendata lectione et proprietatis [et]
                uerborum et ueri sensus admoneri possent.</ab>
            <ab type="trad" corresp="#Pla1522_Asulanus_p1_8" ana="#PH">Ah, si seulement nous avions
                conservées chacune d’entre elles ou si celles que nous possédons demeuraient sans
                lacune et présentaient une métrique cohérente ! Les commentateurs d’aujourd’hui ne
                s’épuiseraient pas, avec un soin inquiet, à chercher la signification des mots et la
                pensée de l’auteur ; une lecture correcte leur révèlerait la propriété des mots et
                leur sens véritable.</ab>
            <ab type="orig" xml:id="Pla1522_Asulanus_p1_9">Veruntamen quanta diligentia fieri potuit
                    <persName ref="#manutius_aldus">Aldus</persName> noster et <persName
                    ref="#erasmus_desiderius">Erasmus Roterodamus</persName> illas olim castigarunt,
                quorum exemplar nos librariis nostris proponentes has XX describendas curauimus,
                easque sacratissimo nomini tuo dicatas uoluimus Archiepiscope amplissime ob
                singularem praedicationem <persName>Petri Alcyonii</persName> uiri clarissimi de
                tuis uirtutibus.</ab>
            <ab type="trad" corresp="#Pla1522_Asulanus_p1_9" ana="#PH #PA">Autrefois, pourtant,
                    <seg>notre cher Alde et Érasme de Rotterdam les ont corrigées, avec toute la
                    diligence possible <note>Erasme a travaillé 10 mois à Venise, en 1508, dans
                        l’atelier d’Alde Manuce</note></seg>. Pour proposer ce travail à nos
                libraires, nous avons fait imprimer les vingt comédies et nous avons voulu qu’elles
                fussent dédiées à ton très vénérable nom, illustrissime Archevêque, en raison du
                singulier éloge que fit de tes vertus le très renommé <seg>Petrus Alcyonius
                        <note>Pietro Alcionio (c. 1487-1527), humaniste vénitien, commença sa
                        carrière comme correcteur dans l’atelier des Alde, avant d’engager une
                        brillante carrière d’helléniste, à Florence d’abord, puis à Rome, dans
                        l’entourage de son protecteur, le pape Clément VII. Il traduisit Aristote,
                        notamment</note></seg>.</ab>
            <ab type="orig" xml:id="Pla1522_Asulanus_p1_10">Is enim eum te esse affirmabat qui
                solus, aut cum paucis, quam recte castigatae esse, iudicare posses propter summam
                literarum graecarum, et latinarum peritiam, et totius antiquitatis cognitionem.</ab>
            <ab type="trad" corresp="#Pla1522_Asulanus_p1_10" ana="#PH #PA">Celui-ci en effet
                affirmait que tu étais presque le seul à pouvoir apprécier la qualité des
                corrections, tant sont grandes tes compétences en lettres grecques et latines et ta
                connaissance de l’Antiquité tout entière. </ab>
            <ab type="orig" xml:id="Pla1522_Asulanus_p1_11">Itaque non solum in editione huius
                praestantissimi Poetae nomen tuum celebrari uidebis, sed etiam in multis aliis
                auctoribus Graecis et Latinis, qui propediem a nobis excusi emittentur, quoniam
                illorum dignitatem uel ob singularem tuam doctrinam tueri, uel ob summam
                auctoritatem amplificare poteris.</ab>
            <ab type="trad" corresp="#Pla1522_Asulanus_p1_11" ana="#PA">C’est pourquoi tu verras
                célébrer ton nom non seulement dans l’édition de ce poète majeur, mais aussi dans
                celles de nombreux autres auteurs grecs et latins, que nous ferons imprimer sous peu
                ; tu pourras de fait garantir leur prestige par ta science exceptionnelle ou bien
                l’augmenter par ton immense autorité.</ab>
            <ab type="orig" xml:id="Pla1522_Asulanus_p1_12">Vale.</ab>
            <ab type="trad" corresp="#Pla1522_Asulanus_p1_12">Porte-toi bien.</ab>







        </body>
    </text>
</TEI>
