<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">

    <teiHeader>

        <fileDesc>

            <titleStmt>

                <!--Ci-dessous le titre du paratexte-->
                <title xml:lang="la">De Farnabio ad lectorem </title>
                <author ref="#audoenus_ioannes">Ioannes Audoenus</author>

                <respStmt>
                    <resp>Transcription</resp>
                    <name>Pascale PARE-REY</name>
                </respStmt>
                <respStmt>
                    <resp>Traduction</resp>
                    <name>Pascale PARE-REY</name>
                </respStmt>
                <respStmt>
                    <resp>Encodage</resp>
                    <name>Diandra CRISTACHE</name>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <!-- Ajouter autant de tâches que nécessaire -->
                <!-- Attention ! Ne pas modifier les tâches ci-dessous -->
                <respStmt>
                    <resp>Modélisation et structuration</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Elysabeth HUE-GAY</persName>
                </respStmt>
                <principal>Malika BASTIN-HAMMOU</principal>
            </titleStmt>

            <!-- Informations de publication sur le laboratoire numérique du projet-->
            <publicationStmt>
                <publisher/>
                <availability>
                    <p>Le site est en accès restreint</p>
                </availability>
                <pubPlace>ithac.elan-numerique.fr</pubPlace>
            </publicationStmt>

            <sourceDesc>
                <bibl>
                    <title>L. et M. Annaei Senecae atque aliorum tragoediae. Animadversionibus et
                        notis marginalibus fideliter emendatae atque illustratae. </title>
                    <author ref="#sen">Seneca</author>
                    <editor ref="#farnabius_thomas"> Thomas Farnabius </editor>
                    <publisher ref="#kingstonius_felix">Felix Kingstonius</publisher>
                    <pubPlace ref="#londinium">Londini</pubPlace>
                    <date when="1613">1613</date>
                    <ref
                        target="https://www.google.fr/books/edition/L_et_M_Annaei_Senecae_atque_aliorum_trag/racUAAAAQAAJ?hl=fr&amp;gbpv=1&amp;dq=Dramata+si+spectes,+est+alta&amp;pg=RA1-PT14&amp;printsec=frontcover"
                        >https://www.google.fr/books/edition/L_et_M_Annaei_Senecae_atque_aliorum_trag/racUAAAAQAAJ?hl=fr&amp;gbpv=1&amp;dq=Dramata+si+spectes,+est+alta&amp;pg=RA1-PT14&amp;printsec=frontcover</ref>
                </bibl>
                <listBibl>
                    <bibl>"Book 8 in Latin: Epigrammatum Ioannis Owen Cambri-Britanni Oxoniensis, d
                        tres maecenates, libri tres". The Shakespeare Institute (in Latin).
                        Retrieved 13 June 2020.<ref
                            target="http://www.philological.bham.ac.uk/owen/8eng.html"
                            >http://www.philological.bham.ac.uk/owen/8eng.html</ref></bibl>
                    <bibl>Cousin, John William (1910), "Owen, John (epigrammatist)", A Short
                        Biographical Dictionary of English Literature, London: J. M. Dent &amp;
                        Sons</bibl>
                    <bibl>Enck, J.J. (1949). "John Owen's Epigrammata". Harvard Library Bulletin. 3:
                        431–434.</bibl>
                    <bibl>Foster, Jonathan (1975). "The Tempora Mutantur Symphony of Joseph Haydn".
                        Haydn Yearbook. 9: 328.</bibl>
                    <bibl>Jones, John Henry (1959). "Own, John (1564? - 1628?), epigrammatist".
                        Dictionary of Welsh Biography.</bibl>
                    <bibl>Schroeder, David P. (1997). Haydn and the Enlightenment: The Late
                        Symphonies and Their Audience. Clarendon Press. p. 69. ISBN
                        978-0-19-816682-5.</bibl>
                    <bibl>Sutton, Dana F. (1999). "Hypertext critical edition of the Epigrammata of
                        John Owen (Ioannis Audoenus)". Irvine: The University of California. <ref
                            target="http://www.philological.bham.ac.uk/owen/"
                            >http://www.philological.bham.ac.uk/owen/</ref></bibl>
                    <bibl>Wicham, William; Winton, Bishop of. "Book 8 (English)". The Shakespeare
                        Institute. Retrieved 13 June 2020. <ref
                            target="http://www.philological.bham.ac.uk/owen/8eng.html"
                            >http://www.philological.bham.ac.uk/owen/8eng.html</ref></bibl>
                    <bibl>Willems, Alphonse (1880). Les Elzevier. Brussels. p. 78.</bibl>
                </listBibl>
            </sourceDesc>
        </fileDesc>

        <profileDesc>

            <abstract>
                <p>Johannes Audoenus / Johannes Owenius / Juan Owen / John Owen (c.1550/1560 –
                    c.1622/1628) fait l’éloge de l’éditeur, Farnaby, dans cette paire de distiques
                    élégiaques adressée au lecteur où il joue sur les participes lectus, lectissimus
                    et intellectus. </p>
            </abstract>

            <langUsage>
                <language ident="la">Latin</language>
                <language ident="grc">Grec</language>
            </langUsage>

        </profileDesc>

        <revisionDesc>
            <change when="2022-03-01">Sarah GAUCHER : identifiants Header, mise en conformité
                Elan</change>
            <change when="2021-03-29">Diandra CRISTACHE : Mise à jour et chargement sur le
                Pensoir</change>
            <change when="2020-09-21">Diandra CRISTACHE : Encodage de la transcription et de la
                traduction</change>
        </revisionDesc>

    </teiHeader>

    <text>
        <body ana="#PH #PE">
            <head type="orig" xml:id="Sq1513_Audoenus_p1_0">De <persName ref="#farnabius_thomas"
                    >Farnabio</persName> ad lectorem </head>
            <head type="trad" corresp="#Sq1513_Audoenus_p1_0">Sur Farnaby, au lecteur </head>

            <ab type="orig" xml:id="Sq1513_Audoenus_p1_1"><l>Qui tantum modo lectus erat lectissimus
                    autor,</l>
                <l>Nunc intellectus, non modo lectus, erit.</l><l>Suspensum <persName ref="#sen"
                        >Senecae</persName> te littera nulla tenebit,</l>
                <l>
                    <persName ref="#farnabius_thomas">Farnabii</persName> dum te spiritus intus
                    agit.</l>
            </ab>
            <ab type="trad" corresp="#Sq1513_Audoenus_p1_1"><l>Celui qui était seulement lu, auteur
                    élu entre tous,</l>
                <l>Sera maintenant compris, non seulement lu.</l><l>Aucune lettre de Sénèque ne
                    t’arrêtera,</l>
                <l>Tant que l’esprit de Farnaby demeure en toi.</l>
            </ab>

            <ab type="orig" xml:id="Sq1513_Audoenus_p1_2"><persName ref="#audoenus_ioannes">Ioannes
                    Audoenus</persName></ab>
            <ab type="trad" corresp="#Sq1513_Audoenus_p1_2">John Owen </ab>


        </body>
    </text>
</TEI>
