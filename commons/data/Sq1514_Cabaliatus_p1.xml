<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
    <teiHeader>

        <fileDesc>

            <titleStmt>
                <title xml:lang="la">Polydori Comitis Cabaliati in defensionem Caietani Cremonensis,
                    praeceptoris sui </title>
                <author ref="#cabaliatus_polydorus">Polydorus Cabaliatus</author>

                <!-- Déclarations de responsabilité : indiquent qui a effectué quelle tâche -->
                <!-- Tâche n°1 -->
                <respStmt>
                    <resp>Transcription</resp>
                    <name>Pascale PARE-REY</name>
                </respStmt>
                <respStmt>
                    <resp>Traduction</resp>
                    <name>Pascale PARE-REY</name>
                </respStmt>
                <respStmt>
                    <resp>Encodage</resp>
                    <name>Pascale PARE-REY</name>
                    <name>Diandra CRISTACHE</name>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <!-- Attention ! Ne pas modifier les tâches ci-dessous -->
                <principal>Malika BASTIN-HAMMOU</principal>
                <respStmt>
                    <resp>Modélisation et structuration</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Elysabeth HUE-GAY</persName>
                </respStmt>
                <respStmt>
                    <resp>Première version du modèle d'encodage</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Anne Garcia-Fernandez</persName>
                </respStmt>
            </titleStmt>

            <!-- Informations de publication sur le laboratoire numérique du projet. Ne pas modifier-->
            <publicationStmt>
                <publisher/>
                <availability>
                    <p>Le site est en accès restreint</p>
                </availability>
                <pubPlace>ithac.elan-numerique.fr</pubPlace>
            </publicationStmt>

            <sourceDesc>
                <bibl>
                    <title>L. Annei Senecae Tragoediae, pristinae integritati restitutae per
                        exactissimi judici viros, post Avantium et Philologum. D. Erasmum
                        Roterodamum. Gerardum Vercellanum. Aegidium Maserium. cum metrorum
                        praesertim tragicorum ratione ad calcem operis posita. explanatae
                        diligentissime tribus commentariis G. Bernardino Marmita. Daniele Gaietano.
                        Jodoco Badio Ascensio</title>
                    <author ref="#sen">Seneca</author>
                    <editor ref="#erasmus_desiderius">D. Erasmus Roterodamus</editor>
                    <editor ref="#uercellanus_gerardus">Gerardus Vercellanus</editor>
                    <editor ref="#maserius_aegidius">Aegidius Maserius</editor>
                    <publisher ref="#ascensius_iodocus">Jodocus Badius Ascensius </publisher>
                    <pubPlace ref="#lutetia">Lutetiae</pubPlace>
                    <date when="1514">1514</date>
                    <biblScope unit="folio">F. Aa4v </biblScope>
                </bibl>
                <listBibl>
                    <bibl>Paré-Rey, Pascale, "Les éditions des tragédies de Sénèque conservées à la
                        Bibliothèque nationale de France (XVe-XIXe s.)", in L’Antiquité à la BnF,
                        17/01/2018, <ref target="https://antiquitebnf.hypotheses.org/1643"
                            >https://antiquitebnf.hypotheses.org/1643</ref></bibl>
                </listBibl>
            </sourceDesc>

        </fileDesc>

        <profileDesc>
            <abstract>
                <p>Daniele Gaietano, un des trois commentateurs de cette édition, reçoit un poème en
                    distiques élégiaques de la part d’un certain « Polydorus Cabaliatus » (sans
                    doute un pseudonyme) qui blâme les critiques virulents qui se sont élevés contre
                    Gaietano.</p>
                <p/>
            </abstract>
            <!-- recense les langues susceptibles d'être utilisées et la façon de les encoder -->
            <langUsage>
                <language ident="fr">Français</language>
                <language ident="la">Latin</language>
                <language ident="gr">Grec</language>
            </langUsage>
        </profileDesc>

        <revisionDesc>
            <!-- à utiliser si nouvel intervenant sur le fichier -->
            <change when="2022-03-01">Sarah GAUCHER : identifiants Header, mise en conformité
                Elan</change>
            <change who="Diandra Cristache" when="2021-05-23">Diandra CRISTACHE : mise à jour et
                publication sur le Pensoir</change>
            <change who="Malika BASTIN-HAMMOU" when="2020-02-27">Malika BASTIN-HAMMOU : changements
                décrits dans le mail du 27 février 2020</change>
        </revisionDesc>

    </teiHeader>


    <text>
        <!-- Cette portion concerne le texte lui-même -->
        <body ana="#PA">
            <head type="orig" xml:id="Sq1514_Gaietanus_p1_0">Polydori Comitis <persName
                    ref="#cabaliatus_polydorus">Cabaliati</persName> in defensionem <persName
                    ref="#gaietanus_danielis" role="destinataire">Caietani Cremonensis</persName>,
                praeceptoris sui</head>
            <head type="trad" corresp="#Sq1514_Gaietanus_p1_0">De son compagnon « Polydore Chevalier
                », pour la défense de Gaetano de Crémone, son précepteur</head>

            <ab type="orig" xml:id="Sq1514_Gaietanus_p1_1"><l>Dentibus emissum superis inuise
                    profanas</l>
                <l>Tu lolligeneis hoc temerator opus ?</l>
                <l>Non uomitu scelerare pudet gemmata uerendi</l>
                <l>Maiestate (scelus liuide) scripta uiri ?</l>
                <l>Quo pudibunde uidens fremitu afflictare sub imis</l>
                <l>Visceribus ? Titulum non truculente times ? </l>
                <l>Tartarea traiectus aqua per compita damnas</l>
                <l>Haec quae sollicitum te sine fine docent. </l>
                <l>Quo grauiore potes, stimulate furoribus, ausu</l>
                <l>Regis stellifici saeue nocere trono ?</l>
                <l>Quid rationis egens libro cunctaris aperto ?</l>
                <l>Lucida quid taetro polluis ista sono?</l>
                <l>Auctor hic Elysiis si forte citetur ab oris,</l>
                <l>Non poterat sensum sic reserare suum.</l></ab>
            <ab type="trad" corresp="#Sq1514_Gaietanus_p1_1">
                <l>Par tes dents supérieures dignes d’un calmar,<note>Il est difficile de traduire
                        le nom <foreign xml:lang="la">dentibus</foreign> autrement si l’on veut le
                        qualifier par l’adjectif <foreign xml:lang="la">superis</foreign>, le tout
                        signifiant les « dents du haut », alors que le sens figuré de « morsures,
                        attaques » conviendrait mieux à l’action du calmar (<foreign xml:lang="la"
                            >lolligo, inis</foreign>, f : seiche, calmar). Mais les espèces géantes
                        du céphalopode ont bien des petites dents et des crochets bordant les
                        ventouses de leurs tentacules.</note></l>
                <l>tu profanes, odieux corrupteur, cet ouvrage qui vient de paraître ?</l>
                <l>N’as-tu pas honte de souiller (<seg>crime commis par jalousie<note><foreign
                                xml:lang="la">Liuidus</foreign> a peut-être aussi le sens propre de
                            « bleuâtre, noirâtre », renvoyant à l’encre du mollusque précédemment
                            évoqué.</note></seg>) de tes vomissures, </l>
                <l>les écrits perlés par la majesté d’un homme vénérable ?</l>
                <l>Par quel frisson, en le voyant, es-tu frappé, infâme, au fond </l>
                <l>de tes entrailles ? Ne crains-tu pas, cruel, son titre ?</l>
                <l>Après être passé par les eaux du Tartare, à travers ses carrefours, </l>
                <l>tu condamnes ce qui t’instruis, alors que tu es dans des alarmes
                    perpétuelles.</l>
                <l>Par quelle audace plus grave, aiguillonné par tes fureurs, </l>
                <l>peux-tu nuire, cruel, au trône d’un roi étoilé ?</l>
                <l>Qu’hésites-tu, privé de ta raison, alors que ton livre reste ouvert ? </l>
                <l>Que salis-tu ces lumières de tes bruits affreux ?</l>
                <l>Si par hasard notre auteur était rappelé des rivages élyséens,</l>
                <l>il ne pourrait dans ces conditions dévoiler son sens. </l></ab>

        </body>
    </text>
</TEI>
<!-- Pour plus de précisions sur les règles de transcription, consulter le tableau "guide_encodage_ithac" -->
