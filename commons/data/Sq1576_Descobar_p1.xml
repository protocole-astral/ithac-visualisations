<?xml version="1.0" encoding="UTF-8"?>


<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">

  <teiHeader>

    <fileDesc>

      <titleStmt>

        <title xml:lang="la">Ad Martinum Antonium Delrio Augustinus Descobar Benventanus</title>
        <author ref="#descobar_augustinus">Augustinus Descobar Benventanus</author>

        <!-- Déclarations de responsabilité : indiquent qui a effectué quelle tâche -->
        <!-- Tâche n°1 -->
        <respStmt>
          <resp>Transcription</resp>
          <name>Valérie THIRION</name>
        </respStmt>
        <!-- Tâche n°2 -->
        <respStmt>
          <resp>Traduction</resp>
          <name>Pascale PARÉ-REY</name>
        </respStmt>
        <!-- Tâche n°3 -->
        <respStmt>
          <resp>Encodage</resp>
          <name>Valérie THIRION</name>
          <name>Sarah GAUCHER</name>
        </respStmt>
        <!-- Ajouter autant de tâches que nécessaire -->
        <!-- Attention ! Ne pas modifier les tâches ci-dessous -->
        <respStmt>
          <resp>Modélisation et structuration</resp>
          <persName>Elisabeth GRESLOU</persName>
          <persName>Elysabeth HUE-GAY</persName>
        </respStmt>
        <principal>Malika BASTIN-HAMMOU</principal>
      </titleStmt>

      <!-- Informations de publication sur le laboratoire numérique du projet-->
      <publicationStmt>
        <publisher/>
        <availability>
          <p>Le site est en accès restreint</p>
        </availability>
        <pubPlace>ithac.elan-numerique.fr</pubPlace>
      </publicationStmt>

      <sourceDesc>

        <bibl>
          <title>In L. Annaei Senecae Cordubensis Poetae grauissimi Tragoedias decem ; Scilicet
            Herculem Furentem, Herculem Œtaeum, Medeam, Hippolytum, Œdipum, Thebaidem, Thyestem,
            Troades, Agamemnonem, Octauiam, Amplissima adversaria quae loco commentarii esse possunt
            ex bibliotheca Martini Antonii Delrio, I. C. </title>
          <!-- l'attribut @level permet d'indiquer s'il s'agit d'une monographie (m), d'un article ou chapitre d'ouvrage (a) ou d'un périodique (j) -->
          <author ref="#sen">Seneca</author>
          <pubPlace ref="#antuerpia">Antverpiae</pubPlace>
          <publisher ref="#plantinus_christophorus">Christophorus Plantinus</publisher>
          <date when="1576">1576</date>
          <editor ref="#delrio_martinus">Martinus Antonius Delrio</editor>
          <ref target="https://numelyo.bm-lyon.fr/f_view/BML:BML_00GOO0100137001102809949"/>
          <ref
            target="https://books.google.fr/books?id=YWZID-M1KUEC&amp;printsec=frontcover&amp;hl=fr&amp;source=gbs_ge_summary_r&amp;cad=0#v=onepage&amp;q&amp;f=false/"/>
          <ref target="https://books.google.fr/books?vid=BML37001103093519"/>
        </bibl>
        <listBibl>
          <bibl><author>Florence de Caigny</author>, <title level="u">« Les <title>Syntagma tragœdiæ
                latinæ</title> de Del Rio (1593-1594) : pour une lecture jésuite des pièces de
              Sénèque »</title>, colloque « Anthropologie tragique et création poétique de
            l’Antiquité au XVII<hi rend="sup">e</hi> siècle français », Nice, 24-26 novembre
            2016.</bibl>

          <bibl><author>M. Dreano</author>, <title level="a">« Un commentaire des tragédies de
              Sénèque au XVI<hi rend="sup">e</hi> siècle par Martin-Antoine Del Rio »</title>, dans
              <title level="m">Les tragédies de Sénèque et le théâtre de la Renaissance</title>, éd.
              <editor>J. Jacquot</editor>
            <foreign xml:lang="la">et al.</foreign>, <pubPlace>Paris</pubPlace>, <publisher>Éd. du
              CNRS</publisher>, <date>1964</date>, <biblScope unit="page"
            >p. 203-209</biblScope>.</bibl>

          <bibl><author>J. Machielsen</author><title level="m">Martin Delrio, Demonology and
              Scholarship in the Counter-Reformation</title>, <pubPlace>Oxford</pubPlace>,
              <publisher>OUP</publisher>, <date/>2015.</bibl>
          <bibl>Paré-Rey, Pascale, "Les éditions des tragédies de Sénèque conservées à la
            Bibliothèque nationale de France (XVe-XIXe s.)", in L’Antiquité à la BnF, 17/01/2018,
              <ref target="https://antiquitebnf.hypotheses.org/1643"
              >https://antiquitebnf.hypotheses.org/1643</ref></bibl>
        </listBibl>
      </sourceDesc>

    </fileDesc>


    <profileDesc>

      <abstract>
        <p>Éloge de Delrio en distiques élégiaques pour son travail d’éditeur.</p>
      </abstract>

      <langUsage>
        <!-- ne pas modifier -->
        <language ident="fr">Français</language>
        <language ident="la">Latin</language>
      </langUsage>

    </profileDesc>

    <revisionDesc>
      <!-- pour indiquer des modifications apportées au fichier -->
      <change when="2022-03-01">Sarah GAUCHER : identifiants Header, mise en conformité
        Elan</change>
      <change who="Diandra CRISTACHE" when="2021-05-23">Diandra CRISTACHE : mise à jour et
        publication sur le Pensoir</change>
      <change when="2021-01-22">Valérie Thirion: encodage de la transcription</change>
      <change when="2022-01-06">Pascale Paré: mise à jour bibliographique </change>
    </revisionDesc>

  </teiHeader>

  <text>
    <body>
      <head type="orig" xml:id="Sq1576_Descobar_p1_0">Ad <persName ref="#delrio_martinus"
          role="destinataire">Martinum Antonium Delrio</persName>
        <persName ref="#descobar_augustinus">Augustinus Descobar Benventanus</persName></head>
      <!-- titre du paratexte -->
      <head corresp="#Sq1576_Descobar_p1_0" type="trad">À Martin Antonio Delrio, <seg>Augustin
          Descobar Beneventano<note>Marco Beneventanus Cosmographe napolitain, il édite à Rome en
            1507-1508 la <title>Géographie</title> de Ptolémée. Francisco Escobar/ Franciscus
            Scobarius : philologue et docteur en médecine. Enseigna la rhétorique dans les
            universités de Paris, de Rome et de Barcelone ; <title>Constitutiones sacrae
              provinciales Tarraconenses, a D. Mattia Sorribes collectae, cura F. Scobarii
              editae</title>
          </note></seg></head>

      <ab type="orig" xml:id="Sq1576_Descobar_p1_1">
        <l><persName ref="#delrio_martinus">Antoni</persName>, splendor patriae, decus addite
          nostrae</l>
        <l>Hesperiae, atque mei maxima pars animi,</l>
        <l>Qui potis antiquum <persName ref="#sen">Seneca</persName> illustrare leporem</l>
        <l>Felix,<persName ref="#sen">Annaei</persName> dignus et ingenio,</l>
        <l>Quod Graium Latio tanta grauitate cothurnum</l>
        <l>Rettulit, ut palmam praeripiat merito</l>
        <l>Italiae, Ausoniosque, premens, assurgat in alta</l>
        <l>Carmina quae <persName ref="#soph">Sophocli</persName> Calliopeia dedit.</l>
      </ab>
      <ab corresp="#Sq1576_Descobar_p1_1" type="trad" ana="#TH_hist">
        <l>Antoine, splendeur, ornement de notre patrie,</l>
        <l>Hespérie et plus grande partie de mon âme,</l>
        <l>Toi qui peux mettre en lumière le charme antique de Sénèque,</l>
        <l>Béni des dieux, et digne du génie d’Annaeus ;</l>
        <l>Le cothurne grec qu’il a rapporté avec une si grande profondeur</l>
        <l>vers le Latium, pour ravir la palme de l’Italie</l>
        <l>à bon droit, rabaissant les Ausoniens, pour se hisser jusqu’aux hauteurs</l>
        <l><seg>des poèmes de Sophocle que Calliope lui confia<note>Transposition des tragédies
              grecques à la latine.</note></seg>.</l>
      </ab>

      <ab type="orig" xml:id="Sq1576_Descobar_p1_2">
        <l>Nam quis te dignus magis est, qui maxima doctis</l>
        <l>Virginibus cura es et qui utriusque, nites</l>
        <l>Muneribus linguae : quibus ad penetralia iuris</l>
        <l>Aptius esse nihil sat tua scripta probant.</l>
      </ab>
      <ab corresp="#Sq1576_Descobar_p1_2" type="trad" ana="#PA">
        <l>Car qui est plus digne que toi, qui représentes la plus grande protection </l>
        <l>pour les savantes jeunes filles et qui brilles</l>
        <l><seg>par tes bons offices en l’une et l’autre langue<note>Allusion aux compétences en
              latin et en grec – voir note suivante – de Delrio ; mais il ne semble pas avoir écrit
              en grec. Traduction un peu délicate.</note></seg>, dans lesquelles tes écrits prouvent
          suffisamment</l>
        <l>qu’il n’est rien de plus approprié aux mystères du <seg>droit<note>M. A. Delrio fut
              successivement philologue, juriste et prêtre jésuite. Il étudia très jeune, apprit de
              nombreuses langues anciennes et vivantes (grec et latin, mais aussi chaldéen, hébreu,
              flamand, espagnol, français, italien, allemand). Après ces études de lettres et de
              langues, il se tourna vers le droit, obtint son titre de docteur en droit civil et fut
              nommé conseiller de Brabant par Philippe II. </note></seg>.</l>
      </ab>

      <ab type="orig" xml:id="Sq1576_Descobar_p1_3">
        <l>Nam legum implicitos placeat seu soluere nodos,</l>
        <l>Seu forte ad Musas te tibi surripias :</l>
        <l>Ornat pacifera te <persName>Papinianus</persName> oliua,</l>
        <l>Cingit Apollinea fronde Thalia comas.</l>
      </ab>
      <ab corresp="#Sq1576_Descobar_p1_3" type="trad" ana="#PA">
        <l>De fait, qu’il te plaise de résoudre les nœuds embrouillés des lois</l>
        <l>Ou que d’aventure tu te dérobes à toi-même pour gagner les Muses,</l>
        <l><persName>Papinien<note>Jurisconsulte, ami de Septime Sévère.</note></persName> te décore
          de l’olive porteuse de paix,</l>
        <l>Thalie couronne ta chevelure de <seg>feuillage apollinien<note>Du laurier, symbole de la
              victoire.</note></seg>.</l>
      </ab>

    </body>
  </text>
</TEI>
