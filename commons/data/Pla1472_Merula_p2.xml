<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">

    <teiHeader>

        <fileDesc>

            <titleStmt>

                <title xml:lang="la">De uita Comoediisque Plauti excerpta quaedam ex Auctoribus
                    grauissimis. </title>
                <!-- Indiquer la langue du paratexte dans l'attribut xml:lang : "la" pour le latin et "grc" pour le grec -->
                <!-- Attention ! ne pas faire de retour charriot dans le titre -->
                <author ref="#merula_georgius">Georgius Alexandrinus Merula</author>
                <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence en minuscules, précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->

                <!-- Déclarations de responsabilité : indiquent qui a effectué quelle tâche -->
                <!-- Ajouter autant de tâches (<respStmt>) que nécessaire -->
                <respStmt>
                    <resp>Transcription</resp>
                    <name>Matthieu FERRAND</name>
                </respStmt>
                <respStmt>
                    <resp>Traduction</resp>
                    <name>Matthieu FERRAND</name>
                </respStmt>
                <respStmt>
                    <resp>Encodage</resp>
                    <name>Diandra CRISTACHE</name>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <!-- Attention ! Ne pas modifier les tâches ci-dessous -->
                <respStmt>
                    <resp>Modélisation et structuration</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Elysabeth HUE-GAY</persName>
                </respStmt>
                <principal>Malika BASTIN-HAMMOU</principal>
            </titleStmt>

            <!-- Informations de publication sur le laboratoire numérique du projet. Ne pas modifier-->
            <publicationStmt>
                <publisher/>
                <availability>
                    <p>Le site est en accès restreint</p>
                </availability>
                <pubPlace>ithac.elan-numerique.fr</pubPlace>
            </publicationStmt>

            <!-- Informations bibliographiques sur l'édition ancienne d'où le paratexte est tiré -->
            <sourceDesc>
                <bibl>
                    <title>Plautinae uiginti comoediae magna ex parte emendatae per Georgium
                        Alexandrinum [Merulam]</title>
                    <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->
                    <author ref="#pl">Plautus</author>
                    <pubPlace ref="#uenetia">Venetiae</pubPlace>
                    <publisher ref="#colonia_ioannes">Joannes de Colonia</publisher>
                    <publisher ref="#spira_uindelinus">Vindelinus de Spira</publisher>
                    <date when="1472">1472</date>
                    <!-- dans l'attribut @when, l'année en chiffre arabes -->
                    <editor ref="#merula_georgius">Georgius Alexandrinus Merula</editor>
                    <editor role="traducteur" ref="#identifiant_person"/>
                    <ref target="https://gallica.bnf.fr/ark:/12148/bpt6k596344.image"
                        >https://gallica.bnf.fr/ark:/12148/bpt6k596344.image</ref>
                    <!-- pour les liens google book, remplacer les & par &amp; pour que ça fonctionne -->
                </bibl>
            </sourceDesc>
        </fileDesc>

        <profileDesc>

            <abstract>
                <p/>
            </abstract>

            <langUsage>
                <!-- ne pas modifier -->
                <language ident="fr">Français</language>
                <language ident="la">Latin</language>
                <language ident="grc">Grec</language>
            </langUsage>

        </profileDesc>
        <revisionDesc>
            <change when="2022-02-28">Sarah GAUCHER : identifiants Header, mise en conformité
                Elan</change>
        </revisionDesc>
    </teiHeader>

    <text>
        <body>

            <!-- RAPPEL : on encode le texte latin ou grec; on n'encode dans le texte de la traduction que les notes d'éditeur (=membres du projet) -->

            <!-- Le couple d'éléments <head> contient le titre ou l'incipit du paratexte -->
            <!-- L'identifiant du TITRE est composé du titre de la fiche TT suivi d'un underscore (_) puis du chiffre 0 -->
            <head type="orig" xml:id="identifiant_du_titre">De uita Comoediisque <persName ref="#pl"
                    >Plauti</persName> excerpta quaedam ex Auctoribus grauissimis.</head>
            <!-- Le deuxième élément du couple contient la traduction française du titre ou incipit -->
            <!-- L'identifiant de la traduction est le même que celui du latin, précédé d'un hashtag (#) -->
            <head corresp="#identifiant_du_titre" type="trad">Extraits sur la vie et les comédies de
                Plaute, tirés des auteurs les plus éminents.</head>

            <!-- Chaque couple d'éléments <ab> contient le texte du paratexte (cf. tableau dans la fiche TT) -->
            <!-- Attention ! Ne pas oublier de mettre à jour les identifiants en rouge ou utiliser le fichier xml qui permet de générer automatiquement les <ab> et leurs identifiants -->
            <!-- L'identifiant de l'UNITE TEXTUELLE est composé du titre de la fiche TT suivi d'un underscore (_) puis du chiffre 1, 2, 3... -->

            <ab type="orig" xml:id="Pla1472_Merula_p2_1"><persName ref="#pl">Plautus</persName>,
                linguae Latinae pater et cuius sermone (ut <persName ref="#uarro">Varo</persName>
                Epii <persName>Stolonis</persName> sententia dixit) <seg type="paraphrase">musae
                    locutae forent si latine loqui uoluissent<bibl resp="#MF"><author ref="#quint"
                            >Quint.</author>, <title ref="#quint_io">I.O.</title>
                        <biblScope>10.1.99</biblScope></bibl></seg>, Sarsinas ex Vmbria fuisse
                legitur. </ab>
            <ab type="trad" corresp="#Pla1472_Merula_p2_1">Plaute, père de la langue latine dont les
                Muses (comme Varon l’écrivit, d’après <seg>Epius Stolon<note> Il s’agit en réalité
                        de Lucius Aelius Stilo, philologue et maître de Cicéron et Varron.
                    </note></seg>) auraient parlé la langue si elles avaient voulu parler latin, est
                originaire, dit-on, de Sarsina en Ombrie. </ab>
            <ab type="orig" xml:id="Pla1472_Merula_p2_2">Nam ad natalem terram alludens seruilem
                iocum in <title ref="#pl_most">Mustelaria</title> aduersus Simonem qui umbram usque
                a domo abesse aiebat sic intulit : </ab>
            <ab type="trad" corresp="#Pla1472_Merula_p2_2">De fait, faisant allusion à sa terre
                natale, il rapporta ainsi, dans la <title>Mostellaria</title>, le jeu de mots d’un
                esclave qui répondait à Simon ; ce dernier prétendait qu’il n’y avait pas d’ombre
                chez lui : </ab>
            <ab type="orig" xml:id="Pla1472_Merula_p2_3"><cit type="hyperref">
                    <quote>Quid Sarsinatis ecqua est si umbram non habes ?</quote>
                    <bibl resp="#MF"><author ref="#pl">Pl.</author>, <title ref="#pl_most"
                            >Most.</title>
                        <biblScope>770</biblScope>.<note>Le jeu de mots repose sur le double sens du
                            mot « umbra » : « ombre » mais aussi « Ombrienne ».</note></bibl>
                </cit></ab>
            <ab type="trad" corresp="#Pla1472_Merula_p2_3">« N’as-tu pas au moins l’Ombrie
                sarsinate, à défaut d’ombre ? »</ab>
            <ab type="orig" xml:id="Pla1472_Merula_p2_4">Floruit in scaena quinto decimo ferme anno
                postquam bellum aduersus Poenos secundum sumptum est. </ab>
            <ab type="trad" corresp="#Pla1472_Merula_p2_4">Son floruit sur la scène se situe quinze
                ans environ après le début de la seconde guerre punique. </ab>
            <ab type="orig" xml:id="Pla1472_Merula_p2_5">Nam ut <persName ref="#licinius">Portius
                    Licinnius</persName> dixit: </ab>
            <ab type="trad" corresp="#Pla1472_Merula_p2_5">De fait, comme l’écrivit Portius
                Licinnius : </ab>
            <ab type="orig" xml:id="Pla1472_Merula_p2_6"><cit type="refauteur">
                    <quote>Punico bello secundo Musa pennato gradu intulit sese bellicosam in Romuli
                        gentem feram.</quote>
                    <bibl resp="#MF"><author ref="#gell">Gell.</author>, <title ref="#gell_noct"
                            >Noct.</title>
                        <biblScope>17.21.44</biblScope>.</bibl>
                </cit></ab>
            <ab type="trad" corresp="#Pla1472_Merula_p2_6">« Pendant la seconde guerre punique, la
                Muse de son pas ailé fit son entrée dans la race belliqueuse et farouche de
                Romulus ».</ab>
            <ab type="orig" xml:id="Pla1472_Merula_p2_7">Quo item tempore <persName ref="#cato"
                    >Marcus Cato</persName> Orator clarus in ciuitate est habitus. </ab>
            <ab type="trad" corresp="#Pla1472_Merula_p2_7">A cette époque, Marcus Caton était
                considéré comme un orateur célèbre. </ab>
            <ab type="orig" xml:id="Pla1472_Merula_p2_8">Et quamquam a pluribus Graecis comicis
                fabulas sumptas in Latinum conuerterit, <persName ref="#hor">Horatii</persName>
                tamen sententia <persName ref="#pl">Plautus</persName>
                <cit type="refauteur">
                    <quote>ad exemplum Siculi properare Epicharmi</quote>
                    <bibl resp="#MF"><author ref="#hor">Hor.</author>, <title ref="#hor_ep"
                            >Ep.</title>
                        <biblScope>2.1.58</biblScope>.<note resp="#SG">Horace donne <foreign
                                xml:lang="la">ad exemplar</foreign>.</note></bibl>
                </cit> dicitur. </ab>
            <ab type="trad" corresp="#Pla1472_Merula_p2_8">Et bien qu’il ait traduit en latin des
                pièces empruntées à plusieurs comiques grecs, on dit que Plaute, selon Horace, a
                suivi aussi l’exemple d’Epicharme le Sicilien.</ab>
            <ab type="orig" xml:id="Pla1472_Merula_p2_9"><seg type="allusion">Hic ut <persName
                        ref="#uarro">Varro</persName> et plerique alii memoriae prodiderunt cum
                    pecunia omni quam in operis artificum scaenicorum pepererat in mercationibus
                    perdita inops Romam rediuisset et ob quaerendum uictum ad curcumagendas molas
                    quae trusatiles appellantur operam pistori locasset, in pistrino, Saturionem et
                    Addictum et tertiam quandam fabulam cuius nomen non legitur scripsit.<bibl
                        resp="#MF"><author ref="#gell">Gell.</author>, <title ref="#gell_noct"
                            >Noct.</title>
                        <biblScope>3.3.14</biblScope>.<note>Le passage reprend mot-à-mot Aulu-Gelle
                            en recomposant la syntaxe.</note></bibl></seg></ab>
            <ab type="trad" corresp="#Pla1472_Merula_p2_9">Là, comme Varron et beaucoup d’autres
                l’ont rapporté, tandis que, après avoir perdu dans le commerce tout l’argent qu’il
                avait gagné par la pratique des arts scéniques, il était revenu à Rome sans un sou
                et avait loué ses services à un boulanger, pour gagner sa pitance, afin de tourner
                la meule qu’on dit « à bras », il écrivit au moulin le Saturion, et l’Insolvable et
                une troisième pièce dont on ne connaît pas le nom.</ab>
            <ab type="orig" xml:id="Pla1472_Merula_p2_10">Mortuus est circiter centesimam et
                quadragesimam quintam olympiadem quantum ex uerbis <persName ref="#hier"
                    >Hieronymi</persName> in <title ref="#hier_euschron">Eusebii Pamphili
                    Chronico</title> colligere possumus. </ab>
            <ab type="trad" corresp="#Pla1472_Merula_p2_10">Il est mort vers la 145e olympiade, pour
                autant qu’on puisse en juger d’après les mots de Jérôme dans la chronique d’Eusébius
                    Pamphilus.<note>Jérôme a traduit le deuxième livre de la Chronique d’Eusèbe de
                    Césarée (voir l’édition en ligne de ce texte : <ref
                        target="http://www.tertullian.org/fathers/jerome_chronicle_06_latin_part2.htm"
                        >http://www.tertullian.org/fathers/jerome_chronicle_06_latin_part2.htm</ref>).
                </note></ab>
            <ab type="orig" xml:id="Pla1472_Merula_p2_11">Iussitque Epigramma tale suo sepulchro
                incidi quod an Plauti foret dubitasset <persName ref="#gell">Aulus Gelius</persName>
                nisi a <persName ref="#uarro">Marco Varrone</persName> positum esset in libro de
                Poetis primo : </ab>
            <ab type="trad" corresp="#Pla1472_Merula_p2_11">Et il ordonna de faire graver sur son
                tombeau cette épigramme dont Aulu-Gelle aurait douté qu’elle fût de Plaute si elle
                n’avait été recueillie par Marcus Varron dans son premier livre <title>Sur les
                    Poètes</title> :</ab>
            <ab type="orig" xml:id="Pla1472_Merula_p2_12"><cit type="hyperref">
                    <quote><l>Postquam est morte captus <persName ref="#pl">Plautus</persName></l>
                        <l>Comoedia luget, scaena est deserta</l>
                        <l>Deinde risus ludus iocusque et numeri</l>
                        <l>Innumeri simul omnes collachrymarunt.</l></quote>
                    <bibl resp="#MF"><author ref="#gell">Gell.</author>, <title ref="#gell_noct"
                            >Noct.</title>
                        <biblScope>1.24.3</biblScope>.</bibl>
                </cit>
            </ab>
            <ab type="trad" corresp="#Pla1472_Merula_p2_12"><l>« Après que Plaute a été emporté par
                    la mort</l>
                <l>La Comédie pleure, la scène est déserte,</l>
                <l>Enfin les rires, les jeux, les ébats, les vers</l>
                <l>La prose, tout, de concert, verse des larmes. »</l>
            </ab>
            <ab type="orig" xml:id="Pla1472_Merula_p2_13"><seg type="allusion">Plurimae sub
                        <persName ref="#pl">Plauti</persName> nomine Comoediae apud priscos
                    ferebantur quarum una et uiginti <persName ref="#uarro">Varronianae</persName>
                    dictae sunt quas idcirco a ceteris segregauit quoniam dubiosae non erant.<bibl
                        resp="#MF"><author ref="#gell">Gell.</author>, <title ref="#gell_noct"
                            >Noct.</title>
                        <biblScope>3.3.3</biblScope>.</bibl></seg></ab>
            <ab type="trad" corresp="#Pla1472_Merula_p2_13">De très nombreuses comédies étaient,
                chez les Anciens, attribuées à Plaute, parmi lesquelles vingt-et-une sont dites
                « varroniennes » que Varron distingua des autres car leur origine ne faisait aucun
                doute.</ab>
            <ab type="orig" xml:id="Pla1472_Merula_p2_14">Sed consensu omnium <persName ref="#pl"
                    >Plauti</persName> esse censebantur. </ab>
            <ab type="trad" corresp="#Pla1472_Merula_p2_14">Mais tout le monde était bien d’avis
                qu’elles étaient de Plaute. </ab>
            <ab type="orig" xml:id="Pla1472_Merula_p2_15">Quasdam item alias probauit adductus stilo
                atque facetia sermonis <persName ref="#pl">Plauto</persName> congruentis easque iam
                nominibus aliorum occupatas <persName ref="#pl">Plauto</persName> uindicauit. </ab>
            <ab type="trad" corresp="#Pla1472_Merula_p2_15">Varron en reconnut quelques autres,
                d’après leur style et leurs plaisanteries qui correspondaient bien à Plaute ; et,
                bien qu’elles aient été précédemment attribuées à d’autres poètes, il les lui
                adjugea. </ab>
            <ab type="orig" xml:id="Pla1472_Merula_p2_16">Aliae dicuntur ambiguae utrum <persName
                    ref="#pl">Plauti</persName> an aliorum fuerint. </ab>
            <ab type="trad" corresp="#Pla1472_Merula_p2_16">Pour d’autres, dit-on, on ignore si
                elles sont de Plaute ou d’un autre. </ab>
            <ab type="orig" xml:id="Pla1472_Merula_p2_17">Illud idem subdit <persName ref="#gell"
                    >Gelius</persName> : <seg type="paraphrase"><persName ref="#uarro">Marcum
                        Varronem</persName> scripsisse Plautium fuisse quempiam Poetam Comoediarum
                    cuius quoniam fabulae Plauti inscriptae forent acceptas esse quasi Plautinas cum
                    essent non a <persName ref="#pl">Plauto</persName> Plautinae sed a Plautio
                    Plautianae. Ascriptae autem <persName ref="#pl">Plauto</persName> Comoediae
                    circiter centum atque triginta fuerunt. Sed homo eruditissimus
                        <persName>Laelius</persName> quinque et uiginti eius solas existimauit. Nec
                    dubium est quin quaedam Comoediae quae scriptae a <persName ref="#pl"
                        >Plauto</persName> non uidentur et nomini eius adiciuntur ueterum Poetarum
                    fuerint et ab eo retractae et expolitae sint ac propterea excipiant dictum
                        <persName ref="#pl">Plautinum</persName>.<bibl resp="#MF"><author
                            ref="#gell">Gell.</author>, <title ref="#gell_noct">Noct.</title>
                        <biblScope>3.3.7-13</biblScope>.<note>Ce passage est un centon
                            d'Aulu-Gelle.</note></bibl></seg></ab>
            <ab type="trad" corresp="#Pla1472_Merula_p2_17">Aulu-Gelle ajoute cela encore : Marcus
                Varron écrivit qu’un certain Plautius était auteur de comédies. Et comme des pièces
                avaient l’indication <seg>Plauti<note>Forme ambiguë de génitif, commune à Plautus et
                        à Plautius.</note></seg> elles avaient été reçues comme des pièces
                plautiniennes; elles n’étaient pourtant pas de Plaute (c’est-à-dire plautiniennes)
                mais bien de Plautius (c’est-à-dire plautiennes). Cent trente comédies environ ont
                donc été attribuées à Plaute. Mais Laelius, un homme très érudit, estima que seules
                vingt-cinq d’entre elles étaient de lui. Il n’est pas douteux que certaines comédies
                qui ne semblent pas écrites par Plaute et lui sont pourtant adjugées furent
                composées par d’anciens poètes et furent par lui remaniées et perfectionnées ; pour
                cette raison on les dit « plautiniennes ».</ab>
            <ab type="orig" xml:id="Pla1472_Merula_p2_18">Sed de iudicio <persName ref="#uarro"
                    >Varronis</persName>, <persName>Laelii</persName> et
                    <persName>Fauorini</persName> qui <title>Neruulariam</title>
                <persName ref="#pl">Plauti</persName> esse dixit et eius qui <title>Fretum</title>
                nomen esse id comoediae <persName ref="#pl">Plauti</persName> putauit, plura
                leguntur in libro qui inscriptus est <title ref="#gell_noct">Noctium
                    Atticarum</title>. </ab>
            <ab type="trad" corresp="#Pla1472_Merula_p2_18">Mais, sur ce que pensent Varron,
                Laelius, Favorinus – qui écrivit que <title>Nervularia</title> était une pièce de
                Plaute – et celui qui pensa que <title>Fretum</title> était le nom d’une comédie de
                Plaute, on en lit davantage dans le livre appelé <title>Les Nuits Attiques</title>. </ab>
            <ab type="orig" xml:id="Pla1472_Merula_p2_19"><seg type="allusion">Quem uero locum
                        <persName ref="#pl">Plautus</persName> inter Comicos Latinos habeat, docet
                        <persName ref="#uolc_sed">Vulcatius Sedigitus</persName> in Poetica,
                    illustris in libro quem scripsit de Poetis, ubi idem quid de iis sentiat qui
                    comoedias fecerunt et quem praestare ex omnibus ceteris putet.<bibl resp="#MF"
                            ><author ref="#gell">Gell.</author>, <title ref="#gell_noct"
                            >Noct.</title>
                        <biblScope>15.24</biblScope>.</bibl></seg></ab>
            <ab type="trad" corresp="#Pla1472_Merula_p2_19">Quelle place tient Plaute parmi les
                comiques latins, Vulcatius Sedigitus l’enseigne en sa Poétique, dans le livre qu’il
                écrivit sur les poètes illustres où le même indique ce qu’il pense de ceux qui ont
                composé des comédies et quel auteur, d’après lui, l’emporte sur tous les
                autres.</ab>
            <ab type="orig" xml:id="Pla1472_Merula_p2_20"><cit type="nonref">
                    <quote>Ac deinceps quo quemque in loco et honore ponat his uerbis suis
                        demonstrat : <l>Multos incerto certare hanc rem uidimus</l>
                        <l>Palmam poetae comico cui deferant</l>
                        <l>Cum me iudice errorem dissoluam tibi</l>
                        <l>Contra ut si quis sentiat nihil sentiat</l>
                        <l><persName ref="#caecil">Cecilio</persName> palmam statuo de comico</l>
                        <l><persName ref="#pl">Plautus</persName> secundus facile exuperat
                            ceteros,</l>
                        <l>Dein <persName ref="#naeu">Neuius</persName>, qui feruet, pretio in
                            tertio est.</l>
                        <l>Si quid quarto detur, dabitur <persName ref="#licinius"
                                >Licinio</persName>.</l>  <l>Post insequi <persName ref="#licinius"
                                >Licinium</persName> facio <persName ref="#attilius"
                                >Attilium</persName>.</l>
                        <l>In sexto consequitur hos <persName ref="#ter">Terentius</persName>,</l> 
                                <l><persName ref="#turpil">Turpilius</persName> septimum, <persName
                                ref="#trabea">Trabea</persName> octauum optinet,</l>  <l>Nono loco
                            esse facile facio <persName ref="#luscius">Luscium</persName>. </l>
                        <l>Decimum addo antiquitatis causa <persName ref="#enn"
                            >Ennium</persName>.</l></quote>
                    <bibl resp="#MF"><author ref="#gell">Gell.</author>, <title ref="#gell_noct"
                            >Noct.</title>
                        <biblScope>15.24</biblScope>.</bibl>
                </cit></ab>
            <ab type="trad" corresp="#Pla1472_Merula_p2_20">A quelle place, ensuite, et à quel rang
                d’honneur il faut porter chacun, il le montre par ces mots : <l>« Nous en avons vu
                    beaucoup engager sur ce point un combat incertain :</l>
                <l>A quel poète comique faut-il remettre la palme ?</l>
                <l>Par mon jugement je mettrai fin à ton hésitation. </l>
                <l>De sorte que, si quelqu’un est d’une opinion contraire, ce sera en vain.</l>
                <l>Je donne à <seg>Cécilius<note>Pour l’identification de chacun de ces poètes, nous
                            renvoyons à l’édition des <title>Nuits Attiques</title> procurée par
                            R. Marache, Paris, Les Belles Lettres, t. III, 1989, p. 227-228.
                        </note></seg> la palme du comique.</l>
                <l>Plaute, après lui, l’emporte aisément sur tous les autres.</l>
                <l>Puis Naevius, qui bouillonne, obtient le troisième prix.</l>
                <l>S’il faut en donner un quatrième, on le donnera à Licinius.</l>
                <l>Après Licinius, je fais suivre Attilius.</l>
                <l>Au sixième rang Térence les suit,</l>
                <l>Turpilius tient le septième, Trabea le huitième,</l>
                <l>Au neuvième rang je place facilement Luscius. </l>
                <l>Au dixième j’ajoute Ennius pour son antiquité. »</l></ab>
        </body>
    </text>
</TEI>
