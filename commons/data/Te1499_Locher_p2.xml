<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">

    <teiHeader>

        <fileDesc>

            <titleStmt>

                <title xml:lang="la">Ad lectorem Epigramma eiusdem Philomusi</title>
                <!-- Indiquer la langue du paratexte dans l'attribut xml:lang : "la" pour le latin et "grc" pour le grec -->
                <!-- Attention ! ne pas faire de retour charriot dans le titre -->
                <author ref="#locher_iacobus">Iacobus Locher Philomusus</author>
                <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence en minuscules, précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->

                <!-- Déclarations de responsabilité : indiquent qui a effectué quelle tâche -->
                <!-- Ajouter autant de tâches (<respStmt>) que nécessaire -->
                <respStmt>
                    <resp>Transcription</resp>
                    <name>Christiane LOUETTE</name>
                    <name>Christian NICOLAS</name>
                </respStmt>
                <respStmt>
                    <resp>Traduction</resp>
                    <name>Christiane LOUETTE</name>
                    <name>Christian NICOLAS</name>
                </respStmt>
                <respStmt>
                    <resp>Relecture</resp>
                    <name>Laure Hermand</name>
                </respStmt>
                <respStmt>
                    <resp>Encodage</resp>
                    <name>Sarah GAUCHER</name>
                </respStmt>


                <!-- Attention ! Ne pas modifier les tâches ci-dessous -->
                <principal>Malika BASTIN-HAMMOU</principal>
                <respStmt>
                    <resp>Modélisation et structuration</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Elysabeth HUE-GAY</persName>
                </respStmt>
                <respStmt>
                    <resp>Première version du modèle d'encodage</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Anne Garcia-Fernandez</persName>
                </respStmt>
            </titleStmt>

            <!-- Informations de publication sur le laboratoire numérique du projet. Ne pas modifier-->
            <publicationStmt>
                <publisher/>
                <availability>
                    <p>Le site est en accès restreint</p>
                </availability>
                <pubPlace>ithac.elan-numerique.fr</pubPlace>
            </publicationStmt>

            <!-- Informations bibliographiques sur l'édition ancienne d'où le paratexte est tiré -->
            <sourceDesc>
                <bibl>
                    <title>Terentius cum directorio uocabulorum, sententiarum, glossa interlineali
                        artis comicae, commentariis Donato, Guidone, Ascensio</title>
                    <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->
                    <author ref="#ter">Terentius</author>
                    <pubPlace ref="#argentoratum">Argentina</pubPlace>
                    <publisher ref="#gruninger_ioannes">Ioannes Grunigerus</publisher>
                    <date when="1499">1499</date>
                    <!-- dans l'attribut @when, l'année en chiffre arabes -->
                </bibl>

                <listBibl>
                    <head>Bibliographie</head>
                    <bibl>Dietl (C.), Die Dramen Jacob Lochers und die frühe Humanistenbühne im
                        süddeutschen Raum. De Gruyter, Berlin und New York 2005.</bibl>
                    <bibl>Hehle (J.), « Locher, Jakob », dans Allgemeine Deutsche Biographie (ADB),
                        vol. 19, Leipzig, Duncker et Humblot, 1884, p. 59-63.</bibl>
                    <bibl>Hermand-Schebat (L.), « Texte et image dans les éditions latines
                        commentées de Térence (Lyon, Trechsel, 1493 et Strasbourg, Grüninger, 1496)
                        », Camenae 10 (2011).</bibl>
                    <bibl>Kühlmann (W.) et Niehl (R.) : Locher (Philomusus), Jakob, in : F.-J.
                        Worstbrock (éd.): Verfasserlexikon Deutscher Humanismus 1480-1520, vol. 2.
                        Walter de Gruyter, Berlin + New York 2009, pp. 62–86.</bibl>
                    <bibl>Lawton (H.W.), Térence en France au XVIe siècle, Genève, Slatkine repr.,
                        1970-1972 (1ère éd. : Paris, Jouve, 1926), 2 vol., vol. 1 : « Éditions et
                        traductions ».</bibl>
                    <bibl>Mertens (D.), Jacobus Locher Philomusus als humanistischer Lehrer der
                        Universität Tübingen, in : Bausteine zur Tübinger Universitätsgeschichte 3
                        (1987), pp. [11]-38.</bibl>
                    <bibl>Ritter (F.), Catalogue des incunables alsaciens de la Bibliothèque
                        Nationale et Universitaire de Strasbourg, Strasbourg, 1938, 448.</bibl>
                    <bibl>Ukena (P.), « Locher, Jakob », dans Neue Deutsche Biographie (NDB), vol.
                        14, Berlin 1985, Duncker et Humblot, p. 743–744.</bibl>
                    <!-- créer un <bibl> par référence -->
                </listBibl>

            </sourceDesc>
        </fileDesc>

        <profileDesc>

            <abstract>
                <p>Liste des paratextes (titre, auteur, destinataire, folios) N.B. les paratextes
                    p3-p14 figurent à l’identique dans l’édition de Térence chez Grüninger de 1496
                    et sont traités à cet endroit.</p>
                <!-- s'il s'agit d'une liste, utiliser les éléments ci-dessous -->
                <list>
                    <item>- P1 : une épître dédicatoire de Jakob Locher à Johann Grüninger, annoncée
                        comme un éloge de Térence, f. 1r </item>
                    <item>- P2 : une épître en vers de Jakob Locher au lecteur, f. 1r</item>
                    <item>- P3 : Illustration de L’Andrienne, f. 7r.</item>
                    <item>- P4 : Légende et argument de l’illustration de L’Andrienne, f. 7v</item>
                    <item>- P5 : Illustration de L’Eunuque, f. 34r</item>
                    <item>- P6 : Légende et argument de L’Eunuque, f. 35vr</item>
                    <item>- P7 : Illustration de L’Heautontimorumenus, f. 61r</item>
                    <item>- P8 : Légende et argument de L’Heautontimorumenus, f 62r</item>
                    <item>- P9 : Légende et argument des Adelphes, f. 91r</item>
                    <item>- P10 : Illustration des Adelphes, f. 92r</item>
                    <item>- P11 : Illustration de Phormion, f. 122v</item>
                    <item>- P12 : Légende et argument de Phormion, f 123r</item>
                    <item>- P13 : Légende et argument de L’Hécyre, f. 151v</item>
                    <item>- P14 : Illustration de L’Hécyre, f. 152r</item>
                    <item>- P15 : Vie de Térence, extraite de Pétrarque, f.177v-178r</item>
                    <item>- P16 : Colophon, 178r (donné dans le descriptif ci-dessus)</item>
                    <item>- P17 : Epigramme de Heinrich Bebel, éloge de la lecture de Térence, f.
                        178r</item>

                </list>
            </abstract>

            <langUsage>
                <!-- ne pas modifier -->
                <language ident="fr">Français</language>
                <language ident="la">Latin</language>
                <language ident="grc">Grec</language>
            </langUsage>

        </profileDesc>
        <revisionDesc>
            <!-- à utiliser si nouvel intervenant sur le fichier -->
            <change when="2022-02-28">Sarah GAUCHER : identifiants Header, mise en conformité
                Elan</change>
            <change when="2022-01-17">Sarah Gaucher : création de la fiche TEI ; encodage de la
                transcription et de la traduction</change>

        </revisionDesc>
    </teiHeader>

    <text>
        <body>

            <!-- RAPPEL : on encode le texte latin ou grec; on n'encode dans le texte de la traduction que les notes d'éditeur (=membres du projet) -->

            <!-- Le couple d'éléments <head> contient le titre ou l'incipit du paratexte -->
            <!-- L'identifiant du TITRE est composé du titre de la fiche TT suivi d'un underscore (_) puis du chiffre 0 -->
            <head type="orig" xml:id="Te1499_Locher_p2_0">Ad lectorem Epigramma eiusdem
                Philomusi</head>
            <!-- Le deuxième élément du couple contient la traduction française du titre ou incipit -->
            <!-- L'identifiant de la traduction est le même que celui du latin, précédé d'un hashtag (#) -->
            <head corresp="#Te1499_Locher_p2_0" type="trad">Au lecteur, épigramme <seg>du même
                        <note>Jakob Locher. Anobli par Maximilien 1er en 1497, Jakob Locher (resté
                        catholique toute sa vie) est né en Bavière à Echingen en 1471 et mort à
                        Ingolstadt en 1528. Il fut étudiant à Bâle, Fribourg et Ingolstadt où il
                        devint professeur en 1498, avant de prendre un poste à Fribourg en 1503 puis
                        à nouveau à Ingolstadt de 1506 à sa mort. Poète lauréat couronné par
                        Maximilien 1er en 1497, il publia des poèmes, dont un panégyrique de
                        Maximilien, et composa des pièces en latin. Il est aussi le premier
                        traducteur en allemand d’Horace. Le surnom de Philomusus dont il semble
                        s’être auto-affublé est récurrent notamment chez Martial. C’est ainsi aussi
                        que le nomme dans ses vers Conrad Keltis, qui fut son
                    professeur.</note></seg> ‘Ami des Muses’</head>
            <ab type="orig" xml:id="Te1499_Locher_p2_1">
                <l>Pegmata celsa uolet qui contemplare <persName ref="#ter"
                    >Terenti</persName></l><l>Et lusus lepidos blandidicosque sales, </l><l>Qui
                    uolet et nitidi spectacula pulchra theatri </l><l>Visere comoedi gestaque grata
                    uafri, </l><l>Qui uolet et mores hominum spectare profanos </l><l>Consutosque
                    dolos sacrilegumque nefas, </l><l>Qui uolet et contra uirtutes discere claras
                    </l><l>Et uitae specimen eloquiumque uetus, </l><l>Spectatum ueniat taetrica non
                    fronte disertos </l><l>Sermones quos haec pagina docta canit.</l>
            </ab>
            <ab type="trad" corresp="#Te1499_Locher_p2_1" ana="#TH_dram #TH_repr #TH_fin">Si l’on
                veut contempler les hauts tréteaux de Térence, ses jeux gracieux et ses
                plaisanteries badines, si l’on veut aussi assister aux beaux spectacles du brillant
                théâtre et aux gestes agréables du comédien adroit, si l’on veut aussi examiner les
                mœurs criminelles des hommes, leur tissu de ruses et leurs crimes sacrilèges, si
                l’on veut au contraire apprendre les éclatantes vertus, un exemple de vie et la
                langue antique, que l’on vienne voir, sans prendre un air sombre, les conversations
                éloquentes que chante cette page savante.</ab>
            <ab type="orig" xml:id="Te1499_Locher_p2_2"><l>Induat et pedibus soccos et comica serta
                    </l><l>Imponat capiti, non sine laude, suo.</l>
            </ab>
            <ab type="trad" corresp="#Te1499_Locher_p2_2" ana="#TH_repr">Qu’il chausse aussi les
                socques et mette sur sa tête les guirlandes de la comédie, pour sa gloire.</ab>
            <ab type="orig" xml:id="Te1499_Locher_p2_3"><l>Argutas, lepidas, facundas, atque dicaces
                    </l><l>Audiet hic uoces mellifluosque iocos.</l>
            </ab>
            <ab type="trad" corresp="#Te1499_Locher_p2_3" ana="#TH_repr">Il y entendra des voix
                expressives, agréables, éloquentes et railleuses et des jeux succulents.</ab>
            <ab type="orig" xml:id="Te1499_Locher_p2_4"><l>Et simul in picto comedos scammate uafros
                    </l><l>Spectabit : lector si sapit ista legat !</l></ab>
            <ab type="trad" corresp="#Te1499_Locher_p2_4" ana="#TH_repr #TH_fin">Et en même temps il
                verra d’habiles comédiens sur la scène peinte ; que le lecteur, s’il est avisé, lise
                cela !</ab>

        </body>
    </text>
</TEI>
