<?xml version="1.0" encoding="UTF-8"?>


<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">

    <teiHeader>

        <fileDesc>

            <titleStmt>

                <title xml:lang="la">Jodoci Badii Ascensii familiaria in Terentium prenotamenta. De
                    forma operibus et laude Terentii. Capi(tulum) XXVI. </title>
                <!-- Indiquer la langue du paratexte dans l'attribut xml:lang : "la" pour le latin et "grc" pour le grec -->
                <!-- Attention ! ne pas faire de retour charriot dans le titre -->
                <author ref="#ascensius_iodocus">Jodocus Badius Ascensius</author>

                <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence en minuscules, précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->

                <!-- Déclarations de responsabilité : indiquent qui a effectué quelle tâche -->
                <!-- Ajouter autant de tâches (<respStmt>) que nécessaire -->
                <respStmt>
                    <resp>Transcription</resp>
                    <name>Laurine Oulama</name>
                </respStmt>
                <respStmt>
                    <resp>Traduction</resp>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <respStmt>
                    <resp>Révision</resp>
                    <name>Laure HERMAND</name>
                </respStmt>
                <respStmt>
                    <resp>Encodage</resp>
                    <name>Diandra CRISTACHE</name>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <respStmt>
                    <resp>Commentaire</resp>
                    <name>Laure HERMAND</name>
                </respStmt>
                <!-- Attention ! Ne pas modifier les tâches ci-dessous -->
                <respStmt>
                    <resp>Modélisation et structuration</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Elysabeth HUE-GAY</persName>
                </respStmt>
                <respStmt>
                    <resp>Première version du modèle d'encodage</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Anne GARCIA-FERNANDEZ</persName>
                </respStmt>
                <principal>Malika BASTIN-HAMMOU</principal>
            </titleStmt>

            <!-- Informations de publication sur le laboratoire numérique du projet. Ne pas modifier-->
            <publicationStmt>
                <publisher/>
                <availability>
                    <p>Le site est en accès restreint</p>
                </availability>
                <pubPlace>ithac.elan-numerique.fr</pubPlace>
            </publicationStmt>

            <!-- Informations bibliographiques sur l'édition ancienne d'où le paratexte est tiré -->
            <sourceDesc>
                <bibl>
                    <title>P. Terentii aphri comicorum elegantissimi Comedie a Guidone Juvenale viro
                        perquam litterato familiariter explanate : et ab Jodoco Badio Ascensio vna
                        cum explanationibus rursum annotate atque recognite : cumque eiusdem
                        Ascensii praenotamentis atque annotamentis suis locis adhibitis quam
                        accuratissime impresse venundantur</title>
                    <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->
                    <author ref="#ter">Terentius</author>
                    <pubPlace ref="#londinium">Londonii</pubPlace>
                    <pubPlace ref="#lutetia">Lutetiae</pubPlace>
                    <publisher ref="#worda_winandus">Winandus de Worda</publisher>
                    <publisher ref="#morinus_michael">Michael Morinus</publisher>
                    <publisher ref="#brachius_ioannes"> Johannes Brachius</publisher>
                    <publisher ref="#ascensius_iodocus">Jodocus Badius Ascensius</publisher>
                    <date when="1504">1504</date>
                    <!-- dans l'attribut @when, l'année en chiffre arabes -->
                    <editor ref="#ascensius_iodocus">Jodocus Badius Ascensius</editor>
                </bibl>

                <listBibl>
                    <bibl>Philippe Renouard, Bibliographie des impressions et des œuvres de Josse
                        Badius Ascensius, Paris, E. Paul et fils et Guillemin, 1908, 3 vol.</bibl>
                    <bibl>M. Lebel, Les préfaces de Josse Bade (1462-1535) humaniste,
                        éditeur-imprimeur et préfacier, Louvain, Peeters, 1988</bibl>
                    <bibl>Paul White, Jodocus Badius Ascensius. Commentary, Commerce and Print in
                        the Renaissance, Oxford University Press, 2013</bibl>
                    <bibl>L. Katz, La presse et les lettres. Les épîtres paratextuelles et le projet
                        éditorial de l’imprimeur Josse Bade (c. 1462-1535), thèse de doctorat
                        soutenue à l’EPHE sous la direction de Perrine Galand, 2013</bibl>
                </listBibl>

            </sourceDesc>
        </fileDesc>

        <profileDesc>

            <abstract>
                <p>CJosse Bade ouvre son commentaire aux comédies de Térence par une longue
                    introduction qu’il nomme lui-même Praenotamenta. Il s’agit de « notes
                    préliminaires » plutôt que d’une « préface » à proprement parler. </p>
                <p>Cette section est composée de 26 chapitres qui forment un traité de poétique en
                    miniature auxquels il adjoint une série de remarques préliminaires sur le
                    prologue et la première scène de l’Andrienne. L’humaniste y développe une ample
                    réflexion sur la comédie. Partant d’une définition de la poésie, Bade aborde
                    ensuite l’origine de la comédie, ses caractéristiques et mène une longue
                    réflexion sur la scénographie antique. Les deux derniers chapitres traitent de
                    la vie et des œuvres de Térence.</p>
                <p>Nous avons choisi de séparer ce très long paratexte, troisième de l’édition
                    badienne, en suivant la division par chapitres de l’auteur.</p>

            </abstract>

            <langUsage>
                <!-- ne pas modifier -->
                <language ident="fr">Français</language>
                <language ident="la">Latin</language>
                <language ident="grc">Grec</language>
            </langUsage>

        </profileDesc>
        <revisionDesc>
            <change when="2022-04-08">Sarah GAUCHER : correction de la transcription, encodage de la
                tranduction, conformité, identifiants Header</change>
        </revisionDesc>
    </teiHeader>

    <text>
        <body>

            <!-- RAPPEL : on encode le texte latin ou grec; on n'encode dans le texte de la traduction que les notes d'éditeur (=membres du projet) -->

            <!-- Le couple d'éléments <head> contient le titre ou l'incipit du paratexte -->
            <!-- L'identifiant du TITRE est composé du titre de la fiche TT suivi d'un underscore (_) puis du chiffre 0 -->
            <head type="orig" xml:id="Te1504_Ascensius_p3z_0">De forma, operibus, et laude Terentii.
                Capitulum XXVI.</head>
            <!-- Le deuxième élément du couple contient la traduction française du titre ou incipit -->
            <!-- L'identifiant de la traduction est le même que celui du latin, précédé d'un hashtag (#) -->
            <head corresp="#Te1504_Ascensius_p3z_0" type="trad">Chapitre XXVI. Sur le physique, les
                œuvres et la renommée de Térence.</head>

            <ab type="orig" xml:id="Te1504_Ascensius_p3z_1">Quantum autem ad corporis formam Donatus
                dicit Terentium fuisse mediocri statura, gracili corpore, colore fusco, quo colore
                Carthaginenses fere sunt.</ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3z_1">Donat dit du physique de Térence qu’il
                était de taille moyenne, mince, qu’il avait le teint brun, teint qui est celui de
                presque tous les Carthaginois.<note>Suétone, <title>Vita Terentii</title>,
                6.</note></ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3z_2">Scripsit autem primum sex comoedias quas
                nunc in manibus habemus, et, ut aliqui attestantur, centum et octo quas mare obruit. </ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3z_2">Il a écrit d’abord les six comédie qui
                nous avons aujourd’hui en main, puis, comme certains l’attestent, les cent-huit que
                la mer engloutit.</ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3z_3">De laude quam in scribendis comoediis
                meruit uariae sunt opiniones.</ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3z_3">Concernant la renommée qu’il s’est
                acquise en écrivant ses comédies, les opinions divergent.</ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3z_4">Seruius enim in proprietate sermonis
                ipsi primum locum dat, in aliis fere nouissimum.<note>Référence nécéssaire voir
                    Te1504_Ascensius_p3x</note></ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3z_4">Servius, en effet, lui donne la
                première place pour la propriété des termes ; pour d’autres aspects, il lui donne
                presque la dernière.</ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3z_5">Alii etiam in aliis rebus ipsum multa
                laude dignum censent.</ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3z_5">Les divers commentateurs le jugent
                digne des plus grandes louanges dans des domaines différents.</ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3z_6">Nam Afranius ipsum omnibus comicis
                prefert scribens in compitalibus <seg>Terentio non similem dices
                        quempiam<note>Afranius, <title>frag.</title> 29 donne Terenti numne similem
                        dicent quempiam ? </note></seg>.</ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3z_6">En effet, Afranius le préfère à tous
                les autres poètes comiques en écrivant : « dans les Compitalia, personne, diras-tu,
                n’est comparable à Térence ».</ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3z_7">Volcatius autem ipsum non solum, Naeuio,
                Plauto et Cecilio, sed etiam Liuio et Attilio postponit.</ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3z_7">Quant à Volcacius, il le place non
                seulement après Naevius, Plaute et Caecilius, mais aussi après Livius et
                    Attilius.<note>La première place du classement de Volcacius revient à Caecilius.
                    Mais les critères sur lesquels se fonde la liste établie ne sont pas explicités.
                    Sur le classement de Volcacius Sedigitus, voir LEHMANN A. 2011, « Volcacius
                    Sedigitus, auteur du premier 'canon' des poètes comiques latins »,
                        <title>Latomus</title> 70, p. 330-355.</note></ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3z_8">Sedigitus ut diximus in VI capitulo
                septimum locum illi dat.</ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3z_8">Sedigitus, comme nous l’avons dit dans
                le chapitre V, lui octroie la septième place.<note>En réalité la
                sixième.</note></ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3z_9">Caesar in puritate sermonis ipsum etiam
                Menandro praeferendum censet.<note>César, <title>Carmina fragmenta</title>,
                    1.</note></ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3z_9">Pour la pureté des termes, César juge
                qu’on doit le préférer même à Ménandre.</ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3z_10">Donatus dicit quod personarum leges
                circa habitum aetatem officium, partes agendi nemo diligentius Terentio custodiuit,
                qui etiam solus ausus est in fictis argumentis cum fidem ueritatis assequerent,
                etiam contra prescripta comica, meretrices interdum non malas introducere quibus
                tamen cur bonae sint et uoluptas per ipsum et causa non desit.<note>Donat,
                        <title>Excerpta ex comoedia</title>, 3.4.</note></ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3z_10">Donat dit que personne n’a observé
                les lois concernant les tenues, l’âge, la fonction des personnages, et leurs rôles
                dans la pièce mieux que Térence, lui qui a seul osé, dans des fictions, conserver un
                haut degré de réalisme, fût-ce contre les règles du genre comique, mettre sur la
                scène des courtisanes qui ne soient pas méchantes, sur la gentillesse desquelles,
                d'ailleurs, il ne manque pas de donner une justification, ce qui procure par là même
                du plaisir.</ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3z_11"><seg>Haec cum artificiosissima
                    Terentius fecerit<note>Donat, <title>Excerpta ex comoedia</title>,
                    3.4.</note></seg>, etiam illud praecipue seruauit quod in scribenda comoedia
                modum et mensuram retinuit.</ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3z_11">Alors que Térence a surmonté ces
                difficultés avec une très grande maîtrise, il a également surtout conservé ce qui,
                dans l’écriture de la comédie, en garant de la mesure et de la modération.</ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3z_12">Nam affectus sic temperauit ut nusquam
                in tragoediam transiliret.<note>Adapté de Donat, <title>Excerpta ex
                    comoedia</title>, 3.5.</note></ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3z_12">En effet, il a modéré les passions,
                si bien qu’il ne verse jamais du côté de la tragédie.</ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3z_13">Eo enim eius fabulae sunt temperamento
                ut neque extumescant ad tragicam celsitudinem neque abiiciantur ad
                    histrionicam.<note>Donat, <title>Excerpta ex comoedia</title>, 3.5.</note></ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3z_13">Ses comédies, en effet, sont d’un
                caractère qui ne s’enfle pas pour atteindre la hauteur de la tragédie ni ne
                s’abaisse à la bouffonnerie.</ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3z_14">Adde, inquit Donatus, ad laudem
                Terentii quod nihil abtrusum ab eo ponatur aut quod ab historicis requirendum sit
                quod saepius Plautus facit et propterea est obscurior in pluribus locis.<note>Donat,
                        <title>Excerpta ex comoedia</title>, 3.6.</note></ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3z_14">Ajoute, nous dit Donat, à la louange
                de Térence qu’il n'y a chez lui rien d'abscons ou qui mérite une enquête historique,
                au contraire de ce qui se passe fréquemment chez Plaute, qui, pour cette raison, est
                obscur en maint endroits.</ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3z_15">Adde quod argumenti ac stili ita
                attente memor est ut nusquam claudicarit aut errauerit quae obesse potuerunt uel
                ideo quodque media primis et postremis ita nexuit, ut nihil additum aut appositum
                alteri sed aptum et solidum et ex se totum ex uno corpore uideatur esse
                    compositum.<note>Légèrement adapté de Donat, <title>Excerpta ex
                    comoedia</title>, 3.7.</note></ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3z_15">En outre, il est si scrupuleusement
                attentif à l'intrigue et au style que jamais il ne fait boiter ou claudiquer ce qui
                pourrait aller à leur encontre, et il noue si bien le milieu au début et à la fin
                que rien n'a l'air d'être un ajout superflu mais que tout est solidaire et paraît
                avoir été composé d'un seul tenant.</ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3z_16">Illud quoque mirabile est in Terentio
                quod non ita miscet personas ut obscura sit earum distinctio ; item quod nihil ad
                populum facit actorem loqui uelut ex tragoedia quod uitium Plauti est
                    frequentissimum.<note>Légèrement adapté de Donat, <title>Excerpta ex
                        comoedia</title>, 3.8.</note></ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3z_16">Il est remarquable également qu'il ne
                mélange pas les personnages sans qu'on puisse les distinguer ; ensuite, il ne fasse
                jamais parler un acteur au public, comme s'il sortait de la comédie, défaut très
                fréquent chez Plaute.</ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3z_17">Adde est illud inter cetera eius
                artificia laude digna quod locupletiora argumenta ex duplicibus negociis delegerit
                ad scribendum.<note>Adapté de Donat, <title>Excerpta ex comoedia</title>,
                    3.9.</note></ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3z_17">Ajoute, entre autres choses dignes de
                louanges le fait qu’il ait choisit d'écrire des intrigues enrichies d'une double
                histoire d'amour.</ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3z_18">Nam excepta <title>Hecyra</title> in
                quam scribit solius Pamphili amorem ceterae quinque comoediae binos adolescentulos
                habent.</ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3z_18">En effet, à l’exception de
                    <title>L’Hécyre</title>, où il écrit l’amour d’un seul homme, Pamphile, les cinq
                autres comédies comprennent chacune deux jeunes hommes.</ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3z_19">Harum autem sex comoediarum nomina sunt
                : <title>Andria</title>, <title>Eunuchus</title>, <title>Heautontimorumenos</title>,
                    <title>Phormio</title>, <title>Adelphi</title> et <title>Hecyra</title> quas
                nunc exponendas aggredior.</ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3z_19">Quant à ces comédies, en voici les
                noms : <title>L'Andrienne</title>, <title>L’Eunuque</title>,
                    <title>L’Heautontimoroumenos</title>, <title>Le Phormion</title>, <title>Les
                    Adelphes</title> et <title>L’Hécyre</title>, comédies que j’entreprends à
                présent de présenter. </ab>

        </body>
    </text>
</TEI>
