<?xml version="1.0" encoding="UTF-8"?>


<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">

    <teiHeader>

        <fileDesc>

            <titleStmt>

                <title xml:lang="la">De prosceniarum ornatu et instructione. Capitulum XI.</title>
                <!-- Indiquer la langue du paratexte dans l'attribut xml:lang : "la" pour le latin et "grc" pour le grec -->
                <!-- Attention ! ne pas faire de retour charriot dans le titre -->
                <author ref="#ascensius_iodocus">Jodocus Badius Ascensius</author>

                <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence en minuscules, précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->

                <!-- Déclarations de responsabilité : indiquent qui a effectué quelle tâche -->
                <!-- Ajouter autant de tâches (<respStmt>) que nécessaire -->
                <respStmt>
                    <resp>Transcription</resp>
                    <name>Bastien Vallet</name>
                    <name>Églantine DERU</name>
                </respStmt>
                <respStmt>
                    <resp>Traduction</resp>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <respStmt>
                    <resp>Révision</resp>
                    <name>Laure HERMAND</name>
                </respStmt>
                <respStmt>
                    <resp>Encodage</resp>
                    <name>Diandra CRISTACHE</name>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <respStmt>
                    <resp>Commentaire</resp>
                    <name>Laure HERMAND</name>
                </respStmt>
                <!-- Attention ! Ne pas modifier les tâches ci-dessous -->
                <respStmt>
                    <resp>Modélisation et structuration</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Elysabeth HUE-GAY</persName>
                </respStmt>
                <respStmt>
                    <resp>Première version du modèle d'encodage</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Anne GARCIA-FERNANDEZ</persName>
                </respStmt>
                <principal>Malika BASTIN-HAMMOU</principal>
            </titleStmt>

            <!-- Informations de publication sur le laboratoire numérique du projet. Ne pas modifier-->
            <publicationStmt>
                <publisher/>
                <availability>
                    <p>Le site est en accès restreint</p>
                </availability>
                <pubPlace>ithac.elan-numerique.fr</pubPlace>
            </publicationStmt>

            <!-- Informations bibliographiques sur l'édition ancienne d'où le paratexte est tiré -->
            <sourceDesc>
                <bibl>
                    <title>P. Terentii aphri comicorum elegantissimi Comedie a Guidone Juvenale viro
                        perquam litterato familiariter explanate : et ab Jodoco Badio Ascensio vna
                        cum explanationibus rursum annotate atque recognite : cumque eiusdem
                        Ascensii praenotamentis atque annotamentis suis locis adhibitis quam
                        accuratissime impresse venundantur</title>
                    <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->
                    <author ref="#ter">Terentius</author>
                    <pubPlace ref="#londinium">Londonii</pubPlace>
                    <pubPlace ref="#lutetia">Lutetiae</pubPlace>
                    <publisher ref="#worda_winandus">Winandus de Worda</publisher>
                    <publisher ref="#morinus_michael">Michael Morinus</publisher>
                    <publisher ref="#brachius_ioannes"> Johannes Brachius</publisher>
                    <publisher ref="#ascensius_iodocus">Jodocus Badius Ascensius</publisher>
                    <date when="1504">1504</date>
                    <!-- dans l'attribut @when, l'année en chiffre arabes -->
                    <editor ref="#ascensius_iodocus">Jodocus Badius Ascensius</editor>
                </bibl>

                <listBibl>
                    <bibl>Philippe Renouard, Bibliographie des impressions et des œuvres de Josse
                        Badius Ascensius, Paris, E. Paul et fils et Guillemin, 1908, 3 vol.</bibl>
                    <bibl>M. Lebel, Les préfaces de Josse Bade (1462-1535) humaniste,
                        éditeur-imprimeur et préfacier, Louvain, Peeters, 1988</bibl>
                    <bibl>Paul White, Jodocus Badius Ascensius. Commentary, Commerce and Print in
                        the Renaissance, Oxford University Press, 2013</bibl>
                    <bibl>L. Katz, La presse et les lettres. Les épîtres paratextuelles et le projet
                        éditorial de l’imprimeur Josse Bade (c. 1462-1535), thèse de doctorat
                        soutenue à l’EPHE sous la direction de Perrine Galand, 2013</bibl>
                </listBibl>

            </sourceDesc>
        </fileDesc>

        <profileDesc>

            <abstract>
                <p>Josse Bade ouvre son commentaire aux comédies de Térence par une longue
                    introduction qu’il nomme lui-même Praenotamenta. Il s’agit de « notes
                    préliminaires » plutôt que d’une « préface » à proprement parler. </p>
                <p>Cette section est composée de 26 chapitres qui forment un traité de poétique en
                    miniature auxquels il adjoint une série de remarques préliminaires sur le
                    prologue et la première scène de l’Andrienne. L’humaniste y développe une ample
                    réflexion sur la comédie. Partant d’une définition de la poésie, Bade aborde
                    ensuite l’origine de la comédie, ses caractéristiques et mène une longue
                    réflexion sur la scénographie antique. Les deux derniers chapitres traitent de
                    la vie et des œuvres de Térence.</p>

            </abstract>

            <langUsage>
                <!-- ne pas modifier -->
                <language ident="fr">Français</language>
                <language ident="la">Latin</language>
                <language ident="grc">Grec</language>
            </langUsage>

        </profileDesc>
        <revisionDesc>
            <change when="2022-04-08">Sarah GAUCHER : correction de la transcription, encodage de la
                tranduction, conformité, identifiants Header</change>
        </revisionDesc>
    </teiHeader>

    <text>
        <body ana="#TH_repr">

            <!-- RAPPEL : on encode le texte latin ou grec; on n'encode dans le texte de la traduction que les notes d'éditeur (=membres du projet) -->

            <!-- Le couple d'éléments <head> contient le titre ou l'incipit du paratexte -->
            <!-- L'identifiant du TITRE est composé du titre de la fiche TT suivi d'un underscore (_) puis du chiffre 0 -->
            <head type="orig" xml:id="Te1504_Ascensius_p3k_0">De proscaeniorum <note>Nous corrigeons
                    le barbarisme <foreign xml:lang="la">proscaeniarum</foreign>, présent dans
                    l’édition de 1504 et corrigé en 1511. </note> ornatu et instructione. Capitulum
                XI.</head>
            <!-- Le deuxième élément du couple contient la traduction française du titre ou incipit -->
            <!-- L'identifiant de la traduction est le même que celui du latin, précédé d'un hashtag (#) -->
            <head corresp="#Te1504_Ascensius_p3k_0" type="trad">Chapitre 11. Sur le décor et la
                disposition des avant-scènes.</head>

            <ab type="orig" xml:id="Te1504_Ascensius_p3k_1">Colligamus ergo paucis ornatum et
                instructionem proscaenii, id est loci in quibus ludebant.</ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3k_1">Résumons donc brièvement le décor et
                la disposition des scènes, c’est-à-dire du lieu où on jouait.</ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3k_2">In cuius prima fronte titulus
                praeponebatur.</ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3k_2">Sur le devant de l’avant-scène, on
                faisait voir le titre.</ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3k_3">In quo quidem titulo ut plurimum tria
                habebantur : nomen scriptoris, nomen fabulae, seu comediae, et nomen actoris seu
                lusoris.</ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3k_3">Ce titre, le plus généralement,
                comprenait trois éléments : le nom de l’auteur, le nom de la pièce ou de la comédie
                et le nom de l’acteur ou du comédien.</ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3k_4">Et si ex Graeco translata fuerit, etiam
                graeci factoris nomen interdum fuit, ut post latius uidebimus.</ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3k_4">Et s’il s’agissait d’une pièce
                traduite du grec, on y trouvait aussi le nom de l’auteur grec, comme nous le verrons
                plus en détail par la suite.</ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3k_5">In pauimento autem proscaenii supra quod
                ambulabant strata erant tapeta ; sub ipsa autem scaena superficies erant domorum cum
                inscriptionibus dominorum suorum.</ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3k_5">Quant au sol de la scène, sur lequel
                les acteurs évoluaient, il était recouvert de tapisseries ; sous le mur de scène il
                y avait les façades des maisons avec l’inscription des noms de leurs
                propriétaires.</ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3k_6">Deinde suppara<note>Les éditions de Bade
                    donnent <foreign xml:lang="la">suppara</foreign>, mais Donat (Evanthius,
                        <title>De fabula</title>, 8.8) dit bien <foreign xml:lang="la"
                        >siparia</foreign>. De fait, les deux mots sont souvent confondus dans les
                        manuscrits.<ptr target="Te1504_Ascensius_p3j_12"/>.</note> seu cortinae
                tenues erant post quos histriones latitabant, et inde lusuri prodibant.</ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3k_6">Ensuite, il y avait de légers voiles
                ou rideaux derrière lesquels se cachaient les acteurs et d’où ils sortaient pour
                jouer.</ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3k_7">In dextra parte proscaenii ara erat
                Liberi patris, in sinistra Apollinis.</ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3k_7">Dans la partie droite de
                l’avant-scène, il y avait un autel dédié à Liber père, dans la partie gauche un
                autre dédié à Apollon.</ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3k_8">Sed post uelamina erat olim chorus qui
                cantica, ut dixi, praecinebat.</ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3k_8">Mais derrière les voiles il y avait
                jadis le chœur, qui, comme je l’ai dit, entonnait des chants.</ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3k_9">Quod autem chorus sit uidebis capitulo
                XV.</ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3k_9">On verra ce qu’il en est du chœur au
                chapitre 15.</ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3k_10">Postquam autem chorus ablatus est,
                quidam nostra tempestate cantica musicalia instituerunt.</ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3k_10">Or, après que le chœur a été
                supprimé, certains de nos contemporains ont mis en place des parties chantées à
                accompagnement musical. <note> Dans ses développements sur le théâtre antique, Bade
                    aime dresser des parallèles avec le théâtre de son époque. Bade pense peut-être
                    au théâtre « médiéval » en latin, comme celui de Ravisius Textor. Mathieu
                    Ferrand a montré la présence de passages chantés dans des moralités latines
                    inédites. Par ailleurs on chante beaucoup dans le théâtre vernaculaire du temps
                    (moralités, farces, mystères, etc.) </note>.</ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3k_11">Plurimi ante singulorum actuum
                inchoationem tibiis aliisque instrumentis musicalibus lusores substituunt per quos
                collecto populo silentium sit.</ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3k_11">Avant le début de chaque acte, de
                nombreux joueurs de flûtes et d’autres instruments de musique se mettent en place
                pour imposer le silence une fois les spectateurs rassemblés.</ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3k_12">Deinde dum musici concinunt, exit
                prologus in habitu suo ; dein persequenter usque ad finem primi actus agunt comediam
                ; quo actu finito iterum ut lusores se pararent ad secundum actum, canitur
                instrumentis musicis et sic usque ad finem.</ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3k_12">Ensuite, pendant que les musiciens
                commencent à jouer, le prologue sort dans son costume particulier ; ensuite on mène
                la comédie jusqu’à la fin du premier acte ; et une fois cet acte terminé, pour que
                les acteurs se préparent pour le deuxième acte, on joue des instruments de musique
                et ainsi de suite jusqu’à la fin.</ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3k_13">Atque postquam dictum est « ualete »,
                iterum canitur et sic populus abit et aulea hoc est uelamina tolluntur.</ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3k_13">Et, après le mot d’adieu, on joue une
                nouvelle fois et ainsi le public s’en va et on tire les tentures, c’est-à-dire les
                rideaux.</ab>
        </body>
    </text>
</TEI>
