<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
    

    <teiHeader>

        <fileDesc>

            <titleStmt>

                <!--Ci-dessous le titre du paratexte-->
                <title xml:lang="la">Iani Guillini Parisini in Authoris Commendationem. </title>
                <author ref="#guillinus_ianus">Ianus Guillinus Parisinus</author>

                <respStmt>
                    <resp>Transcription</resp>
                    <persName>Diandra CRISTACHE</persName>
                </respStmt>
                <respStmt>
                    <resp>Traduction</resp>
                    <persName>Sarah GAUCHER</persName>
                </respStmt>
                <respStmt>
                    <resp>Révision</resp>
                    <name>Christian NICOLAS</name>
                </respStmt>
                <respStmt>
                    <resp>Encodage</resp>
                    <name>Diandra CRISTACHE</name>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <respStmt>
                    <resp>Modélisation et structuration</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Elysabeth HUE-GAY</persName>
                </respStmt>

                <principal>Malika BASTIN-HAMMOU</principal>
            </titleStmt>

            <publicationStmt>
                <publisher/>
                <availability>
                    <p>Le site est en accès restreint</p>
                </availability>
                <pubPlace>ithac.elan-numerique.fr</pubPlace>
            </publicationStmt>

            <sourceDesc>
                <bibl>
                    <title>Sophoclis Tragicorum ueterum facile principis Tragoediae, quotquot
                        extant, septem. Aiax, Electra, Oedipus Tyrannus, Antigone, Oedipus in
                        Colono, Trachiniae, Philoctetes. Nunc primum Latinae factae, et in lucem
                        emissae per Ioannem Lalamantium apud Augustudunum Heduorum Medicum. </title>
                    <author ref="#soph">Sophocles</author>
                    <publisher ref="#morellus_federicus">Federicus Morellus </publisher>
                    <pubPlace ref="#lutetia">Lutetiae</pubPlace>
                    <date when="1557">1557</date>
                    <editor ref="#lalamantius_ioannes">Johannes Lalamantius </editor>
                    <ref
                        target="https://books.google.fr/books?id=WSg7AAAAcAAJ&amp;printsec=frontcover&amp;dq=jean+lalamant&amp;hl=fr&amp;sa=X&amp;ved=0ahUKEwiZwdairdXbAhUDaFAKHRpxAYkQ6AEIKjAA#v=onepage&amp;q=jean%20lalamant&amp;f=false"
                    />
                </bibl>
            </sourceDesc>
        </fileDesc>

        <profileDesc>

            <abstract>
                <p/>
            </abstract>

            <langUsage>
                <language ident="la">Latin</language>
                <language ident="gr">Grec</language>
            </langUsage>

        </profileDesc>

        <revisionDesc>
            <change when="2022-02-11">Sarah GAUCHER : Vérification et ajouts PersName, PlaceName,
                hi, title, quote, seg, foreign. Vérification de la mise en conformité du schéma </change>
            <change when="2021-11-10">Sarah GAUCHER correction de la transcription et encodage de la
                traduction</change>
            <change when="2021-05-08">Diandra CRISTACHE : Mise à jour et chargement sur le
                Pensoir</change>
            <change when="2020-09-21">Diandra CRISTACHE : Encodage de la transcription et de la
                traduction</change>

        </revisionDesc>

    </teiHeader>

    <text>
        <body ana="#PA">
            <head type="orig" xml:id="Soph1557a_Guillinus_0"><persName ref="#guillinus_ianus">Iani
                    Guillini</persName> Parisini in Authoris Commendationem.</head>
            <head type="trad" corresp="#Soph1557a_Guillinus_0">Recommendation de l’auteur par le
                parisien Jean Guillin</head>
            <ab type="orig" xml:id="Soph1557a_Guillinus_1">
                <l>Augustudunum Celticis in Heduis</l>
                <l>Florentissima quondam et praestantissima</l>
                <l>Ciuitas, Autunum Gallis dicta hodie,</l>
                <l>Antiquis olim Bibractis finitima,</l>
                <l>(Nisi ipsammet Bibractas malis dicere)</l>
                <l><persName>Montolonos</persName>, <persName>Chassanos</persName>,
                        <persName>Quintinos</persName>, simul</l>
                <l>Et <persName>Guillaudos</persName>, claros omnes scriptis, tulit. </l>
                <l>Hunc sacrae eximium praeconem paginae:</l>
                <l>Illos iuris celebratos peritia. </l>
            </ab>
            <ab type="trad" corresp="#Soph1557a_Guillinus_1">La cité d’Augustudunum, jadis la plus
                florissante et la plus remarquable chez le peuple celtique des Éduens, aujourd’hui
                appelée Autun par les Gaulois, voisine de l’antique Bibracte (si tu ne préfères pas
                l’appeler elle-même Bibracte) a porté Montholon, Chasseneu, Quintin en même temps
                que Guilliaud, tous célèbres pour leurs écrits, le dernier, remarquable héraut de
                l’écriture sainte et les premiers, que leur expérience du droit a rendus
                célèbres.</ab>

            <ab type="orig" xml:id="Soph1557a_Guillinus_2">
                <l>Ne deesset suus et medicis honos,</l>
                <l>Doctus numerum expleuit <persName ref="#lalamantius_ioannes"
                        >Lalamantius</persName>,</l>
                <l>Graecum qui <persName ref="#soph">Sophoclem</persName> latinis uersibus</l>
                <l>Ita expressit, ipsum dicas ut Sophoclem</l>
                <l>Latine loquutum. Iam uero salebris</l>
                <l>Graecae dictionis, obscurisque locis</l>
                <l>Permultis tantum claritatis attulit, </l>
                <l>Lector ut posthac nullibi haerere queat. </l>
            </ab>
            <ab type="trad" corresp="#Soph1557a_Guillinus_2">Sans manquer de respect <seg>aux
                        médecins<note>Lalamantius est aussi éditeur de Galien et de plusieurs
                        traités hippocratiques.</note></seg>, le savant Lalamantius a complété leurs
                rangs, lui qui a exprimé le grec Sophocle en vers latins si bien qu’on dirait que
                Sophocle lui-même parlait latin. Il a à présent tant éclairé les pièges de la langue
                grecque et ses nombreuses obscurités qu’après lui le lecteur ne pourra buter nulle
                part.</ab>

            <ab type="orig" xml:id="Soph1557a_Guillinus_3">
                <l>Ergo tam multis, o beata ciuitas,</l>
                <l>Tamque praeclaris decorata ingeniis,</l>
                <l>Tantis tuis uicissim et ipsa ciuibus</l>
                <l>Iurisperitis, theologis, medicis</l>
                <l>Quid imperti honoris, ut <persName ref="#lalamantius_ioannes"
                        >Lalamantio</persName></l>
                <l>Ad maiora praestanda addas calcaria.</l>
            </ab>
            <ab type="trad" corresp="#Soph1557a_Guillinus_3">Donc, ô bienheureuse cité
                qu’embellissent tant de talents si illustres et de si grands citoyens, tour à tour
                juristes, théologiens, médecins, accorde-nous quelque considération en encourageant
                Lalamantius à s’atteler à de plus grandes entreprises.</ab>

            <ab type="orig" xml:id="Soph1557a_Guillinus_5">
                <l>Tu <persName ref="#lalamantius_ioannes">Lalamanti</persName> medicinam facito</l>
                <l>Et de tuis bene mereri ciuibus</l>
                <l>Illa, qua excellis, Phoebaea arte pergito.</l>
            </ab>
            <ab type="trad" corresp="#Soph1557a_Guillinus_5">Quant à toi, Lalamantius, adonne-toi à
                la Médecine et continue de rendre service à tes citoyens grâce ce savoir-faire de
                Phébus dans lequel tu excelles.</ab>

        </body>
    </text>
</TEI>
