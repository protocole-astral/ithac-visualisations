<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">

    <teiHeader>

        <fileDesc>

            <titleStmt>

                <title xml:lang="la">Plauti uita ex Petro Crinito de Poetis Latinis.</title>
                <!-- Indiquer la langue du paratexte dans l'attribut xml:lang : "la" pour le latin et "grc" pour le grec -->
                <!-- Attention ! ne pas faire de retour charriot dans le titre -->
                <author ref="#crinints_petrus">Petrus Crinitus</author>
                <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence en minuscules, précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->

                <!-- Déclarations de responsabilité : indiquent qui a effectué quelle tâche -->
                <!-- Ajouter autant de tâches (<respStmt>) que nécessaire -->
                <respStmt>
                    <resp>Transcription</resp>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <respStmt>
                    <resp>Traduction</resp>
                    <name>Matthieu FERRAND</name>
                </respStmt>

                <respStmt>
                    <resp>Encodage</resp>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <!-- Attention ! Ne pas modifier les tâches ci-dessous -->
                <respStmt>
                    <resp>Modélisation et structuration</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Elysabeth HUE-GAY</persName>
                </respStmt>
                <principal>Malika BASTIN-HAMMOU</principal>
            </titleStmt>

            <!-- Informations de publication sur le laboratoire numérique du projet. Ne pas modifier-->
            <publicationStmt>
                <publisher/>
                <availability>
                    <p>Le site est en accès restreint</p>
                </availability>
                <pubPlace>ithac.elan-numerique.fr</pubPlace>
            </publicationStmt>

            <!-- Informations bibliographiques sur l'édition ancienne d'où le paratexte est tiré -->
            <sourceDesc>
                <bibl>
                    <title>Ex Plauti comoediis XX quarum carmina magna ex parte in mensum suum
                        restituta sunt M.D.XXII. Index uerborum, quibus paulo abstrusioribus Plautus
                        utitur. Argumenta singularum Comoediarum. Authoris uita. Tralatio dictionum
                        graecarum.</title>
                    <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->
                    <author ref="#pl">Plautus</author>
                    <pubPlace ref="#uenetia">Venetiis</pubPlace>
                    <publisher ref="#asulanus_franciscus">Franciscus Asulanus</publisher>
                    <date when="1522">1522</date>
                    <!-- dans l'attribut @when, l'année en chiffre arabes -->
                    <editor ref="#manutius_aldus">Aldus Manutius</editor>
                    <editor ref="#erasmus_desiderius">Erasmus</editor>
                    <ref
                        target="https://books.google.fr/books?id=PYAKKbWf2F4C&amp;pg=PA226&amp;lpg=PA226&amp;dq=x+Plauti+comoediis+XX+quarum+carmina+magna+ex+parte&amp;source=bl&amp;ots=BSuU0dfsJE&amp;sig=ACfU3U1VEy9SMP1vVO4HTXCHRpUuXiAfrw&amp;hl=fr&amp;sa=X&amp;ved=2ahUKEwiVlp-9pfP1AhXIOcAKHTHbAMwQ6AF6BAgQEAM#v=onepage&amp;q=x%20Plauti%20comoediis%20XX%20quarum%20carmina%20magna%20ex%20parte&amp;f=false"
                        >https://books.google.fr/books?id=PYAKKbWf2F4C&amp;pg=PA226&amp;lpg=PA226&amp;dq=x+Plauti+comoediis+XX+quarum+carmina+magna+ex+parte&amp;source=bl&amp;ots=BSuU0dfsJE&amp;sig=ACfU3U1VEy9SMP1vVO4HTXCHRpUuXiAfrw&amp;hl=fr&amp;sa=X&amp;ved=2ahUKEwiVlp-9pfP1AhXIOcAKHTHbAMwQ6AF6BAgQEAM#v=onepage&amp;q=x%20Plauti%20comoediis%20XX%20quarum%20carmina%20magna%20ex%20parte&amp;f=false</ref>
                    <!-- pour les liens google book, remplacer les & par &amp; pour que ça fonctionne -->
                </bibl>
                <listBibl>
                    <bibl>Sur Pietro Crinito, <ref
                            target="http://www.treccani.it/enciclopedia/del-riccio-baldi-pietro_(Dizionario-Biografico)"
                            >http://www.treccani.it/enciclopedia/del-riccio-baldi-pietro_(Dizionario-Biografico)</ref></bibl>
                    <bibl>Annaclara Cataldi Palau, Gian Francesco d’Asola e la tipografia aldina. La
                        vita, le edizioni, la biblioteca dell’Asolano, Gênes, Sagep Libri e
                        Communicazione, 1998.</bibl>
                    <bibl>AIlaria Pierini, « Comici e tragici nel De poetis latinis di Pietro
                        Crinito », Comico e tragico nella vita del Rinascimento, Florence, Franco
                        Cesati Editore, 2016, p. 191-215</bibl>
                    <bibl>Traduction anglaise de Crinito dans Plautus and the English Renaissance of
                        Comedy de Richard F. Hardin, Madison, Fairleigh Dickinson university press,
                        2018</bibl>
                </listBibl>
            </sourceDesc>
        </fileDesc>

        <profileDesc>

            <abstract>
                <p>Le texte que reproduit, pour la première fois dans une édition plautinienne,
                    Francesco Asolano, a été publié par Pietro Crinito dès 1506 à Florence dans son
                        <title>De poetis latinis</title>. Il puise aux mêmes sources (Aulu-Gelle,
                    III, 3, essentiellement) que les trois <title>Vitae Plauti</title> précédemment
                    publiées dans les éditions du Sarsinate (Merula, 1472 ; Pio, 1500 ; Charpentier,
                    1512) mais cette Vita s’imposera dans les éditions postérieures. Notons du reste
                    que, dès 1512 à Paris, Simon Charpentier, qui avait pu consulter l’édition
                    parisienne du <title>De poetis latinis</title> (Josse Bade, 1508), reprend une
                    partie du texte de Crinito dans sa propre <title>Vita</title>.</p>
            </abstract>

            <langUsage>
                <!-- ne pas modifier -->
                <language ident="fr">Français</language>
                <language ident="la">Latin</language>
                <language ident="grc">Grec</language>
            </langUsage>

        </profileDesc>
        <revisionDesc>
            <change when="2022-02-28">Sarah GAUCHER : identifiants Header, mise en conformité
                Elan</change>
            <change when="2022-02-18">Sarah GAUCHER : création de la fiche TEI, encodage de la
                transcription et de la traduction</change>
        </revisionDesc>
    </teiHeader>

    <text>
        <body>
            <head type="orig" xml:id="Pla1522_Crinitus_p1_0"><persName ref="#pl">Plauti</persName>
                uita ex <persName ref="#crinitus_petrus">Petro Crinito</persName>
                <title>de Poetis Latinis</title>.</head>
            <head type="trad" corresp="#Pla1522_Crinitus_p1_0">Vie de Plaute, extraite de l’ouvrage
                    <title>Les poètes latins</title> de Petrus Crinitus <note>Livre I, chap. 3, paru
                    à Florence en 1506, chez Philippo Giunta, publié à Paris en 1508 par Josse Bade
                    ; sur l’humaniste florentin Pietro Crinito (1474-1507) et son ouvrage, voir les
                    travaux cités dans la bibliographie)</note></head>
            <ab type="orig" xml:id="Pla1522_Crinitus_p1_1">Marcus <persName ref="#pl"
                    >Plautus</persName> patria Sarsinas fuit ex Vmbria, <seg type="allusion">quod
                    ipse de se insinuat, in fabula <title ref="#pl_most">Mostellaria</title><bibl
                        resp="#MF"><author ref="#pl">Pl.</author><title ref="#pl_most">Most.</title>
                        <biblScope>770</biblScope>.<note>La <title>Vita Plauti</title> de Merulla,
                            dans l’<foreign xml:lang="la">editio princeps</foreign> de 1472, cite le
                            vers de Plaute.</note></bibl></seg>.</ab>
            <ab type="trad" corresp="#Pla1522_Crinitus_p1_1">Marcus Plautus est originaire de
                Sarsina, en Ombrie, ce que lui-même laisse entendre, dans sa pièce
                    <title>Mostellaria</title>.</ab>
            <ab type="orig" xml:id="Pla1522_Crinitus_p1_2">Et alii ueteres tradunt.</ab>
            <ab type="trad" corresp="#Pla1522_Crinitus_p1_2">D’autres auteurs anciens le rapportent
                également.</ab>
            <ab type="orig" xml:id="Pla1522_Crinitus_p1_3">Constat eum his temporibus Romae uixisse
                atque in scena praestitisse quibus in ciuitate <persName>Pub. Scipio</persName>,
                    <persName>Fuluius Nobilior</persName>, <seg type="allusion">et <persName>M.
                        Cato</persName> excellentes habiti sunt.<bibl resp="#MF"><author ref="#gell"
                            >Gell.</author>, <title ref="#gell_noct">Noct.</title>
                        <biblScope>17.21.47</biblScope>.</bibl></seg></ab>
            <ab type="trad" corresp="#Pla1522_Crinitus_p1_3">Il est certain qu’il vécut à Rome et
                qu’il triompha sur la scène à l’époque où dans la ville, on tenait Publius Scipion,
                Fulvius Nobilior et Marcus Caton pour des personnages éminents.</ab>
            <ab type="orig" xml:id="Pla1522_Crinitus_p1_4">Ingenio perurbano fuit, et maxime
                festiuo, quod cum alia multa indicant, tum Comoediae ipsius plenae iucunditatis ac
                leporum.</ab>
            <ab type="trad" corresp="#Pla1522_Crinitus_p1_4">C’était un homme d’esprit affable et
                tout à fait charmant, ce que montrent, avec bien d’autres choses, ses propres
                comédies, pleines de gaîté et de grâce.</ab>
            <ab type="orig" xml:id="Pla1522_Crinitus_p1_5">Relata sunt ab <persName ref="#gell">A.
                    Gellio</persName> complura in <title ref="#gell_noct">Atticis noctibus</title>
                de eruditione et fabulis <persName ref="#pl">Plautinis</persName>, ex auctoritate
                    <persName ref="#uarro">M. Varronis</persName>, qui <persName ref="#pl"
                    >Plautum</persName> ait, <seg type="paraphrase">cum erogasset omnem pecuniam in
                    scenicis ornamentis ad summam paupertatem fuisse redactum, eaque ratione in
                    Vrbem rediisse et operam pistori locasse ad uictum comparandum his molis
                    circumagendis, quas trusatiles uocant.<bibl resp="#MF"><author ref="#gell"
                            >Gell.</author>, <title ref="#gell_noct">Noct.</title>
                        <biblScope>3.3.14</biblScope>.<note>De fait, l’essentiel du propos qui suit
                            est un centon du chapitre III, 3 des <title>Nuits
                            Attiques</title>.</note></bibl></seg></ab>
            <ab type="trad" corresp="#Pla1522_Crinitus_p1_5"><seg>Aulu-Gelle a rapporté de très
                    nombreuses anecdotes dans les <title>Nuits Attiques</title>
                </seg> en s’appuyant sur son érudition, sur les pièces de Plaute et sur l’autorité
                de Marcus Varron qui dit que Plaute, après avoir dépensé tout son argent pour
                l’organisation des spectacles, fut réduit à la plus grande pauvreté, et pour cette
                raison, revint à Rome et loua ses services à un boulanger pour gagner sa pitance en
                faisant tourner la meule que l’on dit « à bras » </ab>
            <ab type="orig" xml:id="Pla1522_Crinitus_p1_6"><persName ref="#hier"
                    >Hieronymus</persName>
                <cit type="refauteur">
                    <quote>manuales</quote>
                    <bibl resp="#MF"><author ref="#hier">Hier.</author>, <title ref="#hier_euschron"
                            >Eus. Chron.</title>
                        <biblScope>145</biblScope><ptr
                            target="https://www.tertullian.org/fathers/jerome_chronicle_06_latin_part2.htm"
                        />. <note>Jérôme parle en fait de <quote xml:lang="la">molas
                                manuarias</quote> ; Charpentier, qui avait repris l’erreur de
                            Crinito, la corrigeait pourtant dans son édition des
                                <title>Comediae</title> en 1512 : <foreign xml:lang="la"> Hieronimus
                                manuales aut manuarias appellauit</foreign> (<foreign xml:lang="la"
                                >Symonis Charpentarii […] praelibatio seu praefatio</foreign>,
                            phrase 10)</note></bibl>
                </cit> appellauit.</ab>
            <ab type="trad" corresp="#Pla1522_Crinitus_p1_6">Jérôme les appelle « manuelles ».</ab>
            <ab type="orig" xml:id="Pla1522_Crinitus_p1_7">Et cum aliquamdiu in pistrino uersaretur,
                ibidem fabulas aliquot dicitur composuisse ut <title>Saturionem</title> et
                    <title>Addictum</title>.</ab>
            <ab type="trad" corresp="#Pla1522_Crinitus_p1_7"><seg type="paraphrase">Et comme il
                    avait déjà passé un certain temps dans le moulin, on dit qu’il composa là
                    quelques pièces, comme le <title>Saturion</title> et
                        l’<title>Addictum</title>.<bibl resp="#SG"><author ref="#gell"
                            >Gell.</author>, <title ref="#gell_noct">Noct.</title>
                        <biblScope>3.3.14</biblScope>.</bibl></seg></ab>
            <ab type="orig" xml:id="Pla1522_Crinitus_p1_8"><seg type="paraphrase"><persName
                        ref="#uarro">M. Varro</persName> in libro <title ref="#uarro_dpc">de
                        Plautinis Comoediis</title> uerba haec posuit<bibl resp="#MF"><author
                            ref="#gell">Gell.</author>, <title ref="#gell_noct">Noct.</title>
                        <biblScope>3.3.9</biblScope>. <note>le texte de Crinito ne donne pas les
                            mots d’Accius que cite Varron et qu’annonce et reproduit le texte
                            d’Aulu-Gelle ; ici, l’expression <foreign xml:lang="la">haec
                                uerba</foreign> renvoie évidemment à la phrase
                        précédente.</note></bibl></seg>, quae adscribenda existimaui ut a <persName
                    ref="#gell">Gellio</persName> est obseruatum.</ab>
            <ab type="trad" corresp="#Pla1522_Crinitus_p1_8">Marcus Varron, dans son livre
                    <title>Sur les comédies de Plaute</title>, rapporte ces mots, que j’ai jugé bon
                de reproduire, ainsi qu’Aulu-Gelle l’a mentionné.</ab>
            <ab type="orig" xml:id="Pla1522_Crinitus_p1_9"><cit type="refauteur">
                    <quote>Feruntur sub <persName ref="#pl">Plauti</persName> nomine Comoediae
                        circiter C. et XXX.</quote>
                    <bibl resp="#SG"><author ref="#gell">Gell.</author>, <title ref="#gell_noct"
                            >Noct.</title>
                        <biblScope>3.3.11</biblScope></bibl>
                </cit></ab>
            <ab type="trad" corresp="#Pla1522_Crinitus_p1_9">On dit que l’on attribue à Plaute
                cent-trente pièces.</ab>
            <ab type="orig" xml:id="Pla1522_Crinitus_p1_10"><seg type="paraphrase">Sed homo
                    eruditissimus <persName>Laelius</persName> XXV dumtaxat illius esse
                        censuit.<bibl resp="#SG"><author ref="#gell">Gell.</author>, <title
                            ref="#gell_noct">Noct.</title>
                        <biblScope>3.3.12</biblScope>.</bibl></seg></ab>
            <ab type="trad" corresp="#Pla1522_Crinitus_p1_10">Mais <seg>Laelius, homme fort savant
                        <note>Il s’agit de Lucius Aelius Stilo, philologue et maître de Cicéron et
                        Varron</note></seg>, ne lui en a attribué que vingt-cinq.</ab>
            <ab type="orig" xml:id="Pla1522_Crinitus_p1_11"><seg type="paraphrase">Neque dubium est
                    quin ista etiam, quae a Plauto scripta non sunt, ueterum poetarum fuerint, ut M.
                        <persName ref="#acc">Accii</persName> C. Plautii a quo dictae sunt
                    Plautianae fabulae, non Plautinae.<bibl resp="#SG"><author ref="#gell"
                            >Gell.</author>, <title ref="#gell_noct">Noct.</title>
                        <biblScope>3.3.13</biblScope>.</bibl></seg></ab>
            <ab type="trad" corresp="#Pla1522_Crinitus_p1_11">Et il ne fait guère de doute que ce
                qui n’a pas été écrit par Plaute l’a été par d’anciens poètes comme M. Accius C.
                Plautius, dont les pièces sont dites « plautiennes », et non « plautiniennes ».</ab>
            <ab type="orig" xml:id="Pla1522_Crinitus_p1_12">Ceterum cum XXV a ueteribus
                connumerentur <persName ref="#pl">M. Plauti</persName> Comoediae, sciendum est
                praeter illas XX (quae a nostris grammaticis consensu omnium pro <persName ref="#pl"
                    >Plautinis</persName> habentur) tres a <persName ref="#gell">Gellio</persName>
                nominari. <seg type="paraphrase">hoc est <title>Bethiam</title>,
                        <title>Neruulariam</title>, <title>Fretum</title>.<bibl resp="#SG"><author
                            ref="#gell">Gell.</author>, <title ref="#gell_noct">Noct.</title>
                        <biblScope>3.3.6-7</biblScope>.</bibl></seg></ab>
            <ab type="trad" corresp="#Pla1522_Crinitus_p1_12">Du reste, tandis que les anciens
                comptent vingt-cinq comédies de Plaute, il faut savoir que, outre les vingt qui, de
                l’avis général de nos grammairiens, sont bien de Plaute, trois autres sont nommées
                par Aulu-Gelle : <title>Boethia</title>, <title>Nervularia</title> et
                    <title>Fretum</title>.</ab>
            <ab type="orig" xml:id="Pla1522_Crinitus_p1_13"><persName ref="#uarro">Varro</persName>
                et <persName ref="#fest">Sex. Pompeius</persName> alias plures nuncupant, ut <seg
                    type="allusion"><title>Artemonem</title><bibl resp="#SG"><author ref="#fest"
                            >Fest.</author>, <title ref="#fest_dus">De uerborum
                            significatione</title>, <biblScope>164 ; 304 ;
                    396</biblScope>.</bibl></seg>, <seg type="allusion"
                        ><title>Friuolariam</title><bibl resp="#SG"><author ref="#uarro"
                            >Varro</author>, <title ref="#uarro_l">L.</title>
                        <biblScope>7.3.58</biblScope> et <author ref="#fest">Fest.</author>, <title
                            ref="#fest_dus">De uerborum significatione</title>, <biblScope>168 ; 308
                            ; 380 ; 388...</biblScope></bibl></seg>, <seg type="allusion"
                        ><title>Phagornem</title><bibl resp="#SG"><author ref="#uarro"
                            >Varro</author>, <title ref="#uarro_l">L.</title>
                        <biblScope>7.61</biblScope>.</bibl></seg>, <seg type="allusion"
                        ><title>Cestrionem</title><bibl resp="#SG"><author ref="#uarro"
                            >Varro</author>, <title ref="#uarro_l">L.</title>
                        <biblScope>7.67</biblScope>.</bibl></seg> atque <seg type="allusion"
                        ><title>Astrabam</title><bibl resp="#SG"><author ref="#fest">Fest.</author>,
                            <title ref="#fest_dus">De uerborum significatione</title>,
                            <biblScope>342 ; 400</biblScope>.</bibl></seg>, easque inter <persName
                    ref="#pl">Plautinas</persName> referunt.</ab>
            <ab type="trad" corresp="#Pla1522_Crinitus_p1_13">Varron et <seg>Festus Pompeius
                        <note>Il s’agit de Sextus Pompeius Festus, ou Festus Pompeius, grammairien
                        du IIe siècle ap. J.-C.</note></seg> en nomment d’autres encore :
                    <title>Artemo</title>, <title>Frivolaria</title>, <title>Phago</title>,
                    <title>Cestrio</title>, <title>Astraba</title> ; et celles-ci sont comptées au
                nombre des comédies plautiniennes.</ab>
            <ab type="orig" xml:id="Pla1522_Crinitus_p1_14"><seg type="paraphrase">De
                        <title>Astraba</title> tamen <persName ref="#gell">Gellius</persName> et
                        <persName ref="#non">Nonius</persName> dubitarunt.<bibl resp="#MF"><author
                            ref="#gell">Gell.</author>, <title ref="#gell_noct">Noct.</title>
                        <biblScope>11.7.5</biblScope> et <author ref="#non">Non.</author>, <title
                            ref="#non_dcd">De compendiosa doctrina</title>,
                            <biblScope>2.69M</biblScope>.</bibl></seg></ab>
            <ab type="trad" corresp="#Pla1522_Crinitus_p1_14">A propos d’<title>Astraba</title>,
                cependant, Aulu-Gelle et Nonius ont émis des doutes.</ab>
            <ab type="orig" xml:id="Pla1522_Crinitus_p1_15"><seg type="paraphrase">In fabulis suis
                    secutus est Graecos auctores, maxime <persName ref="#demophile"
                        >Demophilum</persName>, <persName ref="#philem">Philemonem</persName> et
                        <persName ref="#epich">Epicharmum Siculum</persName><bibl resp="#MF"><author
                            ref="#hor">Hor.</author>, <title ref="#hor_ep">Ep.</title>
                        <biblScope>2.1.58</biblScope>.</bibl></seg>, ut <persName ref="#hor"
                    >Horatius Flaccus</persName> testatur. </ab>
            <ab type="trad" corresp="#Pla1522_Crinitus_p1_15">Dans ses pièces, Plaute a suivi les
                auteurs grecs, en particulier Démophile, Philémon et le Sicilien Epicharme, comme
                l’atteste Horace.</ab>
            <ab type="orig" xml:id="Pla1522_Crinitus_p1_16">Tantumque scribendi elegantia et salibus
                uisus est praestitisse, ut <persName>Epius Stolo</persName> adfirmare non
                dubitauerit, <seg type="paraphrase">musas ipsas plautino sermone fuisse locuturas,
                    si latine loqui uoluissent.<bibl resp="#MF"><author ref="#quint"
                        >Quint.</author>, <title ref="#quint_io">I.O.</title>
                        <biblScope>10.1.99</biblScope>.</bibl></seg></ab>
            <ab type="trad" corresp="#Pla1522_Crinitus_p1_16">Et il a paru à ce point l’emporter par
                le sel et l’élégance de son style qu’<seg>Epius Stolo <note>Chez Quintilien, il est
                        question de Lucius Aelius Stilon, le maître de Cicéron et de Varron qui a
                        déjà été cité.</note></seg> n’a pas hésité à affirmer que « Les muses
                elles-mêmes auraient parlé la langue de Plaute, si elles avaient voulu parler latin
                » ; </ab>
            <ab type="orig" xml:id="Pla1522_Crinitus_p1_17">Quod a <persName ref="#quint">Fabio
                    Quintiliano</persName> refertur.</ab>
            <ab type="trad" corresp="#Pla1522_Crinitus_p1_17">C’est ce que rapporte Quintilien.</ab>
            <ab type="orig" xml:id="Pla1522_Crinitus_p1_18"><seg>Quocirca <note>Nous corrigeons le
                        texte, qui donne <foreign xml:lang="la">Qui circa</foreign>, d’après la
                            <foreign xml:lang="la">princeps</foreign> de Crinito (Paris,
                        1508)</note></seg>
                <persName ref="#gell">Gellius</persName> censor optumus omnium scriptorum appellat
                eundem <persName ref="#pl">Plautum</persName>
                <seg type="paraphrase">parentem ac principem in omni latina elegantia<bibl
                        resp="#SG"><author ref="#gell">Gell.</author>, <title ref="#gell_noct"
                            >Noct.</title>
                        <biblScope>6.17.4</biblScope>.</bibl></seg>.</ab>
            <ab type="trad" corresp="#Pla1522_Crinitus_p1_18">C’est pourquoi Aulu-Gelle, le meilleur
                juge pour chaque auteur, appelle ce même Plaute « le père et le prince de l’élégance
                latine ».</ab>
            <ab type="orig" xml:id="Pla1522_Crinitus_p1_19"><persName ref="#uolc_sed">Volcatius
                    Sedigitus</persName>, <seg type="paraphrase">cum de ordine Comicorum scribit,
                    post <persName ref="#caecil">Caecilium</persName> collocauit <persName ref="#pl"
                        >Plautum</persName>, ceterisque omnibus praetulit.<bibl resp="#MF"><author
                            ref="#gell">Gell.</author>, <title ref="#gell_noct">Noct.</title>
                        <biblScope>15.24</biblScope>.</bibl></seg></ab>
            <ab type="trad" corresp="#Pla1522_Crinitus_p1_19">Volcatius Sedigitus, quand il écrit
                sur le classement des Comiques, place Plaute après Cécilius, et il le préfère à tous
                les autres. </ab>
            <ab type="orig" xml:id="Pla1522_Crinitus_p1_20"><seg type="paraphrase">Nomen <persName
                        ref="#pl">Plauti</persName> deductum est a planitie pedum<bibl resp="#MF"
                            ><author ref="#fest">Fest.</author>, <title ref="#fest_dus">De uerborum
                            significatione</title>, <biblScope>14</biblScope>.</bibl></seg>, ut est
                auctor <persName ref="#fest">Sex. Pompeius</persName> ; prius enim M. Plotus
                dicebatur.</ab>
            <ab type="trad" corresp="#Pla1522_Crinitus_p1_20">Le nom de Plaute vient des « pieds
                plats » (comme le dit Festus Pompeius) ; car d’abord, on l’appelait Marcus
                Plotus.</ab>
            <ab type="orig" xml:id="Pla1522_Crinitus_p1_21">Hinc semiplotia calciamenta dicta sunt,
                quibus in uenationibus uterentur, sed hac de re suo loco diximus.</ab>
            <ab type="trad" corresp="#Pla1522_Crinitus_p1_21">De là vient le nom des chaussures «
                semiplotia » que portaient les chasseurs. Mais <seg>nous avons déjà parlé de cela en
                    temps voulu <note>Peut-être dans le <title>De grammaticis</title> ou le
                            <title>De historicis et rethoribus</title>, ouvrages de Crinito
                        aujourd’hui perdus, cf. Ilaria Pierini, « Comici e tragici nel De poetis
                        latinis di Pietro Crinito », <title>Comico e tragico nella vita del
                            Rinascimento</title>, Florence, Franco Cesati Editore, 2016, p. 191-215
                        ; ici p. 203, n. 44</note></seg>.</ab>
            <ab type="orig" xml:id="Pla1522_Crinitus_p1_22">Obiisse traditur <seg type="allusion"
                    >paucis annis post <persName ref="#enn">Q. Ennium</persName> olympiade centesima
                        XLV.<bibl resp="#MF"><author ref="#hier">Hier.</author>, <title
                            ref="#hier_euschron">Eus. Chron.</title>
                        <biblScope>168</biblScope><ptr
                            target="https://www.tertullian.org/fathers/jerome_chronicle_06_latin_part2.htm"
                        />.</bibl></seg></ab>
            <ab type="trad" corresp="#Pla1522_Crinitus_p1_22">On rapporte qu’il est mort peu
                d’années après Ennius, lors de la 145e olympiade.</ab>
            <ab type="orig" xml:id="Pla1522_Crinitus_p1_23">Quantum iacturae factum sit in eius
                obitu, testus est idem Poeta his uersibus de se compositis :</ab>
            <ab type="trad" corresp="#Pla1522_Crinitus_p1_23">Quelle perte fut sa mort, le poète
                lui-même en témoigne dans ces vers de sa composition :</ab>
            <ab type="orig" xml:id="Pla1522_Crinitus_p1_24"><cit type="nonref">
                    <quote><l>Postquam est morte captus Plautus </l><l>Comoedia luget, scena est
                            deserta.</l><l> Deinde risus, ludus, locusque et numeri</l><l> Innumeri
                            simul omnes collachrymarunt.</l></quote>
                    <bibl resp="#MF"><author ref="#gell">Gell.</author>, <title ref="#gell_noct"
                            >Noct.</title>
                        <biblScope>15 24</biblScope>.</bibl>
                </cit>
            </ab>
            <ab type="trad" corresp="#Pla1522_Crinitus_p1_24"><l>« Après que Plaute a été emporté
                    par la mort</l><l>La Comédie pleure, la scène est déserte,</l><l>Enfin les
                    rires, les jeux, les ébats, les vers</l><l>La prose, tous, de concert, ont versé
                    des larmes ».</l></ab>

        </body>
    </text>
</TEI>
