<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">

    <teiHeader>

        <fileDesc>

            <titleStmt>

                <title xml:lang="la">Ioannis Dominici Florentii Romani Carmen</title>
                <!-- Attention ! ne pas faire de retour charriot dans le titre -->
                <author ref="#florentius_ioannes">Ioannis Dominicus Florentius</author>

                <!-- Déclarations de responsabilité : indiquent qui a effectué quelle tâche -->
                <!-- Tâche n°1 -->
                <respStmt>
                    <resp>Transcription</resp>
                    <name>Valérie THIRION</name>
                </respStmt>
                <!-- Tâche n°2 -->
                <respStmt>
                    <resp>Traduction</resp>
                    <name>Pascale PARÉ-REY</name>
                </respStmt>
                <!-- Tâche n°3 -->
                <respStmt>
                    <resp>Encodage</resp>
                    <name>Valérie THIRION</name>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <!-- Ajouter autant de tâches que nécessaire -->
                <!-- Attention ! Ne pas modifier les tâches ci-dessous -->
                <respStmt>
                    <resp>Modélisation et structuration</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Elysabeth HUE-GAY</persName>
                </respStmt>
                <principal>Malika BASTIN-HAMMOU</principal>
            </titleStmt>

            <!-- Informations de publication sur le laboratoire numérique du projet-->
            <publicationStmt>
                <publisher/>
                <availability>
                    <p>Le site est en accès restreint</p>
                </availability>
                <pubPlace>ithac.elan-numerique.fr</pubPlace>
            </publicationStmt>

            <sourceDesc>

                <bibl>
                    <title>In L. Annaei Senecae Cordubensis Poetae grauissimi Tragoedias decem ;
                        Scilicet Herculem Furentem, Herculem Œtaeum, Medeam, Hippolytum, Œdipum,
                        Thebaidem, Thyestem, Troades, Agamemnonem, Octauiam, Amplissima adversaria
                        quae loco commentarii esse possunt ex bibliotheca Martini Antonii Delrio, I.
                        C. </title>
                    <!-- l'attribut @level permet d'indiquer s'il s'agit d'une monographie (m), d'un article ou chapitre d'ouvrage (a) ou d'un périodique (j) -->
                    <author ref="#sen">Seneca</author>
                    <pubPlace ref="#antuerpia">Antverpiae</pubPlace>
                    <publisher ref="#plantinus_christophorus">Christophorus Plantinus</publisher>
                    <date when="1576">1576</date>
                    <editor ref="#delrio_martinus">Martinus Antonius Delrio</editor>
                    <ref target="https://numelyo.bm-lyon.fr/f_view/BML:BML_00GOO0100137001102809949"/>
                    <ref
                        target="https://books.google.fr/books?id=YWZID-M1KUEC&amp;printsec=frontcover&amp;hl=fr&amp;source=gbs_ge_summary_r&amp;cad=0#v=onepage&amp;q&amp;f=false/"/>
                    <ref target="https://books.google.fr/books?vid=BML37001103093519"/>
                </bibl>
                <listBibl>
                    <bibl><author>Florence de Caigny</author>, <title level="u">« Les
                                <title>Syntagma tragœdiæ latinæ</title> de Del Rio (1593-1594) :
                            pour une lecture jésuite des pièces de Sénèque »</title>, colloque «
                        Anthropologie tragique et création poétique de l’Antiquité au XVII<hi
                            rend="sup">e</hi> siècle français », Nice, 24-26 novembre 2016.</bibl>

                    <bibl><author>M. Dreano</author>, <title level="a">« Un commentaire des
                            tragédies de Sénèque au XVI<hi rend="sup">e</hi> siècle par
                            Martin-Antoine Del Rio »</title>, dans <title level="m">Les tragédies de
                            Sénèque et le théâtre de la Renaissance</title>, éd.
                            <editor>J. Jacquot</editor>
                        <foreign xml:lang="la">et al.</foreign>, <pubPlace>Paris</pubPlace>,
                            <publisher>Éd. du CNRS</publisher>, <date>1964</date>, <biblScope
                            unit="page">p. 203-209</biblScope>.</bibl>

                    <bibl><author>J. Machielsen</author><title level="m">Martin Delrio, Demonology
                            and Scholarship in the Counter-Reformation</title>,
                            <pubPlace>Oxford</pubPlace>, <publisher>OUP</publisher>,
                        <date/>2015.</bibl>
                    <bibl>Paré-Rey, Pascale, "Les éditions des tragédies de Sénèque conservées à la
                        Bibliothèque nationale de France (XVe-XIXe s.)", in L’Antiquité à la BnF,
                        17/01/2018, <ref target="https://antiquitebnf.hypotheses.org/1643"
                            >https://antiquitebnf.hypotheses.org/1643</ref></bibl>
                </listBibl>
            </sourceDesc>

        </fileDesc>

        <profileDesc>

            <abstract>
                <p>Dans un poème en distiques élégiaques, Ioannis Dominicus Florentius loue Delrio
                    d’avoir fait sortir Sénèque des ténèbres où il était, et l’inscrit dans la
                    lignée des grands noms représentatifs de chaque genre poétique, énumérés au
                    début de ses vers.</p>
            </abstract>

            <langUsage>
                <!-- ne pas modifier -->
                <language ident="fr">Français</language>
                <language ident="la">Latin</language>
            </langUsage>

        </profileDesc>

        <revisionDesc>
            <!-- pour indiquer des modifications apportées au fichier -->
            <change when="2022-03-01">Sarah GAUCHER : identifiants Header, mise en conformité
                Elan</change>
            <change who="Diandra CRISTACHE" when="2021-05-23">Diandra CRISTACHE : mise à jour et
                publication sur le Pensoir</change>
            <change when="2021-01-22">Valérie Thirion: encodage de la transcription</change>
            <change when="2022-01-06">Pascale Paré: mise à jour bibliographique </change>
        </revisionDesc>

    </teiHeader>

    <text>
        <body ana="#PA #PH">
            <head type="orig" xml:id="Sq576_Florentius_p1_0"><persName ref="#florentius_ioannes"
                    >Ioannis Dominici Florentii Romani</persName> Carmen</head>
            <head corresp="#Sq576_Florentius_p1_0" type="trad">Poème du romain <seg>Ioannis
                    Dominicus Florentius<note>Junius, Florentius (15..-15..) Maître de l'université
                        de Paris, originaire de Gouda (diocèse d'Utrecht, Pays-Bas). Procureur de la
                        nation d'Allemagne (1542) <ref target="https://www.idref.fr/195917731"
                            >https://www.idref.fr/195917731</ref> ??</note></seg></head>

            <ab type="orig" xml:id="Sq576_Florentius_p1_1">
                <l>Arma tibi, siluaeque, <persName ref="#uirg">Maro</persName>, et bene olentia
                    rura</l>
                <l>Debent, una tuis aucta uoluminibus :</l>
                <l>Omniparens docti rerum natura <persName ref="#lucr">Lucretii</persName></l>
                <l>Versibus : at satyra, <persName ref="#hor">Flacce</persName>, chelysque,
                    tibi :</l>
                <l>Syllaba quin etiam facundo pura <persName ref="#catul">Catullo</persName> :</l>
                <l>Saeuus amor numeris, culte <persName ref="#tib">Tibulle</persName>, tuis.</l>
            </ab>
            <ab corresp="#Sq576_Florentius_p1_1" type="trad">
                <l>C’est envers toi, Virgile, que sont redevables les armes, les forêts et les
                    champs aux doux parfums,</l>
                <l>développés ensemble dans <seg>tes volumes<note>Référence à
                                l’<title>Énéide</title>, aux <title>Bucoliques</title>, aux
                                <title>Géorgiques</title>.</note></seg> :</l>
                <l>la nature, mère de toutes choses, envers les vers</l>
                <l>du savant Lucrèce ; mais la satire, et la lyrique, c’est envers toi, Horace,</l>
                <l>bien plus, les poèmes purs envers l’éloquent Catulle,</l>
                <l>l’amour cruel, élégant Tibulle, envers tes mesures.</l>
            </ab>

            <ab type="orig" xml:id="Sq576_Florentius_p1_2">
                <l>Te soccus, gratique, sales, Venus ipsa, <persName ref="#ter"
                    >Terenti</persName>,</l>
                <l>Agnoscit, plaudunt tota theatra tibi.</l>
            </ab>
            <ab corresp="#Sq576_Florentius_p1_2" type="trad">
                <l>Le socque et les jeux de mots plaisants, Vénus elle-même, te reconnaissent</l>
                <l>Térence ; des théâtres entiers t’applaudissent.</l>
            </ab>

            <ab type="orig" xml:id="Sq576_Florentius_p1_3">
                <l>Vix cuiquam primas defert epigramma uenustum,</l>
                <l>Auctorem nosti uixque cothurne tuum :</l>
                <l>Iudicio at magni haec, immortalisque <persName>Marulli</persName>,</l>
                <l>Quem merito priscis adnumerare queas,</l>
                <l>Romanos referens qui arguto carmine uates,</l>
                <l>Cuique suum assignat debito honore locum.</l>
            </ab>
            <ab corresp="#Sq576_Florentius_p1_3" type="trad">
                <l>À peine une agréable épigramme décerne-t-elle le premier rôle à quelqu'un que tu
                    connais l’auteur</l>
                <l>et à peine, cher cothurne, connais-tu le tien :</l>
                <l>et cela, au jugement du grand et immortel <seg>Marullus<note>Michael Tarchianota,
                            mieux connu sous le nom de (Michael) Marullus ou encore de Michel
                            Marulle, est né à Constantinople en 1453 et mort en 1500 près de
                            Volterra. Il fit partie du cercle de Laurent le Magnifique et fut connu
                            pour ses élégies latines ; il publia des <title>Epigrammata</title> en
                            1493 et des <title>Hymni</title> de 1489 à 1492, réunis en 1497 sous le
                            titre d'<title>Epigrammata</title>. Ses poèmes ont été récemment
                            édités : (<bibl>Marullo Tarcaniota, 2012</bibl>). </note></seg>,</l>
                <l>que l’on pourrait à bon droit compter parmi les anciens, </l>
                <l>lui qui, évoquant chaque poète romain par son chant précis,</l>
                <l>assigne à chacun sa place avec l’honneur qui lui est dû.</l>
            </ab>

            <ab type="orig" xml:id="Sq576_Florentius_p1_4">
                <l>Idem nunc aliud sentiret, si tua uiuens,</l>
                <l><persName ref="#delrio_martinus">Antoni</persName>, legeret scripta diserta,
                    quibus</l>
                <l><persName ref="#sen">Annaeum</persName> inlustras : nobis squalore, situque</l>
                <l>Ereptum, studio restituisque tuo.</l>
            </ab>
            <ab corresp="#Sq576_Florentius_p1_4" type="trad">
                <l>Il ressentirait également à présent autre chose, si de son vivant,</l>
                <l>Antonio lisait tes habiles écrits, par lesquels</l>
                <l>tu mets Sénèque en lumière : tu l’as arraché à la poussière et à la
                    moisissure</l>
                <l>et nous l’as restitué par ton travail.</l>
            </ab>

            <ab type="orig" xml:id="Sq576_Florentius_p1_5">
                <l>Atque ideo hic tragicae princeps dehinc esto Camoenae,</l>
                <l>Cognita quae nulli delituit domino.</l>
            </ab>
            <ab corresp="#Sq576_Florentius_p1_5" type="trad">
                <l>Et, pour cela, qu’advienne désormais ce prince de la camène tragique</l>
                <l>bien connue, qui ne s’est cachée derrière aucun maître.</l>
            </ab>

            <ab type="orig" xml:id="Sq576_Florentius_p1_6">
                <l>Quod si fato aliquo latebras Medea relinquet,</l>
                <l>Ingenio, <persName ref="#ou">Naso</persName>, nobilitata tuo,</l>
                <l>Illa tui ingenii partus, quae nunc quoque caelum</l>
                <l>Exhorret sceleris conscia forte sui :</l>
                <l>Arbitrii haec fuerit tum fas rescindere nostri</l>
                <l>Decreta, alterutri restituique gradum.</l>
            </ab>
            <ab corresp="#Sq576_Florentius_p1_6" type="trad">
                <l>Or si Médée abandonne ses cachettes par quelque hasard,</l>
                <l>rendue fameuse par ton génie, Ovide,</l>
                <l>fruit de ton génie, qui, peut-être consciente de son crime,</l>
                <l>abhorre aussi le ciel à présent :</l>
                <l>il sera alors permis à notre libre-arbitre de déchirer ces décrets</l>
                <l>et de rendre à l’un ou l’autre son grade.</l>
            </ab>

            <ab type="orig" xml:id="Sq576_Florentius_p1_7">
                <l>Tu interea praestans animi, noua gloria saecli</l>
                <l>Nostri, hac mortaleis perge iuuare uia.</l>
            </ab>
            <ab corresp="#Sq576_Florentius_p1_7" type="trad">
                <l>Toi entretemps, esprit supérieur,</l><l>gloire nouvelle</l><l>de notre siècle,
                    continue d’aider les mortels dans cette voie. </l>
            </ab>

            <ab type="orig" xml:id="Sq576_Florentius_p1_8">
                <l>Nam tibi non dederunt superi, sine pectore, corpus,</l>
                <l>Diuitias parca non tribuere manu.</l>
            </ab>
            <ab corresp="#Sq576_Florentius_p1_8" type="trad">
                <l>Car les dieux d’en haut ne t’ont pas donné de corps sans cœur,</l>
                <l>t’ont attribué des richesses d’une main prodigue.</l>
            </ab>

            <ab type="orig" xml:id="Sq576_Florentius_p1_9">
                <l>Ergo age, praeclaras artes monimentaque, patrum</l>
                <l>E tenebris effer qua potes unus ope.</l>
            </ab>
            <ab corresp="#Sq576_Florentius_p1_9" type="trad">
                <l>Donc, allez, porte ces arts illustres et les monuments de tes pères</l>
                <l>hors des ténèbres, par un pouvoir dont seul tu es capable.</l>
            </ab>

            <ab type="orig" xml:id="Sq576_Florentius_p1_10">
                <l>Non unquam te his auxilio uenisse pigebit,</l>
                <l>Non facilem medicas adplicuisse manus.</l>
            </ab>
            <ab corresp="#Sq576_Florentius_p1_10" type="trad">
                <l>Tu ne regretteras jamais de leur avoir porté secours,</l><l>Ni de leur avoir
                    appliqué, dans ta bonhomie, tes mains de médecin. </l>
            </ab>

            <ab type="orig" xml:id="Sq576_Florentius_p1_11">
                <l>Officiis nam obstricta tuis tua nomina tradet</l>
                <l>Posteritas saeclis innumer abilibus.</l>
            </ab>
            <ab corresp="#Sq576_Florentius_p1_11" type="trad">
                <l>La postérité transmet en effet tes noms attachés à tes offices</l>
                <l>pour des siècles innombrables.</l>
            </ab>


        </body>
    </text>
</TEI>
