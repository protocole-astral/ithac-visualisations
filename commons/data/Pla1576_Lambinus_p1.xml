<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">

  <teiHeader>

    <fileDesc>

      <titleStmt>

        <title xml:lang="la">Clarissimo et eruditissimo viro Germano Valenti Guellio PP. Senatori
          amplissimo, Germanus Lambinus Salutem dat.</title>
        <author ref="#lambinus_germanus">Germanus Lambinus</author>

        <respStmt>
          <resp>Modélisation et structuration</resp>
          <persName>Elisabeth GRESLOU</persName>
          <persName>Elysabeth HUE-GAY</persName>
        </respStmt>
        <respStmt>
          <resp>Transcription</resp>
          <name>Mathieu FERRAND</name>
        </respStmt>
        <respStmt>
          <resp>Traduction</resp>
          <name>Mathieu FERRAND</name>
        </respStmt>
        <respStmt>
          <resp>Encodage</resp>
          <name>Dalia DHAR</name>
          <name>Sarah GAUCHER</name>
        </respStmt>
        <principal>Malika BASTIN-HAMMOU</principal>
      </titleStmt>

      <publicationStmt>
        <publisher/>
        <availability>
          <p>Le site est en accès restreint</p>
        </availability>
        <pubPlace>ithac.elan-numerique.fr</pubPlace>
      </publicationStmt>
      <sourceDesc>

        <bibl>
          <title>M. Accius Plautus ex fide atque auctoritate complurium librorum manuscriptorum
            opera Dionys. Lambini Monstroliensis emendatus, ab eodemque commentariis explicatus et
            nunc primum in lucem editus. Adjecta sunt Plautina loca ex antiquis grammaticis
            collecta, et ex commentario antiquarum lectionum Iusti Lipsii multorum Plauti locorum
            illustrationes et emendationes. Additi quoque sunt duo indices copiosissimi prior
            verborum, locutionum et sententiarum, posterior eorum quae commentariis D. Lambini
            continentur</title>
          <author ref="#pl">Plautus</author>
          <pubPlace ref="#lutetia">Lutetia</pubPlace>
          <publisher ref="#albus_ioannes">Jean le Blanc</publisher><publisher ref="#macaeus_ioannes"
            >Joannis Macaeus (Jean Macé)</publisher>
          <date when="1576">octobre 1576</date>
          <editor ref="#lambinus_dionysus">Denis Lambin</editor>
          <editor ref="#helias_iacobus">Jacques Hélie</editor>
          <editor ref="#lambinus_germanus">Germain Lambin</editor>
          <ref target="https://opac.museogalileo.it/imss/resource?uri=000000395500"
            >https://opac.museogalileo.it/imss/resource?uri=000000395500</ref>
        </bibl>
        <listBibl>
          <bibl><author>Astrid Quillien</author>, <title level="a">« Lambin, Denis - Lambinus,
              Dionysius (1519-1572) »</title>, notice bio-bibliographique de l’ANR « Renaissance
            d’Horace », en ligne : <ref
              target="http://www.univ-paris3.fr/index-des-commentateurs-l-z-361031.kjsp?RH=1280408376821"
              >http://www.univ-paris3.fr/index-des-commentateurs-l-z-361031.kjsp?RH=1280408376821</ref>.</bibl>

          <bibl><author>Jacques Chomarat</author>, <title level="m">Prosateurs latin en France à la
              Renaissance</title>,<pubPlace>Paris</pubPlace>, <publisher>PUPS</publisher>,
              <date>1987</date>, <biblScope unit="page">p. 445-449</biblScope>.</bibl>
          <bibl><author>Astrid Quillien</author>, <title level="u">Denis Lambin dans la tourmente de
                l’<foreign xml:lang="la">aetas horatiana</foreign></title>, Mémoire de Maîtrise sous
            la direction de P. Galand, université de Paris IV-Sorbonne, <date>2001</date>.</bibl>
          <bibl><author>Jean-Eude Girot</author>, <hi rend="italic">s. v.</hi> <title level="a">«
              Denis Lambin »</title>, dans <title level="m">Dictionnaire des lettres françaises, Le
                XVI<hi rend="sup">e</hi> siècle</title>, dir. <editor>Georges Grente</editor>,
              <publisher>Paris</publisher>, <publisher>Fayard</publisher>, <date>2001</date>.</bibl>
          <bibl><author>Denys Lambin</author>, <title level="m">Lettres galantes de Denys Lambin,
              1552-1554</title>, texte établi, trad. et annoté par <editor>H. Potez</editor>,
              <pubPlace>Paris</pubPlace>, <publisher>Vrin</publisher>, <date>1941</date>.</bibl>
          <bibl><author>Henri Potez</author>, <title level="a">« La jeunesse de Denis Lambin
              (1519-1548) »</title>, <title level="j">Revue d'histoire littéraire de la
              France</title>, <biblScope unit="volume">9</biblScope>, <date>1902</date>, <biblScope
              unit="page">p. 385-413</biblScope>.</bibl>
          <bibl><author>Astrid Quillien</author>, « Les <title>Orationes</title> de Denis Lambin. La
            défense du grec dans l’<title>Oratio de utilitate linguae graecae et recta graecorum
              latine interpretandorum ratione</title> (22 octobre 1572) », dans <title>Philosophie,
              philologie et poétique de l’Antiquité à la Renaissance</title>, éd.
              <editor>P. Galand</editor> et <editor>C. Lévy</editor>, <title level="j"
              >Camenae</title>, <biblScope unit="volume">1</biblScope>, <date>2007</date>, en
            ligne : <ref target="http://www.paris4.sorbonne.fr/fr/rubrique.php3?id_rubrique=1761"
              >http://www.paris4.sorbonne.fr/fr/rubrique.php3?id_rubrique=1761</ref>.</bibl>
          <bibl><author>Astrid Quillien</author>, <title level="u">Denis Lambin lecteur, traducteur
              royal et défenseur du grec. Étude et traduction annotée de l’<foreign xml:lang="la"
                >oratio</foreign>
              <title>De utilitate linguae Graecae et recta Graecorum Latine interpretandorum
                ratione</title> (22 octobre 1571)</title>, Mémoire de D.E.A. sous la direction de
            P. Galand, université de Paris IV-Sorbonne, <date>2005</date>.</bibl>
          <bibl><author>Linton C. Stevens</author>, <title level="a">« Denis Lambin : Humanist,
              Courtier, Philologist, and Lecteur Royal »</title>, <title level="j">Studies in the
              Renaissance</title>, <biblScope unit="volume">9</biblScope>, <date>1962</date>,
              <biblScope unit="page">p. 234-241</biblScope></bibl>

        </listBibl>
      </sourceDesc>
    </fileDesc>

    <profileDesc>

      <abstract>
        <p>À la fin de sa vie, Denis Lambin entreprend de recenser les comédies de Plaute, mais il
          n’a pu examiner que treize d’entre elles. Son travail est repris par son ami Jacques
          Hélie, professeur de grec au Collège royal, qui termine la recension, ajoute des index et
          publie le tout, avec diverses pièces liminaires. L’ouvrage s’ouvre sur une lettre du fils
          de Lambin à Germain Vaillant de Guelis, poète néo-latin proche de la Pléiade, commentateur
          de Virgile (1575) et conseiller au Parlement de Paris, sous les auspices duquel il place
          le travail de son père.</p>
      </abstract>

      <langUsage>
        <language ident="fr">Français</language>
        <language ident="la">Latin</language>
        <language ident="grc">Grec</language>
      </langUsage>

    </profileDesc>

    <revisionDesc>
      <change when="2022-02-28">Sarah GAUCHER : identifiants Header, mise en conformité
        Elan</change>
      <change when="2020-03-19">Dalia DHAR : Encodage de la transcription et de la
        traduction</change>
    </revisionDesc>

  </teiHeader>

  <text>
    <body ana="#LI">

      <head type="orig" xml:id="Pla1576_Lambinus_p1_0">Clarissimo et eruditissimo uiro <persName
          ref="#guellius_germanus" role="destinataire">Germano Valenti Guellio</persName> PP.
        Senatori amplissimo, <persName ref="#lambinus_germanus">Germanus Lambinus</persName> Salutem
        dat.</head>
      <head type="trad" corresp="#Pla1576_Lambinus_p1_0">Au très illustre et très érudit
          <seg>Germain Vaillant de Guelis<note> Germain Vaillant de Guelis, abbé de Paimpont
            (diocèse de Saint-Malo), poète néo-latin proche de la Pléiade, commentateur de Virgile
              (<title>P. Virgilii Maronis, Opera, et in eum commentationes et Paralipomena Germani
              Valentis Guellii. Eiusdem Virgilii appendix cum Iosephi Scaligeri commentariis et
              castigationibus</title>, Anvers, Christophe Plantin, 1575), fut chanoine de
            Notre-Dame, conseiller au Parlement de Paris à partir de 1557 puis évêque d’Orléans
            (1586-1587). Voir notamment la notice biographique dans les <title>Mémoires historiques,
              critiques et littéraires, par feu M. Bruys</title>, t. II, Paris, J.-T. Hérissant,
            1771, p. 261-262.</note></seg>, sénateur très éminent, Germain Lambin adresse son
        salut</head>

      <ab type="orig" xml:id="Pla1576_Lambinus_p1_1">Non id ago (uir clarissime) ut hac mea
        qualicumque epistola tantillum splendoris et ornamenti paternis uigiliis accedat.</ab>
      <ab type="trad" corresp="#Pla1576_Lambinus_p1_1" ana="#PA">Je n’écris pas, homme très
        illustre, cette lettre, quelle qu’elle soit, pour ajouter le moindre éclat de lumière ni la
        moindre parure aux travaux nocturnes de mon père.</ab>

      <ab type="orig" xml:id="Pla1576_Lambinus_p1_2">Vt enim hactenus in lucem emissae, sic et hae
        satis per se sufficient ad sui commendationem et gloriam.</ab>
      <ab type="trad" corresp="#Pla1576_Lambinus_p1_2" ana="#PA">Comme, en effet, ces travaux
        nocturnes ont jusqu’à maintenant été publiés au grand jour, de même aussi ils suffiront par
        eux-mêmes à le recommander et à le couvrir de gloire.</ab>

      <ab type="orig" xml:id="Pla1576_Lambinus_p1_3">Tum quod is non sum, cui, uel aetas, uel Dei
        specialis quidam fauor, aliquid nominis inter aequales, ne dicam eruditos, attulerit ;</ab>
      <ab type="trad" corresp="#Pla1576_Lambinus_p1_3" ana="#PA">Et puis, je ne suis pas de ceux à
        qui ou bien l’âge, ou bien quelque faveur spéciale de Dieu ont accordé du renom parmi leurs
        pairs, pour ne pas dire parmi les savants ;</ab>

      <ab type="orig" xml:id="Pla1576_Lambinus_p1_4">ac si quid nunc possem, vererer adhuc, ne ea in
        re nimis suspectum esset domesticum testimonium.</ab>
      <ab type="trad" corresp="#Pla1576_Lambinus_p1_4" ana="#PA">et quand bien même cela serait, je
        craindrais que, en la matière, mon témoignage ne passe trop pour celui d’un proche.</ab>

      <ab type="orig" xml:id="Pla1576_Lambinus_p1_5">Quinimo ut intelligas, me, si non doctrinae,
        laboris, et tolerantiae filium imitatorem relictum, observantiae tamen et amoris in te
        certissimum esse monimentum.</ab>
      <ab type="trad" corresp="#Pla1576_Lambinus_p1_5" ana="#PA">J’écris cette lettre, en revanche,
        pour que tu saches bien que, si je ne suis pas, comme fils, le digne imitateur de sa
        science, de son travail et de sa constance, je perpétue cependant le très sûr souvenir de sa
        déférence et de son amitié pour toi.</ab>

      <ab type="orig" xml:id="Pla1576_Lambinus_p1_6">Igitur quae patri, etiam in ipso morbo qui
        uitae diem extremum attulit, in <persName ref="#pl">Plauti</persName> comoedias meditari
        licuit, ea, inquam, in agone apud matrem deposita, tibique forte iampridem consecrata,
        dicare uolui, ut quantum in me esset, uel uota defuncti persoluerem, uel illius saltem
        optatis respondere conarer.</ab>
      <ab type="trad" corresp="#Pla1576_Lambinus_p1_6" ana="#PA">C’est pourquoi, des réflexions que,
        au milieu même de la maladie qui l’a conduit au terme de sa vie, il fut permis à mon père de
        concevoir à propos des comédies de Plaute, de ces réflexions, dis-je, qu’il a laissées chez
        ma mère, au beau milieu du combat, comme il se trouvait que depuis longtemps déjà il te les
        avait adressées, j’ai voulu faire de toi le dédicataire de telle sorte que, autant qu’il
        était en mon pouvoir, je puisse exaucer le vœu du défunt, ou, tout au moins, essayer de
        répondre à son souhait.</ab>

      <ab type="orig" xml:id="Pla1576_Lambinus_p1_7">Nec enim me latet qua benevolentia, non ut
        litteratos caeteros, sed fraterna pene sis eum prosequutus et uicissim, quo in pretio et
        honore, tum propter tuam singularem doctrinam, tum aequi obseruantiam, apud eum fueris ut
        coniectare liceat, si eum Deus et mihi et Reipublicae diutius seruasset, nonnihil tuis
        auspiciis tentaturum fuisse. </ab>
      <ab type="trad" corresp="#Pla1576_Lambinus_p1_7" ana="#PA">En effet, je n’ignore pas de quelle
        bienveillance tu l’as entouré – non point de celle dont tu entoures les autres savants, mais
        d’une bienveillance presque fraternelle – et en retour, quelle estime et quelle
        considération, aussi bien pour ta culture singulière que pour ton amour de l’équité, il
        avait pour toi. Si bien qu’il est possible d’imaginer l’œuvre qu’il allait accomplir sous
        tes auspices, si Dieu l’avait maintenu plus longtemps en vie, pour mon bonheur et celui de
        l'état.</ab>

      <ab type="orig" xml:id="Pla1576_Lambinus_p1_8">Accedit et illud quoddam quasi necessitatis
        uinculum, ut non immerito laborum patris mei frustus consequaris, qui postquam e fonte sacro
        me suscepisti, uices illius quodammodo geras.</ab>
      <ab type="trad" corresp="#Pla1576_Lambinus_p1_8" ana="#PA">Les liens de l’amitié, comme une
        chaîne, imposent aussi que tu recueilles, à juste titre, le fruit des travaux de mon père,
        toi qui, après m'avoir tenu sur les fonts baptismaux, prends sa suite d'une certaine
        façon.</ab>

      <ab type="orig" xml:id="Pla1576_Lambinus_p1_9" ana="#PH">Debuerant autem, fateor, trita esse
        haec commentaria, uerum aliquandiu me sollicitum detinuit, communis utilitatis alioqui
        cupidum, desiderata imprimis, ut uidebatur, patris <foreign xml:lang="grc"
          >ἀκρίϐεια</foreign>, quam in aliis quibuscum libris euulgandis adhibere consueverat ;</ab>
      <ab type="trad" corresp="#Pla1576_Lambinus_p1_9">Or, je l'avoue, les commentaires que voici
        auraient dû être publiés – mais longtemps m'a retenu, inquiet et pourtant désireux d'être
        utile à tous, cette acribie de mon père, que l'on regrettait plus que tout, semblait-il, et
        dont il avait l'habitude de faire preuve lorsqu'il publiait un livre ;</ab>

      <ab type="orig" xml:id="Pla1576_Lambinus_p1_10">notae item ad postremas comoedias obiter et
        tamquam ab aliud agente conscriptae, quadamtenus a proposito reuocabant, donec uicerit et
        parentum auctoritas, et amicorum sincerius iudicium quibus uisum est nil illic detrahendum,
        nil quoque hic expetendum esse, satisque fuisse patri quod sibi ea in re temperarit, et si
        quid (ut morte praeventus fuerat) aut abundaret, aut deesset, non ideo temere quidquam de
        scriptis illius immutandum.</ab>
      <ab type="trad" corresp="#Pla1576_Lambinus_p1_10" ana="#PH">de même, ces notes sur les
        dernières comédies, composées à la hâte et comme en passant, étaient dans une certaine
        mesure un obstacle au projet, tant que l’emportèrent et le jugement autorisé des proches et
        l’avis plus sûr encore de ses amis, qui pensaient qu’ici, rien ne devait être enlevé, là
        rien ne devait être ajouté, qu’il avait suffi à mon père de s’être abstenu en la matière, et
        que si (dès lors qu’il avait été emporté par la mort) quelque chose était en trop, ou bien
        manquait, il ne fallait pas, pour cette raison, avoir l’audace de modifier quoi que ce fût
        dans ses écrits.</ab>

      <ab type="orig" xml:id="Pla1576_Lambinus_p1_11">Suberat tamen eiusmodi nouissima cogitatio,
        commentariis istis aeque ac caeco cuidam proprioque lumine orbato accidisse, cui iam nonnisi
        robustiori aliquo duce praeeunte iter ingredi tutum sit.</ab>
      <ab type="trad" corresp="#Pla1576_Lambinus_p1_11">Pourtant naquit une pensée toute nouvelle :
        à ces commentaires, il était arrivé la même chose qu’à un aveugle, privé de ses yeux, pour
        qui désormais il n’était pas sûr de marcher sans être précédé de quelque guide plus
        robuste.</ab>

      <ab type="orig" xml:id="Pla1576_Lambinus_p1_12">Aduertens autem posterioris cuiusdam aevi
          <title>Maronem</title> tuum grauitate et pondere praestantem ab omnibus digne fuisse
        exceptum, non dubitaui amplius quin, qua gratia et auctoritate apud praestantes
        clarissimosque uiros valet, eadem utique patrem ueteris latinitatis, etsi uulgatioris, non
        minus uenustae tamen et politae <persName ref="#pl">Plautum</persName> haberi faciat ;
        atque, si forsan neruorum debilitate titubet, erigere uel, quod est hoc tempore uerendum
        magis, si detrectantium morsibus appetatur, ab eis uindicare possit.</ab>
      <ab type="trad" corresp="#Pla1576_Lambinus_p1_12" ana="#PA">Or, me rendant compte que ton
          <title>Virgile</title>, dernièrement publié, se distinguant par son autorité et son poids,
        avait été dignement reçu par tous, je n’ai pas davantage douté que, par cette grâce et cette
        autorité qui le font prévaloir parmi les hommes les plus éminents et les plus célèbres, il
        ferait tenir Plaute, surtout, pour le père de la vieille latinité – qui, bien qu’assez
        populaire, n’en est pas moins charmante et élégante – et que, si par hasard Plaute
        chancelait à cause de la faiblesse de ses nerfs, Virgile pourrait le redresser ou bien, ce
        qu’il faut davantage craindre de nos jours, s’il s’offrait aux morsures des détracteurs, il
        pourrait l’en sauver.</ab>

      <ab type="orig" xml:id="Pla1576_Lambinus_p1_13">Viuat ergo, probetur, ametur, horum certe
        omnium pars maxima per me tibi concedetur, quem etiam pater, si superesset, maiori, credo,
        munere non grauatus decoraret.</ab>
      <ab type="trad" corresp="#Pla1576_Lambinus_p1_13" ana="#PA">Qu’il vive donc , qu’on l’approuve
        et qu’on l’aime, que la plus grande part, assurément, de tous ses détracteurs s’incline
        grâce à moi, devant toi, toi que mon père lui-même, s’il avait survécu, honorerait
        volontiers d’une faveur plus grande encore, je crois.</ab>

      <ab type="orig" xml:id="Pla1576_Lambinus_p1_14">Quod si minus quidpiam desiderio suo fuerim
        assecutus, id caeteris in rebus quibus et officium meum et dignitas tua postulabunt,
        compensare nitar.</ab>
      <ab type="trad" corresp="#Pla1576_Lambinus_p1_14" ana="#PA">Et si j’avais échoué à satisfaire
        son désir, je m’efforcerai d’y parvenir partout ailleurs où l’exigeront mon devoir et ton
        mérite.</ab>

      <ab type="orig" xml:id="Pla1576_Lambinus_p1_15">Vale. Lutetiae Parisiorum 17. Octobr.</ab>
      <ab type="trad" corresp="#Pla1576_Lambinus_p1_15">Salut. A Paris, le 17 octobre.</ab>

    </body>
  </text>
</TEI>
