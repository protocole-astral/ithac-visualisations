<?xml version="1.0" encoding="UTF-8"?>


<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">

    <teiHeader>

        <fileDesc>

            <titleStmt>

                <title xml:lang="la">Tranquilli Molossi Cremonensis Apologeticon.</title>
                <author ref="#passius_franciscus">Franciscus Passius Carpensis</author>

                <respStmt>
                    <resp>Modélisation et structuration</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Elysabeth HUE-GAY</persName>
                </respStmt>
                <respStmt>
                    <resp>Encodage</resp>
                    <name>Diandra CRISTACHE</name>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <principal>Malika BASTIN-HAMMOU</principal>
            </titleStmt>

            <publicationStmt>
                <publisher/>
                <availability>
                    <p>Le site est en accès restreint</p>
                </availability>
                <pubPlace>ithac.elan-numerique.fr</pubPlace>
            </publicationStmt>

            <sourceDesc>

                <bibl>
                    <title>Plutus Antiqua Comoedia ex Aristophane quae nuper in Linguam Latinam
                        translata est </title>
                    <author ref="#ar">Aristophanes</author>
                    <pubPlace ref="#parma">Parmae</pubPlace>
                    <publisher ref="#ugoletus_angelus">Angelus Vgoletus</publisher>
                    <date when="1501">1501</date>
                    <editor ref="#passius_franciscus">Franciscus Passius Carpensis</editor>
                </bibl>
            </sourceDesc>
        </fileDesc>

        <profileDesc>

            <abstract>
                <p>Liste des paratextes dans la même édition : <list>
                        <item>1. Illustrissimo ac eruitissimo Alberto Pio principi Carpensi :
                            ingeniorum fautori Franciscus Passius Carpensis domino suo Felicitatem.
                            D.</item>
                        <item>2. Antonii Zandemariae equitis hyerosolymitani Parmensis. ad
                            lectorem.</item>
                        <item>3. Tranquilli Molossi Cremonsis Apologeticon.</item>
                        <item>4. Argumentum</item>
                        <item>5. Francisci Passii Carpensis Plutus Comoedia Incipit. Prologus</item>
                    </list></p>

            </abstract>

            <langUsage>
                <language ident="la">Latin</language>
            </langUsage>

        </profileDesc>

        <revisionDesc>
            <change when="2022-03-03">Sarah GAUCHER : identifiants Header, mise en conformité
                Elan</change>
            <change when="2020-09-27">Diandra CRISTACHE : Encodage de la transcription et de la
                traduction</change>
        </revisionDesc>

    </teiHeader>

    <text>
        <body>

            <head type="orig" xml:id="Ar1501_Passius_p3_0"><persName>Tranquilli Molossi Cremonensis</persName>
                Apologeticon.</head>
            <head corresp="#Ar1501_Passius_p3_0" type="trad">Eloge de <seg>Tranquillus Molossus de
                    Crémone</seg><note>Baldassarre Malosso de Casalmaggiore, dit Tranquillus
                    Molossus, né à Casalmaggiore sur le Pô en 1466. Poète néolatin assez réputé, il
                    fut aussi le précepteur de Pierre Louis (Petrus Aloysius, Pierluigi) Farnese,
                    fils du cardinal (!) Alexandre Farnese, futur pape Paul III. </note></head>

            <ab type="orig" xml:id="Ar1501_Passius_p3_1"><l>Comoediam poeta uobis attulit :</l>
                <l>Veterem quidem : sed quae uidebitur noua</l>
                <l>Fortasse multis qui nihil tecte sciunt.</l>
                <l>Atque id reprehendunt maxime : quod <persName>Naeuium</persName></l>
                <l><persName>Plautum</persName> : <persName>Pacuuium</persName> : poetas optumos :</l>
                <l>Non est sequutus.quos non improbat quidem</l>
                <l>Sed quaerit : illis : hinc et inde : si licet</l>
                <l>Sententias : et mutuarios locos </l>
                <l>Ex commodo transferre : cur uitio sibi</l>
                <l>Vortant : latinam quod facit comoediam</l>
                <l><title>Plutum</title> : prius quae graeca fuerat integra ?</l>
                <l>Nam si facere furtum licet : cur mutuum</l>
                <l>Reddere sibi minus licet ? sed obsecro</l>
                <l>Vos ne putetis : non probare eum tot saeculis</l>
                <l>Ab omnibus iam comprobatos comicos</l>
                <l>Sed is negat sentire recte : qui secus.</l>
                <l>Comoedia est uetus : ueteremque is esse praedicat.</l>
                <l>Cui fabulae uos nunc operam quaeso date :</l>
                <l>Adeste cum silentio : si uiderit</l>
                <l>Plutus fauere uos sibi : non est metus</l>
                <l>Quin ilico omnes uos meros faciat Midas.</l>
                <l>Debemus autem gratias quammaximas</l>
                <l>Illuminatori quidem Aesculapio</l>
                <l>Primum : deinde Passio : sub quo auspice</l>
                <l>Loquitur latine Plutus : et bonis fauet.</l>
            </ab>
            <ab type="trad" corresp="#Ar1501_Passius_p3_1" ana="#T #TH_hist #TH_dram">Le poète vous a apporté une comédie,
                ancienne, certes, mais qui paraîtra nouvelle peut-être à ceux, nombreux, qui ne
                savent rien au fond. <seg>Et ils lui reprochent surtout de ne pas avoir suivi
                    Naevius, Plaute, Pacuvius, les meilleurs poètes. Ceux-là, ce n’est pas qu’il les
                    repousse, évidemment, mais il pose la question : si ces poètes, ici et là, ont
                    le droit de transposer tranquillement des sentences et des passages empruntés,
                    pourquoi lui reproche-t-on à lui de faire une comédie en latin du Plutus, qui
                    était déjà une pièce intégrale en grec ? Car si on a le droit de commettre un
                    larcin, pourquoi n’est-il pas permis de s'attribuer ce qu'on a emprunté
                        ?<note>Voir prologue de <title>L’Eunuque</title> de Térence sur querelles de
                        plagiat et d’emprunt, de transposition ou traduction du grec au latin,
                        liberté de création. Par exemple, sur le vol v.27-28 : <quote xml:lang="la"
                                ><l>Si id est peccatum, peccatum inprudentiast</l><l>poetae, non quo
                                furtum facere studuerit.</l></quote></note></seg> mais par pitié,
                n’allez pas penser que lui désapprouve des comiques approuvés par tous depuis tant
                de siècles ; non, mais il dit que n’a pas le sens commun celui qui pense
                autrement.</ab>
        </body>
    </text>
</TEI>
