<?xml version="1.0" encoding="UTF-8"?>


<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">

   <teiHeader>

      <fileDesc>

         <titleStmt>

            <title xml:lang="la">Imaginibus ac eruditione claro, D. Nicolao Amsdorffo, Theologo,
               Phil. Mel. s.d.</title>
            <author ref="#melanchtonus_philippus">Philip Melanchthon</author>

            <respStmt>
               <resp>Transcription</resp>
               <name>Christian NICOLAS</name>
            </respStmt>
            <respStmt>
               <resp>Traduction</resp>
               <name>Christian Nicolas</name>
            </respStmt>
            <respStmt>
               <resp>Encodage</resp>
               <name>Malika Bastin-Hammou</name>
               <name>Sarah GAUCHER</name>
            </respStmt>
            <respStmt>
               <resp>Commentaire</resp>
               <name>Malika Bastin-Hammou</name>
            </respStmt>
            <respStmt>
               <resp>Modélisation et structuration</resp>
               <persName>Elisabeth GRESLOU</persName>
               <persName>Elysabeth HUE-GAY</persName>
            </respStmt>
            <principal>Malika Bastin-Hammou</principal>
         </titleStmt>

         <publicationStmt>
            <publisher/>
            <availability>
               <p>Le site est en accès restreint</p>
            </availability>
            <pubPlace>ithac.elan-numerique.fr</pubPlace>
         </publicationStmt>

         <sourceDesc>
            <bibl>
               <title>Aristophanis poetae comici nubes</title>
               <author ref="#ar">Aristophanes</author>
               <pubPlace ref="#uitteberga">Vuittembergae</pubPlace>
               <publisher ref="#lotherus_melchior_f">Melchior Lotherus Iunior</publisher>
               <date when="1521">1521</date>
               <editor ref="#melanchtonus_philippus">Philip Melanchthon</editor>
            </bibl>
            <listBibl>
               <bibl/>
            </listBibl>
         </sourceDesc>

      </fileDesc>

      <profileDesc>

         <abstract>
            <p>C’est à Melanchthon (1497-1560) que l’on doit la première édition imprimée séparée
               des Nuées. En 1521, le théologien est âgé de vingt-quatre ans ; d’abord professeur de
               grec à Wittemberg, il est devenu depuis 1520 professeur de théologie et s’implique
               dans les controverses de son temps ; il défend notamment Luther, la « vraie
               philosophie » et le « vrai christianisme ». Sa pratique de l’enseignement du grec
               conjuguée à son intérêt pour la théologie, et notamment de Paul, expliquent sans
               doute le choix de cette édition d’Aristophane. Le poète comique fait partie des
               auteurs par lesquels il est recommandé de débuter l’étude du grec ; mais d’ordinaire,
               c’est le Ploutos qui a la préférence des professeurs. Dans l’étude des Nuées,
               Melanchthon voit l’occasion non seulement d’enseigner la langue aux apprentis
               hellénistes, mais de lire avec eux une critique violente de la philosophie. C’est
               l’argument qu’il développe dans l’adresse au théologien Nicholaus von Amsdorf
               (1583-1565) : les Anciens se méfiaient de la philosophie - Socrate lui-même avouait
               ne rien savoir. Dès lors, les contemporains qui s’y adonnent alors même qu’ils ont
               connaissance des textes sacrées s’égarent et Aristophane se trouve ainsi enrôlé, avec
               d’autres, dans ce que Melanchthon présente comme une condamnation de la philosophie
               par les Anciens.</p>
         </abstract>

         <langUsage>
            <language ident="fr">Français</language>
            <language ident="la">Latin</language>
            <language ident="gr">Grec</language>
         </langUsage>

      </profileDesc>

      <revisionDesc>
         <change when="2022-03-03">Sarah GAUCHER : identifiants Header, mise en conformité
            Elan</change>
         <change>090920</change>
      </revisionDesc>

   </teiHeader>

   <text>
      <body>

         <head type="orig" xml:id="Ar1521_Melanchthon_p1_0">Imaginibus ac eruditione claro, D.
            Nicolao Amsdorffo, Theologo, Phil. Mel. s.d.</head>

         <head corresp="#Ar1521_Melanchthon_p1_0" type="trad">À celui que ses images et son
            érudition ont rendu célèbre, M. Nicolas von Amsdorf, théologien, Philippe Melanchton
            donne son bonjour</head>

         <ab type="orig" xml:id="Ar1521_Melanchthon_p1_1">Quoties animo ueterum studia repeto, nullo
            non seculo uideo apud sapientissimos et ingeniosissimos quosque sapiendi genus, quod
            philosophiam uocant, male audisse, tum, quod id ad ciuilium rerum administrationem
            inutile putarint. </ab>
         <ab corresp="#Ar1521_Melanchthon_p1_1" type="trad">Chaque fois que je me replonge dans
            l’étude des Anciens, je m’aperçois que dans toutes les générations, parmi l’élite
            savante et intellectuelle, le genre de savoir qu’on appelle philosophie a eu mauvaise
            presse, tantôt au motif qu’ils la jugeaient inapte à la conduite des affaires publiques. </ab>

         <ab type="orig" xml:id="Ar1521_Melanchthon_p1_2">Quis enim fuerit in republica
            disputationum usus, de Ideis, de uacuo, de nubibus, deque aliis hoc genus nugalibus
            theorematis, tum quod perpetuis contentionibus et plus satis anxia, curiosaque
            minutiarum quarundam inquisitione, primum generosos, alioqui animos eneruari uiderint,
            deinde et ueri rationem adeo non expediri, ut philosophando magis etiam obscurata
            sit.</ab>
         <ab corresp="#Ar1521_Melanchthon_p1_2" type="trad">Car a-t-on jamais eu au niveau de l’État
            des discussions sur les idées, le vide, les nuages et autres théories fumeuses de ce
            genre ? -, tantôt parce que des débats continuels et une enquête particulièrement
            angoissante et troublante sur telle ou telle minuscule affaire rendaient d’une part des
            esprits par ailleurs bien faits mollassons et que le calcul de la vérité réussissait si
            peu que l’acte de philosopher le rendait encore plus obscur.<note>Condamnation de la
               réflexion platonicienne sur les idées et sur le vide : Melanchthon, <title>Didymi
                  Oratio</title>, CR, 1, 302 (voir Dino Bellucci, <title>Science de la nature et
                  Réformation. La physique au service de la Réforme dans l’enseignement de Philippe
                  Melanchthon</title>, Roma, Ed. Vivere in, 1998, p. 149, n.47 [Sophie
               Aubert-Baillot].</note>
         </ab>

         <ab type="orig" xml:id="Ar1521_Melanchthon_p1_3">Nam, philosophiam si propius contempleris,
            uideas nihil esse, nisi stultas friuolarum opinionum pugnas, cum rerum principia, hic
            atomos, ille Ideas fingit, hic infinitos mundos, ille subinde nouos mundos suborientes
            comminiscitur.</ab>
         <ab corresp="#Ar1521_Melanchthon_p1_3" type="trad">Car si on observait la philosophie de
            plus près, on verrait qu’elle n’est que sottes batailles d’opinions frivoles, puisque
            les principes des choses Untel les fait atomes, tel autre Idées ; tel imagine des mondes
            infinis, tel autre des mondes qui se régénèrent automatiquement ; </ab>

         <ab type="orig" xml:id="Ar1521_Melanchthon_p1_4">Hic bonorum finem in uoluptate, ille in
                  <seg>indolentia<note>Réminiscence de Cicéron, <title>de Finibus</title>, II, 19
                  dans sa critique de l’éthique épicurienne : Aristippe défend l’identification du
                  souverain bien au plaisir, Hiéronyme de Rhodes, à l’absence de douleur (<foreign
                     xml:lang="la">indolentia</foreign>). [Sophie Aubert-Baillot].</note></seg>, hic
            in falso uirtutis nomine, ille in gloria fastigium uitae collocat.</ab>
         <ab corresp="#Ar1521_Melanchthon_p1_4" type="trad">tel met le souverain bien dans le
            plaisir, tel autre dans l’ataraxie, tel englobe la vie suprême sous le faux nom de
            vertu, tel dans la gloire.</ab>

         <ab type="orig" xml:id="Ar1521_Melanchthon_p1_5">Hic deos esse negat ullos, ille somniat
            innumeros, huic nihil, illi omnia moueri uidentur.</ab>
         <ab corresp="#Ar1521_Melanchthon_p1_5" type="trad">Tel nie l’existence de tous les dieux,
            tel autre les rêves innombrables, pour lui aucun mouvement, pour tel autre tout semble
            en mouvement.</ab>

         <ab type="orig" xml:id="Ar1521_Melanchthon_p1_6">Quae opinionum confusio, recentiori
            Academiae in consilio fuit, cur nihil definire, nihil certo comprehendi posse
            doceret.</ab>
         <ab corresp="#Ar1521_Melanchthon_p1_6" type="trad">Ce fatras d’opinions, la Nouvelle
            Académie a eu le projet de l’interroger pour savoir pourquoi elle enseigne que rien ne
            peut se définir, rien être compris avec certitude.</ab>

         <ab type="orig" xml:id="Ar1521_Melanchthon_p1_7">Quod quam stultum dogma sit, quis non
            uidet ?</ab>
         <ab corresp="#Ar1521_Melanchthon_p1_7" type="trad">Qu’il s’agisse d’un dogme stupide, qui
            ne le voit ?</ab>

         <ab type="orig" xml:id="Ar1521_Melanchthon_p1_8">Quin ipse etiam Socrates, philosophiae, ut
            isti dicunt, uniuersae fons, fatetur nihil, praeter <seg xml:lang="grc">ἀμφισβητήσιμον
                  ὄναρ<note>L’expression est tirée du <title>Banquet</title> de Platon ; Socrate
                  s’adresse à Agathon et évoque son savoir, <quote xml:lang="grc">σοφία : ἡ μὲν  γὰρ
                     ἐμὴ ϕαύλη τις ἂν εἴη, ἢ καὶ ἀμϕισβητήσιμος ὥσπερ ὄναρ  οὖσα, ἡ δὲ σὴ λαμπρά τε
                     καὶ πολλὴν ἐπίδοσιν ἔχουσα, ἥ γε  παρὰ σοῦ νέου ὄντος οὕτω σϕόδρα ἐξέλαμψεν καὶ
                     ἐκϕανὴς  ἐγένετο πρῴην ἐν μάρτυσι τῶν ‘Ελλήνων πλέον ἢ τρισμυρίοις.</quote> «
                  Le savoir qui est le mien doit être peu de chose, voir quelque chose d’aussi
                  illusoire qu’un rêve, comparé au tient qui est brillant et qui a un grand avenir,
                  ce savoir qui, chez toi, a brillé avec un tel éclat dans ta jeunesse et qui, hier,
                  s’est manifesté en présence de plus de trente mille Grecs. » (Platon,
                     <title>Banquet</title>, 175e, traduction de Luc Brisson). </note></seg> esse
            sapientiam suam.</ab>
         <ab corresp="#Ar1521_Melanchthon_p1_8" type="trad">Et même Socrate, à leurs dires source de
            toute la philosophie, avoue que sa sagesse n’est que rêve douteux.</ab>

         <ab type="orig" xml:id="Ar1521_Melanchthon_p1_9">Quo magis nostrorum hominum uecordiam
            demiror, qui rem prophanam ad sacra, philosophiam admisere, atque ita, ut ad eam diuina
            etiam ausi sint exigere. </ab>
         <ab corresp="#Ar1521_Melanchthon_p1_9" type="trad">Je m’étonne d’autant plus de la folie de
            nos contemporains qui ont admis cette matière profane qu’est la philosophie à côté des
            matières sacrées au point même d’exiger d’elle du divin !</ab>

         <ab type="orig" xml:id="Ar1521_Melanchthon_p1_10">Sacris libris certae instituendae uitae
            leges, exactae uiciorum ac uirtutum formae, ratio certa cognoscendarum diuinarum rerum,
            et iudicandi humana omnia prodita est, hanc semel admissa philosophia ita conspurcauit,
            ut nusquam germana sacrorum facies adpareat.</ab>
         <ab corresp="#Ar1521_Melanchthon_p1_10" type="trad">C’est aux textes sacrés de décréter les
            lois de la vraie vie, les formes exactes du vice et de la vertu, c’est là qu’on trouve
            la méthode sûre pour reconnaître le divin et juger de toutes les choses humaines ; mais
            cette méthode, la philosophie une fois admise, l’a à ce point cochonnée que nulle part
            n’apparaît plus l’authentique visage du sacré.</ab>

         <ab type="orig" xml:id="Ar1521_Melanchthon_p1_11">Damnarunt illam maximorum regum, ac
            populorum aliquot leges, irriserunt et theatra, quod Crates, Diphilus, Aristophanes et
            alii testantur, adeo suum ueteribus inuentum displicuit. </ab>
         <ab corresp="#Ar1521_Melanchthon_p1_11" type="trad">Elle a été condamnée par les lois des
            plus grands rois et de quelques peuples, moquée par le théâtre, ce dont témoignent
            Cratès, Diphile, Aristophane et d’autres, tant son invention a déplu aux Anciens.</ab>

         <ab type="orig" xml:id="Ar1521_Melanchthon_p1_12">Nos impiam et nugacem amplectimur, quibus
            e coelo demissa est peculiaris sapiendi ratio, uelut <foreign xml:lang="grc"
               >ἔκτυπος</foreign> mentis diuinae.</ab>
         <ab corresp="#Ar1521_Melanchthon_p1_12" type="trad">Mais nous, nous nous en emparons avec
            son impiété et sa sottise, alors que nous avons reçu du ciel une forme spéciale de
            sagesse, sorte d’empreinte de l’esprit de Dieu.</ab>

         <ab type="orig" xml:id="Ar1521_Melanchthon_p1_13">Nam si propria animorum effigies est
            oratio, non dubium est, diuino sermone, diuinam mentem expressam esse. </ab>
         <ab corresp="#Ar1521_Melanchthon_p1_13" type="trad">Car s’il est vrai que l’image propre de
            l’âme c’est le langage, alors sans aucun doute la parole de Dieu exprime l’esprit de
            Dieu.</ab>

         <ab type="orig" xml:id="Ar1521_Melanchthon_p1_14">Iam olim diuus Paulus praeuidit rem
            Christianam philosophicis traditionibus labefactandam esse, quare cum alias, hominum
            doctrinas acer insectatur, tum ad Colossenses scribens, pleno ore ac palam cauendum
            praecipit, ne quis nos per philosophiam depredetur.</ab>
         <ab corresp="#Ar1521_Melanchthon_p1_14" type="trad">Déjà jadis saint Paul a prévenu que le
            christianisme devait être sapé par les écoles philosophiques, raison pour laquelle il
            combat avec acharnement les doctrines humaines et particulièrement quand dans la Lettre
            aux Colossiens il prescrit à pleine voix et explicitement de se garder d’être fait
            prisonnier par la philosophie .</ab>

         <ab type="orig" xml:id="Ar1521_Melanchthon_p1_15">Atque utinam ualuisset Apostoli oraculum,
            cum ueterum calculi, quibus miro consensu philosophia damnata est, contemnerentur.</ab>
         <ab corresp="#Ar1521_Melanchthon_p1_15" type="trad">Ah si l’oracle de l’apôtre avait
            prévalu, quand les cailloux des Anciens, qui par un admirable accord avaient condamné la
            philosophie, suscitaient la réprobation ! </ab>

         <ab type="orig" xml:id="Ar1521_Melanchthon_p1_16">Porro, ne ignoret iuuentus nostra, quo
            illam loco habuerit uetustas, uisum est mihi Aristophanis <title>Nubes</title>, quibus
            Socrates exagitatur, publicare et enarrare.</ab>
         <ab corresp="#Ar1521_Melanchthon_p1_16" type="trad">De plus, pour que notre jeunesse
            n’ignore pas en quelle mésestime l’Antiquité la tenait, il m’a paru bon de publier et
            commenter <title>Les Nuées</title> d’Aristophane, celles qui harcèlent Socrate. </ab>

         <ab type="orig" xml:id="Ar1521_Melanchthon_p1_17">Eas tui nominis auspicio in lucem
            edimus.quod, ut es acerrimo in literis tractandis iuditio, ita efflorescentibus
            melioribus studiis unice faues.</ab>
         <ab corresp="#Ar1521_Melanchthon_p1_17" type="trad">C’est sous l’auspice de ton nom que je
            les livre au jour parce que de même que tu as le goût le plus sûr dans la pratique des
            lettres, de même tu protèges de façon unique le fleurissement des hautes études.</ab>

         <ab type="orig" xml:id="Ar1521_Melanchthon_p1_18">Vale, Vuittembergae. Anno, M.D.XX.</ab>
         <ab corresp="#Ar1521_Melanchthon_p1_18" type="trad">Porte-toi bien. Wittenberg, en l'an
            1520</ab>

      </body>
   </text>
</TEI>
