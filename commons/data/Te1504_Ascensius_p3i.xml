<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">

    <teiHeader>

        <fileDesc>

            <titleStmt>

                <title xml:lang="la">De scenis et prosceniis. Capitulum IX.</title>
                <!-- Indiquer la langue du paratexte dans l'attribut xml:lang : "la" pour le latin et "grc" pour le grec -->
                <!-- Attention ! ne pas faire de retour charriot dans le titre -->
                <author ref="#ascensius_iodocus">Jodocus Badius Ascensius</author>

                <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence en minuscules, précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->

                <!-- Déclarations de responsabilité : indiquent qui a effectué quelle tâche -->
                <!-- Ajouter autant de tâches (<respStmt>) que nécessaire -->
                <respStmt>
                    <resp>Transcription</resp>
                    <name>Bastien Vallet</name>
                    <name>Églantine DERU</name>
                </respStmt>
                <respStmt>
                    <resp>Traduction</resp>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <respStmt>
                    <resp>Révision</resp>
                    <name>Laure HERMAND</name>
                </respStmt>
                <respStmt>
                    <resp>Encodage</resp>
                    <name>Diandra CRISTACHE</name>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <respStmt>
                    <resp>Commentaire</resp>
                    <name>Laure HERMAND</name>
                </respStmt>
                <!-- Attention ! Ne pas modifier les tâches ci-dessous -->
                <respStmt>
                    <resp>Modélisation et structuration</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Elysabeth HUE-GAY</persName>
                </respStmt>
                <respStmt>
                    <resp>Première version du modèle d'encodage</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Anne GARCIA-FERNANDEZ</persName>
                </respStmt>
                <principal>Malika BASTIN-HAMMOU</principal>
            </titleStmt>

            <!-- Informations de publication sur le laboratoire numérique du projet. Ne pas modifier-->
            <publicationStmt>
                <publisher/>
                <availability>
                    <p>Le site est en accès restreint</p>
                </availability>
                <pubPlace>ithac.elan-numerique.fr</pubPlace>
            </publicationStmt>

            <!-- Informations bibliographiques sur l'édition ancienne d'où le paratexte est tiré -->
            <sourceDesc>
                <bibl>
                    <title>P. Terentii aphri comicorum elegantissimi Comedie a Guidone Juvenale viro
                        perquam litterato familiariter explanate : et ab Jodoco Badio Ascensio vna
                        cum explanationibus rursum annotate atque recognite : cumque eiusdem
                        Ascensii praenotamentis atque annotamentis suis locis adhibitis quam
                        accuratissime impresse venundantur</title>
                    <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->
                    <author ref="#ter">Terentius</author>
                    <pubPlace ref="#londinium">Londonii</pubPlace>
                    <pubPlace ref="#lutetia">Lutetiae</pubPlace>
                    <publisher ref="#worda_winandus">Winandus de Worda</publisher>
                    <publisher ref="#morinus_michael">Michael Morinus</publisher>
                    <publisher ref="#brachius_ioannes"> Johannes Brachius</publisher>
                    <publisher ref="#ascensius_iodocus">Jodocus Badius Ascensius</publisher>
                    <date when="1504">1504</date>
                    <!-- dans l'attribut @when, l'année en chiffre arabes -->
                    <editor ref="#ascensius_iodocus">Jodocus Badius Ascensius</editor>
                </bibl>

                <listBibl>
                    <bibl>Philippe Renouard, Bibliographie des impressions et des œuvres de Josse
                        Badius Ascensius, Paris, E. Paul et fils et Guillemin, 1908, 3 vol.</bibl>
                    <bibl>M. Lebel, Les préfaces de Josse Bade (1462-1535) humaniste,
                        éditeur-imprimeur et préfacier, Louvain, Peeters, 1988</bibl>
                    <bibl>Paul White, Jodocus Badius Ascensius. Commentary, Commerce and Print in
                        the Renaissance, Oxford University Press, 2013</bibl>
                    <bibl>L. Katz, La presse et les lettres. Les épîtres paratextuelles et le projet
                        éditorial de l’imprimeur Josse Bade (c. 1462-1535), thèse de doctorat
                        soutenue à l’EPHE sous la direction de Perrine Galand, 2013</bibl>
                    <bibl>G. Torello-Hill et A.J. Turner, The Lyon Terence. Its Tradition and
                        Legacy, Leiden/Boston, Brill, 2020</bibl>
                </listBibl>

            </sourceDesc>
        </fileDesc>

        <profileDesc>

            <abstract>
                <p>Josse Bade ouvre son commentaire aux comédies de Térence par une longue
                    introduction qu’il nomme lui-même Praenotamenta. Il s’agit de « notes
                    préliminaires » plutôt que d’une « préface » à proprement parler. </p>
                <p>Cette section est composée de 26 chapitres qui forment un traité de poétique en
                    miniature auxquels il adjoint une série de remarques préliminaires sur le
                    prologue et la première scène de l’Andrienne. L’humaniste y développe une ample
                    réflexion sur la comédie. Partant d’une définition de la poésie, Bade aborde
                    ensuite l’origine de la comédie, ses caractéristiques et mène une longue
                    réflexion sur la scénographie antique. Les deux derniers chapitres traitent de
                    la vie et des œuvres de Térence.</p>

            </abstract>

            <langUsage>
                <!-- ne pas modifier -->
                <language ident="fr">Français</language>
                <language ident="la">Latin</language>
                <language ident="grc">Grec</language>
            </langUsage>

        </profileDesc>
        <revisionDesc>
            <change when="2022-04-08">Sarah GAUCHER : correction de la transcription, encodage de la
                tranduction, conformité, identifiants Header</change>
        </revisionDesc>
    </teiHeader>

    <text>
        <body ana="#TH_hist #TH_repr">

            <!-- RAPPEL : on encode le texte latin ou grec; on n'encode dans le texte de la traduction que les notes d'éditeur (=membres du projet) -->

            <!-- Le couple d'éléments <head> contient le titre ou l'incipit du paratexte -->
            <!-- L'identifiant du TITRE est composé du titre de la fiche TT suivi d'un underscore (_) puis du chiffre 0 -->
            <head type="orig" xml:id="Te1504_Ascensius_p3i_0">De scaenis et prosceniis. Capitulum
                IX.</head>
            <!-- Le deuxième élément du couple contient la traduction française du titre ou incipit -->
            <!-- L'identifiant de la traduction est le même que celui du latin, précédé d'un hashtag (#) -->
            <head corresp="#Te1504_Ascensius_p3i_0" type="trad">Chapitre 9. Sur les murs de scène et
                les scènes.</head>

            <ab type="orig" xml:id="Te1504_Ascensius_p3i_1">Intra igitur theatrum ab una parte
                opposita spectatoribus erant scaenae et proscaenia, id est loca lusoria ante scaenas
                facta.</ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3i_1">À l’intérieur du théâtre se trouvaient
                donc, dans une partie opposée aux spectateurs, le mur de scène et la scène,
                c’est-à-dire un lieu de jeu se trouvant devant le mur de scène.</ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3i_2">Scaenae autem erant quaedam umbracula
                seu absconsoria in quibus abscondebant lusores donec exire deberent ; ante autem
                scaenas erant quaedam tabulata in quibus personae quae exierant ludebant.</ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3i_2">Quant aux murs de scène, c’étaient des
                abris ou des cachettes où les acteurs se cachaient jusqu’à ce qui leur faille sortir
                ; d’autre part, devant les murs de scène, il y avait des estrades où jouaient les
                personnages qui étaient sortis.</ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3i_3">Scaenas autem quae triplices sunt :
                tragicae, comicae et satyrae. </ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3i_3">Il y a trois sortes de scènes :
                tragiques, comiques et satyriques.</ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3i_4"><persName>Vitruuius</persName> in libro
                quem de architectura composuit ita distinguit ut <seg type="paraphrase">tragicas
                    scaenas columnis et fastigiis et signis regalibus ornandas praecipiat, propterea
                    quod regum gesta ante illas agebantur, comicas autem ad similitudinem domorum
                    priuatarum ut ciuium et communium personarum conficiendas, satyricas uero
                    priscas in quarum proscaeniis satyri ludebant speluncis et montibus arboribusque
                    ornandas propterea quod satyri qui deinde prodibant dii siluestres sunt et in
                    huius modi locis uersantur<bibl resp="#SG"><author ref="#uitr">Vitr.</author>,
                            <title ref="#uitr_arch">De architectura</title>
                        <biblScope>5.6.9.</biblScope><note>Les textes de Bade et de Vitruve
                            s'entremêlent ici.</note></bibl></seg>.</ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3i_4">Vitruve, dans le livre qu’il a composé
                sur l’architecture, les distingue en recommandant d’orner les murs de scène
                tragiques de colonnes, de corniches et de statues de rois, parce qu’on jouait devant
                elles les gestes des rois ; d’aménager les murs de scène comiques pour les faire
                ressembler à des demeures privées, comme celles des citoyens et des gens du commun ;
                d’orner les scènes satyriques anciennes, sur les avant-scènes desquels jouaient des
                satyres, de grottes, de montagnes et d’arbres, parce que les satyres qui y
                évoluaient sont des dieux sylvestres et parce qu’ils habitent dans des endroits de
                ce genre.</ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3i_5">Vt uero <persName ref="#donat"
                    >Donatus</persName> auctor est, <seg type="paraphrase">iuxta proscaenia altaria
                    duo fiebant : alterum Liberi patris, alterum Phoebi, propterea quod ut dixi
                    principium omnium dramaticorum ex Liberi sacrificiis ortum est.<bibl resp="#SG"
                            ><author ref="#euant
                    ">Evanthius</author>, <title
                            ref="#euant_fab">De fabula</title>, <biblScope>8
                    3.</biblScope></bibl></seg></ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3i_5">Comme le dit Donat, on érigeait près
                de la scène deux autels, le premier pour Liber père, le second pour Phébus, parce
                que, comme je l’ai dit, l’origine de toutes les œuvres dramatiques est née des
                sacrifices de Liber. </ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3i_6">Phoebus autem poetarum omnium princeps
                et protector est.</ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3i_6">Quant à Phébus, il est le prince et le
                protecteur de tous les poètes.</ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3i_7">Et, ut idem <persName ref="#donat"
                    >Donatus</persName> auctor est, <seg type="paraphrase">comoedia praecipue in
                    honorem Apollinis Nomius, hoc est pastoris inuenta est<bibl resp="#SG"><author
                            ref="#euant
                                ">Evanthius</author>, <title
                            ref="#euant_fab">De fabula</title>,
                    <biblScope>1.2.</biblScope></bibl></seg>.</ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3i_7">Et, comme le dit le même Donat, la
                comédie a été inventée en premier lieu en l’honneur d’Apollon Nomios, c’est-à-dire
                berger.</ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3i_8"><cit type="refauteur">
                    <quote>Nondum enim<seg type="incise">, inquit,</seg> coactis in urbem
                        Atheniensibus cum Apollini Romio id est pastorum deo constructis aris in
                        honorem diuine rei, circum attice regionis picos uillas et pagos et compita
                        festiuum carmen solemniter cantarent, orta est comedia.</quote>
                    <bibl resp="#SG"><author ref="#euant
                            "
                            >Evanthius</author>, <title ref="#euant_fab">De fabula</title>,
                            <biblScope>1.2.</biblScope><note>Le texte de Bade comporte cependant des
                            différences avec le texte de Donat : <foreign xml:lang="la">uero nondum
                                coactis in urbem Atheniensibus, cum Apollini <foreign xml:lang="grc"
                                    >Νομίῳ uel Ἀγυιαίῳ</foreign>, id est pastorum uicorum ue
                                praesidi deo, instructis aris in honorem diuinae rei circum Atticae
                                uicos uillas pagos et compita festiuum carmen sollemniter
                                cantaretur, <foreign xml:lang="grc">ἀπὸ τῶν κωμῶν καὶ τῆς
                                    ᾠδῆς</foreign> comoedia uocitata est</foreign>
                        </note></bibl>
                </cit></ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3i_8">« La comédie est né avant le
                rassemblement des Athéniens en une ville, alors que pour honorer Apollon Nomios,
                c’est-à-dire le dieu des bergers, après avoir monté des autels en l'honneur du dieu
                pour qui l'on sacrifiait partout à l'entour des bourgs, des fermes, des villages et
                des carrefours d'Attique, on chantait solennellement un hymne cérémoniel. »</ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3i_9">Sed haec hactenus.</ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3i_9">Mais c’est assez pour l’instant.</ab>

        </body>
    </text>
</TEI>
