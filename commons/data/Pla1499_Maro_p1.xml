<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>

    <fileDesc>

      <titleStmt>

        <title xml:lang="la">Andreae Maronis Brisiensis epigramma</title>
        <author ref="#maro_andrea">Andrea Maro Brisiensis</author>

        <respStmt>
          <resp>Transcription</resp>
          <name>Mathieu FERRAND</name>
        </respStmt>

        <respStmt>
          <resp>Traduction</resp>
          <name>Mathieu FERRAND</name>
        </respStmt>

        <respStmt>
          <resp>Encodage</resp>
          <name>Bérénice JARROSSAY</name>
          <name>Sarah GAUCHER</name>
        </respStmt>

        <respStmt>
          <resp>Modélisation et structuration</resp>
          <persName>Elisabeth GRESLOU</persName>
          <persName>Elysabeth HUE-GAY</persName>
        </respStmt>
        <principal>Malika BASTIN-HAMMOU</principal>
      </titleStmt>

      <publicationStmt>
        <publisher/>
        <availability>
          <p>Le site est en accès restreint</p>
        </availability>
        <pubPlace>ithac.elan-numerique.fr</pubPlace>
      </publicationStmt>

      <sourceDesc>


        <bibl>
          <title>Plautinae uiginti comediae emendatissimae cum accuratissima ac luculentissima
            interpraetatione doctissimorum uirorum Petri Vallae Placentinia ac Bernardi Saraceni
            Veneti</title>
          <author ref="#pl">Plautus</author>
          <editor ref="#ualla_ioannes">Joannes Petrus Valla</editor>
          <editor ref="#saracenus_bernardinus">Bernardinus Saracenus</editor>
          <pubPlace ref="#mediolanum">Milan</pubPlace>
          <publisher ref="#beuilaqua_simon">Simon Bevilaqua</publisher>
          <date when="1499">1499</date>
        </bibl>
        <listBibl>
          <bibl><author>F. Calitti</author>, <hi rend="italic">s.v.</hi> « MARONE, Andrea », <title
              level="m">Dizionario biografico degli Italiani</title>, <biblScope unit="volume"
              >vol. 70</biblScope>, <pubPlace>Roma</pubPlace>, <publisher>Istituto della
              enciclopedia italiana</publisher>, <date>2008</date>, en ligne : <ref
              target="https://www.treccani.it/enciclopedia/andrea-marone_%28Dizionario-Biografico%29/"
              >https://www.treccani.it/enciclopedia/andrea-marone_%28Dizionario-Biografico%29/</ref>.</bibl>
          <bibl><author>Ugo da Como</author>, <title level="m">Andrea Marone</title>,
              <pubPlace>Brescia</pubPlace>, <publisher>Fondazione « Ugo da Como » di
              Lonato</publisher>, <date>1959</date>.</bibl>
        </listBibl>

      </sourceDesc>
    </fileDesc>

    <profileDesc>

      <abstract>
        <p>Le volume réunit deux ensembles bien distincts, le premier dû à Joannes Petrus Valla
          (avec paratextes et commentaires), le second à Bernardinus Saracenus (avec paratextes,
          textes des comédies et commentaires). Chacune d’elle a son propre colophon. Ces deux
          ensembles ont-ils été l’objet d’une diffusion séparée ? Raschieri indique que le
          commentaire de Joannes Petrus aurait été publié dès 1489, puis en 1498 et 1499, se fondant
          sur M. Flodr, <title>Incunabula classicorum. Wiegendrucke der griechischen und roemischen
            Literatur</title>, Amsterdam, Adolf. M. Hakkert, 1973. Flodr s’appuie-t-il lui-même sur
          le <title>Repertorium bibliographicum...</title>, Volume 2, 1838, p. 119. Nous n’avons
          trouvé aucune trace de ces éditions antérieures. Autre objet d’étonnement : la deuxième
          partie du volume (commentaire de Saracenus) donne la date 1499 dans son colophon et a donc
          été publié avant la mort de Giorgio, en septembre 1500. Mais le propos de la liminaire 4,
          dans la première partie du volume (commentaire de Valla), semble dire que Giorgio était
          déjà décédé au moment de sa rédaction. Est-ce à dire que le commentaire de Valla est
          postérieur au commentaire de Saracenus, et donc postérieur à la date donnée dans le
          colophon de son commentaire ? </p>
      </abstract>

      <langUsage>
        <language ident="la">Latin</language>
      </langUsage>

    </profileDesc>

    <revisionDesc>
      <change when="2022-02-28">Sarah GAUCHER : identifiants Header, mise en conformité
        Elan</change>
      <change when="2021-03-03">Bérénice JARROSSAY : Encodage de la transcription et de la
        traduction</change>
    </revisionDesc>

  </teiHeader>

  <text>
    <body ana="#PH">


      <head type="orig" xml:id="Pla1499_Maro_p1_0"><persName ref="#maro_andrea">Andreae Maronis
          Brisiensis</persName> epigramma</head>
      <head corresp="#Pla1499_Maro_p1_0" type="trad">Epigramme d’<seg>Andrea Marone de
            Brescia<note>Poète néo-latin, protégé d’Hippolithe d’Este et du pape
          Léon X.</note></seg></head>


      <ab type="orig" xml:id="Pla1499_Maro_p1_1">
        <seg type="allusion"><l>Si cupiunt musae <seg>legier<note>Nous maintenons l’hapax, forme
                archaïsante de l’infinitif passif <foreign xml:lang="#la">legi</foreign>. Peut-être
                faut-il corriger en <foreign xml:lang="la">loquier</foreign> (cf. Ugo da Como, op.
                cit., p. 22 et 25).</note></seg> sermone latino,</l>
          <l>En tandem <persName ref="#pl">Plauti</persName> lingua placere potest.</l>
          <bibl><author ref="#quint">Quint.</author>, <title ref="#quint_io">I.O.</title>
            <biblScope>10.1.99</biblScope>.</bibl>.</seg>
      </ab>
      <ab corresp="#Pla1499_Maro_p1_1" type="trad">Si les Muses veulent être lues en latin, eh bien,
        la langue de Plaute peut leur plaire !</ab>

      <ab type="orig" xml:id="Pla1499_Maro_p1_2">
        <l><persName ref="#ualla_ioannes">Valla</persName> etenim patriae laudis non degener
          heres,</l>
        <l>Vatem barbaricis eruit ex tenebris</l>
        <l>Quem nitidum <persName>Marcus</persName> firmanae gloria gentis</l>
        <l>Aere suo terris primus habere dedit.</l>
      </ab>
      <ab type="trad" corresp="#Pla1499_Maro_p1_2">Car Valla, rejeton digne des louanges de sa
        Patrie, arracha des barbares ténèbres le poète que <seg>Marcus, gloire du peuple de
            Firmo<note>Il s’agit de Marcus Firmanus (de Firmo), pour le compte duquel Simon
            Bevilacqua publie l’ouvrage.</note></seg>, a le premier sur terre, sur ses fonds
        propres, rendu à son éclat.</ab>

      <ab type="orig" xml:id="Pla1499_Maro_p1_3"><l>Vos meritum <persName ref="#ualla_ioannes"
            >Vallae</persName> et <persName ref="#pl">Marci</persName> conferte,</l>
        <l>Camenae :</l>
      </ab>
      <ab type="trad" corresp="#Pla1499_Maro_p1_3">Vous, Camènes, comparez les mérites de Valla et
        de Marcus :</ab>

      <ab type="orig" xml:id="Pla1499_Maro_p1_4"><l>Ille uni <persName ref="#pl">Plauto</persName>
          profuit, hic populis.</l></ab>
      <ab type="trad" corresp="#Pla1499_Maro_p1_4">L’un a rendu service au seul Plaute, le second à
        la foule des lecteurs.</ab>


    </body>
  </text>
</TEI>
