<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">

    <teiHeader>

        <fileDesc>

            <titleStmt>

                <title xml:lang="la">Euripidis uita Guilielmo Xylandro Augustano autore.</title>
                <author ref="#xylandrus_guilelmus">Guilelmus Xylandrus</author>
                <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence en minuscules, précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->

                <respStmt>
                    <resp>Transcription</resp>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <respStmt>
                    <resp>Traduction</resp>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <respStmt>
                    <resp>Révision</resp>
                    <persName>Christian NICOLAS</persName>
                </respStmt>
                <respStmt>
                    <resp>Encodage</resp>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <principal>Malika BASTIN-HAMMOU</principal>
                <respStmt>
                    <resp>Modélisation et structuration</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Elysabeth HUE-GAY</persName>
                </respStmt>
                <respStmt>
                    <resp>Première version du modèle d'encodage</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Anne Garcia-Fernandez</persName>
                </respStmt>
            </titleStmt>

            <publicationStmt>
                <publisher/>
                <availability>
                    <p>Le site est en accès restreint</p>
                </availability>
                <pubPlace>ithac.elan-numerique.fr</pubPlace>
            </publicationStmt>
            <sourceDesc>
                <bibl>
                    <title>Euripidis Tragoediae, quae hodie extant, omnes Latinè soluta oratione
                        redditae, ita ut versus versui respondeat</title>
                    <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->
                    <author ref="#eur">Euripides</author>
                    <pubPlace ref="#basilea">Basileae</pubPlace>
                    <publisher ref="#oporinus_ioannes">Ioannes Oporinus</publisher>
                    <date when="1558">1558</date>
                    <!-- dans l'attribut @when, l'année en chiffre arabes -->
                    <editor ref="#xylandrus_guilelmus">Guilelmus Xylandrus</editor>
                    <editor role="traducteur" ref="#xylandrus_guilelmus">Guilielmus
                        Xylandrus</editor>
                    <editor role="traducteur" ref="#melanchtonus_philippus">Philippus
                        Melanchthon</editor>
                    <ref
                        target="https://www.google.fr/books/edition/Euripidis_tragoediae_quae_hodie_extant_o/47xpAAAAcAAJ?hl=fr&amp;gbpv=1"
                        >https://www.google.fr/books/edition/Euripidis_tragoediae_quae_hodie_extant_o/47xpAAAAcAAJ?hl=fr&amp;gbpv=1</ref>
                </bibl>

                <listBibl>
                    <bibl>Micha Lazarus, ‘Tragedy at Wittenberg: Sophocles in Reformation Europe’,
                        Renaissance Quarterly, 73 (2020), 33–77 <ref
                            target="https://doi.org/10.1017/rqx.2019.494"
                            >https://doi.org/10.1017/rqx.2019.494</ref>.</bibl>
                    <bibl>Michael Lurje, ‘Misreading Sophocles: Or Why Does the History of
                        Interpretation Matter?’, Antike Und Abendland, 52 (2006), 1–15 <ref
                            target="https://doi.org/10.1515/9783110186345.1"
                            >https://doi.org/10.1515/9783110186345.1</ref></bibl>
                    <bibl> Michael Lurje, ‘Facing up to Tragedy: Toward an Intellectual History of
                        Sophocles in Europe from Camerarius to Nietzsche’, in A Companion to
                        Sophocles (Blackwell Publishing Ltd., 2012), pp. 440–61. </bibl>

                    <!-- créer un <bibl> par référence -->
                </listBibl>

            </sourceDesc>
        </fileDesc>

        <profileDesc>

            <abstract>
                <p/>
                <!-- s'il s'agit d'une liste, utiliser les éléments ci-dessous -->
                <list>
                    <item/>
                    <item/>
                    <item/>
                </list>
            </abstract>

            <langUsage>
                <!-- ne pas modifier -->
                <language ident="fr">Français</language>
                <language ident="la">Latin</language>
                <language ident="grc">Grec</language>
            </langUsage>

        </profileDesc>
        <revisionDesc>
            <change when="2022-03-10">Sarah GAUCHER : identifiants Header, mise en conformité
                Elan</change>
            <change when="2021-12-07">Sarah GAUCHER : Encodage de la transcription et de la
                traduction</change>
            <change when="2021-11-19">Sarah GAUCHER : création de la fiche TEI et remplissage du
                Header</change>
        </revisionDesc>
    </teiHeader>

    <text>
        <body>
            <head type="orig" xml:id="Eu1558_Xylandrus_p2_0"><persName>Euripidis</persName> uita
                    <persName>Guilielmo Xylandro Augustano</persName> autore.</head>
            <head corresp="#Eu1558_Xylandrus_p2_0" type="trad">Vie d’Euripide par Guilelmus Xylander
                d'Ausburg.</head>


            <ab type="orig" xml:id="Eu1558_Xylandrus_p2_1"><persName>Euripides</persName> patrem
                habuit <persName>Mnesarchum</persName>, siue <persName>Mnesarchidem</persName>
                cauponem ; matrem <persName>Cleitonem</persName>, <seg type="paraphrase">quam olera
                    uendidisse e <persName>Theopompo</persName>
                    <persName>Gellius</persName> refert.<bibl>Gell. Noct. </bibl></seg></ab>
            <ab type="trad" corresp="#Eu1558_Xylandrus_p2_1">Euripide eut pour père l’aubergiste
                Mnésarque ou Mnésarchide et pour mère Cleito, dont Aulu-Gelle dit, en se fondant sur
                Théopompe, qu’elle était une marchande de légumes.</ab>
            <ab type="orig" xml:id="Eu1558_Xylandrus_p2_2"><seg type="paraphrase"
                        ><persName>Philochorum</persName>
                    <persName>Suidas</persName> autorem citat, qui eam cumprimis nobilem fuisse
                        perhibuerit.<bibl>Suid.</bibl></seg></ab>
            <ab type="trad" corresp="#Eu1558_Xylandrus_p2_2">Suidas convoque l’auteur Philochore qui
                a en premier permis de la faire connaître.</ab>
            <ab type="orig" xml:id="Eu1558_Xylandrus_p2_3">Natus est in Salamina insula ante Athenas
                sita, ea ipsa qua nauali proelio uictus est a Graecis <persName>Xerxes</persName>,
                Olympiade LXXV anno ab Vrbe condita CCLXXIII.</ab>
            <ab type="trad" corresp="#Eu1558_Xylandrus_p2_3">Il naquit sur l’île de Salamine, située
                devant Athènes, celle-là même où Xerxès fut vaincu par les Grecs dans un combat
                naval, l’année de la soixante-quinzième Olympiade, deux-cent-soixante-treize ans
                après la fondation de Rome.</ab>
            <ab type="orig" xml:id="Eu1558_Xylandrus_p2_4">Patri eius Chaldaei praedixerunt, fore ut
                is puer, cum adoleuisset, in certaminibus uictor coronaretur.</ab>
            <ab type="trad" corresp="#Eu1558_Xylandrus_p2_4">Les Chaldéens prédirent à son père que
                cet enfant, alors qu’il aurait grandi, serait couronné dans les concours.</ab>
            <ab type="orig" xml:id="Eu1558_Xylandrus_p2_5">Itaque ad hoc a patre institutus, ut
                esset athleta, Athenis Eleusinio et Theseo certaminibus coronas meruit.</ab>
            <ab type="trad" corresp="#Eu1558_Xylandrus_p2_5">C’est pourquoi, formé par son père à
                l’athlétisme, il remporta à Athènes des couronnes pour des concours aux Eleusinia et
                aux Thesea.</ab>
            <ab type="orig" xml:id="Eu1558_Xylandrus_p2_6">Pictorem quoque fuisse in otio tradunt,
                et tabulas ab eo pictas Megaris aliquando conspectas fuisse.</ab>
            <ab type="trad" corresp="#Eu1558_Xylandrus_p2_6">On raconte qu’il s’est également adonné
                à la peinture et que des tableaux qu’il avait peints se voyaient jadis à
                Mégare.</ab>
            <ab type="orig" xml:id="Eu1558_Xylandrus_p2_7">Inde ad liberalia studia cum se
                contulisset, <persName>Prodicum</persName> sophistam, et
                    <persName>Socratem</persName>, <persName>Anaxagoram</persName>que Clazomenium,
                philosophos audiit.</ab>
            <ab type="trad" corresp="#Eu1558_Xylandrus_p2_7">Ensuite, alors qu’il s’était dirigé
                vers les études libérales, il entendit le sophiste Prodicos, et les philosophes
                Socrate et Anaxagore de Clazomènes.</ab>
            <ab type="orig" xml:id="Eu1558_Xylandrus_p2_8">Quid profecerit, quantumque et ingenio et
                doctrina et ui dicendi ualuerit, non uideo quorsum attineat referre : nam ex
                scriptis eius facile intelligi id a non pinguis ingenii hominibus potest.</ab>
            <ab type="trad" corresp="#Eu1558_Xylandrus_p2_8">Ses progrès et la valeur de son esprit,
                de son savoir et de la force de son discours, je ne vois pas pourquoi il serait
                nécessaire de le rappeler : en effet, ceux qui n’ont pas l’esprit trop lourdaud
                peuvent facilement le tirer de ses écrits.</ab>
            <ab type="orig" xml:id="Eu1558_Xylandrus_p2_9">Ceterum <seg type="paraphrase"
                        ><persName>Anaxagoram</persName> ut uidit ob quasdam nouas opiniones
                    prolatas in discrime uitae peruenisse, missam fecit philosophiae professionem,
                    animumque ad scribendas Tragoedias appulit, annos natus duodeuiginti</seg>, ut
                    <persName>Gellius</persName> refert : certasse eo in genere primum anno aetatis
                uigesimo et quinto scribitur.</ab>
            <ab type="trad" corresp="#Eu1558_Xylandrus_p2_9">Du reste, lorsqu’il vit qu’Anaxagore
                avait mis sa vie en péril pour avoir présenté des opinions nouvelles, il renonça à
                la profession de philosophe et se mit en tête d’écrire des tragédies à dix-huit ans,
                comme le rapporte Aulu-Gelle : on écrit qu’il a concouru pour la première fois dans
                ce genre à vingt-cinq ans.</ab>
            <ab type="orig" xml:id="Eu1558_Xylandrus_p2_10">De numero dramatum, quae conscripserit,
                non satis constat, sicut neque de uictoriis.</ab>
            <ab type="trad" corresp="#Eu1558_Xylandrus_p2_10">Le nombre des pièces qu’il a écrites
                n’est pas bien établi, pas plus que ses victoires.</ab>
            <ab type="orig" xml:id="Eu1558_Xylandrus_p2_11">Dramata alii septuagintaquinque, alii
                nonaginta duo, in quibus fuerint octo Satyrica, numerant :
                    <persName>Suidas</persName> (siue hoc a se, siue ab alio quodam, ut pleraque
                alia, notatum auctore scripserit) <seg type="paraphrase">superesse
                        septuagintaseptem<bibl>Suid.</bibl></seg> ait.</ab>
            <ab type="trad" corresp="#Eu1558_Xylandrus_p2_11">Certains dénombrent soixante-quinze
                pièces, d’autres quatre-vingt-douze, parmi lesquelles huit drames satyriques :
                Suidas (soit qu’il ait écrit cela de lui-même, soit qu’il l’ait emprunté, comme
                beaucoup d’autres choses, à un autre auteur) dit que le nombre dépasse
                soixante-dix-sept. </ab>
            <ab type="orig" xml:id="Eu1558_Xylandrus_p2_12">Hodie extant octodecim.</ab>
            <ab type="trad" corresp="#Eu1558_Xylandrus_p2_12">Aujourd’hui, il en reste
                dix-huit.</ab>
            <ab type="orig" xml:id="Eu1558_Xylandrus_p2_13">Vicisse illi decies et quinquies
                affirmant, alii quinquies modo, ita quidem, ut postremam mortuus
                    <persName>Euripidis</persName> e fratre nepotis opera uictoriam sit
                consecutus.</ab>
            <ab type="trad" corresp="#Eu1558_Xylandrus_p2_13">Certains affirment qu’il a remporté
                quinze victoires, d’autres seulement cinq, bien qu’après sa mort, il ait obtenu une
                dernière victoire grâce à Euripide, le petit-fils de son frère. </ab>
            <ab type="orig" xml:id="Eu1558_Xylandrus_p2_14">Moribus usum fuisse austeris et a risu
                atque confabulatione abhorrentibus multi tradidere eoque nomine etiam ab
                    <persName>Aristophane</persName> exagitatur itemque mulierum maximum fuisse
                osorem.</ab>
            <ab type="trad" corresp="#Eu1558_Xylandrus_p2_14">Beaucoup ont rapporté qu’il avait des
                mœurs austères et détestait le rire et les discussions, qu’Aristophane le critique à
                ce titre et que, de même, il était plein de haine envers les femmes.</ab>
            <ab type="orig" xml:id="Eu1558_Xylandrus_p2_15">Cui quidem rei argumento esse possunt
                tam crebrae atque acerbae muliebris sexus, apud eum passim extantes etiamnum,
                infectationes, etsi haud scio an, priuata offensa domesticoque incommodo impulsus,
                inde in omnes feminas ita inuectus fuerit.</ab>
            <ab type="trad" corresp="#Eu1558_Xylandrus_p2_15">En effet ses attaques si fréquentes et
                si acerbes à l’encontre du sexe féminin, qui demeurent aujourd’hui encore partout
                dans son œuvre, peuvent donner matière à cette assertion, bien que j’ignore s’il a
                attaqué toutes les femmes, parce qu’il y avait été poussé par une offense privée et
                un problème domestique.</ab>
            <ab type="orig" xml:id="Eu1558_Xylandrus_p2_16">Nam cum prius uxorem habuisset
                    <persName>Melitto</persName>, indeque ea repudiata
                    <persName>Choerilam</persName>
                <persName>Mnesilochi</persName> filiam duxisset, ex qua ei nati sunt tres filii,
                    <persName>Mnesarchides</persName> negotiator, <persName>Mnesilochus</persName>
                histrio, et <persName>Euripides</persName> rhetoricae studiosus, hanc in adulterio
                cum <persName>Cephisophonte</persName> fabularum actore deprehendit.</ab>
            <ab type="trad" corresp="#Eu1558_Xylandrus_p2_16">Car, alors qu’il avait eu d’abord
                comme épouse Melitto et qu’ensuite, après l’avoir répudiée, il avait épousé la fille
                de Mnésiloque, Choerilé, de qui il eut trois fils, le marchand Mnésarchide,
                l’histrion Mnésiloque et le rhéteur Euripide, il la surprit en plein adultère avec
                l’acteur Céphisophon.</ab>
            <ab type="orig" xml:id="Eu1558_Xylandrus_p2_17">Sunt qui priorem hanc ei uxorem fuisse,
                ac posteriorem eum parum pudicam expertum autument.</ab>
            <ab type="trad" corresp="#Eu1558_Xylandrus_p2_17">Il y a des hommes qui pensent qu’elle
                fut sa première épouse et qu’il fit l’expérience d’une seconde épouse peu
                chaste.</ab>
            <ab type="orig" xml:id="Eu1558_Xylandrus_p2_18">Vt ut res habet, de illo inter auctores
                conuenit, labe hac et dedecore domestico affectum, praesertim cum a Comicis
                exagitaretur, relictis Athenis ad <persName>Archelaum</persName> Macedoniae regem
                commigrasse, ibique reliquum uitae summo in honore habitum exegisse.</ab>
            <ab type="trad" corresp="#Eu1558_Xylandrus_p2_18">Quoi qu’il en soit, les auteurs
                conviennent à son propos que, blessé par cette souillure et ce déshonneur domestique
                et surtout par les moqueries des comiques, il quitta Athènes et émigra chez le roi
                de Macédoine Archélaos, et qu’il passa là-bas le restant de ses jours dans la plus
                grande considération.</ab>
            <ab type="orig" xml:id="Eu1558_Xylandrus_p2_19">Mortuus est in Macedonia agens, annos
                circirer septuaginta quinque natus, siquidem uerum est, quod fertur, Olympiade
                nonagesimatertia eum uita excessisse.</ab>
            <ab type="trad" corresp="#Eu1558_Xylandrus_p2_19">Il mourut en Macédoine, âgé d’environ
                soixante-quinze ans, si du moins il est vrai que, comme on le dit, sa vie s’étendit
                au-delà de la quatre-vingt treizième Olympiade.</ab>
            <ab type="orig" xml:id="Eu1558_Xylandrus_p2_20">De morte etiam eius non conuenit inter
                scriptores.</ab>
            <ab type="trad" corresp="#Eu1558_Xylandrus_p2_20">Les auteurs ne s’accordent pas non
                plus sur sa mort.</ab>
            <ab type="orig" xml:id="Eu1558_Xylandrus_p2_21">Qui a canibus discerptum narrant, ne hi
                quidem consentiunt, cum alii in luco quodam meditantem a canibus regis
                    <persName>Archelai</persName> forte tum uenatum exeuntis dilaniatum, alii
                Lysimachum regis famulum decem minis ab <persName>Aridaeo</persName> Macedone et
                    <persName>Crateua</persName> Thessalo poetis <persName>Euripidis</persName>
                aemulis corrumptum, ei a cena redeunti, regios, quos ipse alebat, canes immisisse
                dicant.</ab>
            <ab type="trad" corresp="#Eu1558_Xylandrus_p2_21">Ceux qui disent qu’il fut mis en
                pièces par des chiens ne sont pas non plus d’accord entre eux puisque les uns disent
                que, méditant dans un bois, il fut déchiqueté par les chiens du roi Archélaos qui y
                venait d’aventure chasser, les autres que Lysimaque, l’esclave du roi, que le
                macédonien Aridaeus et le thessalien Cratevas avaient corrompu contre dix mines,
                lâcha sur lui, alors qu’il revenait de son repas, les chiens royaux qu’il
                nourrissait.</ab>
            <ab type="orig" xml:id="Eu1558_Xylandrus_p2_22">Sunt qui non a canibus, sed a mulieribus
                laceratum perhibeant, cum noctu ad <persName>Archelai</persName> amasium
                    <persName>Crateuam</persName> aut (ut alii ferunt) uxorem <persName>Nicodici
                    Arethusii</persName> commearet.</ab>
            <ab type="trad" corresp="#Eu1558_Xylandrus_p2_22">Il y a des auteurs qui l’ont imaginé
                mis en pièces non par des chiens mais par des femmes, alors qu’il allait voir de
                nuit l’amant d’Archélaos, Cratevas ou (aux dires d’autres) la femme de Nicodicos
                d’Aréthuse.</ab>
            <ab type="orig" xml:id="Eu1558_Xylandrus_p2_23">Sepultus est in Macedonia, ossaque eius
                Pellam a rege translata ferunt.</ab>
            <ab type="trad" corresp="#Eu1558_Xylandrus_p2_23">Il fut enterré en Macédoine et on dit
                que ses ossements furent ramenés à Pella par le roi.</ab>
            <ab type="orig" xml:id="Eu1558_Xylandrus_p2_24"><seg type="paraphrase">Sepulchrum uero
                    eius summo in honore apud Macedones fuisse, <persName>Gellius</persName> narrat,
                    qui gloriae loco de eo praedicarent : <persName>Euripides</persName>, semper
                    monimentum erit tuum.<bibl> Gell. Noct. 15.20.10</bibl><note>Sepulchrum autem
                        eius et memoriam Macedones eo dignati sunt honore, ut in gloriae quoque loco
                        praedicarent: οὔποτε σὸν μνῆμα, Εὐρίπιδες, ὄλοιτό που, quod egregius poeta
                        morte obita sepultus in eorum terra foret.</note>.<note>Voir Gell. Noct.
                        15.20.10 : Sepulchrum autem eius et memoriam Macedones eo dignati sunt
                        honore, ut in gloriae quoque loco praedicarent: οὔποτε σὸν μνῆμα, Εὐρίπιδες,
                        ὄλοιτό που, quod egregius poeta morte obita sepultus in eorum terra
                        foret.</note>.</seg></ab>
            <ab type="trad" corresp="#Eu1558_Xylandrus_p2_24">Mais Aulu-Gelle raconte que les
                Macédoniens honoraient à tel point son tombeau qu’ils s’écriaient à toute occasion :
                « Euripide, ce monument sera toujours tien ».</ab>
            <ab type="orig" xml:id="Eu1558_Xylandrus_p2_25">Idem refert Macedones Atheniensium
                legatis petentibus ut ossa <persName>Euripidis</persName> ipsis in patriam referenda
                darentur, constanter id summo cum consensu denegasse. </ab>
            <ab type="trad" corresp="#Eu1558_Xylandrus_p2_25">Le même auteur rapport que les
                Macédoniens, alors que des légats athéniens demandaient que les ossements d’Euripide
                leur soient rendus pour les ramener dans sa patrie, ne cessèrent de le leur refuser
                unanimement.</ab>
            <ab type="orig" xml:id="Eu1558_Xylandrus_p2_26">Sane magnum Atheniensibus omnibus luctum
                nuntius de morte eius uiri attulit. </ab>
            <ab type="trad" corresp="#Eu1558_Xylandrus_p2_26">Il est vrai que la nouvelle de sa mort
                plongea tous les Athéniens dans un deuil immense.</ab>
            <ab type="orig" xml:id="Eu1558_Xylandrus_p2_27">Et <persName>Sophocles</persName> ipse
                pullam induit uestem, ac sine coronis actores in scenam introduxit.</ab>
            <ab type="trad" corresp="#Eu1558_Xylandrus_p2_27">Et Sophocle lui-même revêtit un
                vêtement noir et introduisit les acteurs sur scène sans couronne.</ab>
            <ab type="orig" xml:id="Eu1558_Xylandrus_p2_28"><persName>Philemon</persName> autem
                Comicus, quanto desiderio Euripidis teneretur, his uersibus declarauit :</ab>
            <ab type="trad" corresp="#Eu1558_Xylandrus_p2_28">Quant au comique Philémon, il a montré
                dans les vers que voici combien il regrettait d’Euripide :</ab>
            <ab type="orig" xml:id="Eu1558_Xylandrus_p2_29"><cit>
                    <quote><l>Si uerum id esset, a quibusdam ut traditum est,</l>
                        <l>Sensum relinqui mortuis, suspendio</l><l> Finire uitam uellem protinus
                            meam,</l><l> Meum uidere gestiens
                        <persName>Euripidem</persName></l></quote>
                    <bibl resp="#CN"><note>Traduction de Philémon, fr. 40a de Meineke, Fragmenta
                            comicorum Graecorum, vol. 4, Berlin: Reimer, 1839 (repr. Berlin: De
                            Gruyter, 1970) : Εἰ ταῖς ἀληθείαισιν οἱ τεθνηκότες / αἴσθησιν εἶχον,
                            ἄνδρες, ὥς φασίν τινες,/ ἀπηγξάμην ἂν ὥστ’ ἰδεῖν
                        Εὐριπίδην.</note>.</bibl>
                </cit>
            </ab>
            <ab type="trad" corresp="#Eu1558_Xylandrus_p2_29">« S’il était vrai, comme certains nous
                l’ont rapporté, que l’esprit des défunts demeure après la mort, je voudrais aussitôt
                mourir par pendaison, parce que j’aurais le plaisir de voir mon cher Euripide
                ».</ab>
            <ab type="orig" xml:id="Eu1558_Xylandrus_p2_30">Honoratius ei tumulus positus est ab
                Atheniensibus, eique inscripti a <persName>Thucydide</persName> historico aut
                    <persName>Timotheo</persName> carminum scriptore, hi uersus :</ab>
            <ab type="trad" corresp="#Eu1558_Xylandrus_p2_30">Les Athéniens, avec de plus grands
                égards, lui élevèrent un tertre et l’historien Thucydide ou le poète Timothée y
                firent inscrire les vers que voici :</ab>
            <ab type="orig" xml:id="Eu1558_Xylandrus_p2_31"><cit>
                    <quote><l>Graecia tota quidem est monumentum Euripidis ; ossa</l><l> Qua uitam
                            eripuit, terra tenet Macedum.</l><l> Patria, Graecia Graecia Athenae ;
                            plurima, Musis</l><l> Perpetuo dulcem, laus comitatur eum.</l></quote>
                    <bibl resp="#CN"><title>Anthologie Palatine</title>, VII,
                                45.<note><note>Traduction en assez mauvais distiques du poème 7.45
                                de l’ Anthologie Palatine, prêté à Thucydide : Μνᾶμα μὲν Ἑλλὰς ἅπασ’
                                Εὐριπίδου, ὀστέα δ’ ἴσχει / γῆ Μακεδών, ᾗπερ δέξατο τέρμα βίου./
                                πατρὶς δ’ Ἑλλάδος Ἑλλάς, Ἀθῆναι· πλεῖστα δὲ Μούσαις / τέρψας ἐκ
                                πολλῶν καὶ τὸν ἔπαινον ἔχει.</note></note></bibl>
                </cit>
            </ab>
            <ab type="trad" corresp="#Eu1558_Xylandrus_p2_31">« La Grèce tout entière est le tombeau
                d’Euripide ; la terre de Macédoine, où il perdit la vie, conserve ses ossements.
                Mais sa patrie, c’est la Grèce, c’est la Grèce, c’est Athènes : une immense louange
                l’accompagne, lui qui a toujours été agréables aux Muses. »</ab>
            <ab type="orig" xml:id="Eu1558_Xylandrus_p2_32">Extant et alia satis uenusta in hunc
                poetam elogia, quae Latine a nobis conuersa subiiciemus, etsi scimus hac praesertim
                in re nos Graecorum uenustate inferiores.</ab>
            <ab type="trad" corresp="#Eu1558_Xylandrus_p2_32">Il reste aussi d’autres éloges de ce
                poète suffisamment charmants, que nous plaçons ci-dessous et traduisons en latin,
                même si nous savons que, surtout en cette matière, nous sommes inférieurs au charme
                des Grecs.</ab>
            <ab type="orig" xml:id="Eu1558_Xylandrus_p2_33"><persName>Ionis</persName></ab>
            <ab type="trad" corresp="#Eu1558_Xylandrus_p2_33">Du poète Ion</ab>
            <ab type="orig" xml:id="Eu1558_Xylandrus_p2_34"><cit>
                    <quote><l>Perpetuae thalamis, <persName>Euripides</persName>, abdite
                            noctis,</l><l> O salue nigris Pieriae in specubus.</l><l> Infra terram
                            audi, decus immortale paratum</l><l> Est tibi Maenio gratia parque
                            seni.</l></quote>
                    <bibl resp="#CN"><title>Anthologie Palatine</title>, 7.43. <note> Traduction
                            d’Ion, Anthologie Palatine, 7.43 Χαῖρε μελαμπετάλοις, Εὐριπίδη, ἐν
                            γυάλοισι/ Πιερίας τὸν ἀεὶ νυκτὸς ἔχων θάλαμον·/ ἴσθι δ’ ὑπὸ χθονὸς ὤν,
                            ὅτι σοι κλέος ἄφθιτον ἔσται/ ἴσθι δ’ ὑπὸ χθονὸς ὤν, ὅτι σοι κλέος
                            ἄφθιτον ἔσται / aἶσον Ὁμηρείαις ἀενάοις χάρισιν.</note>.</bibl>
                </cit>
            </ab>
            <ab type="trad" corresp="#Eu1558_Xylandrus_p2_34">« Je te salue, Euripide, toi qui
                possèdes un tombeau éternel dans les grottes de Piérie ; bien que tu sois sous
                terre, sois assuré que tu seras l’objet d’une gloire éternelle, égale aux grâces
                éternelles d’Homère. »</ab>
            <ab type="orig" xml:id="Eu1558_Xylandrus_p2_35">In eundem :</ab>
            <ab type="trad" corresp="#Eu1558_Xylandrus_p2_35">Pour le même :</ab>
            <ab type="orig" xml:id="Eu1558_Xylandrus_p2_36"><cit>
                    <quote><l>Etsi te miseranda, <persName>Euripides</persName>, abstulerit
                            mors,</l>
                        <l>et tua membra feri diripuere canes,</l><l> Dulcisona o scenae philomela,
                            decus patriaeque</l><l> Qui Sophiae tragicas miscueras Charites :</l><l>
                            At sub Pelleo tegeris tumulo, hic uti semper</l><l> Pieridum cultor sis
                            prope Pierides.</l>
                    </quote>
                    <bibl resp="#SG"><title>Anthologie Palatine</title>, 7.44. <note>Traduction du
                            poème, 7.44 de l’Anthologie Palatine : Εἰ καὶ δακρυόεις, Εὐριπίδη, εἷλέ
                            σε πότμος, / καί σε λυκορραῖσται δεῖπνον ἔθεντο κύνες, / τὸν σκηνῇ
                            μελίγηρυν ἀηδόνα, κόσμον Ἀθηνῶν, / τὸν σοφίῃ Μουσέων μιξάμενον χάριτα, /
                            ἀλλ’ ἔμολες Πελλαῖον ὑπ’ ἠρίον, ὡς ἂν ὁ λάτρις / Πιερίδων ναίῃς ἀγχόθι
                            Πιερίδων.</note>.</bibl>
                </cit>
            </ab>
            <ab type="trad" corresp="#Eu1558_Xylandrus_p2_36">« Même si une mort affreuse t’a
                emporté, Euripide et que les chiens ont mis en pièce tes membres, toi mélodieux
                rossignol de la scène, gloire de ta patrie, toi qui avais mêlé les grâces tragiques
                à la sagesse, tu es enseveli sous le sol de Pella, afin que, serviteur des Piérides,
                tu demeures près d’elles. »</ab>
            <ab type="orig" xml:id="Eu1558_Xylandrus_p2_37">In eundem : </ab>
            <ab type="trad" corresp="#Eu1558_Xylandrus_p2_37">Pour le même :</ab>
            <ab type="orig" xml:id="Eu1558_Xylandrus_p2_38"><cit>
                    <quote><l>Non equidem bustum hoc Euripidis est monimentum :</l><l>Sed fama uatis
                            noscitur iste locus.</l></quote>
                    <bibl resp="#SG"><title>Anthologie Palatine</title>, 7.46.<note>Traduction du
                            poème, 7.46 de l’Anthologie Palatine : Οὐ σὸν μνῆμα τόδ’ ἔστ’, Εὐριπίδη,
                            ἀλλὰ σὺ τοῦδε·/ τῇ σῇ γὰρ δόξῃ μνῆμα τόδ’ ἀμπέχεται.</note>.</bibl>
                </cit>
            </ab>
            <ab type="trad" corresp="#Eu1558_Xylandrus_p2_38">« Ce buste n’est pas le tombeau
                d’Euripide, mais c’est la gloire du poète qui fait connaître ce monument ».</ab>
            <ab type="orig" xml:id="Eu1558_Xylandrus_p2_39">In eundem :</ab>
            <ab type="trad" corresp="#Eu1558_Xylandrus_p2_39">Pour le même :</ab>
            <ab type="orig" xml:id="Eu1558_Xylandrus_p2_40"><cit>
                    <quote><l>Monimentum habes, Euripides, Achaiam</l><l> Omnem : quid ergo uoce
                            cassus crederis,</l><l> Vocalis omni qui manebis tempore ?</l></quote>
                    <bibl resp="#SG"><title>Anthologie Palatine</title>, 47. <note>Traduction du
                            poème, 7.47 de l’Anthologie Palatine : Ἅπασ’ Ἀχαιὶς μνῆμα σόν γ’,
                            Εὐριπίδη·/ οὔκουν ἄφωνος, ἀλλὰ καὶ λαλητέος.</note></bibl>
                </cit>
            </ab>
            <ab type="trad" corresp="#Eu1558_Xylandrus_p2_40">« Toute la Grèce est ton tombeau,
                Euripide : pourquoi donc te croit-on muet, toi qui chanteras toujours ? »</ab>


        </body>
    </text>
</TEI>
