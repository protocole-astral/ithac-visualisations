<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">

  <teiHeader>

    <fileDesc>

      <titleStmt>

        <title xml:lang="la">Quam grauis immineat saeuis fortuna tyrannis…</title>
        <!-- Attention ! ne pas faire de retour charriot dans le titre -->
        <author ref="#posthius_ioannes">Iohannes Posthius</author>

        <!-- Déclarations de responsabilité : indiquent qui a effectué quelle tâche -->
        <!-- Tâche n°1 -->
        <respStmt>
          <resp>Transcription</resp>
          <name>Pascale PARÉ-REY</name>
        </respStmt>
        <!-- Tâche n°2 -->
        <respStmt>
          <resp>Traduction</resp>
          <name>Pascale PARÉ-REY</name>
        </respStmt>
        <!-- Tâche n°3 -->
        <respStmt>
          <resp>Encodage</resp>
          <name>Valérie THIRION</name>
          <name>Sarah GAUCHER</name>
        </respStmt>
        <!-- Attention ! Ne pas modifier les tâches ci-dessous -->
        <principal>Malika BASTIN-HAMMOU</principal>
        <respStmt>
          <resp>Modélisation et structuration</resp>
          <persName>Elisabeth GRESLOU</persName>
          <persName>Elysabeth HUE-GAY</persName>
        </respStmt>
        <respStmt>
          <resp>Première version du modèle d'encodage</resp>
          <persName>Elisabeth GRESLOU</persName>
          <persName>Anne Garcia-Fernandez</persName>
        </respStmt>
      </titleStmt>

      <!-- Informations de publication sur le laboratoire numérique du projet. Ne pas modifier-->
      <publicationStmt>
        <publisher/>
        <availability>
          <p>Le site est en accès restreint</p>
        </availability>
        <pubPlace>ithac.elan-numerique.fr</pubPlace>
      </publicationStmt>

      <sourceDesc>

        <bibl>
          <title>L. Annaei Senecae Cordubensis Tragoediae. Lectiones variae e manuscriptis libris
            Bibliothecae Palatinae aliisque descriptae Iusti Lipsi Animaduersiones</title>
          <author ref="#sen">Seneca</author>
          <pubPlace ref="#heidelberga">Heidelbergae</pubPlace>
          <publisher ref="#commelinus_hieronymus">Hieronymus Commelinus</publisher>
          <date when="1589">1589</date>
          <editor ref="#commelinus_hieronymus">Hieronymus Commelinus</editor>
          <ref target="https://books.google.fr/books?id=n05hAAAAcAAJ"/>
          <ref
            target="http://dfg-viewer.de/show/?set[mets]=http%3A//www.dilibri.de%2Foai%2F%3Fverb%3DGetRecord%26metadataPrefix%3Dmets%26identifier%3D924028"
          />
        </bibl>
        <listBibl>
          <bibl><editor>W. Kühlmann</editor>, <editor>V. Hartmann</editor>, <editor>S. El
              Kholi</editor>, <editor>B. Spiekermann</editor>, <title level="m">Die deutschen
              Humanisten. Dokumente zur Überlieferung der antiken und mittelalterlichen Literatur in
              der Frühen Neuzeit</title>, Abteilung I: <title level="m">Die Kurpfalz</title>,
            Bd. III: <title level="m">Jacob Micyllus, Johannes Posthius, Johannes Opsopoeus und
              Abraham Scultetus</title>, <pubPlace>Turnhout</pubPlace>,
              <publisher>Brepols</publisher>, <title level="s">Europa Humanistica</title>
            <biblScope unit="volume">9</biblScope>, <date>2010</date>,
            <extent>654 p.</extent></bibl>
          <bibl><author>Klaus Karrer</author>, <title level="m">Johannes Posthius (1537-1597):
              Verzeichnis der Briefe und Werke mit Regesten und Posthius-Biographie</title>,
              <pubPlace>Wiesbaden</pubPlace>, <publisher>Harrassowitz</publisher>,
            <date>1993</date></bibl>
          <bibl><ref target="https://data.bnf.fr/fr/13324009/johannes_posthius/"
              >https://data.bnf.fr/fr/13324009/johannes_posthius/</ref>
          </bibl>
          <!-- EHG : à compléter -->
          <bibl><ref target="https://data.cerl.org/thesaurus/cnp00094238"
              >https://data.cerl.org/thesaurus/cnp00094238</ref></bibl>
          <!-- EHG : à compléter -->
          <bibl>Paré-Rey, Pascale, "Les éditions des tragédies de Sénèque conservées à la
            Bibliothèque nationale de France (XVe-XIXe s.)", in L’Antiquité à la BnF, 17/01/2018,
              <ref target="https://antiquitebnf.hypotheses.org/1643"
              >https://antiquitebnf.hypotheses.org/1643</ref></bibl>
        </listBibl>
      </sourceDesc>
    </fileDesc>

    <profileDesc>

      <abstract>
        <p>Ce poème en distiques élégiaques est adressé à Frédéric IV, futur Électeur du Palatinat,
          par Johannes Posthius / Posth (1537-1597), son médecin. L’éditeur Jérôme Commelin remercie
          par une voie indirecte le futur prince à qui il doit son installation dans la ville
          d'Heidelberg.</p>
      </abstract>

      <langUsage>
        <!-- ne pas modifier -->
        <language ident="fr">Français</language>
        <language ident="la">Latin</language>
      </langUsage>

    </profileDesc>

    <revisionDesc>
      <!-- pour indiquer des modifications apportées au fichier -->
      <change when="2022-03-02">Sarah GAUCHER : identifiants Header, mise en conformité
        Elan</change>
      <change who="Diandra CRISTACHE" when="2021-05-23">Diandra CRISTACHE : mise à jour et
        publication sur le Pensoir</change>
      <change when="2021-03-22">Valérie Thirion: encodage de la transcription</change>
    </revisionDesc>

  </teiHeader>

  <text>
    <body ana="#PA #PHILO">

      <!-- le texte est composée de paragraphes (unités de traduction) <ab> de type orig pour le texte original et trad pour la traduction. Ils sont liés grâce à un identifiant (@xml:id et @corresp)  -->
      <ab type="orig" xml:id="Sq1589_B_Posthius_p1_1">
        <l>Quam grauis immineat saeuis fortuna tyrannis,</l>
        <l>Hoc <persName ref="#sen">Senecae</persName> in primis nobile monstrat opus.</l>
      </ab>
      <ab corresp="#Sq1589_B_Posthius_p1_1" type="trad">
        <l>Combien une fâcheuse fortune menace les cruels tyrans,</l>
        <l>cet ouvrage fameux de Sénèque le montre avant tout.</l>
      </ab>

      <ab type="orig" xml:id="Sq1589_B_Posthius_p1_2">
        <l>Quo picta ueluti in tabula, speculoue potentis</l>
        <l>Qualia sint aulae fata, uidere licet.</l>
      </ab>
      <ab corresp="#Sq1589_B_Posthius_p1_2" type="trad">
        <l>Comme sur une peinture ou dans un miroir, on peut voir</l>
        <l>quels sont les destins de la cour d’un puissant.</l>
      </ab>

      <ab type="orig" xml:id="Sq1589_B_Posthius_p1_3">
        <l>Tristia nempe : suos una quae saepe ministros</l>
        <l>Pessumdans, Stygios cogit adire lacus.</l>
      </ab>
      <ab corresp="#Sq1589_B_Posthius_p1_3" type="trad">
        <l>Funestes, assurément : engloutissant souvent ensemble</l>
        <l>chacun de ses serviteurs, elle les contraint de gagner le Styx.</l>
      </ab>

      <ab type="orig" xml:id="Sq1589_B_Posthius_p1_4">
        <l>Talia principibus teneris bene cognita prosunt</l>
        <l>Exempla, ut fugiant quae nocitura uident ;</l>
        <l>Quae bona sectentur. Quare illustrissime princeps,</l>
        <l>Eximia haec docti dramata uatis ama.</l>
      </ab>
      <ab corresp="#Sq1589_B_Posthius_p1_4" type="trad">
        <l>De tels exemples, bien connus, sont utiles aux jeunes</l>
        <l>princes, pour qu’ils fuient ce qu’ils voient devoir leur nuire ;</l><l>mais qu'il
          recherchent le bien. C’est pourquoi, prince très illustre,</l>
        <l>aime ces remarquables drames d’un docte chantre.</l>
      </ab>

      <ab type="orig" xml:id="Sq1589_B_Posthius_p1_5">
        <l>Et facilem praebe fidis monitoribus aurem,</l>
        <l>Sic patrii fies gloria rara soli.</l>
      </ab>
      <ab corresp="#Sq1589_B_Posthius_p1_5" type="trad">
        <l>Et offre une oreille ouverte aux conseils fiables,</l>
        <l>tu deviendras ainsi une gloire rare du sol paternel.</l>
      </ab>

      <ab type="orig" xml:id="Sq1589_B_Posthius_p1_6">
        <l>Illustrissimus Carissimus Tutelaris</l>
        <l>Addictissimus <persName ref="#posthius_ioannes">Iohannes Posthius Medicus</persName>.</l>
      </ab>
      <ab corresp="#Sq1589_B_Posthius_p1_6" type="trad">
        <l>Vôtre très illustre, très cher, très protecteur,</l>
        <l>très dévoué, Johannes Posthius, médecin.</l>
      </ab>

    </body>
  </text>
</TEI>
