<?xml version="1.0" encoding="UTF-8"?>


<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">

    <teiHeader>

        <fileDesc>
            <titleStmt>

                <!-- Titre et auteur du paratexte (en latin) -->
                <title xml:lang="la">Illustrissimae Dominae Ianae Navarrorum Principi Carolus
                    Girardus Bituricus Salute.</title>
                <author ref="#girardus_carolus">Carolus Girardus Bituricus</author>

                <!-- Déclarations de responsabilité : indiquent qui a effectué quelle tâche -->
                <!-- Tâche n°1 -->
                <respStmt>
                    <resp>Transcription</resp>
                    <name>Christian NICOLAS</name>
                </respStmt>
                <!-- Tâche n°2 -->
                <respStmt>
                    <resp>Traduction</resp>
                    <name>Christian NICOLAS</name>
                </respStmt>
                <!-- Tâche n°3 -->
                <respStmt>
                    <resp>Encodage</resp>
                    <name>Heidi-Morgane Roure</name>
                    <name>Sarah GAUCHER</name>
                </respStmt>

                <!-- Attention ! Ne pas modifier les tâches ci-dessous -->
                <respStmt>
                    <resp>Modélisation et structuration</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Elysabeth HUE-GAY</persName>
                </respStmt>
                <principal>Malika BASTIN-HAMMOU</principal>

            </titleStmt>

            <publicationStmt>
                <!-- Informations de publication sur le laboratoire numérique du projet-->
                <!-- Ne pas modifier les données ci-dessous -->
                <publisher/>
                <availability>
                    <p>Le site est en accès restreint</p>
                </availability>
                <pubPlace>ithac.elan-numerique.fr</pubPlace>
            </publicationStmt>

            <sourceDesc>
                <!-- Informations bibliographiques sur l'édition ancienne d'où le paratexte est tiré -->
                <bibl>
                    <!-- l'attribut @level permet d'indiquer s'il s'agit d'une monographie (m), d'un article ou chapitre d'ouvrage (a) ou d'un périodique (j) -->
                    <title><foreign xml:lang="grc">Ἀριστοφάνους κωμωδοροιῶν ἀρίστου
                            Πλοῦτος.</foreign> Aristophanis Poëte Comici Plutus, iam nunc per
                        Carolum Girardum Bituricum et Latinus factus, et Commentariis insuper sane
                        quam utiliss. recens illustratus. Editio prima.</title>
                    <author ref="#ar">Aristophanes</author>
                    <pubPlace ref="#lutetia">Parisiis</pubPlace>
                    <publisher ref="#dupuys_mathurnius">Mathurinus Dupuys</publisher>
                    <date when="1549">1549</date>
                    <editor>NR</editor>
                </bibl>
            </sourceDesc>

        </fileDesc>

        <profileDesc>

            <abstract>
                <p/>
            </abstract>

            <langUsage>
                <!-- Garder seulement les éléments indiquant les langues utilisées dans le paratexte -->
                <language ident="la">Latin</language>
                <language ident="grc">Grec</language>
            </langUsage>

        </profileDesc>

        <revisionDesc>
            <!-- Ajouter ici autant d'éléments <change> que de modifications effectuées -->
            <change when="2022-03-03">Sarah GAUCHER : identifiants Header, mise en conformité
                Elan</change>
            <change when="2021-01-30">Heidi-Morgane ROURE : Encodage de la transcription et de la
                traduction</change>
        </revisionDesc>

    </teiHeader>

    <text>
        <body>
            <head type="orig" xml:id="Ar1549_Girard_p1_0">Illustrissimae Dominae Ianae Navarrorum
                Principi Carolus Girardus Bituricus Salute.</head>
            <head type="trad" corresp="#Ar1549_Girard_p1_0">À l’illustrissime Jeanne, Princesse de
                Navarre, Charles Girard de Bourges donne le bonjour.</head>

            <ab type="orig" xml:id="Ar1549_Girard_p1_1">Quo tempore Attica regio disciplinarum et
                armorum gloria florebat, Princeps Serenissima, laudibus mirum quantis Athenienses
                Mineruam eueherent, ut Athenarum scilicet suarum praesidem, cuique uni ferendum
                putarent acceptum, quod ceteris nationibus armis et litteris anteponendos se
                existimarent.</ab>
            <ab type="trad" corresp="#Ar1549_Girard_p1_1">À l’époque où la région attique était le
                fleuron des lettres et des armes, Princesse Sérénissime, il est remarquable de voir
                quelle place glorieuse les Athéniens réservaient à Minerve, en tant qu’elle était
                bien sûr la divinité tutélaire de leur Athènes ; ils pensaient que chacun devait
                accepter l’idée qu’ils dominaient les autres nations par les armes et par les
                lettres.</ab>

            <ab type="orig" xml:id="Ar1549_Girard_p1_2">Nulla inuidia, per nos quidem, fruiti sint
                sua Minerua Athenienses, modo Bituriges meminerint de capite Iouis, id est, ab ipso
                Deo Optimo Maximo aliam Mineruam ipsis natam esse et factam, Atheniensium Minerua
                tanto mehercule praestantiorem, quanto ea certiora sunt quae oculis conspiciuntur,
                quam ea sint quae animi quadam imaginatione concipiuntur.</ab>
            <ab type="trad" corresp="#Ar1549_Girard_p1_2">Nous n’aurions nulle jalousie, nous en
                tout cas, de voir les Athéniens profiter de leur Minerve, pourvu que les gens de
                Bourges se souviennent de la tête de Jupiter : je veux dire que c’est de ce Très Bon
                et Très Grand Dieu que leur est née et advenue une autre Minerve, d’autant plus
                prestigieuse, morbleu, par rapport à la Minerve athénienne que sont plus avérés les
                témoignages visuels en regard des inventions de l’imagination et de l’esprit.</ab>

            <ab type="orig" xml:id="Ar1549_Girard_p1_3">Mutum illi et tantummodo surdum idolum
                coluere. Nos nostram Mineruam, praeclarissimam uidelicet tuam matrem Margaritam,
                μαργαρίτν uere incomparabilem, <foreign xml:lang="grc">πέπλῳ</foreign> illo candido,
                auroque interlucenti distincto, hoc est et simplicitatis et prudentiae summo
                splendore nitentem, uiuam cernimus et uidentem : quae non <foreign xml:lang="grc"
                    >φανταστικῇ</foreign> et imaginaria opinione, sed ipsa re hancce Biturigum
                Academiam (ut de reliquis urbis partibus taceam) in dies magis magisque,
                munificentissima liberalitate, prouidentia neque non singulari, pergit fouere et
                exornare, ipsa interim diuinarum humanarumque rerum peritissima et illis usque adeo
                ornata, ut si de prisca illa gente Attica unus aliquis superesse posset, qui nostrae
                Mineruae dicta, scripta et facta aequo animo expenderet, in ea haud dubie multo
                plura et maiora experturus esset quam in sua comminisceretur et confingeret.</ab>
            <ab type="trad" corresp="#Ar1549_Girard_p1_3">Eux, ils ont honoré une idole muette et
                sourde. Alors que nous, notre Minerve, je veux parler bien sûr de ton illustre
                    <seg>mère Marguerite<note>Marguerite de Valois (1492-1549), sœur de François 1er
                        et reine de Navarre depuis 1527, est née à Angoulême. On voit donc que
                        Girard pousse un peu loin le pays des Bituriges de Bourges. Sauf à prendre
                        en compte, outre les Bituriges Cubes (ceux du Berry) aussi les Bituriges
                        Vivisques, qui s’étaient, après la conquête des Gaules, déplacés du Berry
                        vers l’embouchure de la Gironde.</note></seg>, margarita (perle) vraiment
                incomparable, ornée de ce péplum d’une blancheur éclatante brodé de paillettes d’or,
                c’est-à-dire de cette splendeur que donnent la simplicité et l’intelligence, nous la
                contemplons vivante et clairvoyante : ce n’est pas par une vue de l’esprit et de
                l’imagination, mais dans le monde réel, que cette mienne université de Bourges (pour
                ne rien dire des autres lieux de la ville), elle ne cesse, davantage de jour en
                jour, par ses libéralités sans fin et sa bienfaisance absolument unique, de
                l’encourager et de l’embellir ; elle est d’ailleurs elle-même experte en les choses
                divines et humaines et elle en est à ce point décorée que, si pouvait être des
                nôtres aujourd’hui un seul Athénien d’autrefois capable d’évaluer tous les dits,
                écrits et faits de notre Minerve à nous, sans aucun doute trouverait-il chez elle
                plus et mieux que ce qu’il pourrait forger et inventer chez la sienne.</ab>

            <ab type="orig" xml:id="Ar1549_Girard_p1_4">Neque uero Atticus ille idem nobis
                terrificam suae Palladis obiectaret <foreign xml:lang="grc">πανοπλίαν</foreign>, si
                secum reputaret nostram <foreign xml:lang="grc">Ἀθηνᾶν</foreign>, opibus alioqui et
                eximiis uiribus insignem sua <foreign xml:lang="grc">ἐλαίᾶ</foreign> coronatam, non
                minus scite et prudenter insanos belli tumultus sedare posse, quam consultu et
                potenter posset prouocata hosti acre bellum mouere.</ab>
            <ab type="trad" corresp="#Ar1549_Girard_p1_4">Et ce même Athénien ne nous opposerait pas
                la terrifiante "panoplie" de sa Pallas s’il se disait en son for intérieur que notre
                Athéna, par ailleurs tout auréolée de puissance et de forces inouïes et couronnée de
                son olivier, n’est pas moins capable d’apaiser finement et prudemment les tumultes
                déments de la guerre que, quand on l’y pousse, à mener, avec stratégie et force, une
                dure guerre à l’ennemi.</ab>

            <ab type="orig" xml:id="Ar1549_Girard_p1_5">Nam quod attinet ad perpetuam Palladis
                uirginitatem, si hinc nostram Palladem censeret inferiorem, hoc illis demum
                persuadebatur quibus sterilitas fecunditate potior esse credatur. O nimis felicem
                matrem, quae nobis te alteram edidit Palladem tot tantisque et animi et corporis
                dotibus conspicuam. Lubet his externis praeteritis, paucula tantum, quae ad diuinum
                animi tui uigorem pertinent attingere.</ab>
            <ab type="trad" corresp="#Ar1549_Girard_p1_5">Et pour ce qui est de la virginité
                éternelle de Pallas, si l’on en venait à en faire un motif d’infériorité de notre
                Pallas à nous, c’est un critère qui n’avait cours que pour ceux chez qui la
                stérilité vaut mieux que la fécondité. O trop heureuse mère, qui nous a, en ta
                personne, engendré une deuxième Pallas qui se recommande par tant de si grandes
                qualités physiques et intellectuelles ! J’ai plaisir, par ces détours passés et
                étrangers, à m’approcher, ne fût-ce qu’un peu, de ce qui intéresse ta divine force
                d’esprit.</ab>

            <ab type="orig" xml:id="Ar1549_Girard_p1_6">Nemo hominum uiuit quin hoc tempore Gallicam
                linguam quaruncumque gentium linguis iudicet conferendam, paene etiam ausim dicere
                praeponendam. Tu matrem tuam linguae huius peritissimam proxime imitata, usque adeo
                cum illa iam uideris aequanda, ut uel uno hoc nomine non immerito quis te Gallicum
                dixerit <foreign xml:lang="grc">παλλάδιον</foreign>.</ab>
            <ab type="trad" corresp="#Ar1549_Girard_p1_6">Tous nos contemporains estiment que le
                français d’aujourd’hui soutient la comparaison avec toutes les langues du monde et
                même, si j’ose le dire, leur est supérieur. Or toi, en prenant pour modèle ta mère
                si experte en cette langue, tu sembles si bien l’égaler que l’on pourrait à juste
                titre te désigner comme elle du même nom de Palladium français.</ab>

            <ab type="orig" xml:id="Ar1549_Girard_p1_7">Hoc primum specimen fuit, Princeps
                generosissima, quod facile cunctis de excelso tuo animo dedisti. Deinde uero immensa
                ingenii tui uis Gallico hocce ornamento non contenta, non prius conquieuit quam in
                Latium, N. Borbonio usa duce et doctore eruditissimo, peruenerit : ubi uelut
                expugnato captoque Capitolio, sic triumphans ut quis liquido sit deieraturus,
                Latinorum linguam tibi genuinam esse ac peculiarem. Quid autem quod ne ista quidem
                animi tui satisfecerunt sublimitati ? in Atticum usque solum ductore eodem ac
                praemonstratore penetrasti : ubi prompte expediteque scripta tractas Attica.</ab>
            <ab type="trad" corresp="#Ar1549_Girard_p1_7">Il y a eu ce premier témoignage, généreuse
                Princesse, que tu as livré à tous, de la hauteur de ton esprit. Puis l’étendue et la
                puissance de ton génie, non content de cette reconnaissance française, n’a pas voulu
                se reposer avant d’arriver à Rome, guidé par <seg>Nicolas Bourbon <note>Le poète
                        néolatin Nicolas Bourbon (1503-1550) était depuis une dizaine d’années le
                        précepteur de Jeanne d’Albret. C’est avec son maître qu’elle a fait ses
                        premières armes en français puis qu’elle a appris le latin et le grec,
                        apprentissages présentés ici par métaphore comme un voyage à Rome puis à
                        Athènes.</note></seg>, docteur très savant ; là, comme si le Capitole
                s’était fait assiéger et prendre, il a reçu un tel triomphe qu’on pourrait jurer
                clairement que le latin est ta langue maternelle et privilégiée. Mais que dire ?
                Même cela n’a pas suffi à l’excellence de ton génie. Avec le même guide, qui est
                aussi ton premier découvreur, tu t’es engagée sur le sol attique ; là, vite fait
                bien fait, tu traites la littérature grecque.</ab>

            <ab type="orig" xml:id="Ar1549_Girard_p1_8">Quapropter dum ego in <foreign
                    xml:lang="grc">ἀκροπόλει</foreign> illa <foreign xml:lang="grc">Ἀττικῇ</foreign>
                positam te animo contemplarer, mentem meam ilico subiit cogitatio mihi longe
                iucundissima, qua me perpulit ut te adirem, tibi Atticum offerrem munusculum, Plutum
                uidelicet Aristophanicum, quem Attice et Latine cum salibus atque facetiis res
                ferias interim et graues miscentem, non sine uoluptate (ita spero quidem et opto)
                inaudires. Et quoniam sat scio non defuturos qui tuis auspiciis in Atticum agrum ire
                contendent, ne Plutus quidem noster hic caecus illis esset quomodo Plutum Chremyli
                quondam fuisse ferunt, uisum est in gratiam tironum Commentariolis illum
                illustrare.</ab>
            <ab type="trad" corresp="#Ar1549_Girard_p1_8">Aussi, pendant que je te voyais juchée en
                esprit sur cette acropole d’Athènes, m’est venue en tête très vite la pensée de loin
                la plus agréable qui soit et qui m’a poussé à aller vers toi, à t’offrir un petit
                cadeau grec, c’est-à-dire le Plutus d’Aristophane, que tu puisses entendre en grec
                et en latin, avec tous ses bons mots et plaisanteries, mêlant à l’occasion drôlerie
                et gravité et non sans jubilation (du moins c’est mon espoir et mon souhait). Et
                puisque je sais bien que nombreux seront ceux qui tenteront d’entrer dans le domaine
                grec sous tes bons auspices, afin que mon Plutus que voici ne leur soit pas aussi
                aveugle que naguère fut, dit-on, le Plutus de Chrémyle, j’ai jugé bon, pour le
                bien-être des débutants, de l’illustrer de petites notes.</ab>

            <ab type="orig" xml:id="Ar1549_Girard_p1_9">Datum est memoriae, carissima Princeps, ab
                Acropoli Mineruae canes olim arceri consueuisse. Hoc symbolo intelligant maledici
                homines suos dentes et uirulentiam linguae prorsus hinc submouendam. Id quod si
                minus intelligent, et imprudentes tuum Plutum morsicare temerareque tentabunt,
                Aegidem tuam <seg xml:lang="grc">θυσσανόεσσαν, δεινὴν, ἥν περὶ μὲν παντῇ φόβος
                    στεφανοῦται <note>Hom. Il. 5, 738-9 : <quote xml:lang="grc">ἀμφὶ δ’ ἄρ’ ὤμοισιν
                            βάλετ’ αἰγίδα θυσσανόεσσαν / δεινήν, ἣν περὶ μὲν πάντῃ Φόβος
                            ἐστεφάνωται.</quote></note></seg>, id est scutum tuum spectatae
                uirtutis, fimbriis insignitum, formidabile et de quo horror prodit undecunque,
                obtrectatoribus illis obtende : certum est enim fore ut eo conspecto, stupidi statim
                conticescant atque prae iracundia ringentes sua labra praemordeant. </ab>
            <ab type="trad" corresp="#Ar1549_Girard_p1_9">On a gardé le souvenir, très chère
                Princesse, que jadis les chiens de Minerve avaient l’habitude de se maintenir à
                distance de l’Acropole. On doit comprendre symboliquement que ce sont les médisants,
                leurs morsures et la virulence de leur langue qu’il faut empêcher d’approcher. S’ils
                ne comprennent pas et tentent imprudemment de mordiller et de souiller ton Plutus,
                mets devant toi ton égide <seg xml:lang="grc">θυσσανόεσσαν, δεινὴν, ἥν περὶ μὲν
                    παντῇ φόβος στεφανοῦται<note>« &lt;Autour de ses épaules, elle jette l’égide&gt;
                        frangée, redoutable, où s’étalent en couronne Déroute, &lt;Querelle,
                        Vaillance…&gt;». La citation est intégrée habilement à l’énoncé latin et
                        légèrement modifiée dans sa forme verbale, ce qui la rend
                    amétrique.</note></seg>, c’est-à-dire ‘ton bouclier au courage admiré,
                remarquable par ses franges et d’où sourd de tout côté l’horreur’, montre-la à ces
                détracteurs : à l’évidence, quand ils l’auront vue, il se tairont stupéfaits et,
                tout grondants de colère, ils mordront leurs propres babines.</ab>

            <ab type="orig" xml:id="Ar1549_Girard_p1_10">Tantum hoc mihi dicendum superest. Quo tu
                modis omnibus Palladem <foreign xml:lang="grc">τῶν Ἀθηναίων</foreign> propius
                referre uidearis, <foreign xml:lang="grc">δυσβουλίαν</foreign> nostram, id est quae
                inconsultius a me hic dicta factaue fuerint, bene uortas. Quod si ita esse
                perspexero, tuam noctuam nobis uolitasse fortunatissimus acclamabo. </ab>
            <ab type="trad" corresp="#Ar1549_Girard_p1_10">Il ne me reste qu’une chose à dire. Pour
                pouvoir en tout point paraître rappeler au mieux la Pallas athénienne, tourne à mon
                avantage ma <seg xml:lang="grc">δυσβουλία <note>« Décision funeste »</note></seg>,
                c’est-à-dire ce que j’aurai dit ou fait à mauvais escient. Quand j’aurai vu qu’il en
                va ainsi, j’irai partout clamant dans mon bonheur sans borne que ta chouette m’a
                protégé de son vol.</ab>

            <ab type="orig" xml:id="Ar1549_Girard_p1_11">Salue et uale, humanissima Princeps.
                Dominus Deus te nobis multa saecula conseruet incolumem. Apud tuos Biturigas Nonis
                Aprilibus 1548.</ab>
            <ab type="trad" corresp="#Ar1549_Girard_p1_11">Adieu, très humaine Princesse, porte-toi
                bien. Que Dieu te garde à nous intacte pendant de nombreux siècles. Depuis ta bonne
                ville de Bourges, le 5 avril 1548.</ab>
        </body>
    </text>

</TEI>
