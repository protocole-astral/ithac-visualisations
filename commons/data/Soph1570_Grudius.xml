<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
    

    <teiHeader>

        <fileDesc>

            <titleStmt>

                <!--Ci-dessous le titre du paratexte-->
                <title xml:lang="la">Nicolaus Nicolai Grudius Georgio Ratallero S. P. Dolam.</title>
                <author ref="#grudius_nicolaus">Nicolaus Nicolai Grudius</author>

                <respStmt>
                    <resp>Transcription</resp>
                    <persName>Diandra CRISTACHE</persName>
                </respStmt>
                <respStmt>
                    <resp>Traduction</resp>
                    <persName>Sarah GAUCHER</persName>
                </respStmt>
                <respStmt>
                    <resp>Révision</resp>
                    <name>Christian NICOLAS</name>
                </respStmt>
                <respStmt>
                    <resp>Encodage</resp>
                    <name>Diandra CRISTACHE</name>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <respStmt>
                    <resp>Modélisation et structuration</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Elysabeth HUE-GAY</persName>
                </respStmt>

                <principal>Malika BASTIN-HAMMOU</principal>
            </titleStmt>

            <publicationStmt>
                <publisher/>
                <availability>
                    <p>Le site est en accès restreint</p>
                </availability>
                <pubPlace>ithac.elan-numerique.fr</pubPlace>
            </publicationStmt>

            <sourceDesc>
                <bibl>
                    <title>Tragoediae Sophoclis quotquot extant carmine latino redditae. Georgio
                        Ratallero in supremo apud belgas regio senatu Mechliniae Consiliario, et
                        libellorum supplicum Magistro, interprete.</title>
                    <author ref="#soph">Sophocles</author>
                    <date when="1570">1570</date>
                    <publisher ref="#siluius_gulielmus">Gulielmus Siluius</publisher>
                    <pubPlace ref="#antuerpia">Antuerpiae</pubPlace>
                    <editor ref="#ratallerus_georgius">Georgius Ratallerus</editor>
                    <editor role="traducteur" ref="#ratallerus_georgius">Georgius
                        Ratallerus</editor>
                    <ref
                        target="http://reader.digitale-sammlungen.de/de/fs1/object/display/bsb10169464_00005.html"
                    />
                </bibl>
            </sourceDesc>
        </fileDesc>

        <profileDesc>

            <abstract>
                <p/>
            </abstract>

            <langUsage>
                <language ident="la">Latin</language>
                <language ident="gr">Grec</language>
            </langUsage>

        </profileDesc>

        <revisionDesc>
            <change when="2022-02-10">Sarah GAUCHER : Vérification et ajouts PersName, PlaceName,
                hi, title, quote, seg, foreign. Vérification de la mise en conformité du
                schéma</change>
            <change when="2021-11-24">Sarah GAUCHER: Encodage de la traduction et correction de la
                transcription</change>
            <change when="2021-05-09">Diandra CRISTACHE : Mise à jour et chargement sur le
                Pensoir</change>
            <change when="2020-09-21">Diandra CRISTACHE : Encodage de la transcription</change>
        </revisionDesc>

    </teiHeader>

    <text>
        <body>
            <head type="orig" xml:id="Soph1570_Grudius_0"><persName ref="#nicolai_nicolaus">Nicolaus
                    Nicolai Grudius</persName>
                <persName ref="#ratallerus_georgius">Georgio Ratallero</persName> S. P.
                Dolam.</head>
            <head type="trad" corresp="#Soph1570_Grudius_0">Nicolas Grudius fils de Nicolas adresse
                son salut à Georgius Ratallerus à Dole.</head>

            <ab type="orig" xml:id="Soph1570_Grudius_1">Exhibuit mihi, <persName
                    ref="#ratallerus_georgius">Georgi Ratallere</persName>, superioribus diebus a te
                litteras Dominus <persName ref="#stratius_stephanus">Stephanus Stratius</persName> ;
                quibus in extremis salutem mihi adscripseras, non obscurum quidem testimonium animi
                erga me tui, quod gratissimum mihi sane, ut debuit, fuit ; hoc ipso uero die
                tragoedias tres, <title ref="#soph_aj">Aiacem</title>, <title ref="#soph_ant"
                    >Antigonen</title>, <title ref="#soph_el">Electram</title>, quas de Graecis
                    <persName ref="#soph">Sophoclis</persName> ita conuertisti, ut uicturas esse
                dubitare non debeas.</ab>
            <ab type="trad" corresp="#Soph1570_Grudius_1" ana="#PA">Le seigneur Stephanus Stratius m’a montré,
                Georgius Ratallerus, ta lettre il y a quelques jours : à la fin tu m’y avais adressé
                ton salut, un témoignage éclatant de ta disposition à mon égard, ce qui me fut tout
                à fait agréable, ainsi qu’il l’aurait dû ; ce même jour, il m’a montré
                    <title>Ajax</title>, <title>Antigone</title>, <title>Électre</title>, trois
                tragédies que tu as si bien traduites depuis le grec de Sophocle que tu ne dois pas
                douter qu’elles remporteront du succès.</ab>
            <ab type="orig" xml:id="Soph1570_Grudius_2">Ego certe, Latina ista tua postquam legere
                coepi, ita sum admiratus, iisque ita uehementer delector, inducere animum ut nequeam
                credere, iisdem unquam Graecis delectatum peraeque fuisse <persName ref="#q_cic"
                    >Quintum</persName> illum, <persName ref="#cic">Marci Tullii
                    Ciceronis</persName> fratrem, maximum <persName ref="#soph">Sophoclis</persName>
                admiratorem, de quo magnificum non ignoras atque praeclarum illud elogium in 5
                    <title ref="#cic_fin">de Finibus</title>.</ab>
            <ab type="trad" corresp="#Soph1570_Grudius_2" ana="#PA #T">Pour ma part, après que j’ai entrepris de
                lire tes tragédies en latin, je suis si admiratif et j’en tire un si grand plaisir
                que je ne pourrais être porté à croire que ce sont les mêmes qui, en grec, ont un
                jour charmé le fameux Quintus, frère de Marcus Tullius Cicéron, très grand
                admirateur de Sophocle, dont tu n’ignores pas non plus le célèbre éloge dans le
                cinquième livre du <title xml:lang="la">De Finibus</title>.</ab>
            <ab type="orig" xml:id="Soph1570_Grudius_3">Ipsum etiam <persName ref="#cic">Marcum
                    Tullium</persName>, est quod credam, atque <persName ref="#licinius"
                    >Licinium</persName>, si reuiuiscant, aliter longe de <title>Electra</title>
                ista tua quam de <persName ref="#atil_com">Atilii</persName>, <cit type="refauteur">
                    <quote>scriptoris ferrei</quote>
                    <bibl resp="#SG"><author ref="#cic">Cic.</author>, <title ref="#cic_fin"
                            >Fin.</title>
                        <biblScope>1.2.5</biblScope>.</bibl>
                </cit>, iudicaturos esse.</ab>
            <ab type="trad" corresp="#Soph1570_Grudius_3" ana="#PA">Et à mon avis,Marcus Tullius lui-même et
                Licinius, s’ils ressuscitaient, jugeraient tout autrement ton <title>Électre</title>
                que celle d’Atilius, un écrivain de fer.</ab>
            <ab type="orig" xml:id="Soph1570_Grudius_4">Quod utinam sapientissimi hominis ac <cit
                    type="refauteur">
                    <quote>diuini poetae</quote>
                    <bibl resp="#SG"><author ref="#cic">Cic.</author>
                        <title ref="#cic_diu">Div.</title>
                        <biblScope>1.54.</biblScope>. </bibl>
                </cit> (sic enim illum <persName ref="#cic">Cicero</persName> nominat) et reliquas
                tragoedias, in quibus illum faciendis ad summam senectutem uersatum ferunt, eundem
                istum ad modum traductas haberemus, quibus Graece perdiscere non contigit.</ab>
            <ab type="trad" corresp="#Soph1570_Grudius_4" ana="#T">Et puissions-nous, nous qui ne connaissons
                pas le grec à la perfection, posséder également, traduites à ta manière, les autres
                tragédies du plus sage des hommes et du plus divin des poètes (car c’est ainsi que
                l’appelle Cicéron), vers l’écriture desquelles il se tourna, dit-on, à la fin de sa
                vie.</ab>
            <ab type="orig" xml:id="Soph1570_Grudius_5">Sed frustra optantur quae
                interciderunt.</ab>
            <ab type="trad" corresp="#Soph1570_Grudius_5" ana="#PH">Mais c’est en vain qu’on désire des écrits
                aujourd’hui perdus.</ab>
            <ab type="orig" xml:id="Soph1570_Grudius_6">De iis quidem, quae etiamnum exstant, in
                spem uenio atque confido, esse te aliquando perfecturum ut frui similiter
                possimus.</ab>
            <ab type="trad" corresp="#Soph1570_Grudius_6" ana="#PA">Pour celles qui nous restent en tout cas,
                j’en viens à l’espoir et je crois fermement que tu parviendras un jour à ce que nous
                en profitions pareillement.</ab>
            <ab type="orig" xml:id="Soph1570_Grudius_7">Equidem te hortari non dubito, mi <persName
                    ref="#ratallerus_georgius">Ratallere</persName>, ut, quamquam iurisprudentiae
                praecipuam esse nauandam operam prudenter iudicas, aliquid tamen quotidie studii, et
                temporis in eo genere litterarum ponere ne desinas, quae, quemadmodum nominantur,
                sic et humanissimae sunt et ad celebritatem nominis illustrissimae atque
                aeternae.</ab>
            <ab type="trad" corresp="#Soph1570_Grudius_7" ana="#PA">Pour ma part, je n’hésite pas à
                t’exhorter, mon cher Ratallerus, que, bien que tu penses à juste titre qu’il te faut
                en premier lieu servir la jurisprudence, à ne pas cesser de consacrer chaque jour
                ton étude et ton temps aux belles lettres qui sont, comme leur nom l’indique, tout à
                fait humaines, tout à fait brillantes pour l’éclat de ton nom et éternelles.</ab>
            <ab type="orig" xml:id="Soph1570_Grudius_8">Vale meque tuum esse plane puta, atque
                    <persName>Arnoldum</persName> fratris mei filium ad praeclara ista studia
                frequenter hortare.</ab>
            <ab type="trad" corresp="#Soph1570_Grudius_8" ana="#PA">Adieu, pense que je suis tout à fait tien
                et ne cesse pas d’encourager Arnoldus, mon neveu, à entreprendre ces illustres
                études.</ab>
            <ab type="orig" xml:id="Soph1570_Grudius_9">Bruxella, V Octobris MDL.</ab>
            <ab type="trad" corresp="#Soph1570_Grudius_9" ana="#PA">À Bruxelles, le 5 octobre 1550.</ab>
        </body>
    </text>
</TEI>
