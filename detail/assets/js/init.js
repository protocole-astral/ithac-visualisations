document.addEventListener("DOMContentLoaded", function() {
    init_paratxt_html();
}, false);

async function init_paratxt_html() {

    var search_params = getURLparams();
    if (search_params.has("file")) {
        var filename = search_params.get("file");
    } else {
        var filename = "Ae1548_Auratus_p1";
    }
    var url = "../commons/data/";

    var datum_for_metadata = await getDatum(url + filename + ".xml");
    var datum_for_html = await getDatum(url + filename + ".xml", true);

    // metadata'
    var metadata = [
        "paratxt_title",
        "paratxt_author",
        "paratxt_lang",
        "paratxt_date",
        "paratxt_pubplace",
        "paratxt_publisher",
        "paratxt_editor",
        "ancient_title",
        "ancient_author"
    ];

    console.log(datum_for_metadata);
    console.log(datum_for_html);

    var aside_metadata = document.querySelector("aside#metadata");

    var source = [8, 7, 6, 5, 4, 3];
    var source_arr = [];
    for (var i = 0; i < source.length; i++) {
        var key = metadata[source[i]];
        console.log(datum_for_metadata.querySelector("tei teiHeader"));
        var node = datum_for_metadata.querySelector(data_model[key].path);
        var value = (node.firstElementChild != null) ? "" : node.innerHTML;
        source_arr.push(value);
    }
    var p = document.createElement("P");
    var span_def = document.createElement("SPAN");
    span_def.classList.add("definition");
    span_def.innerHTML = "Source";
    var span_val = document.createElement("SPAN");
    span_val.classList.add("value");
    span_val.innerHTML = source_arr.join(", ") + ".";
    p.appendChild(span_def);
    p.appendChild(span_val);
    aside_metadata.appendChild(p);

    for (var i = 0; i < metadata.length; i++) {
        if (metadata[i] == "paratxt_lang") {
            var value = datum_for_metadata.querySelector(data_model[metadata[i]].path).getAttribute("xml:lang");
        } else {
            var node = datum_for_metadata.querySelector(data_model[metadata[i]].path);
            var value = (node.firstElementChild != null) ? "" : node.innerHTML;
        }
        
        var p = document.createElement("P");
        var span_def = document.createElement("SPAN");
        span_def.classList.add("definition");
        span_def.innerHTML = data_model[metadata[i]].definition;
        var span_val = document.createElement("SPAN");
        span_val.classList.add("value");
        span_val.innerHTML = value;

        p.appendChild(span_def);
        p.appendChild(span_val);
        aside_metadata.appendChild(p);
    }

    // body
    var arr_of_head = datum_for_html.querySelectorAll(data_model.paratxt_head.path);
    var arr_of_ab = datum_for_html.querySelectorAll(data_model.paratxt_ab.path);

    var main = document.querySelector("main");
    for (var i = 0; i < arr_of_head.length; i++) {
        var h2 = document.createElement("H2");
        h2.innerHTML = arr_of_head[i].innerHTML;
        main.appendChild(h2);
    }
    for (var i = 0; i < arr_of_ab.length; i++) {
        main.appendChild(arr_of_ab[i]);
    }

    var seg = document.querySelectorAll(".seg");
    seg.forEach(el => el.addEventListener("mouseenter", toggleNote));
    seg.forEach(el => el.addEventListener("mouseleave", toggleNote));
}

function toggleNote() {
    var note = this.querySelector(".note");
    this.classList.toggle("hovered");
    note.classList.toggle("active");
}


/* GESTION URL */

function getURL() {
    var url = new URL(document.location.href);
    return url;
}
function getURLparams() {
    var url = getURL();
    var searchParams = new URLSearchParams(url.search);
    return searchParams;
}